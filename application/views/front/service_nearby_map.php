<nav class="navbar navbar-expand-md navbar-dark main-nav gray_bg section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color"><i class="fas fa-map-marker-alt"></i> 附近的人</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>
<div class="filter_section green_bg">
    <div class="container">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container-fluid">
				<div class="navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<div class="search mx-auto">
								<i class="fa fa-search"></i>
								<input type="text" class="form-control" placeholder="請輸入地址">
							</div>
						</li>
						<li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="position">位置</label>
								<select class="form-select" id="position">
									<option value="1" selected="selected">現在位置</option>
									<?php
									if ($loggedIn) {
									?>
									<option value="2">家居位置</option>
									<option value="3">公司位置</option>
									<?php
									}
									?>
								</select>
							</div>
						</li>
						<li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="range">搜尋範圍</label>
								<select class="form-select" id="range">
									<option value="1" selected="selected">附近1公里</option>
									<option value="3">附近3公里</option>
									<option value="5">附近5公里</option>
								</select>
							</div>
						</li>
						<li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="category">服務分類</label>
								<select class="form-select" id="category">
									<option value="0" selected="selected">任何分類</option>
								<?php
									if (!empty($tag_lists))
									{
										foreach($tag_lists as $idx => $tag)
										{
								?>
									<option value="<?=$tag['id'];?>"><?=$tag['name'];?></option>
								<?php
										}
									}
								?>
								</select>
							</div>
						</li>
						<!-- <li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="subcategory">服務次分類</label>
								<select class="form-select" id="subcategory">
									<option value="1">修甲</option>
								</select>
							</div>
						</li> -->
					</ul>
					<div class="btn-group btn-toggle">
						<button class="btn btn-xs btn-on active btn-map">地圖</button>
						<button class="btn btn-xs btn-off btn-list">列表</button>
					</div>
				</div>
			</div>
		</nav>
    </div>
</div>

<div class="filter_content_section">
	<div id="map" style="height: 450px;"></div>
	<div class="mobile-chat-panel">
		<div class="row mobile-chat-header">
<!--			<div class="col-6 bg-white">-->
<!--				<img class="d-block mx-auto img-fluid" src="--><?//=base_url('assets/templates/landing/assets/img/logo.png');?><!--" />-->
<!--			</div>-->
<!--			<div class="col-3 chat-header-info">-->
<!--				<img class="d-block mx-auto" src="--><?//=base_url('assets/img/front/icon-chat-people.png');?><!--" />-->
<!--				<div class="mobile-chat-count">--><?//=$webinar['num_of_applicants'];?><!--</div>-->
<!--			</div>-->
<!--			<div class="col-3 chat-header-info">-->
<!--				<img class="d-block mx-auto" src="--><?//=base_url('assets/img/front/icon-chat-duration.png');?><!--" />-->
<!--				<div class="mobile-chat-duration">00:00:00</div>-->
<!--			</div>-->
		</div>
		<div class="row mobile-chat-body">
			<div class="col-12 ps-0 pe-0">
				<div class="mobile-chat-message-block">
					<div class="chat-message-wrapper" id="mobile-chat-message-wrapper">
						<div class="container">
							<div class="row">
								<div class="col-3"><input type="checkbox" /> 全選</div>
								<div class="col-5">第1至<span class="total-shops">0</span>間，共<span class="total-shops">0</span>間</div>
								<div class="col-4 text-right"><button class="btn btn-broadcast">廣發訊息</button></div>
							</div>
							<div class="shop-list">
							<!-- <?php
							foreach ($nearby_shops as $idx => $shop)
							{
								?>
								<div class="row">
									<div class="col-2 text-nowrap"><input type="checkbox" /> <span><?=$idx + 1;?>.</span></div>
									<div class="col-10">
										<table class="table shop-info">
											<tbody>
											<tr>
												<td colspan="2"><a href="<?=base_url('service/details/' . $shop['uri']);?>"><span><?=$shop['title'];?></span></a></td>
											</tr>
											<tr>
												<td colspan="2">
													<?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', $shop['rate']) . str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate1.png') . '" />', 5 - $shop['rate']);?>
												</td>
											</tr>
											<tr>
												<td colspan="2"><p><?=$shop['location'];?> | <?=$shop['phone'];?></p></td>
											</tr>
											<tr>
												<td width="75%"><p><?=$shop['business_time'];?></p></td>
												<td class="text-right"><button class="btn btn-message">發訊息</button></td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<?php
							}
							?> -->
							</div>
						</div>
					</div>
				</div>
				<div class="mobile-chat-area">
<!--					<div class="row">-->
<!--						<div class="col-7 input-group">-->
<!--							<input class="form-control send-target" type="text" placeholder="發送給每個人..." readonly />-->
<!--							<div class="input-group-addon after-inputbox d-flex send-target-icon">-->
<!--								<img class="mx-auto d-block my-auto" src="--><?//=base_url('assets/img/front/icon-more.png');?><!--" />-->
<!--							</div>-->
<!--						</div>-->
<!--						<div class="col-5 text-right"><span>聊天模式</span><img src="--><?//=base_url('assets/img/front/icon-chat-mode.png');?><!--" /></div>-->
<!--					</div>-->
<!--					<div class="row mt-1">-->
<!--						<div class="col-12 input-group">-->
<!--							<input class="form-control send-message" name="mobile-message" type="text" placeholder="輸入您的評論..." />-->
<!--							<div class="input-group-addon after-inputbox d-flex send-message-icon" data-content="mobile">-->
<!--								<img class="mx-auto d-block my-auto" src="--><?//=base_url('assets/img/front/icon-send-message.png');?><!--" />-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
				</div>
			</div>
		</div>
	</div>
</div>
<div id="footerSlideContainer">
	<div id="footerSlideButton"><img src="<?=base_url('assets/img/uonlive/vertical-drawer.png');?>" /></div>
	<div id="footerSlideContent">
		<div id="footerSlideText">
			<div class="row chat-header"></div>
			<div class="row chat-body">
				<div class="col-12 ps-0 pe-0">
					<div class="chat-message-block">
						<div class="chat-message-wrapper" id="chat-message-wrapper">
							<div class="container">
								<div class="row">
									<div class="col-3"><input type="checkbox" id="message-all" /> 全選</div>
									<div class="col-5">第1至<span class="total-shops">0</span>間，共<span class="total-shops">0</span>間</div>
									<div class="col-4 text-right"><button class="btn btn-broadcast">廣發訊息</button></div>
								</div>
								<div class="shop-list">
								<!-- <?php
								foreach ($nearby_shops as $idx => $shop)
								{
								?>
								<div class="row">
									<div class="col-2 text-nowrap"><input type="checkbox" /> <span><?=$idx + 1;?>.</span></div>
									<div class="col-10">
										<table class="table shop-info">
											<tbody>
											<tr>
												<td colspan="2"><a href="<?=base_url('service/details/' . $shop['uri']);?>"><span><?=$shop['title'];?></span></a></td>
											</tr>
											<tr>
												<td colspan="2">
													<?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', $shop['rate']) . str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate1.png') . '" />', 5 - $shop['rate']);?>
												</td>
											</tr>
											<tr>
												<td colspan="2"><p><?=$shop['location'];?> | <?=$shop['phone'];?></p></td>
											</tr>
											<tr>
												<td width="75%"><p><?=$shop['business_time'];?></p></td>
												<td class="text-right"><button class="btn btn-message">發訊息</button></td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<?php
								}
								?> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->load->view('front/layouts/partials/popup_message_dialog');
if ($loggedIn) {
?>
<script>
	let resident_lat = <?=$this->session->userdata('uonlive_user')['resident_latitude'] / 10000000;?>;
	let resident_lng = <?=$this->session->userdata('uonlive_user')['resident_longitude'] / 10000000;?>;
	let business_lat = <?=$this->session->userdata('uonlive_user')['business_latitude'] / 10000000;?>;
	let business_lng = <?=$this->session->userdata('uonlive_user')['business_longitude'] / 10000000;?>;
</script>
<?php
}

