<?php
$this->load->view('front/layouts/partials/member_navbar');
?>

<div class="section-member">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/member_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<?php
						if ($update) {
						?>
						<div class="alert alert-success" role="alert">用戶更新成功</div>
						<?php
						}
						?>
						<form id="frmUpdate" action="<?=current_url();?>" method="post">
							<input type="hidden" name="token" value="<?=$member['token'];?>" />
							<input type="hidden" name="login_name" value="<?=$member['email'];?>" />
							<input type="hidden" name="resident_formatted_address" value="<?=$member['resident_formatted_address'];?>" />
							<input type="hidden" name="resident_latitude" value="<?=$member['resident_latitude'] / 10000000;?>" />
							<input type="hidden" name="resident_longitude" value="<?=$member['resident_longitude'] / 10000000;?>" />
							<input type="hidden" name="business_formatted_address" value="<?=$member['business_formatted_address'];?>" />
							<input type="hidden" name="business_latitude" value="<?=$member['business_latitude'] / 10000000;?>" />
							<input type="hidden" name="business_longitude" value="<?=$member['business_longitude'] / 10000000;?>" />
							<input type="hidden" name="avatar_file" />
							<div class="row">
								<div class="col">
									<div class="breadcrumb_service_detail w-100 mt-0">
									<?php
									$this->load->view('front/layouts/partials/thumbnail');
									?>
									</div>
									<h3 class="title-color text-center">帳戶資料</h3>
									<hr />
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="firstname">名字*</label>
									<input type="text" id="firstname" name="first_name" class="form-control" placeholder="請輸入你的名字" value="<?=$member['first_name'];?>">
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="lastname">姓氏*</label>
									<input type="text" id="lastname" name="last_name" class="form-control" placeholder="請輸入你的姓氏" value="<?=$member['last_name'];?>">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="phone">電話號碼*</label>
									<input type="text" id="phone" name="phone" class="form-control" placeholder="請輸入你的電話號碼" value="<?=$member['phone'];?>">
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="email">電郵地址*</label>
									<input type="email" id="email" name="email" class="form-control" placeholder="請輸入你的電郵地址" value="<?=$member['email'];?>">
								</div>
							</div>
							<div class="row">
								<!-- <div class="col-lg-3 col-6 pt-2 pb-2">
									<label for="gender">性別*</label>
									<select id="gender" class="form-control">
										<option value="M">男</option>
										<option value="F">女</option>
									</select>
								</div>
								<div class="col-lg-3 col-6 pt-2 pb-2">
									<label for="age">年齡*</label>
									<select id="age" class="form-control">
										<option value="M">兒童</option>
										<option value="F">少年</option>
										<option value="F">青年</option>
										<option value="F">成人</option>
										<option value="F">老年</option>
									</select>
								</div> -->
								<div class="col pt-2 pb-2">
									<label for="occupation">職業</label>
									<input type="text" id="occupation" name="occupation" class="form-control" placeholder="請輸入你的職業" value="<?=$member['occupation'];?>">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 pt-2 pb-2">
									<label for="resident_area">家居地區</label>
									<select id="resident_area" name="resident_area" class="form-control">
									<?php
									foreach ($areas as $area)
									{
										foreach ($area['district'] as $district)
										{
									?>
									<option value="<?=$district['id'];?>"<?=($member['resident_area'] == $district['id']) ? ' selected="selected"' : '';?>><?=$district['name'];?></option>
									<?php
										}
									}
									?>
									</select>
								</div>
								<div class="col-sm-9 pt-2 pb-2">
									<label for="resident_building">大廈名稱</label>
									<input type="text" id="resident_building" name="resident_building" class="form-control" placeholder="請輸入居住大廈名稱" value="<?=$member['resident_building'];?>">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 pt-2 pb-2">
								</div>
								<div class="col-sm-9 pt-2 pb-2">
									<p id="resident_formatted_address"><?=$member['resident_formatted_address'];?></p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 pt-2 pb-2">
								</div>
								<div class="col-sm-9 pt-2 pb-2">
									<table class="table" id="resident_list">
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 pt-2 pb-2">
									<label for="business_area">公司地區</label>
									<select id="business_area" name="business_area" class="form-control">
									<?php
										foreach ($areas as $area)
										{
											foreach ($area['district'] as $district)
											{
										?>
										<option value="<?=$district['id'];?>"<?=($member['business_area'] == $district['id']) ? ' selected="selected"' : '';?>><?=$district['name'];?></option>
										<?php
											}
										}
										?>
									</select>
								</div>
								<div class="col-sm-9 pt-2 pb-2">
									<label for="business_building">大廈名稱</label>
									<input type="text" id="business_building" name="business_building" class="form-control" placeholder="請輸入工作大廈名稱" value="<?=$member['business_building'];?>">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 pt-2 pb-2">
								</div>
								<div class="col-sm-9 pt-2 pb-2">
									<p id="business_formatted_address"><?=$member['business_formatted_address'];?></p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 pt-2 pb-2">
								</div>
								<div class="col-sm-9 pt-2 pb-2">
									<table class="table" id="business_list">
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="current_password">目前的密碼</label>
									<input type="text" id="current_password" class="form-control" placeholder="目前的密碼" disabled="disabled" value="******" />
								</div>
								<div class="col-lg-6 pt-2 pb-2 text-center">
									<div class="mt-4 checkbox-outline">
										<input type="checkbox" id="edm" class="form-check-input"><label class="form-check-label" for="edm">接收宣傳郵件</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="password">設定密碼</label>
									<div class="input-group">
										<div class="form-outline w-75">
											<input type="password" id="password" name="password" class="form-control" placeholder="請設定你的密碼" onkeyup="checkPasswordStrength();" />
										</div>
										<button type="button" class="btn btn-password-toggle">
											<i class="fa fa-eye-slash"></i>
										</button>
									</div>
									<div id="password-strength">強度：<span id="password-strength-status"></span></div>
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="confirm_password">確認密碼</label>
									<input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="請再輸入你的密碼">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="avatar_profile">會員圖像 (250x250)</label>
									<div class="row">
										<div class="col-md-4">
											<div class="input-group">
												<div id="avatar-drop_zone">請把圖片拖放到這裡</div>
												<br>
												<label class="btn btn-block btn-info" style="width: 140px;">
													瀏覽… <input id="avatar-browse" type="file" accept="image/*" style="display: none;">
												</label>
											</div>
										</div>
										<div class="col-md-8">
											<?php
											$image = base_url('assets/img/uonlive/1x1.png');
											if ($member['avatar_photo'] != "") {
												$file = 'assets/img/uploads/members/' . $member['avatar_photo'];
												if (file_exists(FCPATH . $file)) {
													$image = base_url($file);
												}
											}
											?>
											<img class="img-fluid" id="avatar-media" src="<?=$image;?>" />
											<div class="form-check">
												<input class="form-check-input" type="checkbox" value="1" name="remove_avatar" id="remove-avatar">
												<label class="form-check-label" for="remove-avatar">
													移除會員圖像
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 pt-2 pb-2">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label"></label>
								<div class="col-md-10 inputGroupContainer">
									<div id="progress-container" class="progress">
										<div id="progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%
										</div>
									</div>
									<div id="results"></div>
								</div>
							</div>
							<div class="row pb-4">
								<div class="col pt-2 pb-2">
									<button class="btn btn-save">儲存設定</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
