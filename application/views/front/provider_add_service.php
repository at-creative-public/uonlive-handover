<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-provider-add-service">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
								<?php
								$this->load->view('front/layouts/partials/thumbnail');
								?>
								</div>
								<h3 class="title-color text-center">新增服務</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<label class="col-md-2 control-label"></label>
							<div class="col-md-10 inputGroupContainer">
								<div id="progress-container" class="progress">
									<div id="progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%
									</div>
								</div>
								<div id="results"></div>
							</div>
						</div>
						<form id="media-form" action="<?=current_url();?>" method="POST">
							<input type="hidden" name="content" value="" />
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="service_name">服務名稱</label>
									<input type="text" id="service_name" name="name" class="form-control" placeholder="請輸入服務名稱">
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="show_in_shop">商店主頁上顯示</label>
									<input type="checkbox" name="show_in_shop" id="show_in_shop" value="1" class="form-check-input d-block mt-2 ms-3">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 pt-2 pb-2">
									<label for="summary">簡介</label>
									<input type="text" id="summary" name="summary" class="form-control" placeholder="請輸入簡介">
								</div>
							</div>
							<div class="row">
								<div class="col pt-2 pb-2">
									<label for="content">服務內容</label>
									<textarea id="content" class="form-control w-100" rows="10"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-6 pt-2 pb-2">
									<label for="_img">圖像 (1300x866)</label>
									<div class="input-group">
										<div id="image-drop_zone">請把圖片拖放到這裡</div>
										<br>
										<label class="btn btn-block btn-info" style="width: 140px;">
											瀏覽… <input id="image-browse" type="file" accept="image/*" style="display: none;">
										</label>
									</div>
								</div>
								<div class="col-md-9 col-6 pt-2 pb-2">
									<div id="image-media">
									</div>
								</div>
							</div>
							<div class="row pb-4">
								<div class="col pt-2 pb-2 w-100">
									<a class="btn btn-add-new">新增</a> <a class="btn btn-cancel">返回</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
