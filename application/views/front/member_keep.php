<?php
$this->load->view('front/layouts/partials/member_navbar');
?>

<div class="section-member">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/member_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
									<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
											<li class="breadcrumb-item active" aria-current="page">收藏清單</li>
										</ol>
									</nav>
								</div>
								<h3 class="title-color text-center">收藏清單</h3>
								<hr />
							</div>
						</div>
						<span class="need-share-button_dropdown need-share-button_dropdown-box-horizontal need-share-button_dropdown-middle-right" style="margin-top: -20px;"><span class="need-share-button_link-box need-share-button_link need-share-button_mailto" data-network="mailto"></span><span class="need-share-button_link-box need-share-button_link need-share-button_twitter" data-network="twitter"></span><span class="need-share-button_link-box need-share-button_link need-share-button_pinterest" data-network="pinterest"></span><span class="need-share-button_link-box need-share-button_link need-share-button_facebook" data-network="facebook"></span><span class="need-share-button_link-box need-share-button_link need-share-button_googleplus" data-network="googleplus"></span><span class="need-share-button_link-box need-share-button_link need-share-button_linkedin" data-network="linkedin"></span></span>
						<div class="row">
							<?php
							foreach ($keep_list as $service) {
								$bookmark_icon = 'far';
								if ($service['is_bookmarked']) {
									$bookmark_icon = 'fa';
								}
							?>
							<div class="col-xl-4 col-lg-6 pt-2 pb-2 d-flex align-items-stretch">
								<div class="card service-box">
									<div class="card-header">
										<img src="<?=base_url('assets/img/uploads/providers/' . $service['photo']);?>" class="same-height img-fluid" />
									</div>
									<div class="card-body d-flex flex-column">
										<p><?=$service['location'];?></p>
										<h5 class="title-color"><a href="<?=base_url('service/details/' . $service['shop_uri']);?>"?><?=$service['title'];?></a></h5>
										<div class="tag-list">
											<?php
											foreach ($service['tags'] as $_tag) {
												?>
												<span style="background-color: <?=$tag_lists[$_tag]['color'];?>;"><?=$tag_lists[$_tag]['name'];?></span>
												<?php
											}
											?>
										</div>
									</div>
									<div class="card-footer">
										<div class="row">
											<div class="col-8">
												<?php
												echo str_repeat('<img class="pe-2" src="' . base_url('assets/img/uonlive/icon_rate2.png') .'" />', $service['score']);
												echo str_repeat('<img class="pe-2" src="' . base_url('assets/img/uonlive/icon_rate1.png') .'" />', 5 - $service['score']);
												?>
											</div>
											<div class="col-4 d-flex justify-content-end">
												<div id="share-button_<?=$service['providers_id'];?>" class="need-share-button-default" data-share-position="topCenter" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fa fa-share-alt" aria-hidden="true"></i></span></div>
												<!-- <img src="<?=base_url('assets/img/uonlive/icon_share.png');?>" /> -->
												<a data-member="<?=$member['id'];?>" data-provider="<?=$service['providers_id'];?>" class="pt-1 ps-2 bookmark-page provider_id_<?=$service['providers_id'];?>"><i class="<?=$bookmark_icon;?> fa-bookmark" aria-hidden="true"></i></a>
												<!-- <img class="ps-2" src="<?=base_url('assets/img/uonlive/icon_like1.png');?>" /> -->
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
