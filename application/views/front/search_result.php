<nav class="navbar navbar-expand-md navbar-dark main-nav gray_bg section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color">搜尋結果</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>
<div class="service_search filter_content_section pt-3">
	<div class="container">
		<h3 class="title-color">關鍵字'<?=$keyword;?>'在服務的搜尋結果</h3>
		<div class="row">
			<?php
			if (count($services) == 0) {
			?>
			<div class="col-md-2">找不到匹配的資料</div>
			<?php
			} else {
			?>
			<div class="col-md-2">第1至<?=count($services);?>間，共<?=count($services);?>間</div>
			<?php
			}
			?>
		</div>
		<?php
		foreach ($services as $idx => $service)
		{
			$feature = base_url('assets/img/uonlive/1x1.png');
			if (count($service['photos']) > 0) {
				$feature = base_url('assets/img/uploads/services/' . $service['photos'][0]['photo']);
			}
		?>
		<div class="row">
			<div class="col-md-2"><img src="<?=$feature;?>" class="img-fluid d-block mx-auto" alt=""></div>
			<div class="col-md-8">
				<table class="table">
					<tbody>
					<tr>
						<td style="width: 40px;"><span><?=$idx + 1;?>.</span></td>
						<td><a href="<?=base_url('service/details/' . $service['shop_uri']);?>"><span><?=$service['shop_name'];?></span></a></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$service['head_office_building'];?> | <?=$service['phone'];?></p></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$service['business_hours'];?></p></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-2 text-right mt-auto"></div>
		</div>
		<?php
		}
		?>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col">
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div> -->
</div>

<div class="provider_search filter_content_section mt-3">
	<div class="container">
		<h3 class="title-color">關鍵字'<?=$keyword;?>'在服務提供者的搜尋結果</h3>
		<div class="row">
			<?php
			if (count($providers) == 0) {
			?>
			<div class="col-md-2">找不到匹配的資料</div>
			<?php
			} else {
			?>
			<div class="col-md-2">第1至<?=count($providers);?>間，共<?=count($providers);?>間</div>
			<?php
			}
			?>
		</div>
		<?php
		foreach ($providers as $idx => $provider)
		{
			$feature = base_url('assets/img/uonlive/1x1.png');
			if ($provider['shop_photo']) {
				$feature = base_url('assets/img/uploads/providers/' . $provider['shop_photo']);
			}
		?>
		<div class="row">
			<div class="col-md-2"><img src="<?=$feature;?>" class="img-fluid d-block mx-auto" alt=""></div>
			<div class="col-md-8">
				<table class="table">
					<tbody>
					<tr>
						<td style="width: 40px;"><span><?=$idx + 1;?>.</span></td>
						<td><a href="<?=base_url('service/details/' . $provider['shop_uri']);?>"><span><?=$provider['shop_name'];?></span></a></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$provider['head_office_building'];?> | <?=$provider['phone'];?></p></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$provider['business_hours'];?></p></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-2 text-right mt-auto"></div>
		</div>
		<?php
		}
		?>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col">
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div> -->
</div>

<div class="hashtag_search filter_content_section mt-3">
	<div class="container">
		<h3 class="title-color">關鍵字'<?=$keyword;?>'在關鍵字的搜尋結果</h3>
		<div class="row">
			<?php
			if (count($hashtags) == 0) {
			?>
			<div class="col-md-2">找不到匹配的資料</div>
			<?php
			} else {
			?>
			<div class="col-md-2">第1至<?=count($hashtags);?>間，共<?=count($hashtags);?>間</div>
			<?php
			}
			?>
		</div>
		<?php
		foreach ($hashtags as $idx => $hashtag)
		{
			$feature = base_url('assets/img/uonlive/1x1.png');
			if ($hashtag['shop_photo']) {
				$feature = base_url('assets/img/uploads/providers/' . $hashtag['shop_photo']);
			}
		?>
		<div class="row">
			<div class="col-md-2"><img src="<?=$feature;?>" class="img-fluid d-block mx-auto" alt=""></div>
			<div class="col-md-8">
				<table class="table">
					<tbody>
					<tr>
						<td style="width: 40px;"><span><?=$idx + 1;?>.</span></td>
						<td><a href="<?=base_url('service/details/' . $hashtag['shop_uri']);?>"><span><?=$hashtag['shop_name'];?></span></a></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$hashtag['head_office_building'];?> | <?=$hashtag['phone'];?></p></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$hashtag['business_hours'];?></p></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-2 text-right mt-auto"></div>
		</div>
		<?php
		}
		?>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col">
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div> -->
</div>

<!-- <div class="media_search filter_content_section">
	<div class="container">
		<h3 class="title-color">關鍵字'<?=$keyword;?>'在媒體的搜尋結果</h3>
		<div class="row">
			<?php
			if (count($medias) == 0) {
			?>
			<div class="col-md-2">找不到匹配的資料</div>
			<?php
			} else {
			?>
			<div class="col-md-2">第1至<?=count($medias);?>間，共<?=count($medias);?>間</div>
			<?php
			}
			?>
		</div>
		<?php
		foreach ($medias as $idx => $media)
		{
			?>
			<div class="row">
				<div class="col-md-2"><img src="https://via.placeholder.com/60x60" class="img-fluid d-block mx-auto" alt=""></div>
				<div class="col-md-8">
					<table class="table">
						<tbody>
						<tr>
							<td style="width: 20px;"><input type="checkbox" /></td>
							<td style="width: 40px;"><span><?=$idx + 1;?>.</span></td>
							<td><a href="<?=base_url('service/details/' . $shop['uri']);?>"><span><?=$shop['title'];?></span></a></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td><span><?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', $shop['rate']) . str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate1.png') . '" />', 5 - $shop['rate']);?></span></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td><p><?=$shop['location'];?> | <?=$shop['phone'];?></p></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td><p><?=$shop['business_time'];?></p></td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-2 text-right mt-auto"><button class="btn btn-message text-nowrap">發訊息</button></div>
			</div>
			<?php
		}
		?>
	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div> -->
