<nav class="navbar navbar-expand-md navbar-dark main-nav gray_bg section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		$like_icon = 'far';
		$bookmark_icon = 'far';
		if ($liked) {
			$like_icon = 'fa';
		}
		if ($bookmarked) {
			$bookmark_icon = 'fa';
		}
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color"><?=$provider['tag_name'];?></h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>
<div class="service_item filter_content_section">
	<div class="container">
		<div class="row pt-3 pb-3">
			<div class="col-lg-7 d-flex align-items-center">
				<div class="circular--portrait d-inline-block">
					<img class="profile-pic" src="<?=base_url('assets/img/uploads/providers/' . $provider['shop_photo']);?>" />
				</div>
				<h3 class="d-inline-block title-color ps-3"><?=$provider['shop_name'];?></h3>
			</div>
			<div class="col-lg-5 mt-auto feature-bar">
				<ul class="nav justify-content-end">
					<li class="nav-item">
						<!-- <a class="nav-link need-share-button-default" id="share-button"><i class="fa fa-share-alt" aria-hidden="true"></i></a> -->
						<div id="share-button" class="need-share-button-default" data-share-position="topCenter" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fa fa-share-alt" aria-hidden="true"></i></span></div>
						<span class="need-share-button_dropdown need-share-button_dropdown-box-horizontal need-share-button_dropdown-middle-right" style="margin-top: -20px;"><span class="need-share-button_link-box need-share-button_link need-share-button_mailto" data-network="mailto"></span><span class="need-share-button_link-box need-share-button_link need-share-button_twitter" data-network="twitter"></span><span class="need-share-button_link-box need-share-button_link need-share-button_pinterest" data-network="pinterest"></span><span class="need-share-button_link-box need-share-button_link need-share-button_facebook" data-network="facebook"></span><span class="need-share-button_link-box need-share-button_link need-share-button_googleplus" data-network="googleplus"></span><span class="need-share-button_link-box need-share-button_link need-share-button_linkedin" data-network="linkedin"></span></span>
					</li>
					<li class="nav-item">
						<a data-member="<?=$member_id;?>" data-provider="<?=$provider['id'];?>" class="nav-link like-page"><i class="<?=$like_icon;?> fa-heart" aria-hidden="true"></i></a>
					</li>
					<li class="nav-item">
						<a data-member="<?=$member_id;?>" data-provider="<?=$provider['id'];?>" class="nav-link bookmark-page"><i class="<?=$bookmark_icon;?> fa-bookmark" aria-hidden="true"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7">
				<div class="shop-banner">
					<img class="img-fluid" src="<?=base_url('assets/img/uploads/providers/' . $provider['shop_photo']);?>" />
					<div class="manager-pic rounded-circle ratio ratio-1x1 overflow-hidden">
						<img class="" src="<?=base_url('assets/img/uploads/providers/' . $provider['avatar_photo']);?>" class="img-fluid" />
					</div>
				</div>
				<div class="tags_content">
					<?php
					array_map(function ($a) { echo '<div class="tag"><a href="' . base_url('hashtag/items/' . $a['hashtag_uri']) . '">' . $a['hashtag'] . '</a></div>'; }, $provider['hashtags']);
					?>
					<!-- <div class="tag">Soak Gel手</div>
					<div class="tag">Soak Gel腳</div> -->
				</div>
				<div class="all-items">
					<?php
					foreach ($services as $idx => $service) {
					?>
					<div class="item-title">
						<h3 class="d-inline-block title-color"><?=$service['name'];?></h3>
						<!-- <?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', 3) . str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate1.png') . '" />', 2);?> -->
					</div>
					<div class="row">
						<div class="col-sm-10">
							<p><?=$service['summary'];?></p>
						</div>
						<div class="col-sm-2 text-right"><span class="title-color hover" id="item-handler-<?=$idx;?>"><i class="fa fa-angle-right" aria-hidden="true"></i> 詳情</span></div>
					</div>
					<div id="service-item-<?=$idx;?>">
						<div class="row">
							<?php
							foreach ($service['photos'] as $photo) {
							?>
							<div class="col pt-3 pb-3">
								<a href="<?=base_url('assets/img/uploads/services/' . $photo['photo']);?>" data-toggle="lightbox"><img class="square-image d-block mx-auto" src="<?=base_url('assets/img/uploads/services/' . $photo['photo']);?>" /></a>
							</div>
							<?php
							}
							?>
						</div>
						<div class="row">
							<div class="col">
								<?=$service['content'];?>
							</div>
						</div>
					</div>
					<hr />
					<?php
					}
					?>
				</div>
				<div class="row">
					<div class="col text-center pt-3 pb-3">
						<button type="button" data-member="<?=$member_id;?>" data-provider="<?=$provider['id'];?>" class="btn-leave-message btn btn-uonlive w-50">聯絡查詢</button>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="col-12">
						<h5>客戶評論</h5>
					</div>
				</div>
				<div class="comment-area">
					<!-- <div class="row align-items-end">
						<div class="col-sm-4 col-6">
							<table class="table align-bottom">
								<tbody>
									<tr>
										<td><h4 class="title-color no-margin-bottom">4.1<span>/5</span></h4></td>
										<td><p class="no-margin-bottom">38則評論</p><?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', 3) . str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate1.png') . '" />', 2);?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-sm-5 col-6">
							<h4 class="title-color text-center no-margin-bottom">93%<span>推薦給朋友 14/15</span></h4>
						</div>
						<div class="col-sm-3 col">
						</div>
					</div> -->
					<div class="row pt-2">
						<div class="col">
							<ul class="nav">
								<li class="nav-item">
									<p class="title-color no-margin-bottom">最多用家評論：<span class="text-black"><?=$mostComment;?></span></p>
								</li>
								<!-- <li class="nav-item">
									<img class="ps-3" src="<?=base_url('assets/img/uonlive/icon_rate2.png');?>" />5(22)
								</li>
								<li class="nav-item">
									<img class="ps-3" src="<?=base_url('assets/img/uonlive/icon_rate2.png');?>" />4(10)
								</li>
								<li class="nav-item">
									<img class="ps-3" src="<?=base_url('assets/img/uonlive/icon_rate2.png');?>" />3(1)
								</li>
								<li class="nav-item">
									<img class="ps-3" src="<?=base_url('assets/img/uonlive/icon_rate2.png');?>" />2(5)
								</li>
								<li class="nav-item">
									<img class="ps-3" src="<?=base_url('assets/img/uonlive/icon_rate2.png');?>" />1(0)
								</li> -->
							</ul>
						</div>
					</div>
					<?php
					foreach ($user_comments as $idx => $comment)
					{
					?>
					<div class="row user-comments">
						<div class="col-md-2"><img src="<?=$comment['avatar'];?>" class="img-fluid d-block mx-auto" alt=""></div>
						<div class="col-md-2"><?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', $comment['rate']) . str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate1.png') . '" />', 5 - $comment['rate']);?></div>
						<div class="col-md-6">
							<p class="title-color no-margin-bottom"><?=$comment['user'];?></p>
							<p class="no-margin-bottom"><?=$comment['date'] . ' | ' . $comment['comment'];?></p>
						</div>
						<div class="col-md-2 text-right mt-auto text-nowrap"><p class="title-color no-margin-bottom">我會推薦給朋友</p></div>
					</div>
					<?php
					}
					echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
					?>
					<!-- <div class="show_more mt-4 mb-4">
						<div class="small_text light-gray">查看全部評論</div>
					</div> -->
				</div>
			</div>
			<div class="col-lg-5">
				<div class="card shop-info">
					<div class="card-header"><h3 class="title-color text-center">商戶資訊</h3></div>
					<div class="card-body">
						<table class="table">
							<tbody>
								<!-- <tr>
									<td class="col-2 title-color">信譽</td>
									<td colspan="2"><?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', 5);?></td>
								</tr> -->
								<tr>
									<td class="col-2 title-color">店長</td>
									<td colspan="2"><?=$provider['owner_name'];?></td>
								</tr>
								<tr>
									<td class="col-2 title-color">年資</td>
									<?php
										$d1=new DateTime(); 
										$d2=new DateTime(date("Y-m-d", $provider['created_at']));
										$Months = $d2->diff($d1); 
										$howeverManyMonths = (($Months->y) * 12) + ($Months->m);
									?>
									<td colspan="2"><?=sprintf('%d年%d月', $Months->y, $Months->m);?></td>
								</tr>
								<tr>
									<td class="col-2 title-color">地區</td>
									<td colspan="2">
										<p class="title-color no-margin-bottom">總店</p>
										<p><?=$provider['district_name'] . '區' . $provider['head_office_building'];?></p>
										<!-- <p class="title-color no-margin-bottom">分店</p>
										<p>灣仔洛克道168號灣景中心28樓</p> -->
									</td>
								</tr>
								<tr>
									<td class="col-2 title-color">電話</td>
									<td colspan="2"><?=$provider['phone'];?>
									<?php
									if (count($provider['socials']['whatsapp']) > 0) {
										echo ' / Whatsapp <a href="https://wa.me/' . $provider['socials']['whatsapp']['social_url'] . '">' . $provider['socials']['whatsapp']['social_url'] . '</a>';
									}
									?>
									</td>
								</tr>
								<tr>
									<td class="col-2 title-color">電郵</td>
									<td colspan="2"><?=$provider['email'];?></td>
								</tr>
								<tr>
									<td class="col-2 title-color no-padding-bottom">時間</td>
									<td class="no-padding-bottom"><?=$provider['business_hours'];?></td>
								</tr>
								<!-- <tr>
									<td class="col-2 title-color">服務對象</td>
									<td colspan="2">少年/青年/成人/老年</td>
								</tr> -->
								<tr>
									<td class="col-2 title-color">媒體</td>
									<td colspan="2" class="">
										<?php
										if (count($provider['socials']['facebook']) > 0) {
										?>
										<div>
										<img src="<?=base_url('assets/img/uonlive/i_fb.png');?>" class="" /> <a href="https://www.facebook.com/<?=$provider['socials']['facebook']['social_url'];?>">FACEBOOK</a>
										</div>
										<?php
										}
										if (count($provider['socials']['instagram']) > 0) {
										?>
										<div>
										<img src="<?=base_url('assets/img/uonlive/i_ig.png');?>" class="" /> <a href="https://www.instagram.com/<?=$provider['socials']['instagram']['social_url'];?>">INSTAGRAM</a>
										</div>
										<?php
										}
										if (count($provider['socials']['youtube']) > 0) {
										?>
										<div>
										<img src="<?=base_url('assets/img/uonlive/i_youtube.png');?>" class="" /> <a href="https://www.youtube.com/<?=$provider['socials']['youtube']['social_url'];?>">YOUTUBE</a>
										</div>
										<?php
										}
										?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="card-footer"></div>
				</div>
				<div class="card shop-news mt-3 mb-3">
					<div class="card-header"></div>
					<div class="card-body">
						<h3 class="title-color text-center">新聞</h3>
						<?php if(!empty($all_news)){ ?>
							<?php foreach($all_news as $news){?>
								<div class="news_item pt-3">
									<div class="row">
										<div class="col-12">
											<div class="news_title_wrapper">
												<div class="news_title">
													<div>
														<h6><?=$news['title']?></h6>
													</div>
												</div>
											</div>
											<div class="news_content">
												<div>
													<?=$news['content']?>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php }?>
						<?php
						if (count($all_news) > 100) {
						?>
						<div class="show_more mt-4 mb-4">
							<div class="small_text light-gray">查看全部新聞</div>
						</div>
						<?php
						}
						?>
						<?php 
							}
							else {
								echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
							}
						?>
						<h3 class="title-color text-center pt-4">圖片</h3>
						<?php if(!empty($all_images)){ ?>
						<div class="row">
							<?php foreach($all_images as $image){ ?>
								<div class="col-12 col-md-6 col-lg-4 pt-3">
									<a href="<?=base_url('assets/img/uploads/medias/' . $image);?>" data-toggle="lightbox"><img src="<?=base_url('assets/img/uploads/medias/' . $image);?>" class="img-fluid" alt="" /></a>
								</div>
							<?php }?>
						</div>
						<?php
						if (count($all_images) > 100) {
						?>
						<div class="show_more mt-4 mb-4">
							<div class="small_text light-gray">查看全部圖片</div>
						</div>
						<?php
						}
						?>
						<?php 
						}
						else {
							echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
						}
						?>
						<h3 class="title-color text-center pt-4">影片</h3>
						<div class="detail_video_wrapper">
							<?php if(!empty($all_videos)){ ?>
							<div class="card-deck text-center row">
							<?php foreach($all_videos as $video){ ?>
								<div class="card mb-3 box-shadow col-lg-6 pt-3">
									<div class="card-header">
									<a href="<?=$video['url'];?>" data-toggle="lightbox"><img src="<?=base_url('assets/img/uploads/medias/' . $video['image']);?>" class="w-100" alt="" /></a>
									</div>
									<div class="card-body d-flex flex-column">
										<h6 class="video_title pt-2"><?=$video['title']?></h6>
									</div>
								</div>
							<?php }?>
							</div>
							<?php
							if (count($all_videos) > 100) {
							?>
							<div class="show_more mt-4 mb-4">
								<div class="small_text light-gray">查看全部影片</div>
							</div>
							<?php
							}
							?>
							<?php 
							}
							else {
								echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
							}
							?>
						</div>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->load->view('front/layouts/partials/popup_message_dialog');