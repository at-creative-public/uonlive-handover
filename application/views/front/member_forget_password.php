<nav class="navbar navbar-expand-md navbar-dark main-nav section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color">歡迎進入UonLIVE會員專區</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>

<div class="section-member mb-5">
	<div class="container">
		<?php
		if (! is_null($member)) {
			if (count($member) == 0) {
		?>
		<div class="text-center alert alert-danger" role="alert">找不到用戶登記電郵</div>
		<?php
			}
			else {
		?>
		<div class="text-center alert alert-success" role="alert">新的密碼已發送到用戶的登記電郵</div>
		<?php
			}
		}
		?>
		<form id="frmForgetPassword" action="<?=current_url();?>" method="post">
			<div class="row">
				<div class="col">
					<h3 class="text-center">忘記密碼</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xxl-3 col-xl-1 d-none d-lg-block"></div>
				<div class="col-xxl-6 col-xl-10">
					<div class="container">
						<div class="row">
							<div class="col pt-4 pb-2">
								<input type="email" id="email" name="email" class="form-control" placeholder="用戶登記電郵">
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<a class="btn btn-submit">發送</a>
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<nav style="--bs-breadcrumb-divider: '|';" aria-label="breadcrumb">
									<ol class="breadcrumb text-center">
										<li class="breadcrumb-item"><a href="<?=base_url('member/register');?>">註冊新用戶</a></li>
										<li class="breadcrumb-item"><a href="<?=base_url('member/login');?>">登入</a></li>
									</ol>
								</nav>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xxl-3 col-xl-1 d-none d-lg-block"></div>
			</div>
		</form>
	</div>
</div>
