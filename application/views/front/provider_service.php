<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-provider-service-management">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
								<?php
								$this->load->view('front/layouts/partials/thumbnail');
								?>
								</div>
								<h3 class="title-color text-center">服務管理</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col-3">
								<div class="col pt-2 pb-2">
									<a href="<?=base_url('provider/add_service');?>" class="btn btn-save">新增服務</a>
								</div>
							</div>
						</div>
						<div class="row pt-3">
							<div class="col">
								<!-- drag table test -->
								<section class="service-list">
<!--									<div class="fixed-table-toolbar">-->
<!--										<div class="bars pull-left">-->
<!--											<div id="toolbar"><strong>Landing Page 次目錄</strong></div>-->
<!--										</div>-->
<!--										<div class="pull-right search"></div>-->
<!--									</div>-->
<!---->
<!--									<div id="info" class="content">(Drag the <span style="color:red">red square</span> to re-arrange the-->
<!--										order.)-->
<!--									</div>-->
									<!-- test drag table -->
									<table class="w-100 table-header">
										<thead>
										<tr>
											<th width="20"></th>
											<th width="60" class="text-center">顯示次序</th>
											<th width="280">服務名稱</th>
											<th width="120">更新時間</th>
											<th width="50" class="text-center">顯示</th>
											<th width="50"></th>
											<th width="50"></th>
										</tr>
										</thead>
									</table>
									<ul id="content-list" class="ui-sortable">
									<?php
										foreach ($services as $idx => $service) {
									?>
										<li id="listItem_<?=$service['id'];?>" class="border my-2" style="border: 1px solid #808080; border-radius: 10px;">
											<table class="w-100 content">
												<tbody>
												<tr id="<?=$service['id'];?>">
													<td width="20" align="center">
														<div class="handle"></div>
													</td>
													<td width="60">
														<div class="rank_no text-center"><?=$service['displayorder'];?></div>
													</td>
													<td width="280"><img src="<?=base_url('assets/img/uploads/services/' . $service['photo']);?>" style="max-width: 100px;object-fit: cover;width: 100px;height: 100px;" class="img-fluid" /> <?=$service['name'];?></td>
													<td width="120"><?=$service['last_updated'];?></td>
													<td width="50" class="text-center"><input type="checkbox" value="1"<?=($service['show'] == 1) ? 'checked="checked"' : '';?> /></td>
													<td width="50" class="text-center"><a href="<?=base_url('provider/modify_service/' . $service['token']);?>" class="btn btn-edit">編輯</a></td>
													<td width="50" class="text-center">
														<button type="submit" class="btn btn-delete remove">删除</button>
													</td>
												</tr>
												</tbody>
											</table>
										</li>
									<?php
									}//foreach ($test_drag_content as $contentrow) {
									?>
									</ul>
								</section>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
