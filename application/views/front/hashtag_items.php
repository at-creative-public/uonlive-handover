<nav class="navbar navbar-expand-md navbar-dark main-nav gray_bg section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color">最新項目</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>
<div class="provider_search filter_content_section">
	<div class="container">
		<h3 class="title-color">關鍵字'<?=$tag;?>'在服務提供者的搜尋結果</h3>
		<div class="row">
			<?php
			if (count($providers) == 0) {
			?>
			<div class="col-md-2">找不到匹配的資料</div>
			<?php
			} else {
			?>
			<div class="col-md-2">第1至<?=count($providers);?>間，共<?=count($providers);?>間</div>
			<?php
			}
			?>
		</div>
		<?php
		foreach ($providers as $idx => $provider)
		{
			$feature = base_url('assets/img/uonlive/1x1.png');
			if ($provider['shop_photo']) {
				$feature = base_url('assets/img/uploads/providers/' . $provider['shop_photo']);
			}
		?>
		<div class="row">
			<div class="col-md-2"><img src="<?=$feature;?>" class="img-fluid d-block mx-auto" alt=""></div>
			<div class="col-md-8">
				<table class="table">
					<tbody>
					<tr>
						<td style="width: 40px;"><span><?=$idx + 1;?>.</span></td>
						<td><a href="<?=base_url('service/details/' . $provider['shop_uri']);?>"><span><?=$provider['shop_name'];?></span></a></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$provider['head_office_building'];?> | <?=$provider['phone'];?></p></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$provider['business_hours'];?></p></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-2 text-right mt-auto"></div>
		</div>
		<?php
		}
		?>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col">
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div> -->
</div>

<div class="provider_search filter_content_section">
	<div class="container">
		<h3 class="title-color">關鍵字'<?=$tag;?>'在媒體的搜尋結果</h3>
		<div class="row">
			<?php
			if (count($medias) == 0) {
			?>
			<div class="col-md-2">找不到匹配的資料</div>
			<?php
			} else {
			?>
			<div class="col-md-2">第1至<?=count($medias);?>間，共<?=count($medias);?>間</div>
			<?php
			}
			?>
		</div>
		<?php
		foreach ($medias as $idx => $media)
		{
			$feature = base_url('assets/img/uonlive/1x1.png');
			if ($media['shop_photo']) {
				$feature = base_url('assets/img/uploads/providers/' . $media['shop_photo']);
			}
		?>
		<div class="row">
			<div class="col-md-2"><img src="<?=$feature;?>" class="img-fluid d-block mx-auto" alt=""></div>
			<div class="col-md-8">
				<table class="table">
					<tbody>
					<tr>
						<td style="width: 40px;"><span><?=$idx + 1;?>.</span></td>
						<td><a href="<?=base_url('service/details/' . $media['shop_uri']);?>"><span><?=$media['shop_name'];?></span></a></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$media['head_office_building'];?> | <?=$media['phone'];?></p></td>
					</tr>
					<tr>
						<td></td>
						<td><p><?=$media['business_hours'];?></p></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-2 text-right mt-auto"></div>
		</div>
		<?php
		}
		?>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col">
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div> -->
</div>
