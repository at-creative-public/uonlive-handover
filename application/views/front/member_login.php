<nav class="navbar navbar-expand-md navbar-dark main-nav section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color">歡迎進入UonLIVE會員專區</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>

<div class="section-member mb-5">
	<div class="container">
		<?php
		if (! is_null($member)) {
			if (count($member) == 0) {
		?>
		<div class="alert alert-danger" role="alert">用戶登記電郵/密碼不符</div>
		<?php
			}
		}
		?>
		<form id="frmLogin" action="<?=current_url();?>" method="post">
			<div class="row">
				<div class="col">
					<h3 class="text-center">登入</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xxl-3 col-xl-1 d-none d-lg-block"></div>
				<div class="col-xxl-6 col-xl-10">
					<div class="container">
						<div class="row">
							<div class="col pt-4 pb-2">
								<input type="email" id="email" name="email" class="form-control" placeholder="用戶登記電郵">
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<input type="password" id="password" name="password" class="form-control" placeholder="密碼">
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<a class="btn btn-login">馬上登入</a>
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<nav style="--bs-breadcrumb-divider: '|';" aria-label="breadcrumb">
									<ol class="breadcrumb text-center">
										<li class="breadcrumb-item"><a href="<?=base_url('member/register');?>">註冊新用戶</a></li>
										<li class="breadcrumb-item"><a href="<?=base_url('member/forget_password');?>">忘記密碼</a></li>
									</ol>
								</nav>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xxl-3 col-xl-1 d-none d-lg-block"></div>
			</div>
		</form>
	</div>
</div>
