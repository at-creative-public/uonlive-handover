<?php
$this->load->view('front/layouts/partials/member_navbar');
?>

<div class="section-member">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/member_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
								<?php
								$this->load->view('front/layouts/partials/thumbnail');
								?>
								</div>
								<h3 class="alert-danger text-center">升級為服務提供者／品牌 - 付款失敗</h3>
								<hr />
							</div>
						</div>
						<div class="row pb-4">
							<div class="col-12">
								<h5 class="title-color font-weight-bold">檢視你的訂單</h5>
								<div class="fading_secondary_theme_background_color default_border_radius">
									<div class="inner_padding">
										<h5 class="secondary_theme_color font-weight-bold">升級為服務提供者／品牌</h5>
										<p class="secondary_theme_color font-weight-bold">期限：<?=date("Y-m-d", $expiry_date);?> - <?=date("Y-m-d", strtotime("+30 days", $expiry_date));?>，共30日　<br />　　　（日期以成功付款日為準）</p>
										<p class="secondary_theme_color font-weight-bold">付款方式：信用卡</p>
										<p class="secondary_theme_color font-weight-bold">費用：HK$<?=number_format($this->config->item('uonlive')['upgrade_fee']);?></p>
										<hr>
										<div class="d-flex">
											<span class="color_grey font-weight-bold">今日交付（港幣）</span>
											<span class="ml-auto color_grey font-weight-bold">$<?=number_format($this->config->item('uonlive')['upgrade_fee']);?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
