<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-member-conversion-history">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
									<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
											<li class="breadcrumb-item active" aria-current="page">聯絡記錄</li>
										</ol>
									</nav>
								</div>
								<h3 class="title-color text-center">聯絡記錄</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<select id="page-type">
									<option value=""<?=($type == '') ? ' selected="selected"' : '';?>>所有</option>
									<option value="unread"<?=($type == 'unread') ? ' selected="selected"' : '';?>>未閱讀</option>
									<option value="read"<?=($type == 'read') ? ' selected="selected"' : '';?>>已閱讀</option>
								</select>
								<table class="table table-striped">
									<thead>
									<tr>
										<th>留言日期和時間</th>
										<th>會員</th>
										<th>標題</th>
										<th>狀態</th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach ($conversions as $conversion) {
									?>
									<tr>
										<td><?=date("Y-m-d H:i:s", $conversion['created_at']);?></td>
										<td><?=$conversion['first_name'] . ', ' . $conversion['last_name'];?></td>
										<td><?=$conversion['title'];?></td>
										<td><?=$conversion['is_read'] ? '已閱讀' : '未閱讀';?></td>
										<td><a href="<?=base_url('provider/conversion_details/' . $conversion['message_uri']);?>">詳情</a></td>
									</tr>
									<?php
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
