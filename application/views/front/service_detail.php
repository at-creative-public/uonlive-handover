<div class="gray_bg section_breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_service_detail">
                    <i class="fas fa-home"></i>
                    &nbsp;&gt;&nbsp;
                    <span>
                        服務分類
                    </span>
                    &nbsp;&gt;&nbsp;
                    <span class="breadcrumb_service_detail_current">扮靚</span>
                </div>
                <div class="service_detail_title">
                    <div class="text-center">
                        <h1 class="title-color">扮靚</h1>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="filter_section green_bg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="filter_items_wrapper">
                    <div class="filter_item col">
                        地區
                    </div> 
                    <div class="filter_item col">
                        職業
                    </div> 
                    <div class="filter_item col">
                        時間
                    </div> 
                    <div class="filter_item col">
                        價錢
                    </div> 
                    <div class="filter_item col">
                        年齡
                    </div> 
                    <div class="filter_item col">
                        性別
                    </div> 
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="filter_address_search_wrapper">
                    <div class="location_nearby">
                        <i class="fas fa-map-marker-alt"></i>&nbsp;附近的人
                    </div>
                    <div class="search_section_wrapper">
                        <div class="search_icon_wrapper">
                            <img src="<?=base_url('assets/img/uonlive/search.png" alt="">
                        </div>
                        <input type="text" name="search" placeholder="請輸入關鍵字">
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>

<div class="filter_content_section">
    <div class="container">
        <div class="row">
            <div class="col-12"> 
                <div class="content_section_wrapper">
                    <?php if(!empty($service_tags)){ ?>
                        <?php foreach($service_tags as $tag){ 
                            $random_color = "rgb(".rand(120,225).",".rand(120,225).",".rand(120,225).")";
                        ?>
                            <div class="tag" style="background-color:<?=$random_color?>">
                                <?=$tag?>
                            </div>
                        <?php }?>
                    <?php }?>
                   
                    <div class="content_tabs_wrapper">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">全部</button>
                                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">圖片</button>
                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">影片</button>
                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">新聞</button>
                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">服務</button>
                            </div>
                        </nav>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="items_wrapper">
                                    <!--start news section-->
                                    <div class="news_wrapper">
                                      
                                        <?php if(!empty($all_news)){ ?>
                                            <?php foreach($all_news as $news){?>
                                            <div class="news_item">
                                                <div class="row">
                                                    <div class="col-8 col-md-10">
                                                        <div class="news_title_wrapper">
                                                            <div class="news_title_tag">
                                                                新聞
                                                            </div>
                                                            <div class="news_title">
                                                                <div>
                                                                    <h5><?=$news['news_title']?></h5>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="news_content">
                                                            <div>
                                                                <?=$news['news_content']?>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="col-4 col-md-2">
                                                        <div class="author_wrapper">
                                                            <div class="author_image_wrapper">
                                                                <img src="<?=$news['news_image']?>" class="w-100" alt="">
                                                            </div> 
                                                            <div class="news_author_name"><?=$news['news_author']?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php }?>
                                        <?php } ?>

                                        <div class="show_more mt-4 mb-4">
                                            <div class="small_text">查看全部新聞</div>
                                        </div>
                                    </div>
                                    <!--end news section-->

                                    <!--start images section-->
                                    <div class="images_wrapper">
                                        <div class="title font_green_color mb-3"><h2>「扮靚」的圖片搜尋結果</h2></div>
                                        <div class="carousel_wrapper left right mb-4">
                                            <div class="arrow_left">
                                                <i class="fas fa-chevron-left"></i>
                                            </div>
                                            <div class="owl-carousel owl-theme images_section_carousel owl-loaded">
                                                <?php if(!empty($all_images)){ ?>
                                                    <?php foreach($all_images as $image){ ?>
                                                        <div class="item">
                                                            <img src="<?=$image?>" class="w-100" alt="">
                                                        </div>
                                                    <?php }?>
                                                <?php } ?> 
                                            </div>
                                            <div class="arrow_right">
                                                <i class="fas fa-chevron-right"></i>
                                            </div>
                                        </div>
                                        <div class="show_more mt-4 mb-4">
                                            <div class="small_text">查看全部圖片</div>
                                        </div>
                                    </div>
                                    <!--end images section-->

                                    <!--start video section-->
                                    <div class="detail_video_wrapper">
                                        <div class="title font_green_color mb-3"><h2>「扮靚」的圖片搜尋結果</h2></div>
                                        <div class="row">
                                            <?php if(!empty($all_video)){ ?>
                                                <?php foreach($all_video as $video){ ?>
                                                <div class="col-12 col-md-3">
                                                    <div class="video_item">
                                                        <div  class="section_top_wrapper">
                                                            <img src="<?=$video['image']?>" class="w-100" alt="">
                                                        </div>
                                                        <div class="section_bottom_wrapper">
                                                            <div class="title_wrapper font_green_color mb-4">
                                                                <div class="title">
                                                                    <h6><?=$video['title']?></h6>
                                                                </div>
                                                            </div>
                                                            <div class="small_text text-right">
                                                                <?=$video['author']?>
                                                            </div>   
                                                        </div>
                                                    </div>        
                                                </div>
                                                <?php }?>
                                            <?php }?>
                                        </div>
                                        <div class="show_more mt-4 mb-4">
                                            <div class="small_text">查看全部影片</div>
                                        </div>
                                    </div>
                                    <!--end video section-->

                                    <!--start service section-->
                                    <div class="latest-services">
                                        <div class="row">
                                            <?php
                                            foreach ($latest_services as $service) {
                                            ?>
                                            <div class="col-xl-3 col-lg-4 col-md-6 col-12 pt-3 pb-3">
                                                <div class="card service-box">
                                                    <div class="card-header">
                                                        <img src="<?=base_url('assets/img/uploads/services/' . $service['photo']);?>" class="img-fluid" />
                                                    </div>
                                                    <div class="card-body">
                                                        <p><?=$service['location'];?></p>
                                                        <h5 class="title-color"><?=$service['title'];?></h5>
                                                        <div class="tag-list">
                                                        <?php
                                                        foreach ($service['tags'] as $_tag) {
                                                        ?>
                                                            <span style="background-color: <?=$tags[$_tag]['color'];?>;"><?=$tags[$_tag]['title'];?></span>
                                                        <?php
                                                        }
                                                        ?>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="row">
                                                            <div class="col-8">
                                                            <?php
                                                            echo str_repeat('<img class="pe-2" src="' . base_url('assets/img/uonlive/icon_rate2.png') .'" />', $service['score']);
                                                            echo str_repeat('<img class="pe-2" src="' . base_url('assets/img/uonlive/icon_rate1.png') .'" />', 5 - $service['score']);
                                                            ?>
                                                            </div>
                                                            <div class="col-4 d-flex justify-content-end">
                                                                <img src="<?=base_url('assets/img/uonlive/icon_share.png');?>" />
                                                                <img class="ps-2" src="<?=base_url('assets/img/uonlive/icon_like1.png');?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <!--end service section-->

                                    <!--start pagination section-->
                                    <div class="pagination_wrapper">
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination">
                                                <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                                </li>
                                                <?php for($i=1;$i<=6;$i++){?>
                                                    <li class="page-item"><a class="page-link" href="#"><?=$i?></a></li>
                                                <?php } ?>
                                                <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>

                                    <!--end pagination section-->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">b</div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">c</div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">d</div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">e</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


