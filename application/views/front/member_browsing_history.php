<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-member-browsing-history">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/member_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
									<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
											<li class="breadcrumb-item active" aria-current="page">瀏覽記錄</li>
										</ol>
									</nav>
								</div>
								<h3 class="title-color text-center">瀏覽記錄</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<select id="page-type">
									<option value=""<?=($type == '') ? ' selected="selected"' : '';?>>所有</option>
									<option value="provider"<?=($type == 'provider') ? ' selected="selected"' : '';?>>服務提供者</option>
									<option value="brand"<?=($type == 'brand') ? ' selected="selected"' : '';?>>品牌</option>
									<option value="category"<?=($type == 'category') ? ' selected="selected"' : '';?>>服務分類</option>
								</select>
								<table class="table table-striped">
									<thead>
									<tr>
<!--										<th>用戶</th>-->
										<th>瀏覽日期和時間</th>
										<th>種類</th>
										<th>服務提供者／品牌／服務分類</th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach ($histories as $history) {
										switch ($history['page_type']) {
											case 'c':
												$location = '<a href="' . base_url('service/' . $history['page_info']['name']) . '">' . $history['page_info']['name'] . '</a>';
												$page_type = '服務分類';
												break;
											case 's':
												$location = '<a href="' . base_url('service/details/' . $history['page_info']['shop_uri']) . '">' . $history['page_info']['shop_name'] . '</a>';
												$page_type = '服務提供者 / 品牌';
												break;
										}
									?>
									<tr>
<!--										<td>--><?//=$history['name'];?><!--</td>-->
										<td><?=date("Y-m-d H:i:s", $history['created_at']);?></td>
										<td><?=$page_type;?></td>
										<td><?=$location;?></td>
									</tr>
									<?php
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
