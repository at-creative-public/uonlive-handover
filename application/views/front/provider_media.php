<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-provider-media-management">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
								<?php
								$this->load->view('front/layouts/partials/thumbnail');
								?>
								</div>
								<h3 class="title-color text-center">媒體管理</h3>
								<hr />
							</div>
						</div>
						<div class="row filter-bar p-2">
							<div class="col-3">商店主頁上顯示</div>
							<div class="col-2">圖片 <input type="checkbox" checked="checked" /></div>
							<div class="col-2">影片 <input type="checkbox" checked="checked" /></div>
							<div class="col-2">新聞 <input type="checkbox" checked="checked" /></div>
						</div>
						<div class="row pt-3">
							<div class="col-3">
								<div class="col pt-2 pb-2">
									<a href="<?=base_url('provider/add_media');?>" class="btn btn-save">新增媒體</a>
								</div>
							</div>
						</div>
						<div class="row pt-3">
							<div class="col-lg-1 title-color">媒體類型</div>
							<div class="col-lg-4 title-color">媒體／標題</div>
							<div class="col-lg-3 title-color">更新時間</div>
							<div class="col-lg-1 text-lg-center text-start title-color">顯示</div>
							<div class="col-lg-3 title-color"></div>
						</div>
						<?php
						if ($medias) {
							foreach ($medias as $media) {
						?>
						<div class="row p-2 mt-2 mb-2 border rounded-3 align-items-center" style="min-height: 120px;">
							<?php
							switch ($media['media_type']) {
								case "PHOTO":
									$type_name = '相片';
									$content = '<img src="' . base_url('assets/img/uploads/medias/' . $media['media']['filename']) . '" style="max-width: 100px;object-fit: cover;width: 100px;height: 100px;" class="img-fluid" />';
									break;
								case "NEWS":
									$type_name = '新聞';
									$content = $media['media']['title'];
									break;
								case "VIDEO":
									$type_name = '影片';
									$content = '<img src="' . base_url('assets/img/uploads/medias/' . $media['media']['filename']) . '" style="max-height: 100px;object-fit: cover;" class="img-fluid" />' . $media['media']['title'];
									break;
							}
							?>
							<div class="col-lg-1"><?=$type_name;?></div>
							<div class="col-lg-4"><?=$content;?></div>
							<div class="col-lg-3"><?=date("Y-m-d H:i:s", $media['created_at']);?></div>
							<div class="col-lg-1 text-lg-center text-start"><input type="checkbox" value="1"<?=($media['visible'] == 1) ? 'checked="checked"' : '';?> /></div>
							<div class="col-lg-3"><a href="<?=base_url('provider/modify_media/' . $media['token']);?>" class="btn btn-edit">編輯</a><button type="submit" class="btn btn-delete remove">删除</button></div>
						</div>
						<?php
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
