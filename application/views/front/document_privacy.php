	<div class="section-category mt-4 mb-5">
		<div class="container">
			<div class="row">
				<div class="col">
					<h3 class="text-center title-color">隱私政策</h3>
					<p>UonLIVE Asia Ltd（統稱「UonLIVE」、「我們」），其人員、管理人員和工作人員在任何時候都應該尊重客戶的私隱，並努力確保從您那裡收集到的所有個人資料和/或您存儲和/或傳輸給我們的信息的安全。我們的政策可能會不時修訂，如果有任何此類更改，我們將以書面形式通知您。</p>
					<ol>
						<li>您的私隱：作為UonLIVE的會員，我們將在您成為我們會員、參加活動、向我們訂閱，或要求我們提供服務時收集您的個人資料。如果UonLIVE的營運受到中國香港特別行政區以外的私隱法規的管轄，本聲明應在當地法律的切實可行範圍內盡可能適用。</li>
						<li>收集的個人資料：為了開展UonLIVE的業務，當您成為會員或訪問我們的網站時，我們會要求您提供個人資料（包括但不限於<?=base_url();?>（“網站”）），當您報名活動，或向我們提交任何評論或反饋時，我們或者需要您驗證您的身份。您可能會被要求提供個人資料包括但不限於您的姓名、聯繫電話、電子郵件地址、通信地址、性別、出生日期和月份、年齡、付款明細、職業、教育和職業，和/或個人興趣。所有個人資料將在自願、公開和公平的基礎上收集。如果您未滿18歲，則必須先徵得父母或監護人的同意，然後才能向我們披露您的任何個人資料。</li>
						<li>收集個人資料的目的：UonLIVE收集和使用您的個人資料乃用於：由UonLIVE/其服務提供商（我們或會聘請服務供應商協助提供我們的服務）提供活動和服務，個人資料將用於執行您的指示或回應您的詢問；和/或用於直接營銷，包括為您設計更多的活動和服務；和/或以建立UonLIVE營運所需要的客戶數據庫；和/或遵守中國香港特別行政區或其他相關管轄區的法律、政府或法規要求，包括UonLIVE或其他類別的數據接收方應遵守的披露或通知要求。</li>
						<li>個人資料分享和披露：除非UonLIVE在您參加活動時收到您的反對意見或後續通知，否則我們可能會向以下各方提供您的個人信息：UonLIVE的員工和管理人員，但限於在上述第3條所述的產品和服務範圍內；UonLIVE的承包商/服務提供商（由我方聘請協助向您提供服務）；和/或法院或監管機構發出傳票、法院命令或進行法律程序。</li>
						<li>保留個人資料：除非違反任何適用的法定要求，否則您的個人資訊將僅保留至完成資訊收集目的所需的期間。 管理個人資料：您有權隨時更正或編輯您的個人資料，包括您是否希望我們就直接營銷目的、活動促銷優惠以及新服務與您聯繫。您可以通過登錄<?=base_url('member/login');?>更正以行使您的權利。或者，您可以直接發送書面要求至it@uonlive.com（註明客戶服務）。 UonLIVE在確信訪問或更正請求的真實性和有效性後，將盡一切努力在法定要求規定的期限內遵守並回復請求。 準確性： UonLIVE將採取一切切實可行的措施，確保收集的個人資料是準確的。請您在訂閱UonLIVE服務時提供準確的個人資料，並在以後個人資訊變更時告知UonLIVE。</li>
						<li>安全：數據安全是我們的首要任務。您提供的所有個人資料將妥善儲存在我們的資料庫系統內，只有獲授權的人士才能存取。我們努力確保您的個人資料不會受到未經授權或意外的存取、處理或刪除。我們通過實施適當的物理、電子和管理措施來保護和保護您的個人資料，從而保持對數據安全的承諾。</li>
						<li>直接營銷：UonLIVE可能會不時與您聯繫，以提供有關我們活動和服務/會員計劃的最新信息。如果您不想接收這些信息，請登錄<?=base_url('member/login');?>退出訂閱。或者，您可以單擊我們的電子宣傳材料中顯示的取消訂閱鏈接；或者可以直接發送書面要求至it@uonlive.com（註明客戶服務）。根據法定要求，UonLIVE將遵守您的請求，不將您的個人數據用於直接營銷目的。除非上述另有指示，UonLIVE可將您在正常業務過程中收集的任何個人數據用於直接營銷目的。</li>
						<li>如果您對本聲明有任何疑問，請通過電子郵件與我們聯繫，電子郵件為it@uonlive.com。</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
