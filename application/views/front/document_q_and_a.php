<div class="section-category mt-4 mb-5">
		<div class="container">
			<div class="row">
				<div class="col">
					<h3 class="text-center title-color">常見問題</h3>
					<ol>
						<li><span class="title-color">How to sell files on Dropbox Shop</span>
							<br />You can use Dropbox Shop to sell anything from e-books and recipes to prints and workout videos, directly from your Dropbox account.
							<br />Dropbox Shop is currently a beta experience and is subject to additional terms. The Dropbox Shop beta is available to users on Dropbox Basic, Plus, and Professional plans in the US. If you’re on a team account or you’re based outside of the US, you can sign up for the waitlist by filling out this request form. To share feedback about Dropbox Shop, email support-shop@dropbox.com.
						</li>
						<li><span class="title-color">FAQs about selling on Dropbox Shop</span>
							<br />This article answers frequently asked questions about selling on Dropbox Shop.
							<br />Do buyers need to have a Dropbox account to purchase items on Dropbox Shop?
							<br />To purchase view-only content, a buyer must have a Dropbox account. For all other types of files, anyone can buy your items, regardless of whether they have an account with Dropbox.
						</li>
						<li><span class="title-color">How to create Dropbox Shop listings</span>
							<br />You can sell digital files, like images and videos, directly from Dropbox with Dropbox Shop. You can sell any file type that can be uploaded to your Dropbox account. Learn more about some of the most commonly sold file types.
						</li>
						<li><span class="title-color">How to create a listing on Dropbox Shop</span>
							<br />To create a listing:
							<ol>
								<li>Sign in to dropbox.com.</li>
								<li>Click the grid icon in the top left corner.</li>
								<li>Click Shop from the menu that appears. Dropbox Shop opens in a new tab.
									<ul>
										<li>Note: If it’s your first listing, add a name and a URL for your storefront. Then, you can read and accept the Dropbox Shop seller terms and click Create storefront.</li>
									</ul>
								</li>
								<li>Click Add product in the top left corner. You can also click Products, then Add product.</li>
								<li>To add the files you want to sell to your listing, click Upload from device or Add from Dropbox.
									<ul>
										<li>Note: All the files you add to your listing must be smaller than your available storage space.</li>
									</ul>
								</li>
								<li>Buyers can download the file they purchase by default. To prevent people from downloading your file, check View Only.</li>
							</ol>
						</li>
				   </ol>
				</div>
			</div>
		</div>
	</div>
