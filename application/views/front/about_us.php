	<div class="section-category mt-4 mb-5">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<img class="d-block mx-auto" src="<?=base_url('assets/img/uonlive/UonLIVE_logo.png');?>" />
					<h5 class="text-center">地理與人力資源信息系統平台</h5>
				</div>
				<div class="col-md-6">
					<p>以前的UonLIVE平台就是一個網上電台，從2006年到2012年之間，產生了多於280個DJ，製造了超過300個節目，是首間網上電台做戶外直播節目。</p>
					<p>2022年新的UonLIVE平台，根據時代變遷, 從以前培育一班人做DJ為目的，改為幫助一班人改變在最有利的環境下，了解自己的能力，開創新領域，建立自己獨特的生意基地。</p>
					<p>UonLIVE平台就是拉近用戶的需要來配搭有選擇的服務，產品，貿易和代理。</p>
					<p>增加本土的人際關係，我們希望生意從自己地方為起點， Fingertips 概念就是UonLIVE獨有的特色。</p>
					<p>UonLIVE牽引很多各行各業的合作，創造生意商機。</p>
				</div>
			</div>
		</div>
	</div>
