<nav class="navbar navbar-expand-md navbar-dark main-nav section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color">商戶登記</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>

<div class="section-business-registration mt-4 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-xxl-2 col-xl-1 d-none d-lg-block"></div>
			<div class="col-xxl-8 col-xl-10">
				<div class="container">
					<div class="row">
						<div class="col">
							<p>歡迎聯絡我們，了解更多Uonlive 如何幫您發展事業，支援團隊會盡快回覆您。</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 pt-2 pb-2">
							<input type="text" class="form-control" placeholder="聯絡人姓名*">
						</div>
						<div class="col-lg-6 pt-2 pb-2">
							<input type="text" class="form-control" placeholder="電話號碼*">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 pt-2 pb-2">
							<input type="email" class="form-control" placeholder="電郵地址*">
						</div>
						<div class="col-lg-6 pt-2 pb-2 d-none d-lg-block">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 pt-2 pb-2">
							<input type="text" class="form-control" placeholder="網店／品牌名稱*">
						</div>
						<div class="col-lg-6 pt-2 pb-2">
							<input type="text" class="form-control" placeholder="服務類別*">
						</div>
					</div>
					<div class="row">
						<div class="col pt-2 pb-2">
							<input type="text" class="form-control" placeholder="地址（中文）*">
						</div>
					</div>
					<div class="row">
						<div class="col pt-2 pb-2">
							<input type="text" class="form-control" placeholder="期望合作形式，例如：有關商店上架／登廣告等">
						</div>
					</div>
					<div class="row">
						<div class="col pt-2 pb-2">
							<a class="btn btn-register">登記</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xxl-2 col-xl-1 d-none d-lg-block"></div>
		</div>
	</div>
</div>
