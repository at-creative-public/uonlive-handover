<nav class="navbar navbar-expand-md navbar-dark main-nav section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color">註冊用戶</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>

<div class="section-member-register mb-5">
	<div class="container">
		<?php
		if (! is_null($insert_id)) {
			if ($insert_id == 0) {
		?>
		<div class="alert alert-danger" role="alert">新增用戶失敗</div>
		<?php
			}
			else {
		?>
		<div class="alert alert-success" role="alert">新增用戶成功</div>
		<?php
			}
		}
		?>
		<form id="frmRegister" action="<?=current_url();?>" method="post">
			<input type="hidden" name="resident_formatted_address" value="" />
			<input type="hidden" name="resident_latitude" value="0" />
			<input type="hidden" name="resident_longitude" value="0" />
			<input type="hidden" name="business_formatted_address" value="" />
			<input type="hidden" name="business_latitude" value="0" />
			<input type="hidden" name="business_longitude" value="0" />
			<div class="row">
				<div class="col-xxl-2 col-xl-1 d-none d-lg-block"></div>
				<div class="col-xxl-8 col-xl-10">
					<div class="container">
						<div class="row">
							<div class="col pt-4 pb-2">
								<p class="text-end">*必須填寫</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 pt-2 pb-2">
								<label for="firstname">名字*</label>
								<input type="text" id="firstname" name="first_name" class="form-control" placeholder="請輸入你的名字">
							</div>
							<div class="col-lg-6 pt-2 pb-2">
								<label for="lastname">姓氏*</label>
								<input type="text" id="lastname" name="last_name" class="form-control" placeholder="請輸入你的姓氏">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 pt-2 pb-2">
								<label for="phone">電話號碼*</label>
								<input type="text" id="phone" name="phone" class="form-control" placeholder="請輸入你的電話號碼">
							</div>
							<div class="col-lg-6 pt-2 pb-2">
								<label for="email">電郵地址*</label>
								<input type="email" id="email" name="email" class="form-control" placeholder="請輸入你的電郵地址">
							</div>
						</div>
						<div class="row">
							<!-- <div class="col-lg-3 col-6 pt-2 pb-2">
								<label for="gender">性別*</label>
								<select id="gender" class="form-control">
									<option value="M">男</option>
									<option value="F">女</option>
								</select>
							</div>
							<div class="col-lg-3 col-6 pt-2 pb-2">
								<label for="age">年齡*</label>
								<select id="age" class="form-control">
									<option value="M">兒童</option>
									<option value="F">少年</option>
									<option value="F">青年</option>
									<option value="F">成人</option>
									<option value="F">老年</option>
								</select>
							</div> -->
							<div class="col pt-2 pb-2">
								<label for="occupation">職業</label>
								<input type="text" id="occupation" name="occupation" class="form-control" placeholder="請輸入你的職業">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 pt-2 pb-2">
								<label for="resident_area">家居地區</label>
								<select id="resident_area" name="resident_area" class="form-control">
									<?php
									foreach ($areas as $area)
									{
										foreach ($area['district'] as $district)
										{
									?>
									<option value="<?=$district['id'];?>"><?=$district['name'];?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
							<div class="col-sm-9 pt-2 pb-2">
								<label for="resident_building">大廈名稱</label>
								<input type="text" id="resident_building" name="resident_building" class="form-control" placeholder="請輸入居住大廈名稱">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 pt-2 pb-2">
							</div>
							<div class="col-sm-9 pt-2 pb-2">
								<p id="resident_formatted_address"></p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 pt-2 pb-2">
							</div>
							<div class="col-sm-9 pt-2 pb-2">
								<table class="table" id="resident_list">
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 pt-2 pb-2">
								<label for="business_area">公司地區</label>
								<select id="business_area" name="business_area" class="form-control">
								<?php
									foreach ($areas as $area)
									{
										foreach ($area['district'] as $district)
										{
									?>
									<option value="<?=$district['id'];?>"><?=$district['name'];?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
							<div class="col-sm-9 pt-2 pb-2">
								<label for="business_building">大廈名稱</label>
								<input type="text" id="business_building" name="business_building" class="form-control" placeholder="請輸入工作大廈名稱">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 pt-2 pb-2">
							</div>
							<div class="col-sm-9 pt-2 pb-2">
								<p id="business_formatted_address"></p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3 pt-2 pb-2">
							</div>
							<div class="col-sm-9 pt-2 pb-2">
								<table class="table" id="business_list">
									<tbody>
									</tbody>
								</table>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6 pt-2 pb-2">
								<label for="password">設定密碼*</label>
								<div class="input-group">
									<div class="form-outline w-75">
										<input type="password" id="password" name="password" class="form-control" placeholder="請設定你的密碼" onkeyup="checkPasswordStrength();" />
									</div>
									<button type="button" class="btn btn-password-toggle">
										<i class="fa fa-eye-slash"></i>
									</button>
								</div>
								<div id="password-strength">強度：<span id="password-strength-status"></span></div>
							</div>
							<div class="col-lg-6 pt-2 pb-2">
								<label for="confirm_password">確認密碼*</label>
								<input type="password" id="confirm_password" class="form-control" placeholder="請再輸入你的密碼">
							</div>
						</div>
						<div class="row">
							<div class="col-6 pt-2 pb-2 text-center">
								<button class="btn btn-register">註冊</button>
							</div>
							<div class="col-6 pt-2 pb-2 text-center">
								<button class="btn btn-cancel">取消</button>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xxl-2 col-xl-1 d-none d-lg-block"></div>
			</div>
		</form>
	</div>
</div>
