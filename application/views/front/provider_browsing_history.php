<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-provider-browsing-history">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
									<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
											<li class="breadcrumb-item active" aria-current="page">瀏覽記錄</li>
										</ol>
									</nav>
								</div>
								<h3 class="title-color text-center">瀏覽記錄</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<table class="table table-striped">
									<thead>
									<tr>
										<th>用戶</th>
										<th>瀏覽日期和時間</th>
										<th>瀏覽來源</th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach ($histories as $history) {
									?>
									<tr>
										<td><?=$history['name'];?></td>
										<td><?=$history['date'];?></td>
										<td><?=$history['location'];?></td>
									</tr>
									<?php
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
