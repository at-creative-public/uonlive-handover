<nav class="navbar navbar-expand-md navbar-dark main-nav gray_bg section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
		<?php
		$this->load->view('front/layouts/partials/thumbnail');
		?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color"><i class="fas fa-map-marker-alt"></i> 附近的人</h1></a>
		<div class="w-100 d-flex justify-content-end">
		</div>
	</div>
</nav>
<div class="filter_section green_bg">
    <div class="container">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container-fluid">
				<div class="navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<div class="search mx-auto">
								<i class="fa fa-search"></i>
								<input type="text" class="form-control" placeholder="請輸入地址">
							</div>
						</li>
						<li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="position">位置</label>
								<select class="form-select" id="position">
									<option value="1" selected="selected">現在位置</option>
									<?php
									if ($loggedIn) {
									?>
									<option value="2">家居位置</option>
									<option value="3">公司位置</option>
									<?php
									}
									?>
								</select>
							</div>
						</li>
						<li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="range">搜尋範圍</label>
								<select class="form-select" id="range">
									<option value="1" selected="selected">附近1公里</option>
									<option value="3">附近3公里</option>
									<option value="5">附近5公里</option>
								</select>
							</div>
						</li>
						<li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="category">服務分類</label>
								<select class="form-select" id="category">
									<option value="0" selected="selected">任何分類</option>
								<?php
									if (!empty($tag_lists))
									{
										foreach($tag_lists as $idx => $tag)
										{
								?>
									<option value="<?=$tag['id'];?>"><?=$tag['name'];?></option>
								<?php
										}
									}
								?>
								</select>
							</div>
						</li>
						<!-- <li class="nav-item d-flex align-items-center">
							<div class="">
								<label class="form-label ps-4 pe-2 text-white" for="subcategory">服務次分類</label>
								<select class="form-select" id="subcategory">
									<option value="1">修甲</option>
								</select>
							</div>
						</li> -->
					</ul>
					<div class="btn-group btn-toggle">
						<button class="btn btn-xs btn-off btn-map">地圖</button>
						<button class="btn btn-xs btn-on active btn-list">列表</button>
					</div>
				</div>
			</div>
		</nav>
    </div>
</div>

<div class="service_nearby_details filter_content_section pb-3">
	<div class="container">
		<div class="row">
			<div class="col-md-2">第1至<span class="total-shops">0</span>間，共<span class="total-shops">0</span>間</div>
			<div class="col-md-8"><input type="checkbox" id="message-all" /> 全選</div>
			<!-- <div class="col-md-2 text-right"><button class="btn btn-broadcast">廣發訊息</button></div> -->
		</div>
		<div class="shop-list">
		<!-- <?php
		foreach ($nearby_shops as $idx => $shop)
		{
			?>
			<div class="row">
				<div class="col-md-2"><img src="https://via.placeholder.com/60x60" class="img-fluid d-block mx-auto" alt=""></div>
				<div class="col-md-8">
					<table class="table">
						<tbody>
						<tr>
							<td style="width: 20px;"><input type="checkbox" /></td>
							<td style="width: 40px;"><span><?=$idx + 1;?>.</span></td>
							<td><a href="<?=base_url('service/details/' . $shop['uri']);?>"><span><?=$shop['title'];?></span></a></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td><span><?=str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate2.png') . '" />', $shop['rate']) . str_repeat('<img src="' . base_url('assets/img/uonlive/icon_rate1.png') . '" />', 5 - $shop['rate']);?></span></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td><p><?=$shop['location'];?> | <?=$shop['phone'];?></p></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td><p><?=$shop['business_time'];?></p></td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-2 text-right mt-auto"><button class="btn btn-message text-nowrap">發訊息</button></div>
			</div>
			<?php
		}
		?> -->
		</div>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col">
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div> -->
</div>
<?php
if ($loggedIn) {
?>
<script>
	let resident_lat = <?=$this->session->userdata('uonlive_user')['resident_latitude'] / 10000000;?>;
	let resident_lng = <?=$this->session->userdata('uonlive_user')['resident_longitude'] / 10000000;?>;
	let business_lat = <?=$this->session->userdata('uonlive_user')['business_latitude'] / 10000000;?>;
	let business_lng = <?=$this->session->userdata('uonlive_user')['business_longitude'] / 10000000;?>;
</script>
<?php
}
?>
