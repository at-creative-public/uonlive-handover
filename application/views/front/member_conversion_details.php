<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-member-conversion-history">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/member_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
									<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
											<li class="breadcrumb-item" aria-current="page"><a href="<?=base_url('member/conversion_history');?>">聯絡記錄</a></li>
											<li class="breadcrumb-item active" aria-current="page"><?=$conversion['title'];?></li>
										</ol>
									</nav>
								</div>
								<h3 class="title-color text-center">聯絡記錄</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-2 pt-2 pb-2">
								聯絡人：
							</div>
							<div class="col-12 col-sm-10 pt-2 pb-2">
								<?=$conversion['first_name'] . ', ' . $conversion['last_name'];?>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-2 pt-2 pb-2">
								日期：
							</div>
							<div class="col-12 col-sm-10 pt-2 pb-2">
								<?=date("Y-m-d H:i:s", $conversion['created_at']);?>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-2 pt-2 pb-2">
								標題：
							</div>
							<div class="col-12 col-sm-10 pt-2 pb-2">
								<?=$conversion['title'];?>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-2 pt-2 pb-2">
								內容：
							</div>
							<div class="col-12 col-sm-10 pt-2 pb-2">
								<?=$conversion['message'];?>
							</div>
						</div>
						<hr />
						<h5 class="fw-bold">回覆</h5>
						<table class="table table-striped">
							<tbody>
							<?php
							foreach ($conversion['replies'] as $reply) {
							?>
							<tr>
								<td>
									<div>日期：<?=date("Y-m-d H:i:s", $reply['created_at']);?></div>
									<?php
									if ($reply['message_type'] == 'ask') {
										$person = $reply['first_name'] . ', ' . $reply['last_name'];
									}
									else {
										$person = $reply['shop_name'];
									}
									?>
									<div>回覆人：<?=$person;?></div>
									<div>內容：<?=$reply['message'];?></div>
								</td>
							</tr>
							<?php
							}
							?>
							</tbody>
						</table>
						<form id="">
							<input type="hidden" name="member" value="<?=$conversion['member_id'];?>" />
							<input type="hidden" name="provider" value="<?=$conversion['provider_id'];?>" />
							<input type="hidden" name="parent_id" value="<?=$conversion['id'];?>" />
							<div class="form-group">
								<label for="message-content">內容</label>
								<textarea class="form-control" id="message-content"></textarea>
							</div>
						</form>
						<button type="button" class="mt-2 btn-send-message btn btn-success">發送</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
