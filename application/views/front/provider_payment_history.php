<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-provider-payment-history">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
									<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
											<li class="breadcrumb-item active" aria-current="page">付款記錄</li>
										</ol>
									</nav>
								</div>
								<h3 class="title-color text-center">付款記錄</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<div class="col pt-2 pb-2">
								<table class="table table-striped">
									<thead>
									<tr>
										<th>付款日期和時間</th>
										<th>付款項目</th>
										<th>款項</th>
										<th>狀態</th>
										<th>付款方式</th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach ($histories as $history) {
									?>
									<tr>
										<td><?=$history['date'];?></td>
										<td><?=$history['item'];?></td>
										<td>HKD <?=number_format($history['amount']);?></td>
										<td><?=$history['status'];?></td>
										<td><?=($history['payment_status'] == 'COMPLETE') ? $history['method'] : '--';?></td>
									</tr>
									<?php
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
