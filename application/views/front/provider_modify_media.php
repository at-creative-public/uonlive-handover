<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-provider-modify-media">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
								<?php
								$this->load->view('front/layouts/partials/thumbnail');
								?>
								</div>
								<h3 class="title-color text-center">修改媒體</h3>
								<hr />
							</div>
						</div>
						<div class="row">
							<label class="col-md-2 control-label"></label>
							<div class="col-md-10 inputGroupContainer">
								<div id="progress-container" class="progress">
									<div id="progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%
									</div>
								</div>
								<div id="results"></div>
							</div>
						</div>
						<form id="media-form" action="<?=current_url();?>" method="POST">
							<input type="hidden" name="token" value="<?=$media['token'];?>" />
							<input type="hidden" name="media_type" value="<?=$media['media_type'];?>" />
							<input type="hidden" name="category" value="<?=$provider['tag_list_id'];?>" />
							<input type="hidden" name="tag_2nd_list" value="<?=$provider['tag_2nd_list_id'];?>" />
							<input type="hidden" name="media_file" />
							<input type="hidden" name="news_summary" value="" />
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="media_type">媒體類型</label>
									<select id="media_type" disabled="disabled" class="form-control">
										<option value="photo"<?=($media['media_type'] == 'PHOTO') ? ' selected="selected"' : '';?>>圖片</option>
										<option value="news"<?=($media['media_type'] == 'NEWS') ? ' selected="selected"' : '';?>>新聞</option>
										<option value="video"<?=($media['media_type'] == 'VIDEO') ? ' selected="selected"' : '';?>>影片</option>
									</select>
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="show_in_shop">商店主頁上顯示</label>
									<input type="checkbox" id="show_in_shop" name="show_in_shop" class="form-check-input d-block mt-2 ms-3" value="1"<?=($media['visible'] == 1) ? ' checked="checked"' : '';?>>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="category">分類</label>
									<select id="category" class="form-control" disabled="disabled">
										<?php
										foreach ($tag_lists as $tag) {
										?>
										<option value="<?=$tag['id'];?>"<?=($media['category_id'] == $tag['id']) ? ' selected="selected"' : '';?>><?=$tag['name'];?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="tags">關鍵字（以逗號分隔）</label>
									<input id="tags" name="tags" class="form-control" value="">
								</div>
							</div>
							<?php
							if ($media['media_type'] == 'PHOTO') {
							?>
							<div class="photo-block">
								<div class="row">
									<div class="col-lg-3">
										<label for="_img">圖像 (1300x866)</label>
										<div class="input-group">
											<div id="photo_drop_zone">請把圖片拖放到這裡</div>
											<br>
											<label class="btn btn-block btn-info" style="width: 140px;">
												瀏覽… <input id="photo_browse" type="file" accept="image/*" style="display: none;">
											</label>
										</div>
									</div>
									<div class="col-lg-3">
										<img id="photo-photo" src="<?=base_url('assets/img/uploads/medias/' . $media['media']['filename']);?>" class="img-fluid" />
									</div>
									<div class="col-lg-6">
									</div>
								</div>
							</div>
							<?php
							}
							if ($media['media_type'] == 'VIDEO') {
							?>
							<div class="video-block">
								<div class="row pt-3">
									<div class="col">
										<label for="video-title">影片標題*</label>
										<input type="text" id="video-title" name="video_title" class="form-control" value="<?=$media['media']['title'];?>" placeholder="請輸入影片標題">
									</div>
								</div>
								<div class="row pt-3">
									<div class="col">
										<label for="video-link">連結</label>
										<input type="text" id="video-link" name="video_link" class="form-control" value="<?=$media['media']['url'];?>" placeholder="請輸入連結">
									</div>
								</div>
								<div class="row pt-3">
									<div class="col-lg-3 col-6 pt-2 pb-2">
										<label for="_img">圖像 (1300x866)</label>
										<div class="input-group">
											<div id="video_drop_zone">請把圖片拖放到這裡</div>
											<br>
											<label class="btn btn-block btn-info" style="width: 140px;">
												瀏覽… <input id="video_browse" type="file" accept="image/*" style="display: none;">
											</label>
										</div>
									</div>
									<div class="col-lg-6">
										<label>預覽</label><br />
										<img id="video-photo" src="<?=base_url('assets/img/uploads/medias/' . $media['media']['filename']);?>" style="max-height: 100px;object-fit: cover;" class="img-fluid" />
									</div>
									<div class="col-lg-3">
									</div>
								</div>
							</div>
							<?php
							}
							if ($media['media_type'] == 'NEWS') {
							?>
							<div class="news-block">
								<div class="row pt-3">
									<div class="col">
										<label for="news-title">新聞標題*</label>
										<input type="text" id="news-title" name="news_title" value="<?=$media['media']['title'];?>" class="form-control" placeholder="請輸入新聞標題">
									</div>
								</div>
								<div class="row pt-3">
									<div class="col pt-2 pb-2">
										<label for="news-summary">內容簡介</label>
										<textarea id="news-summary" class="form-control w-100" rows="10"><?=$media['media']['content'];?></textarea>
									</div>
								</div>
								<div class="row pt-3">
									<div class="col">
										<label for="news-link">連結</label>
										<input type="text" id="news-link" name="news_link" class="form-control" value="<?=$media['media']['url'];?>" placeholder="請輸入連結">
									</div>
								</div>
							</div>
							<?php
							}
							?>
						</form>
						<div class="row pb-4 pt-4">
							<div class="col pt-2 pb-2 w-100">
								<a class="btn btn-save">修改</a> <a class="btn btn-cancel">返回</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
