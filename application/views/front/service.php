<nav class="navbar navbar-expand-md navbar-dark main-nav gray_bg section_breadcrumb">
	<div class="container">
		<div class="breadcrumb_service_detail w-100 mt-0">
			<?php
			$this->load->view('front/layouts/partials/thumbnail');
			?>
		</div>
		<a class="navbar-brand order-first order-md-0 mx-0" href="#"><h1 class="title-color"><?=$title;?></h1></a>
		<div class="w-100 d-flex justify-content-end">
			<a href="<?=base_url('service/nearby_map');?>" class="btn btn-nearby" style="background-color: #999; color: #fff;"><i class="fas fa-map-marker-alt me-2"></i>附近的人</a>
		</div>
	</div>
</nav>
<div class="filter_section green_bg">
    <div class="container">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container-fluid">
				<h6 class="text-white">服務提供者 — 篩選：</h6>
				<div class="navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<a class="nav-link text-white text-nowrap" id="filter_service_region">地區</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white text-nowrap" id="filter_service_subcategory">子分類</a>
						</li>
						<!-- <li class="nav-item">
							<a class="nav-link text-white text-nowrap" id="filter_service_age">年齡</a>
						</li> -->
						<!-- <li class="nav-item">
							<a class="nav-link text-white text-nowrap" id="filter_service_gender">性別</a>
						</li> -->
					</ul>
					<!-- <div class="service_target hover">
						服務對象配對　<i class="fa fa-angle-down"></i>
					</div> -->
					<!-- <form class="search d-flex">
						<i class="fa fa-search"></i>
						<input type="text" class="form-control" placeholder="請輸入關鍵字">
					</form> -->
				</div>
			</div>
		</nav>
    </div>
	<div class="service_target_bar">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container-fluid">
					<h6 class="text-white">服務對象 — 篩選：</h6>
					<div class="navbar-collapse" id="navbarSupportedContent1">
						<ul class="navbar-nav me-auto mb-2 mb-lg-0">
							<li class="nav-item">
								<a class="nav-link text-white text-nowrap" id="filter_target_region">地區</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-white text-nowrap" id="filter_target_time">時間</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-white text-nowrap" id="filter_target_age">年齡</a>
							</li>
							<!-- <li class="nav-item">
								<a class="nav-link text-white text-nowrap" id="filter_target_gender">性別</a>
							</li> -->
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
	<div class="row service_region_popup">
		<div class="col">
			<div class="container">
				<div class="row seven-cols">
					<?php
					$id = 0;
					foreach ($regions as $region)
					{
					?>
					<div class="col-md-1 text-center">
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th><?=$region['name'];?></th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach ($region['district'] as $district)
							{
							?>
							<tr>
								<td><input type="checkbox" name="_region" value="<?=$district['id'];?>"<?=in_array($district['id'], $_regions) ? ' checked="checked"' : '';?> /></td>
								<td><?=$district['name'];?></td>
							</tr>
							<?php
								$id++;
							}
							?>
							</tbody>
						</table>
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="row service_subcategory_popup">
		<div class="col">
			<div class="container">
				<div class="row seven-cols">
					<?php
					foreach ($tag_2nd as $idx => $tag2)
					{
					?>
					<div class="col-md-1 text-center">
					<?php
						if ($tag2['name'] != '') {
					?>
						<table class="table">
							<tbody>
								<tr>
									<td><input type="checkbox" name="_tag" value="<?=$tag2['id'];?>"<?=in_array($tag2['id'], $_tags) ? ' checked="checked"' : '';?> /></td>
									<td><?=$tag2['name'];?></td>
								</tr>
							</tbody>
						</table>
					<?php
						}
					?>
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="row service_age_popup">
		<div class="col">
			<div class="container">
				<div class="row seven-cols">
					<?php
					foreach ($ages as $idx => $age)
					{
						?>
						<div class="col-md-1 text-center">
							<?php
							if ($age != '') {
								?>
								<table class="table">
									<tbody>
									<tr>
										<td><input type="checkbox" value="<?=$idx;?>" /></td>
										<td><?=$age;?></td>
									</tr>
									</tbody>
								</table>
								<?php
							}
							?>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="row service_gender_popup">
		<div class="col">
			<div class="container">
				<div class="row seven-cols">
					<?php
					foreach ($genders as $idx => $gender)
					{
						?>
						<div class="col-md-1 text-center">
							<?php
							if ($gender != '') {
								?>
								<table class="table">
									<tbody>
									<tr>
										<td><input type="checkbox" value="<?=$idx;?>" /></td>
										<td><?=$gender;?></td>
									</tr>
									</tbody>
								</table>
								<?php
							}
							?>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div> -->
</div>

<div class="filter_content_section">
    <div class="container">
		<div class="row">
			<div class="col-12">
				<div class="content_section_wrapper">
					<?php if(!empty($categories)){ ?>
						<?php foreach($categories as $filename => $value){
							$random_color = "rgb(".rand(120,225).",".rand(120,225).",".rand(120,225).")";
							?>
							<div class="tag" style="background-color:<?=$random_color?>">
								<?=$value['title'];?>
							</div>
						<?php }?>
					<?php }?>
				</div>
			</div>
		</div>
        <div class="row">
			<div class="col-lg-7 left-side">
				<div class="title font_gray_color mb-0"><h3>最新項目</h3></div>
				<div class="hot_items mb-3">
					<?php
					if (count($hot_items) == 0) {
						echo "找不到包含所有搜索詞的結果";
					}
					else {
						$new = array_map(function($val) { return '<a href="' . base_url('hashtag/items/' . $val['uri']) . '">' . $val['name'] . '</a>'; } , $hot_items);
						echo implode('、', $new);
					}
					?>
				</div>
				<div class="title font_gray_color mt-4"><h3>最新服務提供者</h3></div>
				<div class="hot_places mb-3">
					<?php
					if (count($hot_service_providers) == 0) {
						echo "找不到包含所有搜索詞的結果";
					}
					else {
						$new = array_map(function($val) { return '<a href="' . base_url('service/details/' . $val['shop_uri']) . '">' . $val['shop_name'] . '</a>'; } , $hot_service_providers);
						echo implode('、', $new);
					}
					?>
				</div>
				<div class="title font_gray_color mt-4"><h3>最新品牌</h3></div>
				<div class="hot_brands mb-3">
					<?php
					if (count($hot_brands) == 0) {
						echo "找不到包含所有搜索詞的結果";
					}
					else {
						$new = array_map(function($val) { return '<a href="' . base_url('service/details/' . $val['shop_uri']) . '">' . $val['brand'] . '</a>'; } , $hot_brands);
						echo implode('、', $new);
					}
					?>
				</div>
				<!--start service section-->
				<span class="need-share-button_dropdown need-share-button_dropdown-box-horizontal need-share-button_dropdown-middle-right" style="margin-top: -20px;"><span class="need-share-button_link-box need-share-button_link need-share-button_mailto" data-network="mailto"></span><span class="need-share-button_link-box need-share-button_link need-share-button_twitter" data-network="twitter"></span><span class="need-share-button_link-box need-share-button_link need-share-button_pinterest" data-network="pinterest"></span><span class="need-share-button_link-box need-share-button_link need-share-button_facebook" data-network="facebook"></span><span class="need-share-button_link-box need-share-button_link need-share-button_googleplus" data-network="googleplus"></span><span class="need-share-button_link-box need-share-button_link need-share-button_linkedin" data-network="linkedin"></span></span>
				<div class="pb-4 small_font_size latest-services">
					<div class="container posrel">
						<div class="section_content">
							<div class="title_wrapper">
								<div class="posrel text-center tab-box">
									<div class="">
										<div class="row">
											<div class="col-md-4 d-flex align-items-center">
												<h2 class="text-start title-color">最新<span>服務</span></h2>
											</div>
											<div class="col-md-8">
												<ul class="nav pe-3">
													<li class="nav-item">
														<a class="nav-link" id="service-provider">服務提供者</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="service-brand">品牌</a>
													</li>
													<!-- <li class="nav-item">
														<a class="nav-link" href="#">分銷商</a>
													</li> -->
													<li class="nav-item">
														<a class="nav-link active" id="service-all" aria-current="page">全部</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<?php
									if (count($latest_services) > 4) {
									?>
									<div class="show_more_btn">
										<a href="#">查看更多</a>
									</div>
									<?php
									}
									?>
								</div>
							</div>
							<div class="row">
								<?php
								if (count($latest_services) > 0) {
									foreach ($latest_services as $service) {
										switch ($service['type']) {
											case "B":
												$service_class = 'service-brand';
												break;
											case "P":
												$service_class = 'service-provider';
												break;
										}
										$like_icon = 'far';
										if ($service['is_liked']) {
											$like_icon = 'fa';
										}
								?>
								<div class="col-xxl-4 col-xl-6 col-12 pt-3 pb-3 d-flex align-items-stretch <?=$service_class;?>">
									<div class="card service-box">
										<div class="card-header">
											<img src="<?=base_url('assets/img/uploads/services/' . $service['photo']);?>" class="same-height img-fluid" />
											<div class="manager-pic rounded-circle ratio ratio-1x1 overflow-hidden">
												<img class="" src="<?=base_url('assets/img/uploads/providers/' . $service['avatar_photo']);?>" class="img-fluid" />
											</div>
										</div>
										<div class="card-body d-flex flex-column">
											<p><?=$service['location'];?></p>
											<h5 class="title-color"><a href="<?=base_url('service/details/' . $service['uri']);?>"><?=$service['title'];?></a></h5>
											<div class="tag-list">
												<?php
												foreach ($service['tags'] as $idx => $_tag) {
													?>
													<span style="background-color: <?=$tag_lists[$idx + 1]['color'];?>;"><?=$tag_lists[$idx + 1]['name'];?></span>
													<?php
												}
												?>
											</div>
										</div>
										<div class="card-footer">
											<div class="row">
												<div class="col-8">
													<?php
													echo str_repeat('<img class="pe-1" src="' . base_url('assets/img/uonlive/icon_rate2.png') .'" />', $service['score']);
													echo str_repeat('<img class="pe-1" src="' . base_url('assets/img/uonlive/icon_rate1.png') .'" />', 5 - $service['score']);
													?>
												</div>
												<div class="col-4 d-flex justify-content-end">
													<div id="share-button_latest_<?=$service['id'];?>" class="need-share-button-default" data-share-position="topCenter" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fa fa-share-alt" aria-hidden="true"></i></span></div>
													<!-- <img src="<?=base_url('assets/img/uonlive/icon_share.png');?>" /> -->
													<a data-member="<?=$member_id;?>" data-provider="<?=$service['providers_id'];?>" class="pt-1 ps-2 like-page provider_id_<?=$service['providers_id'];?>"><i class="<?=$like_icon;?> fa-heart" aria-hidden="true"></i></a>
													<!-- <img class="ps-2" src="<?=base_url('assets/img/uonlive/icon_like1.png');?>" /> -->
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
									}
								}
								else {
									echo "找不到包含所有搜索詞的結果";
								}
								?>
							</div>
						</div>
					</div>
				</div>

				<div class="case_section mb-5 hot-services">
					<div class="container posrel">
						<div class="title_wrapper">
							<div class="posrel text-center tab-box">
								<div class="">
									<div class="row">
										<div class="col-md-4 d-flex align-items-center">
											<h2 class="text-start title-color">熱門<span>服務</span></h2>
										</div>
										<div class="col-md-8">
											<ul class="nav pe-3">
												<li class="nav-item">
													<a class="nav-link" id="hottest-provider">服務提供者</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" id="hottest-band">品牌</a>
												</li>
												<!-- <li class="nav-item">
													<a class="nav-link" href="#">分銷商</a>
												</li> -->
												<li class="nav-item">
													<a class="nav-link active" id="hottest-all" aria-current="page">全部</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<?php
								if (count($latest_services) > 4) {
								?>
								<div class="show_more_btn">
									<a href="#">查看更多</a>
								</div>
								<?php
								}
								?>
							</div>
						</div>

						<div class="row">
							<?php
							if (count($hot_services) > 0) {
								foreach ($hot_services as $service) {
									switch ($service['type']) {
										case "B":
											$service_class = 'hottest-brand';
											break;
										case "P":
											$service_class = 'hottest-provider';
											break;
									}
									$like_icon = 'far';
									if ($service['is_liked']) {
										$like_icon = 'fa';
									}
							?>
							<div class="col-xxl-4 col-xl-6 col-12 pt-3 pb-3 d-flex align-items-stretch <?=$service_class;?>">
								<div class="card service-box">
									<div class="card-header">
										<img src="<?=base_url('assets/img/uploads/services/' . $service['photo']);?>" class="same-height img-fluid" />
										<div class="manager-pic rounded-circle ratio ratio-1x1 overflow-hidden">
											<img class="" src="<?=base_url('assets/img/uploads/providers/' . $service['avatar_photo']);?>" class="img-fluid" />
										</div>
									</div>
									<div class="card-body d-flex flex-column">
										<p><?=$service['location'];?></p>
										<h5 class="title-color"><a href="<?=base_url('service/details/' . $service['uri']);?>"><?=$service['title'];?></a></h5>
										<div class="tag-list">
											<?php
											foreach ($service['tags'] as $idx => $_tag) {
												?>
												<span style="background-color: <?=$tag_lists[$idx + 1]['color'];?>;"><?=$tag_lists[$idx + 1]['name'];?></span>
												<?php
											}
											?>
										</div>
									</div>
									<div class="card-footer">
										<div class="row">
											<div class="col-8">
												<?php
												echo str_repeat('<img class="pe-1" src="' . base_url('assets/img/uonlive/icon_rate2.png') .'" />', $service['score']);
												echo str_repeat('<img class="pe-1" src="' . base_url('assets/img/uonlive/icon_rate1.png') .'" />', 5 - $service['score']);
												?>
											</div>
											<div class="col-4 d-flex justify-content-end">
												<div id="share-button_hottest_<?=$service['id'];?>" class="need-share-button-default" data-share-position="topCenter" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fa fa-share-alt" aria-hidden="true"></i></span></div>
												<!-- <img src="<?=base_url('assets/img/uonlive/icon_share.png');?>" /> -->
												<a data-member="<?=$member_id;?>" data-provider="<?=$service['providers_id'];?>" class="pt-1 ps-2 like-page provider_id_<?=$service['providers_id'];?>"><i class="<?=$like_icon;?> fa-heart" aria-hidden="true"></i></a>
												<!-- <img class="ps-2" src="<?=base_url('assets/img/uonlive/icon_like1.png');?>" /> -->
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
								}
							}
							else {
								echo "找不到包含所有搜索詞的結果";
							}
							?>
						</div>
					</div>
				</div>
				<!--end service section-->

				<!--start pagination section-->
				<!--
				<div class="pagination_wrapper">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<?php for($i=1;$i<=6;$i++){?>
								<li class="page-item"><a class="page-link" href="#"><?=$i?></a></li>
							<?php } ?>
							<li class="page-item">
								<a class="page-link" href="#" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
				-->
				<!--end pagination section-->
			</div>
            <div class="col-lg-5 right-side">
                <div class="content_section_wrapper pt-0 pb-0">
                    <div class="content_tabs_wrapper pt-0">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">全部</button>
                                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">圖片</button>
                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">影片</button>
                                <button class="nav-link" id="nav-news-tab" data-bs-toggle="tab" data-bs-target="#nav-news" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">新聞</button>
                            </div>
                        </nav>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="items_wrapper">
                                    <!--start news section-->
                                    <div class="news_wrapper">
										<div class="title font_gray_color mb-3"><h3>「<?=$title;?>」的新聞搜尋結果</h3></div>
                                        <?php 
										if (!empty($all_news)) {
                                            foreach($all_news as $news) {
										?>
                                            <div class="news_item">
                                                <div class="row">
                                                    <div class="col-12 col-lg-9">
                                                        <div class="news_title_wrapper">
                                                            <div class="news_title">
                                                                <div>
                                                                    <h6><?=$news['title']?></h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="news_content">
                                                            <div>
                                                                <?=$news['content']?>
                                                            </div>
                                                        </div>
														<div class="tags_content">
															<?php
															foreach ($news['hashtags'] as $tag)
															{
															?>
															<div class="tag"><?=$tag['name'];?></div>
															<?php
															}
															?>
														</div>
                                                    </div>
                                                    <div class="col-12 col-lg-3">
														<img src="<?=base_url('assets/img/uploads/providers/' . $news['avatar']);?>" class="img-fluid d-block mx-auto" alt="">
														<div class="news_author_name"><a href="<?=base_url('service/details/' . $news['shop_uri']);?>"><?=$news['author']?></a></div>
                                                    </div>
                                                </div>
                                            </div>
											<?php
												if (count($all_images) > 3) {
											?>
											<div class="show_more mt-4 mb-4">
                                            	<div class="small_text light-gray">查看全部新聞</div>
                                        	</div>
											<?php
												}
											}
                                        }
										else {
											echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
										}
										?>
                                    </div>
                                    <!--end news section-->

                                    <!--start images section-->
                                    <div class="images_wrapper">
										<div class="title font_gray_color mb-3"><h3>「<?=$title;?>」的圖片搜尋結果</h3></div>
										<?php 
											if (!empty($all_images)) {
										?>
										<div class="row mb-3">
										<?php
												foreach($all_images as $image) { 
										?>
											<div class="col-12 col-md-6 col-lg-4">
												<a href="<?=base_url('assets/img/uploads/medias/' . $image);?>" data-toggle="lightbox"><img src="<?=base_url('assets/img/uploads/medias/' . $image);?>" class="img-group-same-height img-fluid" alt="" /></a>
											</div>
										<?php
												}
										?>
										</div>
										<?php
												if (count($all_images) > 3) {
										?>
										<div class="show_more mt-4 mb-4">
											<div class="small_text light-gray">查看全部圖片</div>
										</div>
										<?php 
												}
											}
											else {
												echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
											}
										?>
                                    </div>
                                    <!--end images section-->

                                    <!--start video section-->
                                    <div class="detail_video_wrapper">
                                        <div class="title font_gray_color mb-3"><h3>「<?=$title;?>」的影片搜尋結果</h3></div>
										<?php 
										if (!empty($all_videos)) { 
										?>
										<div class="card-deck text-center row">
										<?php 
											foreach($all_videos as $video) { 
										?>
											<div class="card mb-3 box-shadow col-lg-6">
												<div class="card-header">
												<a href="<?=$video['url'];?>" data-toggle="lightbox"><img src="<?=base_url('assets/img/uploads/medias/' . $video['image']);?>" class="w-100" alt="" /></a>
												</div>
												<div class="card-body d-flex flex-column">
													<h6 class="video_title pt-2"><?=$video['title']?></h6>
													<div class="video_footer mt-auto">
														<p class="video_author"><a href="<?=base_url('service/details/' . $video['shop_uri']);?>"><?=$video['author']?></a></p>
														<div class="tags">
															<?php
															foreach ($video['hashtags'] as $tag)
															{
																?>
															<div class="tag"><?=$tag['name'];?></div>
																<?php
															}
															?>
														</div>
													</div>
												</div>
											</div>
										<?php 
											}
										?>
										</div>
										<?php
											if (count($all_videos) > 3) {
										?>
                                        <div class="show_more mt-4 mb-4">
                                            <div class="small_text light-gray">查看全部影片</div>
                                        </div>
										<?php 
											}
										}
										else {
											echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
										}
										?>
                                    </div>
                                    <!--end video section-->
								</div>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="images_wrapper mt-4">
                                    <?php 
										if (! empty($all_images)) { 
									?>
									<div class="row">
										<?php 
											foreach($all_images as $image) { 
										?>
										<div class="col-12 col-lg-4 mb-3">
											<a href="<?=base_url('assets/img/uploads/medias/' . $image);?>" data-toggle="lightbox"><img src="<?=base_url('assets/img/uploads/medias/' . $image);?>" class="img-group-same-height w-100" alt=""></a>
										</div>
										<?php 
											}
										?>
									</div>
                                    <?php 
										} 
										else {
											echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
										}
									?> 
                                </div>                     
                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <div class="detail_video_wrapper mt-4">
									<?php 
										if (!empty($all_videos)) { 
									?>
									<div class="card-deck text-center row">
									<?php 
											foreach($all_videos as $video) { 
									?>
										<div class="card mb-3 box-shadow col-lg-6">
											<div class="card-header">
												<a href="<?=$video['url'];?>" data-toggle="lightbox"><img src="<?=base_url('assets/img/uploads/medias/' . $video['image']);?>" class="w-100" alt="" /></a>
											</div>
											<div class="card-body d-flex flex-column">
												<h6 class="video_title pt-2"><?=$video['title']?></h6>
												<div class="video_footer mt-auto">
													<p class="video_author"><a href="<?=base_url('service/details/' . $video['shop_uri']);?>"><?=$video['author']?></a></p>
													<div class="tags">
														<?php
														foreach ($video['hashtags'] as $tag)
														{
														?>
														<div class="tag"><?=$tag['name'];?></div>
														<?php
														}
														?>
													</div>
												</div>
											</div>
										</div>
									<?php
											}
									?>
									</div>
									<?php 
										}
										else {
											echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
										}
									?>
                                </div> 
							</div>
                            <div class="tab-pane fade" id="nav-news" role="tabpanel" aria-labelledby="nav-news-tab">
								<div class="news_wrapper">
								<?php 
									if (!empty($all_news)) { 
										foreach($all_news as $news) {
								?>
									<div class="news_item">
										<div class="row">
											<div class="col-12 col-lg-9">
												<div class="news_title_wrapper">
													<div class="news_title">
														<div>
															<h6><?=$news['title']?></h6>
														</div>
													</div>
												</div>
												<div class="news_content">
													<div>
														<?=$news['content']?>
													</div>
												</div>
												<div class="tags_content">
													<?php
													foreach ($news['hashtags'] as $tag)
													{
														?>
														<div class="tag"><?=$tag['name'];?></div>
														<?php
													}
													?>
												</div>
											</div>
											<div class="col-12 col-lg-3">
												<img src="<?=base_url('assets/img/uploads/providers/' . $news['avatar']);?>" class="img-fluid d-block mx-auto" alt="">
												<div class="news_author_name"><a href="<?=base_url('service/details/' . $news['shop_uri']);?>"><?=$news['author']?></a></div>
											</div>
										</div>
									</div>
								<?php 
										}
									} 
									else {
										echo '<div class="mb-3">找不到包含所有搜索詞的結果</div>';
									}
								?>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
