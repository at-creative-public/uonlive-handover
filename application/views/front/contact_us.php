	<div class="section-category mt-5 mb-5">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p>UonLIVE平台就是拉近用戶的需要來配搭有選擇的服務，產品，貿易和代理。</p>
					<p>增加本土的人際關係，我們希望生意從自己地方為起點， Fingertips 概念就是UonLIVE獨有的特色。</p>
					<p>UonLIVE牽引很多各行各業的合作，創造生意商機。</p>
					<div class="mb-3">
						<img src="<?=base_url('assets/img/uonlive/footer_add.png');?>" /> 香港干諾道西88號粵財大廈11樓
					</div>
					<div class="mb-3">
						<img src="<?=base_url('assets/img/uonlive/footer_mail.png');?>" /> <?=$this->config->item('site_settings')['company_email'];?>
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col-2"><a href="https://www.facebook.com/<?=$this->config->item('uonlive')['social_network']['facebook'];?>/"><img src="<?=base_url('assets/img/uonlive/icon_fb.png');?>" /></a></div>
							<div class="col-2"><a href="mailto:<?=$this->config->item('site_settings')['company_email'];?>"><img src="<?=base_url('assets/img/uonlive/icon_mail.png');?>" /></a></div>
							<div class="col-2"></div>
							<div class="col-2"></div>
							<div class="col-4"></div>
						</div>
					</div>
                </div>
				<div class="col-md-6">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.7296931151836!2d114.14355534937539!3d22.288226948895954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3404007fa36a1dfd%3A0x66992922f4ab7f5a!2sGuangdong%20Finance%20Building%2C%2088%20Connaught%20Rd%20W%2C%20Sheung%20Wan!5e0!3m2!1sen!2shk!4v1672906447503!5m2!1sen!2shk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
				</div>
			</div>
		</div>
	</div>
