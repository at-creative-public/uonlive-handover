<?php
$this->load->view('front/layouts/partials/member_navbar');
?>
<div class="section-provider">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php
				$this->load->view('front/layouts/partials/provider_sidebar');
				?>
			</div>
			<div class="col-md-9">
				<div class="box-with-shadow">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="breadcrumb_service_detail w-100 mt-0">
								<?php
								$this->load->view('front/layouts/partials/thumbnail');
								?>
								</div>
								<h3 class="title-color text-center">網店簡介</h3>
								<hr />
							</div>
						</div>
						<?php
						if ($update) {
						?>
						<div class="alert alert-success" role="alert">服務提供者／品牌更新成功</div>
						<?php
						}
						?>
						<form id="frmUpdate" action="<?=current_url();?>" method="post">
							<input type="hidden" name="login_name" value="<?=$this->session->userdata('uonlive_user')['email'];?>" />
							<input type="hidden" name="head_office_formatted_address" value="<?=$provider['head_office_formatted_address'];?>" />
							<input type="hidden" name="latitude" value="<?=$provider['latitude'] / 10000000;?>" />
							<input type="hidden" name="longitude" value="<?=$provider['longitude'] / 10000000;?>" />
							<input type="hidden" name="avatar_file" />
							<input type="hidden" name="shop_file" />
							<div class="row">
								<div class="col-6 pt-2 pb-2">
									<label for="brand_provider">服務提供者／品牌</label>
									<select id="brand_provider" name="brand_provider" class="form-control">
										<option value="P"<?=($provider['brand_provider'] == 'P') ? ' selected="selected"' : '';?>>服務提供者</option>
										<option value="B"<?=($provider['brand_provider'] == 'B') ? ' selected="selected"' : '';?>>品牌</option>
									</select>
								</div>
								<div class="col-3 pt-2 pb-2">
									<label for="tag_list_id">服務分類</label>
									<select id="tag_list_id" name="tag_list_id" class="form-control">
										<?php
										foreach ($categories as $category) {
										?>
										<option data-code="<?=$category['code'];?>" value="<?=$category['id'];?>"<?=($provider['tag_list_id'] == $category['id']) ? ' selected="selected"' : '';?>><?=$category['name'];?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-3 pt-2 pb-2">
									<label for="tag_2nd_list_id">服務子分類</label>
									<select id="tag_2nd_list_id" name="tag_2nd_list_id" class="form-control">
										<?php
										foreach ($subcategories as $subcategory) {
										?>
										<option data-code="<?=$subcategory['code'];?>" value="<?=$subcategory['id'];?>"<?=($provider['tag_2nd_list_id'] == $subcategory['id']) ? ' selected="selected"' : '';?>><?=$subcategory['name'];?></option>
										<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="shopname">網店名稱</label>
									<input type="text" id="shopname" name="shop_name" value="<?=$provider['shop_name'];?>" class="form-control" placeholder="請輸入網店名稱">
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="owner">店長名字</label>
									<input type="text" id="owner" name="owner_name" value="<?=$provider['owner_name'];?>" class="form-control" placeholder="請輸入店長名字">
								</div>
							</div>
							<div class="row">
								<div class="col-6 pt-2 pb-2">
									<label for="head_office_area">總店地區</label>
									<select id="head_office_area" name="head_office_area" class="form-control">
									<?php
									foreach ($areas as $area)
									{
										foreach ($area['district'] as $district)
										{
											echo sprintf('<option value="%s"%s>%s</option>', $district['id'], ($district['id'] == $provider['head_office_area']) ? ' selected="selected"' : '', $district['name']);
										}
									}
									?>
									</select>
								</div>
								<div class="col-6 pt-2 pb-2">
									<label for="head_office_building">大廈名稱</label>
									<input type="text" id="head_office_building" name="head_office_building" value="<?=$provider['head_office_building'];?>" class="form-control" placeholder="請輸入大廈名稱">
								</div>
							</div>
							<div class="row">
								<div class="col-6 pt-2 pb-2">
								</div>
								<div class="col-6 pt-2 pb-2">
									<p id="head_office_formatted_address"><?=$provider['head_office_formatted_address'];?></p>
								</div>
							</div>
							<div class="row">
								<div class="col-6 pt-2 pb-2">
								</div>
								<div class="col-6 pt-2 pb-2">
									<table class="table" id="provider_list">
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="phone">電話號碼*</label>
									<input type="text" id="phone" name="phone" value="<?=$provider['phone'];?>" class="form-control" placeholder="請輸入你的電話號碼">
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="whatsapp">Whatsapp*</label>
									<input type="text" id="whatsapp" name="whatsapp" value="<?=(count($provider['socials']['whatsapp']) == 0) ? '' : $provider['socials']['whatsapp']['social_url'];?>" class="form-control" placeholder="請輸入你的Whatsapp">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="email">電郵地址*</label>
									<input type="email" id="email" name="email" value="<?=$provider['email'];?>" class="form-control" placeholder="請輸入你的電郵地址">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<div class="provider-block">
										<label for="business_hours">營業時間</label>
										<textarea id="business_hours" name="business_hours" class="form-control" style="height: 85%;"><?=$provider['business_hours'];?></textarea>
									</div>
									<div class="brand-block">
										<label for="acting_brand">代理品牌（以逗號分隔）</label>
										<textarea id="acting_brand" name="acting_brand" class="form-control"><?=$provider['brands'];?></textarea>
										<div class="row">
											<div class="form-check d-inline-block pt-2 ms-3 col-4">
												<input class="form-check-input" name="brand_required[]" type="checkbox" value="1"<?=in_array(1, $provider['brand_required_array']) ? ' checked="checked"' : '';?> id="lookForDistributor">
												<label class="form-check-label" for="lookForDistributor">找分銷商</label>
											</div>
											<div class="form-check d-inline-block pt-2 ms-3 col-4">
												<input class="form-check-input" name="brand_required[]" type="checkbox" value="2"<?=in_array(2, $provider['brand_required_array']) ? ' checked="checked"' : '';?> id="lookForPartner">
												<label class="form-check-label" for="lookForPartner">找合作夥伴</label>
											</div>
											<div class="form-check d-inline-block pt-2 ms-3 col-4">
												<input class="form-check-input" name="brand_required[]" type="checkbox" value="3"<?=in_array(3, $provider['brand_required_array']) ? ' checked="checked"' : '';?> id="lookForInvestor">
												<label class="form-check-label" for="lookForInvestor">找投資者</label>
											</div>
											<div class="form-check d-inline-block pt-2 ms-3 col-4">
												<input class="form-check-input" name="brand_required[]" type="checkbox" value="4"<?=in_array(4, $provider['brand_required_array']) ? ' checked="checked"' : '';?> id="specialSale">
												<label class="form-check-label" for="specialSale">特價</label>
											</div>
											<div class="form-check d-inline-block pt-2 ms-3 col-4">
												<input class="form-check-input" name="brand_required[]" type="checkbox" value="5"<?=in_array(5, $provider['brand_required_array']) ? ' checked="checked"' : '';?> id="clearance">
												<label class="form-check-label" for="clearance">清貨</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="tags">關鍵字</label>
									<textarea id="tags" name="tags" class="form-control"><?=implode(',', array_map(function($a) { return $a['hashtag']; }, $provider['hashtags']));?></textarea>
									<div style="float: right;">
										<button id="add-keyword" type="button" class="btn btn-success mt-2"><i class="fa fa-plus" aria-hidden="true"></i></button>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<p style="margin-left: 0.9rem; color: #008b8b; margin-bottom: 0;">媒體（只需填寫用戶名稱）</p>
									<table class="table">
										<tbody>
											<tr>
												<td>
													<label for="facebook">Facebook</label>
												</td>
												<td>
													<input type="text" id="facebook" name="facebook" value="<?=(count($provider['socials']['facebook']) == 0) ? '' : $provider['socials']['facebook']['social_url'];?>" class="form-control" placeholder="">
												</td>
											</tr>
											<tr>
												<td>
													<label for="instagram">Instagram</label>
												</td>
												<td>
													<input type="text" id="instagram" name="instagram" value="<?=(count($provider['socials']['instagram']) == 0) ? '' : $provider['socials']['instagram']['social_url'];?>" class="form-control" placeholder="">
												</td>
											</tr>
											<tr>
												<td>
													<label for="youtube">Youtube</label>
												</td>
												<td>
													<input type="text" id="youtube" name="youtube" value="<?=(count($provider['socials']['youtube']) == 0) ? '' : $provider['socials']['youtube']['social_url'];?>" class="form-control" placeholder="">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="status">狀態</label>
									<select id="status" name="status" class="form-control">
										<option value="draft"<?=($provider['status'] == 'draft') ? ' selected="selected"' : '';?>>草稿</option>
										<option value="publish"<?=($provider['status'] == 'publish') ? ' selected="selected"' : '';?>>已發佈</option>
									</select>
								</div>
							</div>
							<div class="row">
								<label class="col-md-2 control-label"></label>
								<div class="col-md-10 inputGroupContainer">
									<div id="progress-container" class="progress">
										<div id="progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%
										</div>
									</div>
									<div id="results"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 pt-2 pb-2">
									<label for="avatar_profile">店長圖片 (250x250)</label>
									<div class="row">
										<div class="col-md-4">
											<div class="input-group">
												<div id="avatar-drop_zone">請把圖片拖放到這裡</div>
												<br>
												<label class="btn btn-block btn-info" style="width: 140px;">
													瀏覽… <input id="avatar-browse" type="file" accept="image/*" style="display: none;">
												</label>
											</div>
										</div>
										<div class="col-md-8">
											<?php
											$image = base_url('assets/img/uonlive/1x1.png');
											if ($provider['avatar_photo'] != "") {
												$file = 'assets/img/uploads/providers/' . $provider['avatar_photo'];
												if (file_exists(FCPATH . $file)) {
													$image = base_url($file);
												}
											}
											?>
											<img class="img-fluid" id="avatar-media" src="<?=$image;?>" />
											<div class="form-check">
												<input class="form-check-input" type="checkbox" value="1" name="remove_avatar" id="remove-avatar">
												<label class="form-check-label" for="remove-avatar">
													移除店長圖片
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 pt-2 pb-2">
									<label for="shop_profile">網店橫幅 (1300x866)</label>
									<div class="row">
										<div class="col-md-4">
											<div class="input-group">
												<div id="shop-drop_zone">請把圖片拖放到這裡</div>
												<br>
												<label class="btn btn-block btn-info" style="width: 140px;">
													瀏覽… <input id="shop-browse" type="file" accept="image/*" style="display: none;">
												</label>
											</div>
										</div>
										<div class="col-md-8">
											<?php
											$image = base_url('assets/img/uonlive/1x1.png');
											if ($provider['shop_photo'] != "") {
												$file = 'assets/img/uploads/providers/' . $provider['shop_photo'];
												if (file_exists(FCPATH . $file)) {
													$image = base_url($file);
												}
											}
											?>
											<img class="img-fluid" id="shop-media" src="<?=$image;?>" />
											<div class="form-check">
												<input class="form-check-input" type="checkbox" value="1" name="remove_banner" id="remove-banner">
												<label class="form-check-label" for="remove-banner">
													移除網店橫幅
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row pb-4">
								<div class="col pt-2 pb-2">
									<button class="btn btn-save">儲存設定</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="keywordDialogModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100 main_theme_color font-weight-bold text-center">建議關鍵字</h5>
      </div>
      <div class="modal-body">
		<form class="">
			<div class="row">
				<?php
				foreach ($keywords as $keyword) {
				?>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="form-check">
						<input type="checkbox" class="form-check-input" name="keyword" value="<?=$keyword['name'];?>" id="keyword-<?=$keyword['id'];?>" />
						<label class="form-check-label" for="keyword-<?=$keyword['id'];?>"><?=$keyword['name'];?></label>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <div class="text-center">
			<a id="keyword-dialog-insert" class="text-white text-decoration-none hyperlink_button main_theme_background_color normal_font_size">
                加入
            </a>
            <a id="keyword-dialog-close" class="ms-2 text-white text-decoration-none hyperlink_button main_theme_background_color normal_font_size">
                離開
            </a>
        </div>
      </div>
    </div>
  </div>
</div>