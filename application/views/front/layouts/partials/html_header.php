<meta charset="UTF-8">

<title>UonLIVE</title>
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">

<meta name="description" content="">
<meta name="author" content="UonLIVE">
<meta name="keywords" content="">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">

<meta property="og:title" content="UonLIVE">
<meta property="og:description" content="地理與人力資源信息系統平台">
<meta property="og:image" content="<?=base_url('assets/img/uploads/' . $website_settings['meta_logo'] . '?' . $hash);?>">
<meta property="og:site_name" content="UonLIVE">

<meta itemprop="name" content="UonLIVE">
<meta itemprop="description" content="地理與人力資源信息系統平台">
<meta itemprop="image" content="<?=base_url('assets/img/uploads/' . $website_settings['meta_logo'] . '?' . $hash);?>">

<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="UonLIVE">
<meta name="twitter:description" content="地理與人力資源信息系統平台">
<meta name="twitter:creator" content="@UonLIVE">
<meta name="twitter:image" content="<?=base_url('assets/img/uploads/' . $website_settings['meta_logo'] . '?' . $hash);?>">
<meta name="twitter:image:alt" content="UonLIVE">

<meta name="weibo:type" content="webpage">
<meta name="weibo:website:title" content="UonLIVE">
<meta name="weibo:webpage:description" content="地理與人力資源信息系統平台">
<meta name="weibo:webpage:image" content="<?=base_url('assets/img/uploads/' . $website_settings['meta_logo'] . '?' . $hash);?>">

<link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/favicons/apple-touch-icon.png');?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('assets/favicons/favicon-32x32.png');?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url('assets/favicons/favicon-16x16.png');?>">
<link rel="manifest" href="<?=base_url('assets/favicons/site.webmanifest');?>">
<link rel="mask-icon" href="<?=base_url('assets/favicons/safari-pinned-tab.svg');?>" color="#5bbad5">
<link rel="shortcut icon" href="<?=base_url('assets/favicons/favicon.ico');?>">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="<?=base_url('assets/favicons/browserconfig.xml');?>">
<meta name="theme-color" content="#ffffff">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+HK:wght@100;300;400;700&display=swap" rel="stylesheet">

<link href="<?=base_url('assets/vendor/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" />
<link href="<?=base_url('assets/front/css/bs_extra.css?' . $hash);?>" rel="stylesheet" />
<link href="<?=base_url('assets/vendor/fontawesome/css/all.css');?>" rel="stylesheet" />
<!-- <link href="<?=base_url('assets/vendor/fancybox/jquery.fancybox.min.css');?>" rel="stylesheet" /> -->
<!-- <link href="<?=base_url('assets/front/css/owl.theme.default.min.css?' . $hash);?>" rel="stylesheet" />
<link href="<?=base_url('assets/vendor/owlcarousel2/assets/owl.carousel.min.css');?>" rel="stylesheet" /> -->
<?php
foreach ($css_file as $css) {
?>
    <link href="<?=$css . ((strpos($css, '?') === false) ? '?' : '&') . $hash;?>" rel="stylesheet" />
<?php
}
?>
<link href="<?=base_url('assets/front/css/uonlive.css?' . $hash);?>" rel="stylesheet" />
<link href="<?=base_url('assets/front/css/navigation.css?' . $hash);?>" rel="stylesheet" />
<link href="<?=base_url('assets/front/css/frontend.css?' . $hash);?>" rel="stylesheet" />
<link href="<?=base_url('assets/front/css/footer.css?' . $hash);?>" rel="stylesheet" />
<?php
$path = 'assets/front/css/' . $controller . '.css';
if (file_exists(FCPATH . $path)) {
?>
<link href="<?=base_url($path . '?' . $hash);?>" rel="stylesheet" />
<?php
}
if ($method != 'index') {
    $path = 'assets/front/css/' . $controller . '_' . $method . '.css';
    if (file_exists(FCPATH . $path)) {
?>
<link href="<?=base_url($path . '?' . $hash);?>" rel="stylesheet" />
<?php
    }
}
