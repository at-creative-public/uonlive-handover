<div class="member-sidebar box-with-shadow">
	<div class="profile rounded-circle ratio ratio-1x1 overflow-hidden w-50 mx-auto mt-4">
		<?php
		$image = base_url('assets/img/uonlive/1x1.png');
		if ($member['avatar_photo'] != "") {
			$file = 'assets/img/uploads/members/' . $this->session->userdata('uonlive_user')['avatar_photo'];
			if (file_exists(FCPATH . $file)) {
				$image = base_url($file);
			}
		}
		?>
		<img src="<?=$image;?>" alt="..."
			 class="img-fluid">
	</div>
	<h4 class="text-center pt-3 pb-3"><?=$this->session->userdata('uonlive_user')['first_name'];?></h4>
	<a href="<?=base_url('member/info');?>" class="<?=($method=='info') ? 'active' : '';?>">
		<div class="menu-item text-center">帳戶資料</div>
	</a>
	<a href="<?=base_url('member/browsing_history');?>" class="<?=($method=='browsing_history') ? 'active' : '';?>">
		<div class="menu-item text-center">瀏覽記錄</div>
	</a>
	<a href="<?=base_url('member/like');?>" class="<?=($method=='like') ? 'active' : '';?>">
		<div class="menu-item text-center">喜愛清單</div>
	</a>
	<a href="<?=base_url('member/keep');?>" class="<?=($method=='keep') ? 'active' : '';?>">
		<div class="menu-item text-center">收藏清單</div>
	</a>
	<a href="<?=base_url('member/conversion_history');?>" class="<?=($method=='conversion_history') ? 'active' : '';?>">
		<div class="menu-item text-center">聯絡記錄</div>
	</a>
	<a href="<?=base_url('member/logout');?>" class="">
		<div class="menu-item text-center">登出</div>
	</a>
</div>
