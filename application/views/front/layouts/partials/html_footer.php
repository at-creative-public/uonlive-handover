<script src="<?=base_url('assets/vendor/jquery.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- <script src="<?=base_url('assets/vendor/fancybox/jquery.fancybox.min.js');?>"></script> -->
<!-- <script src="<?=base_url('assets/vendor/owlcarousel2/owl.carousel.min.js');?>"></script> -->
<!-- <script src="http://localhost:4837/assets/front/js/navbar.js?epohR49CNz"></script>
<script src="http://localhost:4837/assets/front/js/custom.js?epohR49CNz"></script> -->
<?php
if (count($js_file) > 0) {
	foreach ($js_file as $js)
	{
		echo sprintf('	<script %s src="%s%s%s"></script>', $js['extra'], $js['script'], (strpos($js['script'], '?') === false) ? '?' : '&', $hash);
	}
}
?>
<script src="<?=base_url('assets/front/js/common.js?' . $hash);?>"></script>
<script src="<?=base_url('assets/front/js/navbar.js?' . $hash);?>"></script>
<?php
$path = 'assets/front/js/' . $controller . '.js';
if (file_exists(FCPATH . $path)) {
?>
<script src="<?=base_url($path . '?' . $hash);?>"></script>
<?php
}
if ($method != 'index') {
	$path = 'assets/front/js/' . $controller . '_' . $method . '.js';
	if (file_exists(FCPATH . $path)) {
?>
<script src="<?=base_url($path . '?' . $hash);?>"></script>
<?php
	}
}
