<div class="container bg-white member-dashboard-tabs">
	<div class="row h-100">
		<div class="col-md-6">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link<?=($controller=="member") ? ' active' : '' ?>" data-type="member" aria-current="page" href="<?=base_url('member/info');?>">用戶</a>
				</li>
				<?php
				$expired = null;
				if (array_key_exists('expired_at', $this->session->userdata('uonlive_user')['provider'])) {
					$expired = $this->session->userdata('uonlive_user')['provider']['expired_at'];
				}
				if (! is_null($expired) && ($expired >= strtotime('now'))) {
				?>
				<li class="nav-item">
					<a class="nav-link<?=($controller=="provider") ? ' active' : '' ?>" data-type="provider" href="<?=base_url('provider/info');?>">服務提供者</a>
				</li>
				<?php
				}
				?>
			</ul>
		</div>
		<div class="col-md-6">
			<?php
			if (is_null($expired) || ($expired < strtotime('now'))) {
			?>
			<ul class="nav justify-content-end">
				<li class="btn-upgrade nav-item">
					<a class="nav-link" href="<?=base_url('member/upgrade');?>"><img width="23" height="19" src="<?=base_url('assets/img/uonlive/icon_upgrade.svg');?>" /> 升級為服務提供者／品牌</a>
				</li>
			</ul>
			<?php
			}
			else {
			?>
			<ul class="nav justify-content-end">
				<li class="nav-item">
				服務提供者有效日期至：<span class="title-color"><?=date("Y-m-d", $this->session->userdata('uonlive_user')['provider']['expired_at']);?></span>（<a class="nav-link d-inline-block" href="<?=base_url('member/renew');?>">續期</a>）
				</li>
			</ul>
			<?php
			}
			?>
		</div>
	</div>
</div>
