                                        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="<?=base_url();?>"><i class="fas fa-home"></i></a></li>
                                                <?php
                                                for ($i = 0; $i < count($thumbnails) - 1; $i++) {
                                                    echo sprintf('					<li class="breadcrumb-item"><a href="%s">%s</a></li>', $thumbnails[$i]['url'], $thumbnails[$i]['title']);
                                                }
                                                ?>
												<li class="breadcrumb-item active" aria-current="page"><?=$thumbnails[count($thumbnails) - 1]['title'];?></li>
											</ol>
										</nav>
