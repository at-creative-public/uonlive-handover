<nav class="navigation-section navbar navbar-light navbar-expand-xl bg-white clean-navbar logo_background">
    <div id="navbar_container" class="container">
        <div class="logo_bar d-block d-md-none">
            <a class="navbar-brand logo" href="<?=base_url();?>">
                <img id="logo" src="<?=base_url('assets/img/uploads/' . $website_settings['web_logo'] . '?' . $hash);?>" class="fullWidth"/>
            </a>
        </div>
        <div class="d-flex">
            <div style="width:100%;">
                <button id='nav-menu-controller' data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="row flex_end hidden_xs">
                    <div class="col-4">
                        <div class="header_logo">
                            <a href="<?=base_url();?>">
                                <div class="header_logo_wrapper">
                                    <img src="<?=base_url('assets/img/uploads/' . $website_settings['web_logo'] . '?' . $hash);?>" alt=""
                                        class="fullWidth">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-8">
                        <form id="frmSearch" method="post" action="<?=base_url('search/result');?>">
                            <div class="search mx-auto">
                                <i class="fa fa-search"></i>
                                <input type="text" name="search" id="search" class="form-control" placeholder="請輸入關鍵字">
                            </div>
                        </form>
                        <div class="collapse navbar-collapse">
                            <ul class="navbar-nav" style="width:100%;">
                                <li class="nav-item  ms-auto"><a class="nav-link  center" id="about_us" href="<?=base_url('about_us');?>">關於我們</a>
                                <li class="nav-item dropdown ms-auto">
                                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false" id="dropdownMenuLink"><span class="nav-label">服務分類</span> <span class="caret"></span></a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
										<?php
										foreach ($tag_lists as $tag) {
										?>
                                        <li class="dropdown-item center"><a href="<?=base_url('service/' . $tag['name']);?>"><?=$tag['name'];?></a></li>
										<?php
										}
										?>
                                    </ul>
                                </li>
                                <li class="nav-item  ms-auto"><a class="nav-link  center" id="business_registration" href="<?=base_url('business_registration');?>">商戶登記</a>
                                <li class="nav-item  ms-auto"><a class="nav-link  center" id="contact_us" href="<?=base_url('contact_us');?>">聯絡我們</a>
                                <!-- <li class="nav-item  ms-auto"><a class="nav-link  center" id="#" href="<?=base_url('member/info');?>">會員專區</a> -->
                                <?php
                                if (! $loggedIn) {
                                ?>
                                <li class="nav-item ms-auto member-login"><a class="nav-link center" id="member_login" href="<?=base_url('member/login');?>">登入</a></li>
                                <?php
                                }
                                else {
                                ?>
                                <li class="nav-item ms-auto member-loggedin"><a class="nav-link center" id="member_login" href="<?=base_url('member/info');?>"><i class="fa fa-user" aria-hidden="true"></i> Hi <?=$this->session->userdata('uonlive_user')['first_name'];?></a></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="navbar" class="collapse pb-3 pt-2 d-xl-none">
            <ul id="mobile_menu" class="navbar-nav ml-auto">
                <div style="text-align:right;">
                    <button id="menuwebbutton" class="navbar-toggler active" type="button" data-toggle="collapse"
                        data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="true"
                        aria-label="Toggle navigation">
                    <span class="toggler-icon" style="background-color:#d6bcab"></span>
                    <span class="toggler-icon" style="background-color:#d6bcab"></span>
                    <span class="toggler-icon" style="background-color:#d6bcab"></span>
                    </button>
                </div>
                <li class="nav-item  odd center " style="z-index:1000"><a class="nav-link  center" href="<?=base_url('about_us');?>">關於我們</a></li>
                <li class="nav-item dropdown even center">
                    <a  class="nav-link dropdown-toggle"  role="button" data-bs-toggle="dropdown" role="button" aria-expanded="false" id="dropdownMenuLinkMobile"><span class="nav-label">服務分類</span> <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLinkMobile">
					<?php
					foreach ($tag_lists as $tag) {
					?>
					<li class="dropdown-item center"><a href="<?=base_url('service/' . $tag['name']);?>"><?=$tag['name'];?></a></li>
					<?php
					}
					?>
                    </ul>
                </li>
                <li class="nav-item  odd center " style="z-index:988"><a class="nav-link  center" href="<?=base_url('business_registration');?>">商戶登記</a></li>
                <li class="nav-item even center  " style="z-index:987"><a class="nav-link  center" href="<?=base_url('contact_us');?>">聯絡我們</a></li>
                <li class="nav-item  odd center " style="z-index:986"><a class="nav-link  center" href="<?=base_url('member/info');?>">會員專區</a></li>
                <li class="nav-item center even" style="z-index:985"><a href="<?=base_url('member/login');?>" class="nav-link center"><span class="nav-label">登入</span></a></li>
                <!-- <li class="nav-item  center" style="z-index:996;display:none;">
                    <div class="row header_icons_wrapper no-gutters">
                        <div class="col-3">
                            <div class="">
                                <a href="#" target="_blank">
                                	<img src="http://localhost:4837/public/images/noah/icon_fb_o.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="">
                                <a href="#">
                                	<img src="http://localhost:4837/public/images/noah/icon_ws_o.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="">
                                <a class="open_wechat_qrcode">
                                <img src="http://localhost:4837/public/images/noah/icon_wc_o.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="">
                                <a href="#">
                                <img src="http://localhost:4837/public/images/noah/icon_search.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </li> -->
            </ul>
        </div>
    </div>
</nav>