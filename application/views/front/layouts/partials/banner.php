    <div class="">
		<div class="d-none d-md-block">
			<img src="<?=base_url('assets/front/img/uploads/' . $banner_desktop);?>" class="fullWidth"
				alt="<?=$desktop_banner_alttext;?>">
		</div>

		<div class="d-block d-md-none">
			<img src="<?=base_url('assets/front/img/uploads/' . $banner_mobile);?>" class="fullWidth"
				alt="<?=$mobile_banner_alttext;?>">
		</div>
	</div>
