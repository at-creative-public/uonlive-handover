<div class="provider-sidebar box-with-shadow">
	<div class="profile rounded-circle ratio ratio-1x1 overflow-hidden w-50 mx-auto mt-4">
		<?php
		$image = base_url('assets/img/uonlive/1x1.png');
		if ($provider['avatar_photo'] != "") {
			$file = 'assets/img/uploads/providers/' . $provider['avatar_photo'];
			if (file_exists(FCPATH . $file)) {
				$image = base_url($file);
			}
		}
		?>
		<img src="<?=$image;?>" alt="..."
			 class="img-fluid">
	</div>
	<h4 class="text-center pt-3 pb-3"><?=$provider['shop_name'];?></h4>
	<a href="<?=base_url('provider/info');?>" class="<?=($method=='info') ? 'active' : '';?>">
		<div class="menu-item text-center">網店簡介</div>
	</a>
	<a href="<?=base_url('provider/service');?>" class="<?=($method=='service') ? 'active' : '';?>">
		<div class="menu-item text-center">服務管理</div>
	</a>
	<a href="<?=base_url('provider/media');?>" class="<?=($method=='media') ? 'active' : '';?>">
		<div class="menu-item text-center">媒體管理</div>
	</a>
	<a href="<?=base_url('provider/browsing_history');?>" class="<?=($method=='browsing_history') ? 'active' : '';?>">
		<div class="menu-item text-center">瀏覽記錄</div>
	</a>
	<a href="<?=base_url('provider/conversion_history');?>" class="<?=($method=='conversion_history') ? 'active' : '';?>">
		<div class="menu-item text-center">聯絡記錄</div>
	</a>
	<a href="<?=base_url('provider/payment_history');?>" class="<?=($method=='payment_history') ? 'active' : '';?>">
		<div class="menu-item text-center">付費記錄</div>
	</a>
</div>
