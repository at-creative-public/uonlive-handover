<div class="modal fade" id="messageDialogModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100 main_theme_color font-weight-bold text-center">留言</h5>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body">
        <form id="messageForm">
          <div class="form-group">
            <label for="message-title">標題</label>
            <input type="text" class="form-control" id="message-title" placeholder="請輸入標題">
          </div>
          <div class="form-group">
            <label for="message-content">內容</label>
            <textarea class="form-control" id="message-content"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="text-center ">
          <button type="button" class="btn-send-message btn btn-uonlive">發送</button>
          <button type="button" class="btn-close-dialog btn btn-uonlive">關閉</button>
        </div>
      </div>
    </div>
  </div>
</div>