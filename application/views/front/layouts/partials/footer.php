    <div class="footer">
		<div class="container">
			<div class="row footer_row">
				<div class="col-lg-4 col-md-6 pt-3 pb-3">
					<h4 class="title-color" style="padding-top: 10px;">想獲取更多資訊</h4>
					<div class="searchBar">
						<form action="<?=base_url('contact_us/save_contact');?>" method="post">
							<input type="email" placeholder="你的電郵地址*" name="contact_email">
							<button type="submit">登記</i></button>
						</form>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 pt-3 pb-3">
					<div class="footer_section_wrapper">
						<table class="">
							<tr>
								<td class="v-top">
									<i class="fas fa-map-marker-alt"></i>
								</td>
								<td>
									11/F, Guangdong Finance Building,<br />88 Connaught Road West, Hong Kong
								</td>
							</tr>

							<tr>
								<td class="v-top">
									<i class="fas fa-envelope"></i>
								</td>
								<td>
									<a href="mailto:<?=$this->config->item('site_settings')['company_email'];?>">
									<?=$this->config->item('site_settings')['company_email'];?>
									</a>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="col-lg-4 col-md-12 pt-3 pb-3">
					<h4 class="title-color" style="padding-top: 10px;">下載應用程式</h4>
					<img src="<?=base_url('assets/img/uonlive/icon_app1.png');?>" alt="" />
					<img src="<?=base_url('assets/img/uonlive/icon_app2.png');?>" alt="" />
					<div class="pt-2 d-flex justify-content-start">
						<a href="https://www.facebook.com/<?=$this->config->item('uonlive')['social_network']['facebook'];?>/" target="_blank">
							<img class="pe-2" src="<?=base_url('assets/img/uonlive/icon_fb.png');?>" alt="" />
						</a>
						<a href="https://wa.me/<?=$this->config->item('uonlive')['social_network']['whatsapp'];?>" target="_blank">
							<img class="ps-2 pe-2" src="<?=base_url('assets/img/uonlive/icon_what.png');?>" alt="" />
						</a>
						<a href="mailto:<?=$this->config->item('site_settings')['company_email'];?>" target="_blank">
							<img class="ps-2" src="<?=base_url('assets/img/uonlive/icon_mail.png');?>" alt="" />
						</a>
					</div>
				</div>
			</div>
			<div class="row copy_right">
				<div class="col-6">
					<ul class="nav">
						<li class="nav-item">
							<a class="nav-link" href="<?=base_url('document/privacy');?>">隱私政策</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=base_url('document/terms_of_use');?>">使用條款</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=base_url('document/q_and_a');?>">常見問題</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=base_url('contact_us');?>">聯絡我們</a>
						</li>
					</ul>
				</div>
				<div class="col-6">
					<p class="text-end">COPYRIGHT © 2022 <?=$this->config->item('site_settings')['company_name'];?>. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
