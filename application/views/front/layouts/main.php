<!DOCTYPE html>
<head>
	<?php
	$this->load->view('front/layouts/partials/html_header', ['hash' => $hash, ]);
	?>
</head>

<body>
	<?php
	$this->load->view('front/layouts/partials/navbar', [
		'hash' => $hash, 
		'loggedIn' => $loggedIn,
	]);
	if ($use_banner) {
		$this->load->view('front/layouts/partials/banner', [
			'hash' => $hash, 
			'controller' => $controller, 
			'method' => $method,
			'banner_desktop' => $banner_desktop,
			'desktop_banner_alttext' => $desktop_banner_alttext,
			'banner_mobile' => $banner_mobile,
			'mobile_banner_alttext' => $mobile_banner_alttext,
		]);
	}
	if ($method == 'index') {
		$this->load->view('front/' . $controller, ['hash' => $hash, ]);
	}
	else {
		$this->load->view('front/' . $controller . '_' . $method, ['hash' => $hash, ]);
	}
	$this->load->view('front/layouts/partials/footer.php', ['hash' => $hash, ]);
	?>
	<script>
	var selected = "";
	var base_url = '<?=base_url();?>';
	<?php
	foreach ($scripts as $script) {
		echo $script . PHP_EOL;
	}
	?>
	</script>
	<?php
	$this->load->view('front/layouts/partials/html_footer.php', ['hash' => $hash, ]);
	?>
</body>
</html>
