	<div class="section-category mt-4 mb-5">
		<div class="container">
			<div class="row">
				<div class="col pt-4 pb-4 ps-2 pe-2" style="border: 1px solid #ccc; border-radius: 15px;-webkit-box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.28);
	box-shadow: 0px 0px 6px 2px rgba(0,0,0,0.28);">
					<div class="container">
						<h2 class="title-color text-center">服務<span>分類</span></h2>
						<div class="row ten-cols">
							<?php
							foreach ($tag_lists as $tag) {
							?>
							<div class="col-xl-1 col-lg-2 col-3 p-2 h-over" style="border-radius: 15px;">
								<a href="<?=base_url('service/' . $tag['name']);?>" class="text-decoration-none">
									<img src="<?=base_url('assets/img/uploads/categories/' . $tag['code'] . '.svg?' . $hash);?>" class="img-fluid mx-auto d-block" />
									<p class="text-center text-caption"><?=$tag['name'];?></p>
								</a>
							</div>
							<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<span class="need-share-button_dropdown need-share-button_dropdown-box-horizontal need-share-button_dropdown-middle-right" style="margin-top: -20px;"><span class="need-share-button_link-box need-share-button_link need-share-button_mailto" data-network="mailto"></span><span class="need-share-button_link-box need-share-button_link need-share-button_twitter" data-network="twitter"></span><span class="need-share-button_link-box need-share-button_link need-share-button_pinterest" data-network="pinterest"></span><span class="need-share-button_link-box need-share-button_link need-share-button_facebook" data-network="facebook"></span><span class="need-share-button_link-box need-share-button_link need-share-button_googleplus" data-network="googleplus"></span><span class="need-share-button_link-box need-share-button_link need-share-button_linkedin" data-network="linkedin"></span></span>
	<div class="pb-4 small_font_size latest-services">
		<div class="container posrel">
			<div class="section_content">
				<div class="title_wrapper">
					<div class="posrel text-center tab-box">
						<div class="">
							<div class="row">
								<div class="col-md-4 d-flex align-items-center">
									<h2 class="text-start title-color">最新<span>服務</span></h2>
								</div>
								<div class="col-md-8">
									<ul class="nav pe-3">
										<li class="nav-item">
											<a class="nav-link" id="service-provider">服務提供者</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="service-brand">品牌</a>
										</li>
										<!-- <li class="nav-item">
											<a class="nav-link" href="#">分銷商</a>
										</li> -->
										<li class="nav-item">
											<a class="nav-link active" id="service-all" aria-current="page">全部</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<?php
						if (count($latest_services) > 100) {
						?>
						<div class="show_more_btn">
							<a href="#">查看更多</a>
						</div>
						<?php
						}
						?>
					</div>
				</div>
				<div class="row">
					<?php
					foreach ($latest_services as $latest) {
						switch ($latest['type']) {
							case "B":
								$service_class = 'service-brand';
								break;
							case "P":
								$service_class = 'service-provider';
								break;
						}
						$like_icon = 'far';
						$bookmark_icon = 'far';
						if ($latest['is_liked']) {
							$like_icon = 'fa';
						}
						if ($latest['is_bookmarked']) {
							$bookmark_icon = 'fa';
						}
						?>
					<div class="col-xl-3 col-lg-4 col-md-6 col-12 pt-3 pb-3 d-flex align-items-stretch <?=$service_class;?>">
						<div class="card service-box">
							<div class="card-header">
								<img src="<?=base_url('assets/img/uploads/services/' . $latest['photo']);?>" class="same-height img-fluid" />
							</div>
							<div class="card-body d-flex flex-column">
								<p><?=$latest['location'];?></p>
								<h5 class="title-color"><a href="<?=base_url('service/details/' . $latest['uri']);?>"><?=$latest['title'];?></a></h5>
								<div class="tag-list">
									<?php
									foreach ($latest['tags'] as $tag_idx) {
									?>
									<span style="background-color: <?=$tag_lists[$tag_idx - 1]['color'];?>"><?=$tag_lists[$tag_idx - 1]['name'];?></span>
									<?php
									}
									?>
								</div>
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-8"></div>
									<div class="col-4 d-flex justify-content-end">
										<div id="share-button_latest_<?=$latest['id'];?>" class="need-share-button-default" data-share-position="topCenter" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fa fa-share-alt" aria-hidden="true"></i></span></div>
										<!-- <img src="<?=base_url('assets/img/uonlive/icon_share.png');?>" /> -->
										<a data-member="<?=$member_id;?>" data-provider="<?=$latest['providers_id'];?>" class="pt-1 ps-2 like-page provider_id_<?=$latest['providers_id'];?>"><i class="<?=$like_icon;?> fa-heart" aria-hidden="true"></i></a>
										<!-- <img class="ps-2" src="http://localhost:4837/public/images/uonlive/icon_like1.png" /> -->
										<a data-member="<?=$member_id;?>" data-provider="<?=$latest['providers_id'];?>" class="pt-1 ps-2 bookmark-page provider_id_<?=$latest['providers_id'];?>"><i class="<?=$bookmark_icon;?> fa-bookmark" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="case_section mb-5 hot-services">
		<div class="container posrel">
			<div class="title_wrapper">
				<div class="posrel text-center tab-box">
					<div class="">
						<div class="row">
							<div class="col-md-4 d-flex align-items-center">
								<h2 class="text-start title-color">熱門<span>服務</span></h2>
							</div>
							<div class="col-md-8">
								<ul class="nav pe-3">
									<li class="nav-item">
										<a class="nav-link" id="hottest-provider">服務提供者</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="hottest-band">品牌</a>
									</li>
									<!-- <li class="nav-item">
										<a class="nav-link" href="#">分銷商</a>
									</li> -->
									<li class="nav-item">
										<a class="nav-link active" id="hottest-all" aria-current="page">全部</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<?php
					if (count($hot_services) > 100) {
					?>
					<div class="show_more_btn">
						<a href="#">查看更多</a>
					</div>
					<?php
					}
					?>
				</div>
			</div>

			<div class="row">
			<?php
				foreach ($hot_services as $hot) {
					switch ($hot['type']) {
						case "B":
							$service_class = 'hottest-brand';
							break;
						case "P":
							$service_class = 'hottest-provider';
							break;
					}
					$like_icon = 'far';
					$bookmark_icon = 'far';
					if ($hot['is_liked']) {
						$like_icon = 'fa';
					}
					if ($hot['is_bookmarked']) {
						$bookmark_icon = 'fa';
					}
				?>
				<div class="col-xl-3 col-lg-4 col-md-6 col-12 pt-3 pb-3 d-flex align-items-stretch <?=$service_class;?>">
					<div class="card service-box">
						<div class="card-header">
							<img src="<?=base_url('assets/img/uploads/services/' . $hot['photo']);?>" class="same-height img-fluid" />
						</div>
						<div class="card-body d-flex flex-column">
							<p><?=$hot['location'];?></p>
							<h5 class="title-color"><a href="<?=base_url('service/details/' . $hot['uri']);?>"><?=$hot['title'];?></a></h5>
							<div class="tag-list">
								<?php
								foreach ($hot['tags'] as $tag_idx) {
								?>
								<span style="background-color: <?=$tag_lists[$tag_idx - 1]['color'];?>"><?=$tag_lists[$tag_idx - 1]['name'];?></span>
								<?php
								}
								?>
							</div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-8"></div>
								<div class="col-4 d-flex justify-content-end">
									<div id="share-button_hottest_<?=$hot['id'];?>" class="need-share-button-default" data-share-position="topCenter" data-share-share-button-class="custom-button"><span class="custom-button"><i class="fa fa-share-alt" aria-hidden="true"></i></span></div>
									<!-- <img src="<?=base_url('assets/img/uonlive/icon_share.png');?>" /> -->
									<a data-member="<?=$member_id;?>" data-provider="<?=$hot['providers_id'];?>" class="pt-1 ps-2 like-page provider_id_<?=$hot['providers_id'];?>"><i class="<?=$like_icon;?> fa-heart" aria-hidden="true"></i></a>
									<!-- <img class="ps-2" src="<?=base_url('assets/img/uonlive/icon_like1.png');?>" /> -->
									<a data-member="<?=$member_id;?>" data-provider="<?=$hot['providers_id'];?>" class="pt-1 ps-2 bookmark-page provider_id_<?=$hot['providers_id'];?>"><i class="<?=$bookmark_icon;?> fa-bookmark" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
