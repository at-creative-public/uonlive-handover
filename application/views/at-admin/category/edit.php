				<div class="form-container">
					<form id="phpform" class="form-horizontal" action="<?=base_url('at-admin/category/save');?>" method="post">
						<input type="hidden" name="code" value="<?=$category['code'];?>" />
						<input type="hidden" name="icon_file" value="<?=$category['code'];?>" />
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="name">類別名稱*</label></div>
							<div class="col-sm-8 mas-box"><input id="name" type="text" class="form-control mas-input"
																 name="name" required
																 value="<?=$category['name'];?>"
																 placeholder="請輸入類別名稱"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">類別圖標*<br />以SVG格式</label>
							<div class="col-md-2 inputGroupContainer">
								<div class="input-group">
									<div id="icon-drop_zone">請把相片拖放到這裡</div>
									<br/>
									<label class="btn btn-block btn-info">
										瀏覽&hellip; <input id="icon-browse" type="file" accept="image/svg+xml" style="display: none;">
									</label>
								</div>
							</div>
							<div class="col-md-8 inputGroupContainer">
								<img class="img-fluid" id="icon-media" src="<?=base_url('assets/img/uploads/categories/' . $category['code'] . '.svg');?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label"></label>
							<div class="col-md-10 inputGroupContainer">
								<div id="progress-container" class="progress">
									<div id="progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%
									</div>
								</div>
								<div id="results"></div>
							</div>
						</div>
						<div class="form-footer">
							<div class="col-sm-3">&nbsp;</div>
							<div class="col-sm-9 mas-box">
								<a id="btnSubmit" class="btn btn-footer" href="javascript:void(0);">提交</a>
								<a class="btn btn-footer reset" href="javascript:window.location.reload(true)">重設</a>
							</div>
						</div>
					</form>
				</div>
