<div class="row">
	<!-- column -->
	<div class="col-lg-6 col-sm-12 padding-20 first">
		<div class="item row">
			<div class="col-lg-3 image"><img style="height: 71.43px;"
				src="<?=base_url('assets/at-admin/img/dashboard-maintenance-register.png');?>">
			</div>
			<div class="col-lg-9" style="border-left:2px solid #77c943;">
				<div class="row" style="padding:.5em 0 .2em 1em;border-bottom:1px solid #77c943;">
					<span style="color:#77c943;font-size:1.1em;">會員數目</span>
				</div>
				<div class="row" style="padding:.5em 0 .2em 1em;">
<!--					<p class="timespan">由2021/01/01起計算至2021/08/27</p>-->
					<p class="total">總共：<?=$member_count;?>位</p>
				</div>
			</div>
		</div>
	</div>

	<!-- end column -->
	<div class="col-lg-6 col-sm-12 padding-20">
		<div class="item row">
			<div class="col-lg-3 image"><img style="max-width:100%;"
				src="<?=base_url('assets/at-admin/img/dashboard-purchase-filter.png');?>">
			</div>
			<div class="col-lg-9" style="border-left:2px solid #77c943;">
				<div class="row" style="padding:.5em 0 .2em 1em;border-bottom:1px solid #77c943;">
					<span style="color:#77c943;font-size:1.1em;">品牌數目</span>
				</div>
				<div class="row" style="padding:.5em 0 .2em 1em;">
<!--					<p class="timespan">由2021/01/01起計算至2021/08/27</p>-->
					<p class="total">總共：<?=$brand_count;?>個</p>

				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6 col-sm-12 padding-20">
		<div class="item row">
			<div class="col-lg-3 image"><img
				src="<?=base_url('assets/at-admin/img/dashboard-purchase.png');?>">
			</div>
			<div class="col-lg-9" style="border-left:2px solid #ff7bac;">
				<div class="row" style="padding:.5em 0 .2em 1em;border-bottom:1px solid #ff7bac;">
					<span style="color:#ff7bac;font-size:1.1em;">服務提供者數目</span>
				</div>
				<div class="row" style="padding:.5em 0 .2em 1em;">
<!--					<p class="timespan">由2021/01/01起計算至2021/08/27</p>-->
					<p class="total">總共：<?=$provider_count;?>個</p>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6 col-sm-12 padding-20">
	</div>

</div>
