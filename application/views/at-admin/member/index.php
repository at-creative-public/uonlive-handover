<div class="row">
	<div class="col-xs-12 col-md-12">
		<div class="widget">
			<div class="widget-header ">
				<span class="widget-caption"></span>
				<div class="widget-buttons">
					<a href="#" data-toggle="maximize">
						<i class="fa fa-expand"></i>
					</a>
					<!--
										<a href="#" data-toggle="collapse">
											<i class="fa fa-minus"></i>
										</a>
										<a href="#" data-toggle="dispose">
											<i class="fa fa-times"></i>
										</a>
					-->
				</div>
			</div>
			<div class="widget-body">
				<div class="table-toolbar">
					<div class="btn-group pull-right">
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered dataTable no-footer"
					   id="editabledatatable">
					<thead>
					<tr role="row">
						<th>
						</th>
						<th>
							會員名稱
						</th>
						<th>
							電郵地址
						</th>
						<th>
							電話
						</th>
						<th>
							註冊日期
						</th>
						<th>
						</th>
					</tr>
					</thead>

					<tbody>
					<?php
					foreach ($data as $idx => $row) {
					?>
					<tr>
						<td><?=$idx + 1;?>.</td>
						<td><?=$row['first_name'] . ', ' . $row['last_name'];?></td>
						<td><?=$row['email'];?></td>
						<td><?=$row['phone'];?></td>
						<td><?=date("Y-m-d", $row['created_at']);?></td>
						<td>
							<a href="<?=base_url('at-admin/member/details/' . $row['token']);?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> 詳情</a>
						</td>
					</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
