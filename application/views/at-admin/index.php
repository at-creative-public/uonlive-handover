<!DOCTYPE html>
<html>
<!-- Head -->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?=$site_settings['company_name'];?></title>
	<?php
	$this->load->view('at-admin/includes/html_header');
	?>
</head>
<!-- /Head -->
<!-- Body -->
<body>
	<!-- Loading Container -->
	<div class="loading-container loading-inactive">
		<div class="loader"></div>
	</div>
	<!--  /Loading Container -->
	<!-- Navbar -->
	<?php
	$this->load->view('at-admin/includes/header');
	?>
	<!-- /Navbar -->
	<!-- Main Container -->
	<div class="main-container container-fluid">
		<!-- Page Container -->
		<div class="page-container">

		<!-- Page Sidebar -->
		<?php
		$this->load->view('at-admin/includes/sidebar');
		?>
		<!-- /Page Sidebar -->
		<!-- Chat Bar -->
		<?php
		if ($showChat) {
			$this->load->view('at-admin/includes/chatbar');
		}
		?>
		<!-- /Chat Bar -->
		<!-- Page Content -->
		<div class="page-content">
			<!-- Page Breadcrumb -->
			<?php
			$this->load->view('at-admin/includes/breadcrumb');
			?>
			<!-- /Page Breadcrumb -->
			<!-- Page Header -->
			<?php
			$this->load->view('at-admin/includes/pageheader');
			?>
			<!-- /Page Header -->
			<!-- Page Body -->
			<div class="page-body">
				<div id="<?=$controller . '--' . $action;?>">
			<?php
			$this->load->view('at-admin/' . $controller . '/' . $action);
			?>
				</div>
			</div>
			<!-- /Page Body -->
		</div>
		<!-- /Page Content -->

	</div>
	<!-- /Page Container -->
	<!-- Main Container -->

</div>
<!-- main content ends -->

<?php
$this->load->view('at-admin/includes/html_footer');
?>
</body>
<!--  /Body -->

</html>
