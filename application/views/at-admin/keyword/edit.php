				<div class="form-container">
					<form id="phpform" class="form-horizontal" action="<?=base_url('at-admin/keyword/save');?>" method="post">
						<input type="hidden" name="code" value="<?=$keyword['code'];?>" />
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="tag_code">類別*</label></div>
							<div class="col-sm-8 mas-box">
								<select id="tag_code" type="text" class="form-select mas-input"
																 name="tag_code" required>
								<?php
								foreach ($tags as $tag) {
									echo sprintf('<option value="%s"%s>%s</option>', $tag['code'], ($tag['code'] == $keyword['tag_code']) ? ' selected="selected"' : '', $tag['name']);
								}
								?>
								</select>
							</div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="tag_2nd_code">子類別*</label></div>
							<div class="col-sm-8 mas-box">
								<select id="tag_2nd_code" type="text" class="form-select mas-input"
																 name="tag_2nd_code" required>
								<?php
								foreach ($tags_2nd as $tag) {
									echo sprintf('<option value="%s"%s>%s</option>', $tag['code'], ($tag['code'] == $keyword['tag_2nd_code']) ? ' selected="selected"' : '', $tag['name']);
								}
								?>
								</select>
							</div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="name">關鍵字名稱*</label></div>
							<div class="col-sm-8 mas-box"><input id="name" type="text" class="form-control mas-input"
																 name="name" required value="<?=$keyword['name'];?>"
																 placeholder="請輸入關鍵字名稱"/>
							</div>
						</div>
						<div class="form-footer">
							<div class="col-sm-3">&nbsp;</div>
							<div class="col-sm-9 mas-box">
								<a id="btnSubmit" class="btn btn-footer" href="javascript:void(0);">提交</a>
								<a class="btn btn-footer reset" href="javascript:window.location.reload(true)">重設</a>
							</div>
						</div>
					</form>
				</div>
