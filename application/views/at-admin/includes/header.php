<div class="navbar">
	<div class="navbar-inner">
		<div class="navbar-container">
			<!-- Navbar Barnd -->
			<div class="navbar-header pull-left">
				<a href="#" class="navbar-brand">
<!--					<small>-->
					<img class="imglogo" style="height: 70px!important; width: auto;" src="<?= base_url('assets/img/logo.png'); ?>" alt=""/>
<!--					</small>-->
				</a>
				<!-- <div style="color: #ef4130;">LEN</div> -->
			</div>
			<!-- /Navbar Barnd -->

			<div>
				<!-- Sidebar Collapse -->
				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="collapse-icon fa fa-bars"></i>
				</div>
				<div id="navbar-app-name" class="hidden-xs visible-lg text-center"><span><?=$this->config->item('site_settings')['company_name'];?></span>
				</div>
				<!-- /Sidebar Collapse -->
			</div>

			<!-- Account Area and Settings --->
			<div class="navbar-header pull-right">
				<div class="navbar-account">
					<ul class="account-area">
						<?php
						if ($showNotification) {
							$this->load->view('at-admin/includes/header_notification');
						}
						if ($showMail) {
							$this->load->view('at-admin/includes/header_messages');
						}
						if ($showTask) {
							$this->load->view('at-admin/includes/header_tasks');
						}
						if ($showChat) {
							$this->load->view('at-admin/includes/header_chats');
						}
						?>
						<li>
							<a class="login-area dropdown-toggle" data-toggle="dropdown">
								<div class="avatar" title="View your public profile">
									<img src="<?=base_url('assets/at-admin/img/icon-admin-user.png');?>">
								</div>
								<section>
									<h2><span class="profile"><span><?=$this->session->userdata('uonlive_admin_user')['username'];?></span></span></h2>
								</section>
							</a>
							<!--Login Area Dropdown-->
							<ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
								<li class="username"><a><?=$this->session->userdata('uonlive_admin_user')['username'];?></a></li>
								<li class="email"><a><?=$this->session->userdata('uonlive_admin_user')['email'];?></a></li>
								<!--Avatar Area-->
								<li>
									<div class="avatar-area">
										<img src="<?=base_url('assets/at-admin/img/icon-admin-user.png');?>"
											 class="avatar">
										<span class="caption">變更圖片</span>
									</div>
								</li>
								<!--Avatar Area-->
								<li class="edit">
									<a href="<?=base_url('at-admin/admin/edit/' . $this->session->userdata('uonlive_admin_user')['user_id']);?>" class="pull-left">資料</a>
									<a href="#" class="pull-right">設定</a>
								</li>
								<!--Theme Selector Area-->
								<?php
								if ($changeTheme) {
									$this->load->view('at-admin/includes/header_change_theme');
								}
								?>
								<!--/Theme Selector Area-->
								<li class="dropdown-footer">
									<a href="<?=base_url('at-admin/admin/logout');?>">
										登出
									</a>
								</li>
							</ul>
							<!--/Login Area Dropdown-->
						</li>
						<!-- /Account Area -->
						<!--Note: notice that setting div must start right after account area list.
						no space must be between these elements-->
						<!-- Settings -->
					</ul>
					<div class="setting">
						<a id="btn-setting" title="Setting" href="#">
							<i class="icon glyphicon glyphicon-cog"></i>
						</a>
					</div>
					<div class="setting-container">
						<label>
							<input type="checkbox" id="checkbox_fixednavbar">
							<span class="text">Fixed Navbar</span>
						</label>
						<label>
							<input type="checkbox" id="checkbox_fixedsidebar">
							<span class="text">Fixed SideBar</span>
						</label>
						<label>
							<input type="checkbox" id="checkbox_fixedbreadcrumbs">
							<span class="text">Fixed BreadCrumbs</span>
						</label>
						<label>
							<input type="checkbox" id="checkbox_fixedheader">
							<span class="text">Fixed Header</span>
						</label>
					</div>
					<!-- Settings -->
				</div>
			</div>
			<!-- /Account Area and Settings -->
		</div>
	</div>
</div>
