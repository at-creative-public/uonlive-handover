<div class="page-sidebar" id="sidebar">
	<!-- Page Sidebar Header-->
	<div class="sidebar-header-wrapper">
		<input type="text" class="searchinput"/>
		<i class="searchicon fa fa-search"></i>
		<div class="searchhelper">搜索報告，圖表，電子郵件或通知</div>
	</div>
	<ul class="nav sidebar-menu">
		<?php
		foreach ($sidebar as $key => $item) {
			$active = ($controller == $key) ? ' class="active"' : '';
		?>
		<li<?=$active;?>>
			<a href="<?=$item['url'];?>">
				<i class="menu-icon <?=$item['icon'];?>"></i>
				<span class="menu-text"><?=$item['name'];?></span>
			</a>
		</li>
		<?php
		}
		?>
	</ul>
</div>
