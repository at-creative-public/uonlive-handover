<!--Basic Scripts-->
<script src="<?=base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?=base_url('assets/js/slimscroll/jquery.slimscroll.min.js');?>"></script>

<!--Beyond Scripts-->
<script src="<?=base_url('assets/js/beyond.js?' . $hash);?>"></script>


<!--Page Related Scripts-->
<!--Sparkline Charts Needed Scripts-->
<script src="<?=base_url('assets/js/charts/sparkline/jquery.sparkline.js');?>"></script>
<script src="<?=base_url('assets/js/charts/sparkline/sparkline-init.js');?>"></script>

<!--Custom Javascript variables-->
<script>
var base_url = '<?=base_url();?>';
var email_validation_pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
<?php
foreach ($scripts as $script) {
	echo $script . PHP_EOL;
}
?>
</script>

<!--Custom system scripts-->
<?php
foreach ($jses as $js) {
	$type = '';
	if ($js['type'] != '') {
		$type = ' type="' . $js['type'] . '" ';
	}
?>
<script <?=$type;?>src="<?=$js['url'] . '?' . $hash;?>"></script>
<?php
}
?>

<!--Custom scripts-->
<script src="<?=base_url('assets/js/custom/common.js?' . $hash);?>"></script>
<script src="<?=base_url('assets/at-admin/js/at-admin.js?' . $hash);?>"></script>
<script src="<?=base_url('assets/at-admin/js/' . $controller . '/' . $action . '.js?' . $hash);?>"></script>
