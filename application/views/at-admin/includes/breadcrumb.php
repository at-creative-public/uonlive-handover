<div class="page-breadcrumbs">
	<ul class="breadcrumb">
		<?php
		$numOfBreadcrumb = count($breadcrumb);
		if ($numOfBreadcrumb > 1) {
			for ($i = 0; $i < $numOfBreadcrumb - 1; $i++) {
				if ($i == 0) {
		?>
		<li>
			<i class="fa fa-home"></i>
			<a href="<?=$breadcrumb[$i]['url'];?>"><?=$breadcrumb[$i]['name'];?></a>
		</li>
		<?php
				}
				else {
		?>
		<li>
			<a href="<?=$breadcrumb[$i]['url'];?>"><?=$breadcrumb[$i]['name'];?></a>
		</li>
		<?php
				}
			}
		}
		?>
		<li class="active"><?=$breadcrumb[$numOfBreadcrumb - 1]['name'];?></li>
	</ul>
</div>
