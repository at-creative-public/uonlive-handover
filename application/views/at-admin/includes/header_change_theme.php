<li class="theme-area">
	<ul class="colorpicker" id="skin-changer">
		<li><a class="colorpick-btn" href="#" style="background-color:#5DB2FF;"
			   rel="<?= base_url('assets/css/skins/blue.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#2dc3e8;"
			   rel="<?= base_url('assets/css/skins/azure.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#03B3B2;"
			   rel="<?= base_url('assets/css/skins/teal.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#53a93f;"
			   rel="<?= base_url('assets/css/skins/green.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#FF8F32;"
			   rel="<?= base_url('assets/css/skins/orange.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#cc324b;"
			   rel="<?= base_url('assets/css/skins/pink.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#AC193D;"
			   rel="<?= base_url('assets/css/skins/darkred.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#8C0095;"
			   rel="<?= base_url('assets/css/skins/purple.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#0072C6;"
			   rel="<?= base_url('assets/css/skins/darkblue.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#585858;"
			   rel="<?= base_url('assets/css/skins/gray.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#474544;"
			   rel="<?= base_url('assets/css/skins/black.min.css'); ?>"></a></li>
		<li><a class="colorpick-btn" href="#" style="background-color:#001940;"
			   rel="<?= base_url('assets/css/skins/deepblue.min.css'); ?>"></a></li>
	</ul>
</li>
