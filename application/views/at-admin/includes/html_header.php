<!-- Favicons -->
<link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/favicons/apple-touch-icon.png');?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('assets/favicons/favicon-32x32.png');?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url('assets/favicons/favicon-16x16.png');?>">
<link rel="manifest" href="<?=base_url('assets/favicons/site.webmanifest');?>">
<link rel="mask-icon" href="<?=base_url('assets/favicons/safari-pinned-tab.svg');?>" color="#5bbad5">
<link rel="shortcut icon" href="<?=base_url('assets/favicons/favicon.ico');?>">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="<?=base_url('assets/favicons/browserconfig.xml');?>">
<meta name="theme-color" content="#ffffff">

<!--Basic Styles-->
<link href="<?=base_url('assets/css/bootstrap.css');?>" rel="stylesheet" />
<link id="bootstrap-rtl-link" href="" rel="stylesheet" />
<link href="<?=base_url('assets/css/font-awesome.min.css');?>" rel="stylesheet" />
<link href="<?=base_url('assets/css/weather-icons.min.css');?>" rel="stylesheet"/>

<!--Fonts-->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">

<!--AwesomeFonts-->

<!--Beyond styles-->
<link id="beyond-link" href="<?=base_url('assets/css/beyond.min.css');?>" rel="stylesheet" />
<link href="<?=base_url('assets/css/demo.min.css');?>" rel="stylesheet" />
<link href="<?=base_url('assets/css/typicons.min.css');?>" rel="stylesheet" />
<link href="<?=base_url('assets/css/animate.min.css');?>" rel="stylesheet" />
<link id="skin-link" href="" rel="stylesheet" type="text/css" />

<!--Custom system style-->
<?php
foreach ($csses as $css) {
?>
<link href="<?=$css . '?' . $hash;?>" rel="stylesheet" />
<?php
}
?>

<!--Custom style-->
<link href="<?=base_url('assets/at-admin/css/at-admin.css?' . $hash);?>" rel="stylesheet" />
<link href="<?=base_url('assets/at-admin/css/' . $controller . '.css?' . $hash);?>" rel="stylesheet" />

<!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
<script src="<?=base_url('assets/js/skins.min.js');?>"></script>
