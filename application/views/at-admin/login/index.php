﻿<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 1.6.0
Purchase: https://wrapbootstrap.com/theme/beyondadmin-adminapp-angularjs-mvc-WB06R48S4
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!--Head-->
<head>
    <meta charset="utf-8" />
    <title>UonLIVE管理員登錄</title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<!-- Favicons -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url('assets/at-admin/favicon/apple-touch-icon.png');?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('assets/at-admin/favicon/favicon-32x32.png');?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url('assets/at-admin/favicon/favicon-16x16.png');?>">
	<link rel="manifest" href="<?=base_url('assets/at-admin/favicon/site.webmanifest');?>">
	<link rel="mask-icon" href="<?=base_url('assets/at-admin/favicon/safari-pinned-tab.svg');?>" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

    <!--Basic Styles-->
    <link href="<?=base_url('assets/vendor/bootstrap4/css/bootstrap.min.css');?>" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<?=base_url('assets/css/font-awesome.min.css');?>" rel="stylesheet" />

    <!--Fonts-->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">

    <!--Beyond styles-->
    <link id="beyond-link" href="<?=base_url('assets/css/beyond.min.css');?>" rel="stylesheet" />
    <link href="<?=base_url('assets/css/demo.min.css');?>" rel="stylesheet" />
    <link href="<?=base_url('assets/css/animate.min.css');?>" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

	<!--Custom style-->
	<link href="<?=base_url('assets/at-admin/css/login.css');?>" rel="stylesheet" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<?=base_url('assets/js/skins.min.js');?>"></script>

	<style>
		.login-form-2 .btnSubmit {
			background-color: #ef4130;
		}
	</style>
</head>
<!--Head Ends-->
<!--Body-->
<body>
    <div class="container login-container-1 animated fadeInDown">
		<div class="row">
			<div class="col-md-6 login-form-1 align-self-center">
				<div>
					<img id="logo" class="d-flex mx-auto"
						 src="<?=base_url('assets/img/uonlive/UonLIVE_logo.png'); ?>" style="max-height:150px;max-width:150px;margin-bottom:0.5em!important;">
				</div>
				<div id="contact" class="hidden-xs hidden-sm">
					<div class="row">
						<div class="hidden-xs hidden-sm col-md-1">&nbsp;</div>
						<div class="col-xs-2 col-md-1 contact-item-right">
							<img src="<?=base_url('assets/at-admin/img/icon-email.png');?>"/>
						</div>
						<!--<div class="col-md-10 contact-item row email">聯絡電郵</div>-->
						<div class="col-md-10 contact-item row email">it@uonlive.com</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 login-form-2 my-auto">
				<div class="vl d-none d-md-block" style="border-color: #ef4130;"></div>
				<?php
				if ($errno != 0) {
				?>
				<div class="alert alert-danger">
					<i class="fa-fw fa fa-times"></i>
					<strong>登入錯誤：</strong> <?=$alert_message;?>
				</div>
				<?php
				}
				?>
				<form method="post" name="form_reg" id="form_reg" action="<?=base_url('at-admin/login');?>">
					<h3 class="text-center" style="color: #ef4130; font-weight: 600; padding: 1em;">管理員登錄</h3>
					<div class="form-group row">
						<label for="userName" class="col-12 col-xl-3 col-form-label" style="color:#666666;">用戶名稱
							:</label>
						<div class="col-12 col-xl-9">
							<input id="userName" name="userName" type="text" style="background-color : #d1d1d1;" class="form-control" placeholder="請輸入用戶名稱" value="">
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="col-12 col-xl-3 col-form-label" style="color:#666666;">密碼
							:</label>
						<div class="col-12 col-xl-9">
							<input id="password" name="password" type="password" style="background-color : #d1d1d1;" class="form-control" placeholder="請輸入密碼" value="">
						</div>
					</div>
					<div class="form-group row ">
						<div class="col-sm-12 mt-4">
							<div class="text-center">
								<input type="submit" class="btnSubmit" value="登入">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="text-center">UonLIVE Asia Ltd</p>
			</div>
		</div>
	</div>

    <!--Basic Scripts-->
    <script src="<?=base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?=base_url('assets/vendor/bootstrap4/js/bootstrap.min.js');?>"></script>
    <script src="<?=base_url('assets/js/slimscroll/jquery.slimscroll.min.js');?>"></script>

    <!--Beyond Scripts-->
    <script src="<?=base_url('assets/js/beyond.js');?>"></script>

    
</body>
<!--Body Ends-->
</html>
