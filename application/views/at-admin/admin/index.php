<div class="row">
	<div class="col-xs-12 col-md-12">
		<div class="widget">
			<div class="widget-header ">
				<span class="widget-caption"></span>
				<div class="widget-buttons">
					<a href="#" data-toggle="maximize">
						<i class="fa fa-expand"></i>
					</a>
					<!--
										<a href="#" data-toggle="collapse">
											<i class="fa fa-minus"></i>
										</a>
										<a href="#" data-toggle="dispose">
											<i class="fa fa-times"></i>
										</a>
					-->
				</div>
			</div>
			<div class="widget-body">
				<div class="table-toolbar">
					<a id="editabledatatable_new" href="javascript:void(0);" class="btn btn-default">
						新增管理員
					</a>
					<div class="btn-group pull-right">
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered dataTable no-footer"
					   id="editabledatatable">
					<thead>
					<tr role="row">
						<th>
							登入名稱
						</th>
						<th>
							電郵地址
						</th>
						<th>
							創建日期
						</th>
						<th>
							最後更新日期
						</th>
						<th>

						</th>
					</tr>
					</thead>

					<tbody>
					<?php
						foreach ($data as $row) {
					?>
					<tr>
						<td><?=$row['username'];?></td>
						<td><?=$row['email'];?></td>
						<td><?=$row['created_at'];?></td>
						<td><?=$row['updated_at'];?></td>
						<td>
							<a href="<?=base_url('at-admin/admin/edit/' . $row['id']);?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
							<?php
							if ($row['id'] != 1) {
							?>
							<a href="#" class="btn btn-danger btn-xs delete" data-id="<?=$row['id'];?>"><i class="fa fa-trash-o"></i> Delete</a>
							<?php
							}
							?>
						</td>
					</tr>
					<?php
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
