<div class="row">
	<div class="col-xs-12 col-md-12">
		<div class="widget">
			<div class="widget-body">
				<table class="table table-striped">
					<tbody>
					<?php
					if ($alert_message != "") {
						if ($errno == 0) {
							$msg_type = 'success';
							$msg_icon = 'check';
						}
						else {
							$msg_type = 'danger';
							$msg_icon = 'times';
						}
					?>
					<tr>
						<td>
							<div class="alert alert-<?=$msg_type;?> fade in">
								<i class="fa-fw fa fa-<?=$msg_icon;?>"></i>
								<?=$alert_message;?>
							</div>
						</td>
					</tr>
					<?php
					}
					?>
					<tr>
						<td>
							<form class="phpForm well form-horizontal" id="admin-form" method="post" action="<?=base_url('at-admin/admin/edit/' . $data['id']);?>">
								<input type="hidden" name="<?=$csrf_name;?>" value="<?=$csrf_hash;?>" />
								<input type="hidden" name="id" value="<?=$data['id'];?>" />
								<fieldset>
									<div class="form-group">
										<label class="col-md-2 control-label">登入名稱</label>
										<div class="col-md-10 inputGroupContainer">
											<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="username" name="username" placeholder="請輸入登入名稱" class="form-control" required="true" value="<?=$data['username'];?>" type="text"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">電郵地址</label>
										<div class="col-md-10 inputGroupContainer">
											<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="email" name="email" placeholder="請輸入電郵地址" class="form-control" required="true" value="<?=$data['email'];?>" type="email"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">密碼</label>
										<div class="col-md-10 inputGroupContainer">
											<div class="input-group">
												<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span><input id="password" name="password" placeholder="請輸入登入密碼" id="password-input" aria-describedby="passwordHelp" class="password-strength__input form-control" style="width: auto;" required="true" value="" type="password" />
												<div class="input-group-append">
													<button class="password-strength__visibility btn btn-outline-secondary" type="button"><span class="password-strength__visibility-icon" data-visible="hidden"><i class="glyphicon glyphicon-eye-open"></i></span><span class="password-strength__visibility-icon js-hidden" data-visible="visible"><i class="glyphicon glyphicon-eye-close"></i></span></button>
												</div>
											</div><small class="password-strength__error text-danger js-hidden">不容許使用這個字符</small><small class="form-text text-muted mt-2" id="passwordHelp">最少需要8個字符，必須包含英文字母、數字及符號</small>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-2"></div>
										<div class="col-md-10">
											<div class="password-strength__bar-block progress mb-4">
												<div class="password-strength__bar progress-bar bg-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">確認密碼</label>
										<div class="col-md-10 inputGroupContainer">
											<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span><input id="confirm" name="confirm" placeholder="請再輸入密碼確認" class="password-strength__confirm form-control" required="false" value="" type="password"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label"></label>
										<div class="col-md-10 inputGroupContainer">
											<button id="btnSubmit" class="password-strength__submit btn btn-footer">提交</button>
											<button id="btnReset" class="btn btn-footer">重設</button>
										</div>
									</div>
								</fieldset>
							</form>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
