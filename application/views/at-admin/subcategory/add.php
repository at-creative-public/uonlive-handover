				<div class="form-container">
					<form id="phpform" class="form-horizontal" action="<?=base_url('at-admin/subcategory/insert');?>" method="post">
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="tag_code">類別*</label></div>
							<div class="col-sm-8 mas-box">
								<select id="tag_code" type="text" class="form-select mas-input"
																 name="tag_code" required>
								<?php
								foreach ($tags as $tag) {
									echo sprintf('<option value="%s">%s</option>', $tag['code'], $tag['name']);
								}
								?>
								</select>
							</div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="name">子類別名稱*</label></div>
							<div class="col-sm-8 mas-box"><input id="name" type="text" class="form-control mas-input"
																 name="name" required
																 placeholder="請輸入子類別名稱"/>
							</div>
						</div>
						<div class="form-footer">
							<div class="col-sm-3">&nbsp;</div>
							<div class="col-sm-9 mas-box">
								<a id="btnSubmit" class="btn btn-footer" href="javascript:void(0);">提交</a>
								<a class="btn btn-footer reset" href="javascript:window.location.reload(true)">重設</a>
							</div>
						</div>
					</form>
				</div>
