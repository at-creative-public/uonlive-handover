<div class="row">
	<div class="col-xs-12 col-md-12">
		<div class="widget">
			<div class="widget-header ">
				<span class="widget-caption"></span>
				<div class="widget-buttons">
					<a href="#" data-toggle="maximize">
						<i class="fa fa-expand"></i>
					</a>
					<!--
										<a href="#" data-toggle="collapse">
											<i class="fa fa-minus"></i>
										</a>
										<a href="#" data-toggle="dispose">
											<i class="fa fa-times"></i>
										</a>
					-->
				</div>
			</div>
			<div class="widget-body">
				<div class="table-toolbar">
					<div class="btn-group pull-right">
					</div>
				</div>
				<table class="table table-striped table-hover table-bordered dataTable no-footer"
					   id="editabledatatable">
					<thead>
					<tr role="row">
						<th>
						</th>
						<th>
							會員名稱
						</th>
						<th>
							訂單號碼
						</th>
						<th>
							項目名稱
						</th>
						<th class="text-right">
							費用
						</th>
						<th>
							繳費日期
						</th>
						<th>
							收費狀態
						</th>
					</tr>
					</thead>

					<tbody>
					<?php
					foreach ($data as $idx => $row) {
					?>
					<tr>
						<td><?=$idx + 1;?>.</td>
						<td><?=$row['first_name'] . ', ' . $row['last_name'];?></td>
						<td><?=$row['order_no'];?></td>
						<td><?=$row['item'] . sprintf('(%s - %s)', date("Y-m-d", $row['effective_date']), date("Y-m-d", $row['expiry_date'])) ;?></td>
						<td class="text-right">HK$ <?=number_format($row['amount']);?></td>
						<td><?=$row['created_at'];?></td>
						<td>
							<?php
								switch($row['payment_status']) {
									case 'COMPLETE':
										echo '<span class="text-success">成功</span>';
										break;
									case 'NOT_START':
										echo '<span class="text-danger">失敗</span>';
										break;
									}
							?>
						</td>
					</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
