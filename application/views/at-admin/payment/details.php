				<div class="form-container">
					<form id="phpform" class="form-horizontal">
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="first_name">名字</label></div>
							<div class="col-sm-3 mas-box"><input id="first_name" type="text" class="form-control mas-input"
																 value="<?=$data['first_name'];?>" disabled="disabled" />
							</div>
							<div class="col-sm-2 text-right mas-title"><label
										for="last_name">姓氏</label></div>
							<div class="col-sm-3 mas-box"><input id="last_name" type="text" class="form-control mas-input"
																 value="<?=$data['last_name'];?>" disabled="disabled" />
							</div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="email">電郵地址</label></div>
							<div class="col-sm-3 mas-box"><input id="email" type="text" class="form-control mas-input"
																 value="<?=$data['email'];?>" disabled="disabled" />
							</div>
							<div class="col-sm-2 text-right mas-title"><label
										for="phone">電話</label></div>
							<div class="col-sm-3 mas-box"><input id="phone" type="text" class="form-control mas-input"
																 value="<?=$data['phone'];?>" disabled="disabled" />
							</div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="occupation">職業</label></div>
							<div class="col-sm-3 mas-box"><input id="occupation" type="text" class="form-control mas-input"
																 value="<?=$data['occupation'];?>" disabled="disabled" />
							</div>
							<div class="col-sm-5 text-right mas-title"></div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="resident_area_string">家居地區</label></div>
							<div class="col-sm-3 mas-box"><input id="resident_area_string" type="text" class="form-control mas-input"
																 value="<?=$data['resident_area_string'];?>" disabled="disabled" />
							</div>
							<div class="col-sm-2 text-right mas-title"><label
										for="resident_building">家居大廈名稱</label></div>
							<div class="col-sm-3 mas-box"><input id="resident_building" type="text" class="form-control mas-input"
																 value="<?=$data['resident_building'];?>" disabled="disabled" />
							</div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="business_area_string">公司地區</label></div>
							<div class="col-sm-3 mas-box"><input id="business_area_string" type="text" class="form-control mas-input"
																 value="<?=$data['business_area_string'];?>" disabled="disabled" />
							</div>
							<div class="col-sm-2 text-right mas-title"><label
										for="business_building">公司大廈名稱</label></div>
							<div class="col-sm-3 mas-box"><input id="business_building" type="text" class="form-control mas-input"
																 value="<?=$data['business_building'];?>" disabled="disabled" />
							</div>
						</div>
						<div class="form-group mas-col">
							<div class="col-sm-2 text-right mas-title"><label
										for="created_at">註冊日期</label></div>
							<div class="col-sm-3 mas-box"><input id="created_at" type="text" class="form-control mas-input"
																 value="<?=date("Y-m-d", $data['created_at']);?>" disabled="disabled" />
							</div>
							<div class="col-sm-2 text-right mas-title"><label
										for="updated_at">最後更新日期</label></div>
							<div class="col-sm-3 mas-box"><input id="updated_at" type="text" class="form-control mas-input"
																 value="<?=($data['updated_at'] == 0) ? '--' : date("Y-m-d", $data['updated_at']);?>" disabled="disabled" />
							</div>
						</div>
						<div class="form-footer">
							<div class="col-sm-3">&nbsp;</div>
							<div class="col-sm-9 mas-box">
								<a href="<?=base_url('at-admin/member');?>" class="btn btn-footer">回主頁</a>
							</div>
						</div>
					</form>
				</div>
