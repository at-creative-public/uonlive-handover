<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SendGrid as Client;
use SendGrid\Mail\Mail;
use SendGrid\Response;

class Twilio_sendgrid {
    // "http://stsa.at-appmaker.com/" was successfully created and added to the next step.
    private $SENDGRID_API_KEY;

	public function __construct()
	{
        $CI = get_instance();
        $this->SENDGRID_API_KEY = $CI->config->item('uonlive')['sendgrid']['api_key'];
	}
    /**
     * Create a Sendgrid Mail instance
     * 
     * $email->setFrom("test@example.com", "Example User");c
     * $email->setSubject("Sending with SendGrid is Fun");
     * $email->addTo("test@example.com", "Example User");
     * $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
     * $email->addContent(
     *    "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
     * );
     *
     * @return Mail
     */
    public function create() {
        return new Mail();
    }

    public function send(Mail $mail, $send = true) {
        if (!$send) {
            return [];
        }
        
        $_sendgrid = new Client($this->SENDGRID_API_KEY);
        try {
            /**@var Response $response */
            $response = $_sendgrid->send($mail);

            return [
                'statusCode' => $response->statusCode(),
                'header' => $response->headers(),
                'body' => $response->body()
            ];
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }
}
