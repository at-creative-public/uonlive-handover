<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_AdminController extends CI_Controller
{

	protected $viewParams = [
		'errno' => 0,
		'alert_message' => '',
		'showNotification' => false,
		'showChat' => false,
		'showMail' => false,
		'showTask' => false,
		'showSettings' => false,
		'changeTheme' => false,
		'breadcrumb' => [],
		'csses' => [],
		'jses' => [],
		'scripts' => [],
		'sidebar' => [],
	];

	public function __construct()
	{
		parent::__construct();
		$this->viewParams['site_settings'] = $this->config->item('site_settings');
		$this->viewParams['csrf_name'] = $this->security->get_csrf_token_name();
        $this->viewParams['csrf_hash'] = $this->security->get_csrf_hash();
		$this->viewParams['hash'] = hash('md5', uniqid('', true));
		$this->viewParams['bcc_emails'] = ['jonsontech01@gmail.com', 'admin@johnsir.net', 'arvid@at-creative.com', 'daniel@at-creative.com'];

        $this->viewParams['breadcrumb'][] = [
			'name' => '主頁',
			'url' => base_url('at-admin/dashboard'),
		];
		$this->viewParams['sidebar'] = [
			'dashboard' => [
				'name' => '儀表板',
				'url' => base_url('at-admin/dashboard'),
				'icon' => 'fa fa-home',
			],
			'category' => [
				'name' => '類別管理',
				'url' => base_url('at-admin/category'),
				'icon' => 'fa fa-user-secret',
			],
			'subcategory' => [
				'name' => '子類別管理',
				'url' => base_url('at-admin/subcategory'),
				'icon' => 'fa fa-user-secret',
			],
			'keyword' => [
				'name' => '關鍵字管理',
				'url' => base_url('at-admin/keyword'),
				'icon' => 'fa fa-user-secret',
			],
			'member' => [
				'name' => '會員列表',
				'url' => base_url('at-admin/member'),
				'icon' => 'fa fa-user-secret',
			],
			'payment' => [
				'name' => '收費列表',
				'url' => base_url('at-admin/payment'),
				'icon' => 'fa fa-user-secret',
			],
			// 'meeting' => [
			// 	'name' => '例會管理',
			// 	'url' => base_url('at-admin/meeting'),
			// 	'icon' => 'fa fa-user-secret',
			// ],
			// 'activity' => [
			// 	'name' => '活動管理',
			// 	'url' => base_url('at-admin/activity'),
			// 	'icon' => 'fa fa-user-secret',
			// ],
			// 'signup' => [
			// 	'name' => '點名管理',
			// 	'url' => base_url('at-admin/signup'),
			// 	'icon' => 'fa fa-user-secret',
			// ],
			// 'whatsnew' => [
			// 	'name' => '最新消息管理',
			// 	'url' => base_url('at-admin/whatsnew'),
			// 	'icon' => 'fa fa-user-secret',
			// ],
			// 'media' => [
			// 	'name' => '媒體管理',
			// 	'url' => base_url('at-admin/media'),
			// 	'icon' => 'fa fa-user-secret',
			// ],
			'admin' => [
				'name' => '管理員帳戶',
				'url' => base_url('at-admin/admin'),
				'icon' => 'fa fa-user-secret',
			],
// 			'student' => [
// 				'name' => '訂閱者帳戶',
// 				'url' => base_url('at-admin/subscriber'),
// 				'icon' => 'fa fa-child',
// 			],
// 			'purchase' => [
// 				'name' => '產品購買',
// 				'url' => base_url('at-admin/purchase'),
// 				'icon' => 'fa fa-gift',
// 			],
// //			'wishlist' => [
// //				'name' => '喜愛清單',
// //				'url' => base_url('at-admin/wishlist'),
// //				'icon' => 'fa fa-heart',
// //			],
// //			'category' => [
// //				'name' => '課程分類',
// //				'url' => base_url('at-admin/category'),
// //				'icon' => 'fa fa-list-alt',
// //			],
// 			'landing' => [
// 				'name' => '登錄頁管理',
// 				'url' => base_url('at-admin/landing'),
// 				'icon' => 'fa fa-book',
// 			],
// 			'payment' => [
// 				'name' => '計劃管理',
// 				'url' => base_url('at-admin/payment'),
// 				'icon' => 'fa fa-money',
// 			],
// 			'payment_method' => [
// 				'name' => '付款方式管理',
// 				'url' => base_url('at-admin/payment_method'),
// 				'icon' => 'fa fa-money',
// 			],
// 			'media' => [
// 				'name' => '媒體管理',
// 				'url' => base_url('at-admin/media'),
// 				'icon' => 'fa fa-money',
// 			],
// 			'email' => [
// 				'name' => '推廣電郵管理',
// 				'url' => base_url('at-admin/email'),
// 				'icon' => 'fa fa-envelope',
// 			],
			'logout' => [
				'name' => '登出',
				'url' => base_url('at-admin/admin/logout'),
				'icon' => 'glyphicon glyphicon-log-out',
			],
		];
	}

	public function checkLoginStatus()
	{
		// print_r ($this->session->userdata('len_user'));
		// exit;
		if (! $this->session->has_userdata('uonlive_admin_user')) {
			header('location: ' . base_url('at-admin/login'));
			exit;
		}
	}

}

