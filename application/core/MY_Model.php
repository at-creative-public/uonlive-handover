<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Model extends CI_Model
{

	public $table = [];

	public function __construct()
	{
		parent::__construct();
	}

	public function encrypt_password($password)
	{
		return sha1($password);
	}

}

