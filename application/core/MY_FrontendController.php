<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_FrontendController extends CI_Controller
{

	public $_data = [];
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		// $this->load->helper('url');
		$this->data['css_file'] = [];
		$this->data['js_file'] = [];
		$this->data['scripts'] = [];
		$this->data['loggedIn'] = false;
        $this->data['hash'] = hash('md5', uniqid('', true));
		$this->load->model('Website_setting_model', 'website_settings');
		$this->data['website_settings'] = $this->website_settings->get();
		$this->load->model('Tag_list_model', 'tag_list');
		$this->data['tag_lists'] = $this->tag_list->get();
		$this->data['thumbnails'] = [];
	}

	private function _generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function checkLogin()
	{
		if ($this->session->has_userdata('uonlive_user')) {
			return true;
		}
		return false;
	}

}

