<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// frontend
$route['service/nearby_map'] = '/service/nearby_map';
$route['service/nearby_list'] = '/service/nearby_list';
$route['service/details/(:any)'] = '/service/details/$1';
$route['service/(:any)'] = '/service/index/$1';
$route['member/conversion_details/(:any)'] = '/member/conversion_details/$1';
// $route["preview/(:any)"] = "/preview/$1";
// $route["index"] = "/front/home/index";
// $route["/"] = "/front/home/index";
// $route['index'] = "front/home/index";
// $route['(:any)'] = "front/page_handler/index/$1";

$route['at-admin'] = 'at-admin/login/index';
$route['at-admin/category/edit/(:any)'] = 'at-admin/category/edit/$1';
$route['at-admin/subcategory/edit/(:any)'] = 'at-admin/subcategory/edit/$1';
$route['at-admin/keyword/edit/(:any)'] = 'at-admin/keyword/edit/$1';
$route['at-admin/member/details/(:any)'] = 'at-admin/member/details/$1';
// $route['at-admin/meeting/edit/(:any)'] = 'at-admin/meeting/edit/$1';
// $route['at-admin/visitor/edit/(:any)'] = 'at-admin/visitor/edit/$1';
// $route['at-admin/signup/event/(:any)'] = 'at-admin/signup/event/$1';
// $route['at-admin/whatsnew/edit/(:any)'] = 'at-admin/whatsnew/edit/$1';
// $route['at-admin/media/edit/(:any)'] = 'at-admin/media/edit/$1';
