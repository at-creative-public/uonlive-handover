<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['uonlive'] = [
    'socials' => [
        'whatsapp', 'facebook', 'instagram', 'youtube',
    ],
    'map_api_key' => '[Google Maps API key]',
    'default_location' => [
        'coords' => [
            'latitude' => 22.3193,
            'longitude' => 114.1694,
        ],
    ],
    'stripe' => [
        "secret_key"      => "[Stripe secret key]",
        "publishable_key" => "[Stripe publishable key]",
    ],
    'sendgrid' => [
        'api_key' => '[Sendgrid API key]',
        'sender' => '[Sender email]',
    ],
    'social_network' => [
        'facebook' => 'example.profile.3',
        'instagram' => '',
        'youtube' => '',
        'whatsapp' => '85298765432',
    ],
    'upgrade_fee' => 300,
];

$config['default_app'] = 'uonlive';
$config['site_settings'] = [
	'default_app' => $config['default_app'],
	'company_name' => 'UonLIVE Asia Ltd',
	'company_name_zh_cn' => 'UonLIVE Asia Ltd',
	'company_email' => 'it@uonlive.com',
];
