<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Business_registration extends MY_FrontendController
{

	private $main_settings;

	public function __construct()
	{
		parent::__construct();
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '.css');
		// $this->data['js_file'][] = base_url('assets/front/js/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.js');
		$this->load->model("Home_setting_model", 'home_setting');
		$main_id = $this->home_setting->get_data_by_uri($this->router->fetch_class());
		$this->main_settings = [];
		if ($main_id !== false)
		{
			$this->load->model('Page_settings_model', 'page_settings');
			$this->main_settings = $this->page_settings->get_page_settings($main_id, 0);
		}
	}

	public function index()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		if (array_key_exists('member_id', $this->session->userdata('uonlive_user')['provider'])) {
			header('location: ' . base_url('provider/info'));
		}
		else {
			header('location: ' . base_url('member/info'));
		}
	}

	public function __index()
	{
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
		];
		$this->data['thumbnails'] = [
			[
				'title' => '商戶登記',
				'url' => '',
			],
		];
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

}

