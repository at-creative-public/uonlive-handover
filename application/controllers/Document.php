<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Document extends MY_FrontendController
{

	private $main_settings;

	public function __construct()
	{
		parent::__construct();
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '.css');
		// $this->data['js_file'][] = base_url('assets/front/js/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.js');
		$this->load->model("Home_setting_model", 'home_setting');
		$main_id = $this->home_setting->get_data_by_uri($this->router->fetch_class());
		$this->main_settings = [];
		if ($main_id !== false)
		{
			$this->load->model('Page_settings_model', 'page_settings');
			$this->main_settings = $this->page_settings->get_page_settings($main_id, 0);
		}
	}

	public function privacy()
	{
		$data = [
			// "required_css" => false,
			// "nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			// "use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			// "need_carousel" => false,
			"controller" => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
		];

		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

	public function terms_of_use()
	{
		$data = [
			// "required_css" => false,
			// "nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			// "use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			// "need_carousel" => false,
			"controller" => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
		];

		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

	public function q_and_a()
	{
		$data = [
			// "required_css" => false,
			// "nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			// "use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			// "need_carousel" => false,
			"controller" => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
		];

		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

}

