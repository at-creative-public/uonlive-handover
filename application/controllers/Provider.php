<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Provider extends MY_FrontendController
{

	private $main_settings;
	private $_provider;

	public function __construct()
	{
		parent::__construct();
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$this->load->model('Provider_model', 'provider');
		$this->_provider = $this->provider->get($this->session->userdata('uonlive_user')['id']);
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '.css');
		// $this->data['js_file'][] = base_url('assets/front/js/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.js');
		$this->load->model("Home_setting_model", 'home_setting');
		$main_id = $this->home_setting->get_data_by_uri($this->router->fetch_class());
		$this->main_settings = [];
		if ($main_id !== false)
		{
			$this->load->model('Page_settings_model', 'page_settings');
			$this->main_settings = $this->page_settings->get_page_settings($main_id, 0);
		}
	}

	public function info()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];
		$update = false;

		if ($this->input->post()) {
			$this->load->model('Member_model', 'member');
			$request = $this->input->post();
			$this->provider->update($request);
			$this->session->set_userdata('uonlive_user', $this->member->getMemberByEmail($request['login_name']));
			$this->_provider = $this->provider->get($this->session->userdata('uonlive_user')['id']);
			$update = true;
		}
		if ($this->_provider['brand_required'] == '') {
			$this->_provider['brand_required'] = '{}';
		}
		$this->_provider['brand_required_array'] = json_decode($this->_provider['brand_required'], true);

		$this->load->model('District_model', 'district');
		$this->load->model('Tag_list_model', 'tag_list');
		$this->load->model('Tag_2nd_list_model', 'tag_2nd_list');
		$this->load->model('Keyword_model', 'keyword');
		$json = [];
		$tag_list = $this->tag_list->get();
		foreach ($tag_list as $idx => $tag) {
			$json[$tag['code']] = $tag;
			$tag_2nd = $this->tag_2nd_list->getByTagCode($tag['code']);
			$json[$tag['code']]['tag_2nd'] = [];
			foreach ($tag_2nd as $idx1 => $tag1) {
				$json[$tag['code']]['tag_2nd'][$tag1['code']] = $tag1;
				$json[$tag['code']]['tag_2nd'][$tag1['code']]['keywords'] = $this->keyword->getByTagCode($tag['code'], $tag1['code']);
			}
		}
		if ($this->_provider['tag_list_id'] == 0) {
			$_tag = $this->tag_list->getFirstTag();
		}
		else {
			$_tag = $this->tag_list->getById($this->_provider['tag_list_id']);
		}
		if ($this->_provider['tag_2nd_list_id'] == 0) {
			$tag_2nd_list = $this->tag_2nd_list->getByTagCode($_tag['code']);
			if ($tag_2nd_list) {
				$_tag_2nd = $tag_2nd_list[0];
			}
			else {
				$_tag_2nd = [];
			}
		}
		else {
			$tag_2nd_list = $this->tag_2nd_list->getById($this->_provider['tag_2nd_list_id']);
			if ($tag_2nd_list) {
				$_tag_2nd = $tag_2nd_list[0];
			}
			else {
				$_tag_2nd = [];
			}
		}
		$keywords = [];
		if (count($_tag_2nd) > 0) {
			$keywords = $this->keyword->getByTagCode($_tag['code'], $_tag_2nd['code']);
		}
		$this->data['scripts'][] = 'var tag_2nd = ' . json_encode($json, JSON_UNESCAPED_UNICODE);
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'areas' => $this->district->getAll(),
			'categories' => $tag_list,
			'subcategories' => $tag_2nd_list,
			'keywords' => $keywords,
			'provider' => $this->_provider,
			'update' => $update,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '服務提供者',
				'url' => base_url('provider/info'),
			],
			[
				'title' => '網店簡介',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
//		$this->data['css_file'][] = base_url('assets/css/beyond.min.css');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/axios/axios.min.js'),
			'extra' => '',
		];
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['scripts'][] = 'var map_api_key = \'' . $this->config->item('uonlive')['map_api_key'] . '\'';
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function browsing_history()
	{
		$this->load->model('Browsing_history_model', 'history');
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
			'histories' => [],
			// 'histories' => $this->history->getByProvider($this->_provider['id']),
		];
		$this->data['loggedIn'] = true;
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function conversion_history()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
		];
		$request = $this->input->get();
		$type = "";
		if (array_key_exists('type', $request)) {
			if (in_array($request['type'], ['unread', 'read'])) {
				$type = $request['type'];
			}
		}
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->model('Conversion_model', 'conversion');
		$data['conversions'] = $this->conversion->getByProvider($this->_provider['id']);
		$data['type'] = $type;
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function conversion_details()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$title = urldecode($this->uri->segment(3));
		if ($title == "") {
			header('location: ' . base_url('provider/conversion_history'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		$this->load->model('Conversion_model', 'conversion');
		$conversion = $this->conversion->getByUri($title);
		if (! $conversion) {
			header('location: ' . base_url('provider/conversion_history'));
			exit;
		}
		$this->conversion->markRead($conversion['id']);
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"provider" => $this->_provider,
			"conversion" => $conversion,
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function payment_history()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"provider" => $this->_provider,
			"histories" => [],
		];
		$this->load->model('Purchasinginfo_model', 'purchasinginfo');
		$purchasing = $this->purchasinginfo->getByMember();
		foreach ($purchasing as $purchase) {
			$data['histories'][] = [
				'date' => $purchase['created_at'],
				'item' => $purchase['item'] . '<br />' . date('Y-m-d', $purchase['effective_date']) . ' - ' . date('Y-m-d', $purchase['expiry_date']),
				'amount' => $purchase['amount'],
				'payment_status' => $purchase['payment_status'],
				'status' => ($purchase['payment_status'] == 'COMPLETE') ? '已付款' : '付款不成功',
				'method' => 'Stripe',
			];
		}
		$this->data['loggedIn'] = true;
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function media()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		$update = false;

		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
			'update' => $update,
		];
		$this->load->model('Media_model', 'media');
		$data['medias'] = $this->media->getAll();
		// $data['medias'] = [
		// 	[
		// 		'type' => 'photo',
		// 		'photo' => 'dummy01.jpg',
		// 		'caption' => '',
		// 		'date' => '2022-02-22 12:53:40',
		// 		'show' => 1,
		// 	],
		// 	[
		// 		'type' => 'news',
		// 		'photo' => '',
		// 		'caption' => '日本IPSA 茵芙莎美膚微整機能液 (流金水)',
		// 		'date' => '2021-12-22 11:22:46',
		// 		'show' => 0,
		// 	],
		// 	[
		// 		'type' => 'news',
		// 		'photo' => '',
		// 		'caption' => 'Bria英國BB濕疹專用乳霜 - 無藥性改善...',
		// 		'date' => '2021-12-21 12:53:40',
		// 		'show' => 1,
		// 	],
		// 	[
		// 		'type' => 'video',
		// 		'photo' => '0.jpg',
		// 		'caption' => '輕鬆美膚 - Youtube',
		// 		'date' => '2021-12-21 12:53:40',
		// 		'show' => 0,
		// 	],
		// 	[
		// 		'type' => 'photo',
		// 		'photo' => 'dummy02.jpg',
		// 		'caption' => '',
		// 		'date' => '2022-02-22 12:53:40',
		// 		'show' => 1,
		// 	],
		// 	[
		// 		'type' => 'video',
		// 		'photo' => '1.jpg',
		// 		'caption' => '油肌保養輕鬆美膚兩年成...',
		// 		'date' => '2021-12-21 12:53:40',
		// 		'show' => 0,
		// 	],
		// ];
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['thumbnails'] = [
			[
				'title' => '服務提供者',
				'url' => base_url('provider/info'),
			],
			[
				'title' => '媒體管理',
				'url' => '',
			],
		];
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function add_media()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];
		// $this->data['loggedIn'] = true;
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		// $this->view('front/provider_add_media', array_merge($data, $this->data));
		$update = false;

		if ($this->input->post()) {
			$request = $this->input->post();
			if (! array_key_exists('show_in_shop', $request)) {
				$request['show_in_shop'] = 0;
			}
			$this->load->model('Media_model', 'media');
			$this->media->create($request);
			header('location: ' . base_url('provider/media'));
			exit;
		}
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
			'update' => $update,
		];
		$this->load->model('Tag_list_model', 'tag');
		$this->data['tag_lists'] = $this->tag->get();
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['thumbnails'] = [
			[
				'title' => '服務提供者',
				'url' => base_url('provider/info'),
			],
			[
				'title' => '媒體管理',
				'url' => base_url('provider/media'),
			],
			[
				'title' => '新增媒體',
				'url' => '',
			],
		];
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function modify_media()
	{
		$update = false;

		if ($this->input->post()) {
			$request = $this->input->post();
			if (! array_key_exists('show_in_shop', $request)) {
				$request['show_in_shop'] = 0;
			}
			$this->load->model('Media_model', 'media');
			$this->media->update($request);
			header('location: ' . base_url('provider/media'));
			exit;
		}

		$token = $this->uri->segment(3);
		$this->load->model('Media_model', 'media');
		$media = $this->media->getByToken($token);
		if (count($media) == 0) {
			header('location: ' . base_url('provider/media'));
			exit;
		}

		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
			'media' => $media,
			'update' => $update,
		];

		$this->load->model('Tag_list_model', 'tag');
		$this->data['tag_lists'] = $this->tag->get();
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['thumbnails'] = [
			[
				'title' => '服務提供者',
				'url' => base_url('provider/info'),
			],
			[
				'title' => '媒體管理',
				'url' => base_url('provider/media'),
			],
			[
				'title' => '修改媒體',
				'url' => '',
			],
		];
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function service()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];
		$update = false;

		$this->load->model('Provider_service_model', 'service');
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
			'update' => $update,
		];
		$data['services'] = [];
		$services = $this->service->getAll($this->_provider['id']);
		foreach ($services as $ser) {
			$data['services'][] = [
				'id' => $ser['id'],
				'displayorder' => $ser['displayorder'],
				'token' => $ser['token'],
				'photo' => $ser['photos'][0]['photo'],
				'name' => $ser['name'],
				'last_updated' => date("Y-m-d H:i:s", ($ser['updated_at'] == 0) ? $ser['created_at'] : $ser['updated_at']),
				'show' => $ser['visible'],
			];
		}
		// $data['services'] = [
		// 	[
		// 		'displayorder' => 1,
		// 		'photo' => 'dummy01.jpg',
		// 		'name' => 'Soak Gel手 (單色)',
		// 		'last_updated' => '2022-02-22 12:53:40',
		// 		'show' => 1,
		// 	],
		// 	[
		// 		'displayorder' => 2,
		// 		'photo' => 'dummy02.jpg',
		// 		'name' => 'Soak Gel腳 (單色)',
		// 		'last_updated' => '2022-02-22 11:22:40',
		// 		'show' => 1,
		// 	],
		// 	[
		// 		'displayorder' => 3,
		// 		'photo' => 'dummy03.jpg',
		// 		'name' => '美甲修護',
		// 		'last_updated' => '2022-02-21 12:53:40',
		// 		'show' => 0,
		// 	],
		// 	[
		// 		'displayorder' => 4,
		// 		'photo' => 'dummy04.jpg',
		// 		'name' => '美甲造型咨詢',
		// 		'last_updated' => '2022-02-21 12:43:40',
		// 		'show' => 0,
		// 	],
		// ];
		$this->data['thumbnails'] = [
			[
				'title' => '服務提供者',
				'url' => base_url('provider/info'),
			],
			[
				'title' => '服務管理',
				'url' => '',
			],
		];
		$this->data['css_file'][] = base_url('assets/css/drag_list.css');
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/jquery-ui/jquery-ui.min.js'),
			'extra' => '',
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function add_service()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];
		// $this->data['loggedIn'] = true;
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		// $this->view('front/provider_add_service', array_merge($data, $this->data));
		if ($this->input->post()) {
			$request = $this->input->post();
			if (! array_key_exists('show_in_shop', $request)) {
				$request['show_in_shop'] = 0;
			}
			else {
				$request['show_in_shop'] = 1;				
			}
			$this->load->model('Provider_service_model', 'service');
			$this->service->create($request);
			header('location: ' . base_url('provider/service'));
			exit;
		}
		$update = false;

		// $providerInfo = $this->provider->get($this->session->userdata('uonlive_user')['id']);
		$this->load->model('Provider_service_model', 'provider_service');
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
			'service' => $this->provider_service->get($this->_provider['id']),
			'update' => $update,
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['thumbnails'] = [
			[
				'title' => '服務提供者',
				'url' => base_url('provider/info'),
			],
			[
				'title' => '服務管理',
				'url' => base_url('provider/service'),
			],
			[
				'title' => '新增服務',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function modify_service()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];
		// $this->data['loggedIn'] = true;
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		// $this->view('front/provider_add_service', array_merge($data, $this->data));
		if ($this->input->post()) {
			$request = $this->input->post();
			if (! array_key_exists('show_in_shop', $request)) {
				$request['show_in_shop'] = 0;
			}
			else {
				$request['show_in_shop'] = 1;
			}
			$this->load->model('Provider_service_model', 'service');
			$this->service->update($request);
			header('location: ' . base_url('provider/service'));
			exit;
		}
		$update = false;

		$token = $this->uri->segment(3);
		$this->load->model('Provider_service_model', 'provider_service');
		$service = $this->provider_service->getByToken($token);
		if (count($service) == 0) {
			header('location: ' . base_url('provider/service'));
			exit;
		}

		// $providerInfo = $this->provider->get($this->session->userdata('uonlive_user')['id']);
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $this->_provider,
			'service' => $this->provider_service->get($this->_provider['id']),
			'update' => $update,
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['thumbnails'] = [
			[
				'title' => '服務提供者',
				'url' => base_url('provider/info'),
			],
			[
				'title' => '服務管理',
				'url' => base_url('provider/service'),
			],
			[
				'title' => '修改服務',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

}

