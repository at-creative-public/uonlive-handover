<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends MY_FrontendController
{

	private $main_settings;

	public function __construct()
	{
		parent::__construct();
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '.css');
		// $this->data['js_file'][] = base_url('assets/front/js/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.js');
		$this->load->model("Home_setting_model", 'home_setting');
		$main_id = $this->home_setting->get_data_by_uri($this->router->fetch_class());
		$this->main_settings = [];
		if ($main_id !== false)
		{
			$this->load->model('Page_settings_model', 'page_settings');
			$this->main_settings = $this->page_settings->get_page_settings($main_id, 0);
		}
	}

	public function login()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false)
		// {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }

		// $banner_records = $this->Home_setting_Model->get();

		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];

		$member = null;

		if ($this->input->post()) {
			$this->load->model('Member_model', 'member');
			$request = $this->input->post();
			$member = $this->member->check_login($request['email'], $request['password']);
			if (count($member) > 0) {
				$this->session->set_userdata('uonlive_user', $member);
				header('location: ' . base_url('member/info'));
				exit;
			}
		}

		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'member' => $member,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '會員專區',
				'url' => '',
			],
		];
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

	public function logout()
	{
		$this->session->unset_userdata('uonlive_user');
		header('location: ' . base_url('/'));
	}

	public function register()
	{
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false)
		// {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }

		// $banner_records = $this->Home_setting_Model->get();

		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];

		$insert_id = null;

		if ($this->input->post()) {
			$this->load->model('Member_model', 'member');
			$request = $this->input->post();
			$insert_id = $this->member->create($request);
		}

		$this->load->model('District_model', 'district');
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
			'areas' => $this->district->getAll(),
			'insert_id' => $insert_id,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '會員專區',
				'url' => base_url('member/info'),
			],
			[
				'title' => '註冊用戶',
				'url' => '',
			],
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/axios/axios.min.js'),
			'extra' => '',
		];
		// $this->data['css_file'][] = base_url('assets/vendor/password-checker/password-checker.css');
		// $this->data['js_file'][] = [
		// 	'script' => base_url('assets/vendor/password-checker/password-checker-edit.js'),
		// 	'extra' => '',
		// ];
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['scripts'][] = 'var map_api_key = \'' . $this->config->item('uonlive')['map_api_key'] . '\'';
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

	public function forget_password()
	{
		$member = null;

		if ($this->input->post()) {
			$this->load->model('Member_model', 'member');
			$request = $this->input->post();
			$member = $this->member->reset_password($request['email']);
		}

		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'member' => $member,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '會員專區',
				'url' => '',
			],
		];
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

	public function upgrade()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$update = false;
		$this->load->model('Provider_model', 'provider');
		$this->load->model('Purchasinginfo_model', 'purchasinginfo');
		$effective_date = $this->purchasinginfo->getExpiryDate();
		$expiry_date = strtotime("+30 days", $effective_date);
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
			'effective_date' => $effective_date,
			'expiry_date' => $expiry_date,
			'update' => $update,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '會員專區',
				'url' => base_url('member/info'),
			],
			[
				'title' => '升級為服務提供者／品牌',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
        $this->data['js_file'][] = [
            'script' => 'https://js.stripe.com/v3/',
			'extra' => '',
        ];
		// $this->data['js_file'][] = [
		// 	'script' => base_url('assets/vendor/axios/axios.min.js'),
		// 	'extra' => '',
		// ];
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		// $this->data['scripts'][] = 'var map_api_key = \'' . $this->config->item('uonlive')['map_api_key'] . '\'';
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function upgrade_success()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$update = false;
		$this->load->model('Provider_model', 'provider');
		$this->load->model('Member_model', 'member');
		$this->load->model('Purchasinginfo_model', 'purchasinginfo');
		$provider = $this->provider->get($this->session->userdata('uonlive_user')['id']);
		if (! array_key_exists('member_id', $provider)) {
			$provider_id = $this->provider->create();
		}
		$expiry_date = $this->purchasinginfo->getExpiryDate();
		$new_order_no = $this->purchasinginfo->getNextOrderNo('INV' . date("Ym"));
		$this->purchasinginfo->update([
				'checkout_session_id' => $this->input->get('session_id'),
				'payment_status' => 'COMPLETE',
				'order_no' => $new_order_no,
			], $this->session->userdata('purchasing_info_id')
		);

		$this->session->set_userdata('uonlive_user', $this->member->reload_info());
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
			'expiry_date' => $expiry_date,
			'order_no' => $new_order_no,
			'update' => $update,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '會員專區',
				'url' => base_url('member/info'),
			],
			[
				'title' => '升級為服務提供者／品牌',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function upgrade_fail()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$update = false;
		$this->load->model('Provider_model', 'provider');
		$this->load->model('Purchasinginfo_model', 'purchasinginfo');
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
			'expiry_date' => $this->purchasinginfo->getExpiryDate(),
			'update' => $update,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '會員專區',
				'url' => base_url('member/info'),
			],
			[
				'title' => '升級為服務提供者／品牌',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function renew()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		header('location: ' . base_url('member/upgrade'));
	}

	public function upgrade_()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$this->load->model('Provider_model', 'provider');
		$this->load->model('Member_model', 'member');
		$this->provider->create();
		$this->session->set_userdata('uonlive_user', $this->member->reload_info());
		header('location: ' . base_url('member/info'));
		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => $this->main_settings['use_banner'],
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => $this->main_settings['banner_desktop'],
		// 	"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => $this->main_settings['banner_mobile'],
		// 	"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
		// 	'controller' => $this->router->fetch_class(),
		// 	"method" => $this->router->fetch_method(),
		// 	"member" => $this->session->userdata('uonlive_user'),
		// ];
		// $this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		// $this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function info()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false)
		// {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }

		// $banner_records = $this->Home_setting_Model->get();

		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];

		$update = false;

		if ($this->input->post()) {
			$this->load->model('Member_model', 'member');
			$request = $this->input->post();
			$this->member->update($request);
			$this->session->set_userdata('uonlive_user', $this->member->getMemberByEmail($request['login_name']));
			$update = true;
		}

		$this->load->model('District_model', 'district');
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'areas' => $this->district->getAll(),
			"member" => $this->session->userdata('uonlive_user'),
			'update' => $update,
		];
		$this->data['thumbnails'] = [
			[
				'title' => '會員專區',
				'url' => base_url('member/info'),
			],
			[
				'title' => '帳戶資料',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/axios/axios.min.js'),
			'extra' => '',
		];
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['scripts'][] = 'var map_api_key = \'' . $this->config->item('uonlive')['map_api_key'] . '\'';
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

	public function like()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false)
		// {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }

		// $banner_records = $this->Home_setting_Model->get();

		// $data = [
		// 	"required_css" => false,
		// 	"nav_active" => "",
		// 	"use_banner" => false,
		// 	"use_breadcrumb" => false,
		// 	"banner_desktop" => 'public/images/uploads/banner_member.png',
		// 	"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
		// 	"banner_mobile" => 'public/images/uploads/banner_member.png',
		// 	"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
		// 	"objects" => $objects,
		// 	"banner_records" => $banner_records,
		// 	"need_carousel" => false,
		// 	"controller" => $this->router->fetch_class(),
		// ];
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->model('Member_like_model', 'like');
		$likes = $this->like->getByMember($this->session->userdata('uonlive_user')['id']);
		$data['like_list'] = [];
		$services_id = [];
		foreach ($likes as $like ) {
			$data['like_list'][] = [
				'id' => $like['id'],
				'providers_id' => $like['providers_id'],
				'photo' => $like['shop_photo'],
				'location' => $like['head_office_building'],
				'shop_uri' => $like['shop_uri'],
				'title' => $like['shop_name'],
				'tags' => [],
				'score' => 3,
				'is_liked' => $this->like->check_liked($this->session->userdata('uonlive_user')['id'], $like['providers_id']),
			];
			$services_id['provider_' . $like['provider_id']] = [
				'title' => $like['shop_name'],
				'url' => base_url('services/details/' . $like['shop_uri']),
			];
		}
		$this->data['css_file'] = [
			base_url('assets/vendor/need_share_button/needsharebutton.min.css'),
			base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css'),
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/need_share_button/needsharebutton.js'),
			'extra' => '',
		];
		$this->data['scripts'][] = 'var services_id = ' . json_encode($services_id, JSON_UNESCAPED_UNICODE) . ';';
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

	public function keep()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->model('Member_bookmark_model', 'bookmark');
		$keeps = $this->bookmark->getByMember($this->session->userdata('uonlive_user')['id']);
		$data['keep_list'] = [];
		$services_id = [];
		foreach ($keeps as $keep ) {
			$data['keep_list'][] = [
				'id' => $keep['id'],
				'providers_id' => $keep['providers_id'],
				'photo' => $keep['shop_photo'],
				'location' => $keep['head_office_building'],
				'shop_uri' => $keep['shop_uri'],
				'title' => $keep['shop_name'],
				'tags' => [],
				'score' => 3,
				'is_bookmarked' => $this->bookmark->check_bookmarked($this->session->userdata('uonlive_user')['id'], $keep['providers_id']),
			];
			$services_id['provider_' . $keep['provider_id']] = [
				'title' => $keep['shop_name'],
				'url' => base_url('services/details/' . $keep['shop_uri']),
			];
		}
		$this->data['css_file'][] = base_url('assets/vendor/need_share_button/needsharebutton.min.css');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/need_share_button/needsharebutton.js'),
			'extra' => '',
		];
		$this->data['scripts'][] = 'var services_id = ' . json_encode($services_id, JSON_UNESCAPED_UNICODE) . ';';
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function browsing_history()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
		];
		$request = $this->input->get();
		$type = "";
		if (array_key_exists('type', $request)) {
			if (in_array($request['type'], ['provider', 'brand', 'category'])) {
				$type = $request['type'];
			}
		}
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->model('Browsing_history_model', 'history');
		$data['histories'] = $this->history->getByMember($this->session->userdata('uonlive_user')['id'], $type);
		$data['type'] = $type;
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function conversion_history()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
		];
		$request = $this->input->get();
		$type = "";
		if (array_key_exists('type', $request)) {
			if (in_array($request['type'], ['unread', 'read'])) {
				$type = $request['type'];
			}
		}
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->model('Conversion_model', 'conversion');
		$data['conversions'] = $this->conversion->getByMember($this->session->userdata('uonlive_user')['id']);
		$data['type'] = $type;
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function conversion_details()
	{
		if (! $this->checkLogin()) {
			header('location: ' . base_url('member/login'));
			exit;
		}
		$title = urldecode($this->uri->segment(3));
		if ($title == "") {
			header('location: ' . base_url('member/conversion_history'));
			exit;
		}
		// $this->load->model("Home_setting_Model");
		// $main_id = $this->Home_setting_Model->getHomeData();
		// $main_settings = [];
		// if ($main_id !== false) {
		// 	$this->load->model('Page_settings_model', 'page_settings');
		// 	$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		// 	$this->load->model('Page_objects_model', 'page_objects');
		// 	$objects = $this->page_objects->load_objects($main_id, 0);
		// }
		// $banner_records = $this->Home_setting_Model->get();
		$this->load->model('Conversion_model', 'conversion');
		$conversion = $this->conversion->getByUri($title);
		if (! $conversion) {
			header('location: ' . base_url('member/conversion_history'));
			exit;
		}
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member" => $this->session->userdata('uonlive_user'),
			"conversion" => $conversion,
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

}

