<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_FrontendController
{

	public function __construct()
	{
		parent::__construct();
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '.css');
		// $this->data['js_file'][] = base_url('assets/front/js/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.js');
	}

	public function index()
	{
		$this->load->model("Home_setting_model");
		$main_id = $this->Home_setting_model->getHomeData();
		$main_settings = [];
		if ($main_id !== false)
		{
			$this->load->model('Page_settings_model', 'page_settings');
			$main_settings = $this->page_settings->get_page_settings($main_id, 0);
		}
		$member_id = $this->session->has_userdata('uonlive_user') ? $this->session->userdata('uonlive_user')['id'] : 0;
		$data = [
			// "required_css" => false,
			// "nav_active" => "",
			"use_banner" => $main_settings['use_banner'],
			// "use_breadcrumb" => false,
			"banner_desktop" => $main_settings['banner_desktop'],
			"desktop_banner_alttext" => $main_settings['desktop_banner_alttext'],
			"banner_mobile" => $main_settings['banner_mobile'],
			"mobile_banner_alttext" => $main_settings['mobile_banner_alttext'],
			// "need_carousel" => false,
			"controller" => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member_id" => $member_id,
		];

		$this->load->model('Provider_service_model', 'service');
		$this->load->model('Member_like_model', 'like');
		$this->load->model('Member_bookmark_model', 'bookmark');
		$latest = $this->service->getLatest();
		$data['latest_services'] = [];
		$services_id = [];
		foreach ($latest as $item) {
			$data['latest_services'][] = [
				'id' => $item['id'],
				'providers_id' => $item['providers_id'],
				'photo' => $item['photos'][0]['photo'],
				'type' => $item['provider_type'],
				'location' => $item['district_name'] . ' / ' . $item['head_office_building'],
				'title' => $item['name'],
				'uri' => $item['provider_uri'],
				'tags' => [
				],
				'score' => 3,
				'is_liked' => $this->like->check_liked($member_id, $item['providers_id']),
				'is_bookmarked' => $this->bookmark->check_bookmarked($member_id, $item['providers_id']),
			];
			$services_id['latest_' . $item['id']] = [
				'title' => $item['name'],
				'url' => base_url('services/details/' . $item['provider_uri']),
			];
		}
		$hottest = $this->service->getHottest();
		$data['hot_services'] = [];
		foreach ($hottest as $item) {
			$data['hot_services'][] = [
				'id' => $item['id'],
				'providers_id' => $item['providers_id'],
				'photo' => $item['photos'][0]['photo'],
				'type' => $item['provider_type'],
				'location' => $item['district_name'] . ' / ' . $item['head_office_building'],
				'title' => $item['name'],
				'uri' => $item['provider_uri'],
				'tags' => [
				],
				'score' => 3,
				'is_liked' => $this->like->check_liked($member_id, $item['providers_id']),
				'is_bookmarked' => $this->bookmark->check_bookmarked($member_id, $item['providers_id']),
			];
			$services_id['hottest_' . $item['id']] = [
				'title' => $item['name'],
				'url' => base_url('services/details/' . $item['provider_uri']),
			];
		}
		$this->data['css_file'][] = base_url('assets/vendor/need_share_button/needsharebutton.min.css');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/need_share_button/needsharebutton.js'),
			'extra' => '',
		];
		$this->data['scripts'][] = 'var services_id = ' . json_encode($services_id, JSON_UNESCAPED_UNICODE) . ';';
		$this->data['scripts'][] = 'var id = ' . $member_id . ';';
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));

	}

}

