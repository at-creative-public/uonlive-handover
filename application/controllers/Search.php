<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Search extends MY_FrontendController
{
	private $main_settings;

	public function __construct()
	{
		parent::__construct();
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '.css');
		// $this->data['js_file'][] = base_url('assets/front/js/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.js');
		$this->load->model("Home_setting_model", 'home_setting');
		$main_id = $this->home_setting->get_data_by_uri($this->router->fetch_class());
		$this->main_settings = [];
		if ($main_id !== false)
		{
			$this->load->model('Page_settings_model', 'page_settings');
			$this->main_settings = $this->page_settings->get_page_settings($main_id, 0);
		}
	}

	public function result()
	{
		if (! $this->input->post()) {
			header('location :' . base_url());
			exit;
		}
		$request = $this->input->post();
		$this->load->model('Provider_model', 'provider');
		$this->load->model('Provider_service_model', 'service');
		$this->load->model('Media_model', 'media');
		$this->load->model('Hashtag_model', 'hashtag');
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"keyword" => $request['search'],
			"services" => $this->service->keywordSearch($request['search']),
			"providers" => $this->provider->keywordSearch($request['search']),
			"medias" => $this->media->keywordSearch($request['search']),
			"hashtags" => $this->hashtag->keywordSearch($request['search']),
		];
		$this->data['thumbnails'] = [
			[
				'title' => '搜索',
				'url' => '',
			],
		];
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

}

