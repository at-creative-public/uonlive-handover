<?php

class Service extends MY_FrontendController
{

	private $main_settings;

	public function __construct()
	{
		parent::__construct();
		// $this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '.css');
		// $this->data['js_file'][] = base_url('assets/front/js/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.js');
		$this->load->model("Browsing_history_model", 'browsing_history');
		$this->load->model("Home_setting_model", 'home_setting');
		$main_id = $this->home_setting->get_data_by_uri($this->router->fetch_class());
		$this->main_settings = [];
		if ($main_id !== false)
		{
			$this->load->model('Page_settings_model', 'page_settings');
			$this->main_settings = $this->page_settings->get_page_settings($main_id, 0);
		}
	}

	public function index()
	{
		$cat = urldecode($this->uri->segment(2));
		$member_id = $this->session->has_userdata('uonlive_user') ? $this->session->userdata('uonlive_user')['id'] : 0;
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			"banner_desktop" => $this->main_settings['banner_desktop'],
			"desktop_banner_alttext" => $this->main_settings['desktop_banner_alttext'],
			"banner_mobile" => $this->main_settings['banner_mobile'],
			"mobile_banner_alttext" => $this->main_settings['mobile_banner_alttext'],
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			"member_id" => $member_id,
		];
		$request = $this->input->get();
		$data['_regions'] = [];
		$data['_tags'] = [];
		if (array_key_exists('r', $request)) {
			$_region = $request['r'];
			$data['_regions'] = explode(',', $_region);
		}
		if (array_key_exists('t', $request)) {
			$_tag = $request['t'];
			$data['_tags'] = explode(',', $_tag);
		}
		$params = [
			'regions' => $data['_regions'],
			'tags' => $data['_tags'],
		];
		$this->load->model('District_model', 'district');
		$data['regions'] = $this->district->getAll();
		$data['periods'] = [
			'上午', '下午', '晚上', '凌晨', '全日', '', '',
		];
		$data['ages'] = [
			'兒童', '少年', '青年', '成人', '老年', '', '',
		];
		$data['genders'] = [
			'男', '女', '', '', '', '', '',
		];
		$this->load->model('Hashtag_model', 'hashtag');
		$this->load->model('Tag_list_model', 'tag');
		$this->load->model('Media_model', 'media');
		$this->load->model('Browsing_history_model', 'history');
		if ($cat != "") {
			$category = $this->tag->getByName($cat);
		}
		else {
			$category = $this->tag->getFirstTag();
			$cat = $category['name'];
		}
		$this->history->add([
			'member_id' => $member_id,
			'page_type' => 'c',
			'page_id' => $category['id'],
		]);
		$medias = $this->media->getAllByCategory($category['id'], $params);
		$data['all_news'] = [];
		$data['all_images'] = [];
		$data['all_video'] = [];
		foreach ($medias as $media) {
			switch ($media['media_type']) {
				case "PHOTO":
					$data['all_images'][] = $media['media']['filename'];
					break;
				case "NEWS":
					$data['all_news'][] = [
						'title' => $media['media']['title'],
						'content' => $media['media']['content'],
						'author' => $media['shop_name'],
						'avatar' => $media['avatar_photo'],
						'hashtags' => $media['hashtags'],
						'shop_uri' => $media['shop_uri'],
					];
					break;
				case "VIDEO":
					$data['all_videos'][] = [
						'title' => $media['media']['title'],
						'image' => $media['media']['filename'],
						'url' => $media['media']['url'],
						'author' => $media['shop_name'],
						'hashtags' => $media['hashtags'],
						'shop_uri' => $media['shop_uri'],
					];
					break;
			}
		}
		$this->load->model('Provider_service_model', 'service');
		$this->load->model('Member_like_model', 'like');
		$this->load->model('Member_bookmark_model', 'bookmark');
		$this->load->model('Tag_2nd_list_model', 'tag_2nd_list');
		$data['tag_2nd'] = $this->tag_2nd_list->getByTagCode($category['code']);
		$latest = $this->service->getLatestByCategory($category['id'], $params);
		$data['latest_services'] = [];
		$services_id = [];
		foreach ($latest as $item) {
			$data['latest_services'][] = [
				'id' => $item['id'],
				'providers_id' => $item['providers_id'],
				'photo' => $item['photos'][0]['photo'],
				'avatar_photo' => $item['avatar_photo'],
				'type' => $item['provider_type'],
				'location' => $item['district_name'] . ' / ' . $item['head_office_building'],
				'title' => $item['name'],
				'uri' => $item['provider_uri'],
				'tags' => [
				],
				'score' => 3,
				'is_liked' => $this->like->check_liked($member_id, $item['providers_id']),
			];
			$services_id['latest_' . $item['id']] = [
				'title' => $item['name'],
				'url' => base_url('services/details/' . $item['provider_uri']),
			];
		}
		$hottest = $this->service->getHottestByCategory($category['id'], $params);
		$data['hot_services'] = [];
		foreach ($hottest as $item) {
			$data['hot_services'][] = [
				'id' => $item['id'],
				'providers_id' => $item['providers_id'],
				'photo' => $item['photos'][0]['photo'],
				'avatar_photo' => $item['avatar_photo'],
				'type' => $item['provider_type'],
				'location' => $item['district_name'] . ' / ' . $item['head_office_building'],
				'title' => $item['name'],
				'uri' => $item['provider_uri'],
				'tags' => [
				],
				'score' => 3,
				'is_liked' => $this->like->check_liked($member_id, $item['providers_id']),
			];
			$services_id['hottest_' . $item['id']] = [
				'title' => $item['name'],
				'url' => base_url('services/details/' . $item['provider_uri']),
			];
		}
		$data['hot_items'] = $this->hashtag->getLatestByCategory($category['id'], $params, 20);
		// $data['hot_items'] = [
		// 	'美甲', '減肥', '激光', '化妝', '抗痘', '保濕', '美白', '抗老', '保養', '抗污染',
		// 	'芳香療法', '美塑療法', '保健', '袪斑', '概念美容', '護膚', '減肥',
		// ];
		// $data['hot_places'] = [
		// 	'中環廣場灣仔', '中環廣場', '國際金融中心二期', '中銀大廈', '如心廣場', '朗豪坊', '天璽', '港島東中心', '國際金融中心一期', '交易廣場',
		// 	'四季匯', '合和中心', '中遠大廈', '凱旋門', '宏利保險大廈', '新鴻基中心', '太古坊一座', '長江集團中心',
		// ];
		$this->load->model('Provider_model', 'provider');
		$data['hot_service_providers'] = $this->provider->getLatestByCategory($category['id'], 'P', $params, 10);
		$this->load->model('Provider_brands_model', 'brands');
		$data['hot_brands'] = $this->brands->getLatestByCategory($category['id'], $params, 10);
		// $data['hot_distributors'] = [
		// 	'Ingrid Millet Paris', 'Skinla信納醫美顧問有限公司', '柏麗代理有限公司', 'Starz Tech 星研國際', '夏蓮娜化妝美容中心', '生活美容', '盈暉國際貿易', '艾菲美容中心', '美詩美容用品公司', 'Skinla信納醫美中心',
		// ];
		$data['title'] = $cat;
		// $this->load->model("Home_setting_Model");
		//$data["info"] = json_decode(json_encode($this->Home_setting_Model->get_front()), true);
		// $this->load->model("News_management_model");
		// $data['news'] = $this->News_management_model->get_front();
		$this->data['css_file'][] = base_url('assets/vendor/need_share_button/needsharebutton.min.css');
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/need_share_button/needsharebutton.js'),
			'extra' => '',
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/bs5-lightbox/index.bundle.min.js'),
			'extra' => '',
		];
		$this->data['scripts'][] = 'var services_id = ' . json_encode($services_id, JSON_UNESCAPED_UNICODE) . ';';
		$this->data['scripts'][] = 'var current_url = \'' . base_url('service/' . $cat) . '\';';
		$this->data['scripts'][] = 'var id = ' . $member_id . ';';
		$this->data['thumbnails'] = [
			[
				'title' => '服務分類',
				'url' => base_url('service'),
			],
			[
				'title' => $cat,
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$data = array_merge($data, json_decode(json_encode($this->home_setting->get_front()), true));
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function nearby_map()
	{
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
		];

		$data['nearby_shops'] = [
		];
		$member_id = $this->session->has_userdata('uonlive_user') ? $this->session->userdata('uonlive_user')['id'] : 0;
		$this->load->helper('common');
		foreach ($data['nearby_shops'] as $idx => $shop) {
			$data['nearby_shops'][$idx]['uri'] = generateSeoURL($shop['title']);
		}
		// $this->load->model("Home_setting_Model");
		//$data["info"] = json_decode(json_encode($this->Home_setting_Model->get_front()), true);
		// $this->load->model("News_management_model");
		// $data['news'] = $this->News_management_model->get_front();
		// https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap
		$this->data['css_file'] = [
			base_url('assets/front/css/popup_message_dialog.css'),
		];
		$this->data['js_file'][] = [
			'script' => "https://maps.googleapis.com/maps/api/js?key=" . $this->config->item('uonlive')['map_api_key'] . '&callback=initMap&libraries=places',
			'extra' => 'async defer',
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		// $this->data['js_file'][] = [
		// 	'script' => base_url('assets/front/js/popup_message_dialog.js'),
		// 	'extra' => '',
		// ];
		$this->data['scripts'][] = 'var services_id = [];';
		$this->data['scripts'][] = 'var id = ' . $member_id . ';';
		// $this->data['scripts'][] = 'if (navigator.geolocation) {' . PHP_EOL .
		// 							'	navigator.geolocation.getCurrentPosition(setDefaultPosition, positionNotAllow);' . PHP_EOL .
		// 							'} else {' . PHP_EOL .
		// 							'	alert (\'無法讀取你的位置坐標\');' . PHP_EOL .
		// 							'	setDefaultPosition({' . PHP_EOL .
		// 							'		coords: {' . PHP_EOL .
		// 							'			latitude: 22.3193,' . PHP_EOL .
		// 							'			longitude: 114.1694,' . PHP_EOL .
		// 							'		}' . PHP_EOL .
		// 							'	});' . PHP_EOL .
		// 							'}';
		$this->data['thumbnails'] = [
			[
				'title' => '服務分類',
				'url' => base_url('service/'),
			],
			[
				'title' => '附近的人',
				'url' => '',
			],
		];
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['scripts'][] = 'var default_location = ' . json_encode($this->config->item('uonlive')['default_location']) . ';';
		$data = array_merge($data, json_decode(json_encode($this->home_setting->get_front()), true));
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function nearby_list()
	{
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
		];
		$this->data['css_file'][] = base_url('assets/front/css/' . $this->router->fetch_class() . '_' . $this->router->fetch_method() . '.css');
		$data['nearby_shops'] = [
		];
		$this->load->helper('common');
		foreach ($data['nearby_shops'] as $idx => $shop) {
			$data['nearby_shops'][$idx]['uri'] = generateSeoURL($shop['title']);
		}
		// $this->load->model("Home_setting_Model");
		//$data["info"] = json_decode(json_encode($this->Home_setting_Model->get_front()), true);
		// $this->load->model("News_management_model");
		// $data['news'] = $this->News_management_model->get_front();
		$data = array_merge($data, json_decode(json_encode($this->home_setting->get_front()), true));
		$this->data['scripts'][] = 'var services_id = [];';
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['thumbnails'] = [
			[
				'title' => '服務分類',
				'url' => base_url('service'),
			],
			[
				'title' => '附近的人',
				'url' => '',
			],
		];
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

	public function details()
	{
		$shop = urldecode($this->uri->segment(3));
		if ($shop == '') {
			header('location: ' . base_url('service'));
			exit;
		}
		$this->load->model('Provider_model', 'provider');
		$this->load->model('Provider_service_model', 'service');
		$this->load->model('Browsing_history_model', 'history');
		$this->load->model('Member_like_model', 'like');
		$this->load->model('Member_bookmark_model', 'bookmark');
		$provider = $this->provider->getByUri($shop);
		$member_id = $this->session->has_userdata('uonlive_user') ? $this->session->userdata('uonlive_user')['id'] : 0;
		$this->history->add([
			'member_id' => $member_id,
			'page_type' => 's',
			'page_id' => $provider['id'],
		]);
		$data = [
			"required_css" => false,
			"nav_active" => "",
			"use_banner" => $this->main_settings['use_banner'],
			"use_breadcrumb" => false,
			'controller' => $this->router->fetch_class(),
			"method" => $this->router->fetch_method(),
			'provider' => $provider,
			'services' => $this->service->getAll($provider['id']),
			'liked' => $this->like->check_liked($member_id, $provider['id']),
			'bookmarked' => $this->bookmark->check_bookmarked($member_id, $provider['id']),
			'member_id' => $member_id,
		];
		$this->data['css_file'] = [
			base_url('assets/vendor/need_share_button/needsharebutton.min.css'),
			// base_url('assets/vendor/ekko-lightbox/ekko-lightbox.css'),
			base_url('assets/front/css/popup_message_dialog.css'),
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/need_share_button/needsharebutton.min.js'),
			'extra' => '',
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/bs5-lightbox/index.bundle.min.js'),
			'extra' => '',
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/vendor/ckeditor5/ckeditor.js'),
			'extra' => '',
		];
		$this->data['js_file'][] = [
			'script' => base_url('assets/front/js/popup_message_dialog.js'),
			'extra' => '',
		];
		$this->data['scripts'][] = 'var services_id = ' . json_encode([], JSON_UNESCAPED_UNICODE) . ';';
		$data['user_comments'] = [
		];
		$this->load->model('Media_model', 'media');
		$medias = $this->media->getAllByProvider($provider['id']);
		$data['all_news'] = [];
		$data['all_images'] = [];
		$data['all_video'] = [];
		foreach ($medias as $media) {
			switch ($media['media_type']) {
				case "PHOTO":
					$data['all_images'][] = $media['media']['filename'];
					break;
				case "NEWS":
					$data['all_news'][] = [
						'title' => $media['media']['title'],
						'content' => $media['media']['content'],
						'author' => $media['shop_name'],
						'hashtags' => $media['hashtags'],
					];
					break;
				case "VIDEO":
					$data['all_videos'][] = [
						'title' => $media['media']['title'],
						'image' => $media['media']['filename'],
						'url' => $media['media']['url'],
						'author' => $media['shop_name'],
						'hashtags' => $media['hashtags'],
					];
					break;
			}
		}
		// $this->load->model("Home_setting_Model");
		//$data["info"] = json_decode(json_encode($this->Home_setting_Model->get_front()), true);
		// $this->load->model("News_management_model");
		// $data['news'] = $this->News_management_model->get_front();
		$data = array_merge($data, json_decode(json_encode($this->home_setting->get_front()), true));
		$this->data['loggedIn'] = $this->session->has_userdata('uonlive_user');
		$this->data['mostComment'] = '';
		$this->data['thumbnails'] = [
			[
				'title' => '服務分類',
				'url' => base_url('service'),
			],
			[
				'title' => $provider['tag_name'],
				'url' => base_url('service/' . $provider['tag_name']),
			],
			[
				'title' => $provider['shop_name'],
				'url' => '',
			],
		];
		$this->load->view('front/layouts/main', array_merge($data, $this->data));
	}

}

