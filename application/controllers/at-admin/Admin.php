<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->checkLoginStatus();
		$this->load->model('Admin_model', 'admin');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '管理員帳戶',
			'url' => '#',
		];
		$this->viewParams['scripts'][] = 'var new_admin_url = \'' . base_url('at-admin/admin/add') . '\'';
		$this->viewParams['scripts'][] = 'var delete_admin_url = \'' . base_url('at-admin/admin/delete') . '\'';
		$this->viewParams['csses'][] = base_url('assets/css/datatables.bootstrap.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/jquery.datatables.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/zeroclipboard.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.tabletools.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.bootstrap.min.js'),
		];
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'data' => $this->admin->getAll(),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

	public function add()
	{
		if ($this->input->post()) {
			$request = $this->input->post();
			$errno = $this->admin->add($request);
			$this->viewParams['errno'] = $errno;
			if ($errno == 0) {
				$this->viewParams['alert_message'] = "已成功加入管理員";
			}
			else {
				$this->load->helper('constant');
				$errMsg = getAllErrorCode();
				$this->viewParams['alert_message'] = $errMsg[$errno];
			}
		}
		$this->viewParams['breadcrumb'][] = [
			'name' => '管理員帳戶',
			'url' => base_url('at-admin/admin'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '新增管理員帳戶',
			'url' => '#',
		];
		$this->viewParams['csses'][] = base_url('assets/vendor/password-checker/password-checker.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/vendor/password-checker/password-checker.js'),
		];
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

	public function edit()
	{
		if ($this->input->post()) {
			$request = $this->input->post();
			$errno = $this->admin->modify($request);
			$this->viewParams['errno'] = $errno;
			if ($errno == 0) {
				$this->viewParams['alert_message'] = "已成功更新管理員資料";
			}
			else {
				$this->load->helper('constant');
				$errMsg = getAllErrorCode();
				$this->viewParams['alert_message'] = $errMsg[$errno];
			}
		}

		$id = $this->uri->segment(4);
		$this->viewParams['breadcrumb'][] = [
			'name' => '管理員帳戶',
			'url' => base_url('at-admin/admin'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '修改管理員帳戶',
			'url' => '#',
		];
		$this->viewParams['csses'][] = base_url('assets/vendor/password-checker/password-checker.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/vendor/password-checker/password-checker-edit.js'),
		];
		$user = $this->admin->one($id);
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'data' => $user,
		];
		if (empty ($data['data'])) {
			redirect('at-admin/admin');
			exit;
		}
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

	public function delete($id = 0)
	{
		if ($id != 0) {
			$errno = $this->admin->remove($id);
		}
		redirect('at-admin/admin');
	}

	public function logout()
	{
		$this->admin->logout();
		redirect('/at-admin');
	}

}
