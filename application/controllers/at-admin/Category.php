<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->checkLoginStatus();
		$this->load->model('Tag_list_model', 'tag_list');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '類別管理',
			'url' => '#',
		];
		$this->viewParams['csses'][] = base_url('assets/css/datatables.bootstrap.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/jquery.datatables.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/zeroclipboard.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.tabletools.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.bootstrap.min.js'),
		];
		$this->viewParams['scripts'][] = 'var new_category_url = \'' . base_url('at-admin/category/add') . '\';';
		$this->viewParams['scripts'][] = 'var delete_category_url = \'' . base_url('at-admin/category/delete') . '\'';
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'data' => $this->tag_list->get(),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

    public function add()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '類別管理',
			'url' => base_url('at-admin/category'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '加入類別',
			'url' => '#',
		];
		$this->load->helper('common');
		$icon_file = generateRandomString(16);
		$this->viewParams['scripts'][] = 'var icon_file = "' . $icon_file . '"';
		$this->load->view('at-admin/index', array_merge($this->viewParams, [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'icon_file' => $icon_file,
		]));
	}

	public function insert()
	{
		$request = $this->input->post();
		$this->tag_list->add($request);
		header('location: ' . base_url('at-admin/category'));
	}

	public function edit($code = ''){
		$this->viewParams['breadcrumb'][] = [
			'name' => '類別管理',
			'url' => base_url('at-admin/category'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '修改類別',
			'url' => '#',
		];
		$category = $this->tag_list->getByCode($code);
		$this->viewParams['scripts'][] = 'var icon_file = "' . $category['code'] . '"';
		$this->load->view('at-admin/index', array_merge($this->viewParams, [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'category' => $category,
		]));
	}

	public function save()
	{
		$request = $this->input->post();
		$this->tag_list->modify($request);
		header('location: ' . base_url('at-admin/category'));
	}

}
