<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->checkLoginStatus();
		$this->load->model('Tag_2nd_list_model', 'tag_2nd_list');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '子類別管理',
			'url' => '#',
		];
		$this->viewParams['csses'][] = base_url('assets/css/datatables.bootstrap.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/jquery.datatables.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/zeroclipboard.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.tabletools.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.bootstrap.min.js'),
		];
		$this->viewParams['scripts'][] = 'var new_subcategory_url = \'' . base_url('at-admin/subcategory/add') . '\';';
		$this->viewParams['scripts'][] = 'var delete_subcategory_url = \'' . base_url('at-admin/subcategory/delete') . '\'';
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'data' => $this->tag_2nd_list->get(),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

    public function add()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '子類別管理',
			'url' => base_url('at-admin/subcategory'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '加入子類別',
			'url' => '#',
		];
		$this->load->model('Tag_list_model', 'tag_list');
		$this->load->view('at-admin/index', array_merge($this->viewParams, [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'tags' => $this->tag_list->get(),
		]));
	}

	public function insert()
	{
		$request = $this->input->post();
		$this->tag_2nd_list->add($request);
		header('location: ' . base_url('at-admin/subcategory'));
	}

	public function edit($code = ''){
		$this->viewParams['breadcrumb'][] = [
			'name' => '子類別管理',
			'url' => base_url('at-admin/subcategory'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '修改子類別',
			'url' => '#',
		];
		$this->load->model('Tag_list_model', 'tag_list');
		$this->load->view('at-admin/index', array_merge($this->viewParams, [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'tags' => $this->tag_list->get(),
			'subcategory' => $this->tag_2nd_list->getByCode($code),
		]));
	}

	public function save()
	{
		$request = $this->input->post();
		$this->tag_2nd_list->modify($request);
		header('location: ' . base_url('at-admin/subcategory'));
	}

}
