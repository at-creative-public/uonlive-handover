<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = [];
		if ($this->input->post()) {
			$this->load->model('Admin_model', 'admin');
			$errno = $this->admin->checkLogin($this->input->post('userName'), $this->input->post('password'));
			if ($errno == 0) {
				redirect('/at-admin/dashboard');
				exit;
			}
			$this->viewParams['errno'] = $errno;
			$this->load->helper('constant');
			$errMsg = getAllErrorCode();
			$this->viewParams['alert_message'] = $errMsg[$errno];
		}
		$this->load->view('at-admin/login/index', array_merge($this->viewParams, $data));
	}

}
