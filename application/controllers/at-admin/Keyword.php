<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keyword extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->checkLoginStatus();
		$this->load->model('Keyword_model', 'keyword');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '關鍵字管理',
			'url' => '#',
		];
		$this->viewParams['csses'][] = base_url('assets/css/datatables.bootstrap.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/jquery.datatables.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/zeroclipboard.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.tabletools.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.bootstrap.min.js'),
		];
		$this->viewParams['scripts'][] = 'var new_keyword_url = \'' . base_url('at-admin/keyword/add') . '\';';
		$this->viewParams['scripts'][] = 'var delete_keyword_url = \'' . base_url('at-admin/keyword/delete') . '\'';
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'data' => $this->keyword->get(),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

    public function add()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '關鍵字管理',
			'url' => base_url('at-admin/keyword'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '加入關鍵字',
			'url' => '#',
		];
		$this->load->model('Tag_list_model', 'tag_list');
		$this->load->model('Tag_2nd_list_model', 'tag_2nd_list');
		$data = [];
		$tag_list = $this->tag_list->get();
		foreach ($tag_list as $idx => $tag) {
			$data[$tag['code']] = $tag;
			$tag_2nd = $this->tag_2nd_list->getByTagCode($tag['code']);
			if ($idx == 0) {
				$tag_2nd_list = array_slice($tag_2nd, 0);;
			}
			$data[$tag['code']]['tag_2nd'] = $tag_2nd;
		}
		$this->viewParams['scripts'][] = 'var tag_2nd = ' . json_encode($data, JSON_UNESCAPED_UNICODE);
		$this->load->view('at-admin/index', array_merge($this->viewParams, [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'tags' => $tag_list,
			'tags_2nd' => $tag_2nd_list,
		]));
	}

	public function insert()
	{
		$request = $this->input->post();
		$this->keyword->add($request);
		header('location: ' . base_url('at-admin/keyword'));
	}

	public function edit($code = ''){
		$this->viewParams['breadcrumb'][] = [
			'name' => '關鍵字管理',
			'url' => base_url('at-admin/keyword'),
		];
		$this->viewParams['breadcrumb'][] = [
			'name' => '修改關鍵字',
			'url' => '#',
		];
		$this->load->model('Tag_list_model', 'tag_list');
		$this->load->model('Tag_2nd_list_model', 'tag_2nd_list');
		$keyword = $this->keyword->getByCode($code);
		$data = [];
		$tag_list = $this->tag_list->get();
		foreach ($tag_list as $idx => $tag) {
			$data[$tag['code']] = $tag;
			$tag_2nd = $this->tag_2nd_list->getByTagCode($tag['code']);
			if ($tag['code'] == $keyword['tag_code']) {
				$tag_2nd_list = array_slice($tag_2nd, 0);;
			}
			$data[$tag['code']]['tag_2nd'] = $tag_2nd;
		}
		$this->viewParams['scripts'][] = 'var tag_2nd = ' . json_encode($data, JSON_UNESCAPED_UNICODE);
		$this->load->view('at-admin/index', array_merge($this->viewParams, [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'tags' => $this->tag_list->get(),
			'tags_2nd' => $tag_2nd_list,
			'keyword' => $keyword,
		]));
	}

	public function save()
	{
		$request = $this->input->post();
		$this->keyword->modify($request);
		header('location: ' . base_url('at-admin/keyword'));
	}

}
