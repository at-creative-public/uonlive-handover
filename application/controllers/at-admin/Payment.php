<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->checkLoginStatus();
		$this->load->model('Purchasinginfo_model', 'purchase');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '收費列表',
			'url' => '#',
		];
		$this->viewParams['csses'][] = base_url('assets/css/datatables.bootstrap.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/jquery.datatables.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/zeroclipboard.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.tabletools.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.bootstrap.min.js'),
		];
		// $this->viewParams['scripts'][] = 'var new_category_url = \'' . base_url('at-admin/category/add') . '\';';
		// $this->viewParams['scripts'][] = 'var delete_category_url = \'' . base_url('at-admin/category/delete') . '\'';
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'data' => $this->purchase->getAll(),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

	public function details($token)
	{
		$this->viewParams['breadcrumb'][] = [
			'name' => '會員列表',
			'url' => '#',
		];
		$this->viewParams['csses'][] = base_url('assets/css/datatables.bootstrap.css');
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/jquery.datatables.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/zeroclipboard.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.tabletools.min.js'),
		];
		$this->viewParams['jses'][] = [
			'type' => '',
			'url' => base_url('assets/js/datatable/datatables.bootstrap.min.js'),
		];
		// $this->viewParams['scripts'][] = 'var new_category_url = \'' . base_url('at-admin/category/add') . '\';';
		// $this->viewParams['scripts'][] = 'var delete_category_url = \'' . base_url('at-admin/category/delete') . '\'';
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'data' => $this->member->getMemberByToken($token),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}
}
