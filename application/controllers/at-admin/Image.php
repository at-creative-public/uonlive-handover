<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->checkLoginStatus();
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	}

	public function upload()
	{
		if (empty($_FILES)) {
			echo json_encode(['success' => false, 'data' => [], 'message' => '沒有上載文件'], JSON_UNESCAPED_UNICODE);
			exit();
		}
		$field = key($_FILES);
		foreach ($_FILES[$field]['error'] as $err) {

			if ($err != 0) {
				echo json_encode(['success' => false, 'data' => [], 'message' => '文件上載失敗'], JSON_UNESCAPED_UNICODE);
				exit();
			}
		}
		if (! $this->input->post()) {
			echo json_encode(['success' => false, 'data' => [], 'message' => '發生錯誤'], JSON_UNESCAPED_UNICODE);
			exit();
		}
		$request = $this->input->post();
		$return = [];
		switch ($request['image_type']) {
			default:
				$fullPath = FCPATH . 'assets/img/uploads/' . $request['image_type'] . '/';
				if (! file_exists($fullPath)) {
					mkdir ($fullPath);
				}
				foreach ($_FILES[$field]['name'] as $idx => $name) {
					$fileExt = pathinfo($name, PATHINFO_EXTENSION);
					$newFilename = $request['filename'];
		
					if (!move_uploaded_file($_FILES[$field]['tmp_name'][$idx], $fullPath . $newFilename . '.' . $fileExt)) {
						echo json_encode(['success' => false, 'data' => [], 'message' => '圖片讀寫錯誤'], JSON_UNESCAPED_UNICODE);
						exit();
					}
					$return[] = $newFilename;
				}
				break;
		}
		echo json_encode(['success' => true, 'data' => $return, 'message' => '圖片上載成功'], JSON_UNESCAPED_UNICODE);
	}
}
