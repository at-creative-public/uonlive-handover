<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->checkLoginStatus();
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->checkLoginStatus();
		$this->viewParams['breadcrumb'][] = [
			'name' => '儀表板',
			'url' => '#',
		];

		if (1 == 0) {
			// Easy Pie Charts Needed Scripts
			$this->viewParams['jses'][] = [
				'type' => '',
				'url' => base_url('assets/js/charts/easypiechart/jquery.easypiechart.js'),
			];
			$this->viewParams['jses'][] = [
				'type' => '',
				'url' => base_url('assets/js/charts/easypiechart/easypiechart-init.js'),
			];

			// Flot Charts Needed Scripts
			$this->viewParams['jses'][] = [
				'type' => '',
				'url' => base_url('assets/js/charts/flot/jquery.flot.js'),
			];
			$this->viewParams['jses'][] = [
				'type' => '',
				'url' => base_url('assets/js/charts/flot/jquery.flot.resize.js'),
			];
			$this->viewParams['jses'][] = [
				'type' => '',
				'url' => base_url('assets/js/charts/flot/jquery.flot.pie.js'),
			];
			$this->viewParams['jses'][] = [
				'type' => '',
				'url' => base_url('assets/js/charts/flot/jquery.flot.tooltip.js'),
			];
			$this->viewParams['jses'][] = [
				'type' => '',
				'url' => base_url('assets/js/charts/flot/jquery.flot.orderbars.js'),
			];
		}
		$this->load->model('Provider_model', 'provider');
		$this->load->model('Member_model', 'member');
		// $this->load->model('Visitor_model', 'visitor');
		$data = [
			'controller' => $this->router->fetch_class(),
			'action' => $this->router->fetch_method(),
			'provider_count' => $this->provider->getActiveProviderCount(),
			'member_count' => $this->member->getActiveCount(),
			'brand_count' => $this->provider->getActiveBrandCount(),
		];
		$this->load->view('at-admin/index', array_merge($this->viewParams, $data));
	}

}
