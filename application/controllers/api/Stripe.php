<?php
require_once(FCPATH . 'vendor/autoload.php');

defined('BASEPATH') OR exit('No direct script access allowed');

class Stripe extends MY_AdminController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		\Stripe\Stripe::setApiKey($this->config->item('uonlive')['stripe']['secret_key']);
	}

	public function index()
	{
		echo "<h1>Access Denied</h1>";
		exit;
	}

	public function config()
	{
		echo json_encode(['publicKey' => $this->config->item('uonlive')['stripe']['publishable_key']]);
	}

	public function create_checkout_session()
	{
		$request = $this->input->get();
		if (! array_key_exists('effective_date', $request)) {
			echo json_encode([]);
			return;
		}
		if (! array_key_exists('expiry_date', $request)) {
			echo json_encode([]);
			return;
		}
		if (! array_key_exists('success_url', $request)) {
			echo json_encode([]);
			return;
		}
		if (! array_key_exists('cancel_url', $request)) {
			echo json_encode([]);
			return;
		}
		// $this->load->model('Activity_enrollment_model', 'enrollment');
		// $enrollment = $this->enrollment->getByToken($request['token']);
        // if (! $enrollment) {
        //     header('location: ' . base_url('branch'));
        //     exit;
        // }
		// switch($enrollment['activity_type']) {
		// 	case "activity":
		// 		$this->load->model('Activity_model', 'activity');
		// 		$activity = $this->activity->getByToken($enrollment['activity']);
		// 		$product_name = $activity['title'];
		// 		$fee = $activity['fee'];
		// 		break;
		// 	case "meeting":
		// 		$this->load->model('Meeting_model', 'meeting');
		// 		$activity = $this->meeting->getByToken($enrollment['activity']);
		// 		$product_name = $activity['branch_name'] . '例會';
		// 		$fee = $activity['meeting_fee'];
		// 		break;
		// }
		// switch($enrollment['user_type']) {
		// 	case "visitor":
		// 		break;
		// 	case "member":
		// 		break;
		// }
		$checkout_session = \Stripe\Checkout\Session::create([
			'success_url' => $request['success_url'] . '?session_id={CHECKOUT_SESSION_ID}',
			'cancel_url' => $request['cancel_url'],
			'payment_method_types' => ['card'],
			'mode' => 'payment',
			'line_items' => [[
				'price_data' => [
					'currency' => 'hkd',
					'product_data' => [
						'name' => '升級為服務提供者／品牌',
					],
					'unit_amount' => $this->config->item('uonlive')['upgrade_fee'] * 100,
				],
				'quantity' => 1,
			]],
		]);
		$this->load->model('Purchasinginfo_model', 'purchasinginfo');
		$id = $this->purchasinginfo->create([
			'member_id' => $this->session->userdata('uonlive_user')['id'],
			'effective_date' => $request['effective_date'],
			'expiry_date' => $request['expiry_date'],
			'item' => $request['item'],
			'amount' => $request['amount'],
			'checkout_session' => print_r ($checkout_session, true),
		]);
		$this->session->set_userdata('purchasing_info_id', $id);
		echo json_encode(['sessionId' => $checkout_session['id']]);
	}

}
