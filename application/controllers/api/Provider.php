<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
	}

    public function service_update_rank()
    {
        $request = $this->input->post();
        // $table_name = $request['table_name'];
        $rank_name = $request['rank_name'];
        $id_name = $request['id_name'];
        $order = $request['order'];
        // $params = array();
        parse_str($order, $searcharray);
        $rank = 1;
        $this->load->model('Provider_service_model', 'service');
        foreach ($searcharray['listItem'] as $value) {
            $this->service->update_rank($value, $rank);
            $rank++;
        }
        return "";
    }

}
