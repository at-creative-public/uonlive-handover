<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
		// if (! $this->session->has_userdata('username')) {
		// 	redirect ('at-admin/login');
		// }
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	}

	public function upload()
	{
		if (empty($_FILES)) {
			echo json_encode(['success' => false, 'data' => [], 'message' => '沒有上載文件'], JSON_UNESCAPED_UNICODE);
			exit();
		}
		$field = key($_FILES);
		foreach ($_FILES[$field]['error'] as $err) {

			if ($err != 0) {
				echo json_encode(['success' => false, 'data' => [], 'message' => '文件上載失敗'], JSON_UNESCAPED_UNICODE);
				exit();
			}
		}
		if (! $this->input->post()) {
			echo json_encode(['success' => false, 'data' => [], 'message' => '發生錯誤'], JSON_UNESCAPED_UNICODE);
			exit();
		}
		$request = $this->input->post();
		$fullPath = FCPATH . 'assets/img/uploads/' . $request['image_type'] . '/';
		if (! file_exists($fullPath)) {
			mkdir ($fullPath);
		}
		$return = [];
		switch ($request['image_type']) {
			case "members":
				$this->load->model('Member_model', 'member');
				$result = $this->member->image_handling($request, $_FILES);
				if (! $result['success']) {
					echo json_encode(['success' => false, 'data' => [], 'message' => $result['error_message']], JSON_UNESCAPED_UNICODE);
					exit();
				}
				$return = $result['data'];
				break;
			case "providers":
				$this->load->model('Provider_model', 'provider');
				$result = $this->provider->image_handling($request, $_FILES);
				if (! $result['success']) {
					echo json_encode(['success' => false, 'data' => [], 'message' => $result['error_message']], JSON_UNESCAPED_UNICODE);
					exit();
				}
				$return = $result['data'];
				break;
			case "services":
				$this->load->model('Provider_service_model', 'service');
				$result = $this->service->image_handling($request, $_FILES);
				if (! $result['success']) {
					echo json_encode(['success' => false, 'data' => [], 'message' => $result['error_message']], JSON_UNESCAPED_UNICODE);
					exit();
				}
				$return = $result['data'];
				break;
			default:
				$fullPath = FCPATH . 'assets/img/uploads/' . $request['image_type'] . '/';
				if (! file_exists($fullPath)) {
					mkdir ($fullPath);
				}
				foreach ($_FILES[$field]['name'] as $idx => $name) {
					$fileExt = pathinfo($name, PATHINFO_EXTENSION);
					$newFilename = md5(rand(1, 10) . microtime()) . '.' . $fileExt;
		
					if (!move_uploaded_file($_FILES[$field]['tmp_name'][$idx], $fullPath . $newFilename)) {
						echo json_encode(['success' => false, 'data' => [], 'message' => '圖片讀寫錯誤'], JSON_UNESCAPED_UNICODE);
						exit();
					}
					$return[] = $newFilename;
				}
				break;
		}
		echo json_encode(['success' => true, 'data' => $return, 'message' => '圖片上載成功'], JSON_UNESCAPED_UNICODE);
	}
}
