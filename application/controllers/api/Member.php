<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
	}

    public function like()
    {
        $request = $this->input->get();
        $this->load->model('Member_like_model', 'like');
        $result = $this->like->toggle($request['member_id'], $request['provider_id']);
		echo json_encode(['success' => true, 'data' => $result, 'message' => '成功'], JSON_UNESCAPED_UNICODE);

    }
 
    public function bookmark()
    {
        $request = $this->input->get();
        $this->load->model('Member_bookmark_model', 'bookmark');
        $result = $this->bookmark->toggle($request['member_id'], $request['provider_id']);
		echo json_encode(['success' => true, 'data' => $result, 'message' => '成功'], JSON_UNESCAPED_UNICODE);

    }

    public function leave_message()
    {
        $request = $this->input->get();
        $this->load->model('Conversion_model', 'conversion');
        foreach ($request['provider_id'] as $provider) {
            $this->conversion->ask([
                'member_id' => $request['member_id'],
                'provider_id' => $provider,
                'parent_id' => $request['parent_id'],
                'title' => $request['title'],
                'message' => $request['message'],
            ]);
        }
		echo json_encode(['success' => true, 'data' => [], 'message' => '成功'], JSON_UNESCAPED_UNICODE);
    }

    public function reply_message()
    {
        $request = $this->input->get();
        $this->load->model('Conversion_model', 'conversion');
        $this->conversion->reply([
            'member_id' => $request['member_id'],
            'provider_id' => $request['provider_id'],
            'parent_id' => $request['parent_id'],
            'title' => $request['title'],
            'message' => $request['message'],
        ]);
		echo json_encode(['success' => true, 'data' => [], 'message' => '成功'], JSON_UNESCAPED_UNICODE);
    }

    public function registered_email()
    {
        $request = $this->input->get();
        if (! array_key_exists('email', $request)) {
            echo json_encode(['success' => false, 'data' => [], 'message' => '參數錯誤'], JSON_UNESCAPED_UNICODE);
            exit;
        }
        $this->load->model('Member_model', 'member');
        $member = $this->member->getMemberByEmail($request['email']);
        if (empty($member)) {
            echo json_encode(['success' => true, 'data' => ['token' => ''], 'message' => '成功'], JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode(['success' => true, 'data' => ['token' => $member['token']], 'message' => '成功'], JSON_UNESCAPED_UNICODE);
    }

    public function check_update_email()
    {
        $request = $this->input->get();
        if (! array_key_exists('email', $request)) {
            echo json_encode(['success' => false, 'data' => [], 'message' => '參數錯誤'], JSON_UNESCAPED_UNICODE);
            exit;
        }
        if (! array_key_exists('token', $request)) {
            echo json_encode(['success' => false, 'data' => [], 'message' => '參數錯誤'], JSON_UNESCAPED_UNICODE);
            exit;
        }
        $this->load->model('Member_model', 'member');
        $member = $this->member->getMemberByEmailToken($request['email'], $request['token']);
        if (empty($member)) {
            echo json_encode(['success' => true, 'data' => ['result' => false], 'message' => '成功'], JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode(['success' => true, 'data' => ['result' => true], 'message' => '成功'], JSON_UNESCAPED_UNICODE);
    }

}
