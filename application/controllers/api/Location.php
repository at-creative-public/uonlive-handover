<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends MY_AdminController
{

	public function __construct()
	{
		parent::__construct();
	}

    public function nearby_shops()
    {
        $request = $this->input->post();
        $this->load->model('Provider_model', 'provider');
        $result = $this->provider->getNearbyShops($request);
		echo json_encode(['success' => true, 'data' => $result, 'message' => '成功'], JSON_UNESCAPED_UNICODE);

    }

}