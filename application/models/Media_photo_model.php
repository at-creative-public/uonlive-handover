<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Media_photo_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_media_photo';
    }

    public function get($media_id)
    {
        return $this->db->where(['media_id' => $media_id])
            ->from($this->table)
            ->get()->row_array();
    }

    public function create($params, $media_id)
    {
        $this->db->insert($this->table, [
            'media_id' => $media_id,
            'filename' => $params['media_file'],
        ]);
    }

    public function update($params, $media_id)
    {
        if ($params['media_file'] != '') {
            $video = $this->get($media_id);
            unlink (FCPATH . 'assets/img/uploads/medias/' . $video['filename']);
            $this->db->where(['media_id' => $media_id])
                ->update($this->table, [
                'filename' => $params['media_file'],
            ]);
        }
    }

}
