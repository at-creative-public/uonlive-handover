<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Media_video_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_media_video';
    }

    public function get($media_id)
    {
        return $this->db->where(['media_id' => $media_id])
            ->from($this->table)
            ->get()->row_array();
    }

    public function keywordSearch($search, $num = 10)
    {
        $result = $this->db->like('title', $search)
            ->limit($num)
            ->from($this->table)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function create($params, $media_id)
    {
        $this->db->insert($this->table, [
            'media_id' => $media_id,
            'title' => $params['video_title'],
            'url' => $params['video_link'],
            'filename' => $params['media_file'],
        ]);
    }

    public function update($params, $media_id)
    {
        $data = [
            'title' => $params['video_title'],
            'url' => $params['video_link'],
        ];
        if ($params['media_file'] != '') {
            $video = $this->get($media_id);
            unlink (FCPATH . 'assets/img/uploads/medias/' . $video['filename']);
            $data['filename'] = $params['media_file'];
        }
        $this->db->where(['media_id' => $media_id])
            ->update($this->table, $data);
    }

}
