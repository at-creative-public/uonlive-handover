<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Media_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_media';
    }

    public function get($id)
    {
        return $this->db->from($this->table)
            ->where(['id' => $id])
            ->get()->row_array();
    }

    public function getByToken($token)
    {
        $result = $this->db->from($this->table)    
            ->where(['token' => $token])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $this->load->model('Media_photo_model', 'photo');
        $this->load->model('Media_news_model', 'news');
        $this->load->model('Media_video_model', 'video');
        $this->load->model('Hashtag_model', 'hashtag');
        switch ($result['media_type'])
        {
            case "NEWS":
                $result['media'] = $this->news->get($result['id']);
                break;
            case "PHOTO":
                $result['media'] = $this->photo->get($result['id']);
                break;
            case "VIDEO":
                $result['media'] = $this->video->get($result['id']);
                break;
        }
        $result['hashtags'] = $this->hashtag->get_media_hashtags($result['id']);
        return $result;
    }

    public function getAll()
    {
        $result = $this->db->from($this->table)
            ->get()->result_array();
        $this->load->model('Media_photo_model', 'photo');
        $this->load->model('Media_news_model', 'news');
        $this->load->model('Media_video_model', 'video');
        $this->load->model('Hashtag_model', 'hashtag');
        foreach ($result as $idx => $res)
        {
            switch ($res['media_type'])
            {
                case "NEWS":
                    $result[$idx]['media'] = $this->news->get($res['id']);
                    break;
                case "PHOTO":
                    $result[$idx]['media'] = $this->photo->get($res['id']);
                    break;
                case "VIDEO":
                    $result[$idx]['media'] = $this->video->get($res['id']);
                    break;
            }
            $result[$idx]['hashtags'] = $this->hashtag->get_media_hashtags($res['id']);
        }
        return $result;
    }

    public function getAllByCategory($category_id, $params)
    {
        $this->db->from($this->table)
            ->select($this->table . '.*, _providers.brand_provider, _providers.shop_name, _providers.shop_uri, _providers.owner_name, _providers.shop_photo, _providers.avatar_photo, _providers.tag_2nd_list_id, _providers.head_office_area')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->where(['category_id' => $category_id]);
        if (count($params['regions']) > 0) {
            $this->db->where_in('head_office_area', $params['regions']);
        }
        if (count($params['tags']) > 0) {
            $this->db->where_in('tag_2nd_list_id', $params['tags']);
        }
        $result = $this->db->get()->result_array();
        $this->load->model('Media_photo_model', 'photo');
        $this->load->model('Media_news_model', 'news');
        $this->load->model('Media_video_model', 'video');
        $this->load->model('Hashtag_model', 'hashtag');
        foreach ($result as $idx => $res)
        {
            switch ($res['media_type'])
            {
                case "NEWS":
                    $result[$idx]['media'] = $this->news->get($res['id']);
                    break;
                case "PHOTO":
                    $result[$idx]['media'] = $this->photo->get($res['id']);
                    break;
                case "VIDEO":
                    $result[$idx]['media'] = $this->video->get($res['id']);
                    break;
            }
            $result[$idx]['hashtags'] = $this->hashtag->get_media_hashtags($res['id']);
        }
        return $result;
    }

    public function getAllByProvider($provider_id)
    {
        $result = $this->db->from($this->table)
            ->select($this->table . '.*, _providers.brand_provider, _providers.shop_name, _providers.owner_name, _providers.shop_photo, _providers.avatar_photo')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->where(['providers_id' => $provider_id])
            ->get()->result_array();
        $this->load->model('Media_photo_model', 'photo');
        $this->load->model('Media_news_model', 'news');
        $this->load->model('Media_video_model', 'video');
        $this->load->model('Hashtag_model', 'hashtag');
        foreach ($result as $idx => $res)
        {
            switch ($res['media_type'])
            {
                case "NEWS":
                    $result[$idx]['media'] = $this->news->get($res['id']);
                    break;
                case "PHOTO":
                    $result[$idx]['media'] = $this->photo->get($res['id']);
                    break;
                case "VIDEO":
                    $result[$idx]['media'] = $this->video->get($res['id']);
                    break;
            }
            $result[$idx]['hashtags'] = $this->hashtag->get_media_hashtags($res['id']);
        }
        return $result;
    }

    public function keywordSearch($search, $num = 10)
    {
        $this->load->model('Media_news_model', 'news');
        $this->load->model('Media_video_model', 'video');
        return [
            'news' => $this->news->keywordSearch($search, $num),
            'video' => $this->video->keywordSearch($search, $num),
        ];
    }

    public function create($params)
    {
        $params['media_type'] = strtoupper($params['media_type']);
        $this->load->helper('common');
        $data = [
            'token' => generateRandomString(32),
            'category_id' => $params['category'],
            'tag_2nd_list_id' => $params['tag_2nd_list'],
            'providers_id' => $this->session->userdata('uonlive_user')['provider']['id'],
            'media_type' => $params['media_type'],
            'visible' => $params['show_in_shop'],
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            ->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        if ($params['tags'] != "") {
            $temp = explode(',', $params['tags']);
            $hashtags = array_map(function ($tag) { return trim($tag); }, $temp);
            $this->load->model('Hashtag_model', 'hashtag');
            $this->hashtag->create($hashtags);
            $this->hashtag->build_media_hashtags($hashtags, $insert_id);
        }
        switch ($params['media_type']) {
            case 'PHOTO':
                $this->load->model('Media_photo_model', 'photo');
                $this->photo->create($params, $insert_id);
                break;
            case 'NEWS':
                $this->load->model('Media_news_model', 'news');
                $this->news->create($params, $insert_id);
                break;
            case 'VIDEO':
                $this->load->model('Media_video_model', 'video');
                $this->video->create($params, $insert_id);
                break;
        }
    }

    public function update($params)
    {
        $media = $this->getByToken($params['token']);
        $data = [
            'category_id' => $params['category'],
            'tag_2nd_list_id' => $params['tag_2nd_list'],
            'visible' => $params['show_in_shop'],
        ];
        $this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
            ->where(['token' => $params['token']])
            ->update($this->table, $data);
        if ($params['tag'] != '') {
            $temp = explode(',', $params['tags']);
            $hashtags = array_map(function ($tag) { return trim($tag); }, $temp);
            $this->load->model('Hashtag_model', 'hashtag');
            $this->hashtag->create($hashtags);
            $this->hashtag->build_media_hashtags($hashtags, $insert_id);
        }
        switch ($params['media_type']) {
            case 'PHOTO':
                $this->load->model('Media_photo_model', 'photo');
                $this->photo->update($params, $media['id']);
                break;
            case 'NEWS':
                $this->load->model('Media_news_model', 'news');
                $this->news->update($params, $media['id']);
                break;
            case 'VIDEO':
                $this->load->model('Media_video_model', 'video');
                $this->video->update($params, $media['id']);
                break;
        }
    }

}
