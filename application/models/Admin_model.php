<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
        $this->table = '_admin';
	}

	public function getByUsername($username)
	{
		return $this->db->where(['username' => $username])
			->from($this->table)
			->get()->row_array();
	}

	public function one($id)
	{
		return $this->db->where(['id' => $id])
			->from($this->table)
			->get()->row_array();
	}

	public function getAll()
	{
		$result = $this->db->from($this->table)
			->get()->result_array();
		for ($i = 0; $i < count($result); $i++) {
			unset($result[$i]['password']);
			unset($result[$i]['remember_token']);
		}
		return $result;
	}

	public function checkEmail($email)
	{
		return $this->db->where(['email' => $email])
			->from($this->table)
			->get()->row_array();
	}

	public function add($params)
	{
		$user = $this->checkEmail($params['email']);
		if ($user) {
			return 4;
		}
		$this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
			->insert($this->table, [
				'username' => $params['username'],
				'password' => $this->encrypt_password($params['password']),
				'email' => $params['email'],
			]);
		return;
	}

	public function modify($params)
	{
		$admin = [
			'username' => $params['username'],
			'email' => $params['email'],
		];
		if (in_array('password', $params)) {
			$admin['password'] = $this->encrypt_password($params['password']);
		}
		$this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
			->where(['id' => $params['id']])
			->update($this->table, $admin);
		return;
	}

	public function checkLogin($username, $password)
	{
		$admin = $this->getByUsername($username);
		if (empty($admin)) {
			return 1;
		}
		if ($admin['password'] != $this->encrypt_password($password)) {
			return 2;
		}
		$userinfo = [
			'username' => $admin['username'],
			'user_id' => $admin['id'],
			'email' => $admin['email'],
		];
		$this->session->set_userdata('uonlive_admin_user', $userinfo);
		return 0;
	}

	public function logout()
	{
		$this->session->unset_userdata('uonlive_admin_user');
		return true;
	}
}
