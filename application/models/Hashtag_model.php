<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Hashtag_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_hashtags';
    }

    private function _insert_if_not_exists($tag)
    {
        $this->load->helper('common');
        if ($this->db->where(['name' => $tag])
            ->from($this->table)
            ->count_all_results() == 0) {
                $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
                ->insert($this->table, [
                    'name' => $tag,
                    'uri' => generateSeoURL($tag),
                    'count' => 0,
                ]);
        }
    }

    public function getLatest($num = 10)
    {
        $result = $this->db->from($this->table)    
            ->order_by('created_at', 'DESC')
            ->limit($num)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function getLatestByCategory($cat, $params, $num = 10)
    {
        $this->db->from($this->table)
            ->select($this->table . '.*, _providers.id AS providers_id, _providers.tag_list_id, _providers.tag_2nd_list_id, _providers.head_office_area')
            ->join('_providers_hashtag', '_providers_hashtag.hashtag_id = ' . $this->table . '.id')
            ->join('_providers', '_providers_hashtag.providers_id = _providers.id')
            ->where(['tag_list_id' => $cat, '_providers.status' => 'publish'])
            ->order_by('created_at', 'DESC')
            ->limit($num);
        if (count($params['regions']) > 0) {
            $this->db->where_in('head_office_area', $params['regions']);
        }
        if (count($params['tags']) > 0) {
            $this->db->where_in('tag_2nd_list_id', $params['tags']);
        }
        $result = $this->db->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function keywordSearch($search, $num = 10)
    {
        $result = $this->db->like('name', $search)
            ->select('_providers_hashtag.hashtag_id, _providers.*')
            ->from($this->table)
            ->join('_providers_hashtag', '_providers_hashtag.hashtag_id = ' . $this->table . '.id')
            ->join('_providers', '_providers_hashtag.providers_id = _providers.id')
            ->where(['_providers.status' => 'publish'])
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function create($params)
    {
        foreach ($params as $tag)
        {
            $this->_insert_if_not_exists(trim($tag));
        }

    }

    public function getMediasByHashtagUri($uri)
    {
        return $this->db->where([$this->table . '.uri' => $uri])
            ->from($this->table)
            ->select($this->table . '.*, _providers.shop_name, _providers.shop_uri, _providers.head_office_area, _providers.head_office_building, _providers.phone, _providers.business_hours, _providers.shop_photo')
            ->join('_media_hashtag', '_media_hashtag.hashtag_id = ' . $this->table . '.id')
            ->join('_media', '_media.id = _media_hashtag.media_id')
            ->join('_providers', '_media.providers_id = _providers.id')
            ->where(['_providers.status' => 'publish'])
            ->get()->result_array();
    }

    public function getProvidersByHashtagUri($uri)
    {
        return $this->db->where([$this->table . '.uri' => $uri])
            ->from($this->table)
            ->select($this->table . '.*, _providers.shop_name, _providers.shop_uri, _providers.head_office_area, _providers.head_office_building, _providers.phone, _providers.business_hours, _providers.shop_photo')
            ->join('_providers_hashtag', '_providers_hashtag.hashtag_id = ' . $this->table . '.id')
            ->join('_providers', '_providers_hashtag.providers_id = _providers.id')
            ->where(['_providers.status' => 'publish'])
            ->get()->result_array();
    }

    public function get_hashtags($hashtags)
    {
        $result = $this->db->where_in('name', $hashtags)
            ->from($this->table)
            ->get()->result_array();
        $return = [];
        foreach ($result as $res)
        {
            $return[$res['id']] = $res;
        }
        return $return;
    }

    public function get_media_hashtags($media_id)
    {
        return $this->db->where(['media_id' => $media_id])
            ->from('_media_hashtag hm')
            ->join($this->table . ' h', 'h.id = hm.hashtag_id')
            ->get()->result_array();
    }

    public function get_provider_hashtags($provider_id)
    {
        $hashtag = $this->db->from('_providers_hashtag pht')
            ->select('pht.*, ht.name as hashtag, ht.uri as hashtag_uri')
            ->join('_hashtags ht', 'pht.hashtag_id = ht.id')
            ->where(['providers_id' => $provider_id])
            ->get()->result_array();
        if ($hashtag) {
            return $hashtag;
        }
        return [];
    }

    public function build_provider_hashtags($hashtags, $provider_id)
    {
        $result = $this->get_hashtags($hashtags);
        $data = [];
        foreach ($result as $res)
        {
            $data[] = [
                'providers_id' => $provider_id,
                'hashtag_id' => $res['id'],
            ];
        }
        $this->db->where(['providers_id' => $provider_id])
            ->delete('_providers_hashtag');
        $this->db->insert_batch('_providers_hashtag', $data);
    }

    public function build_media_hashtags($hashtags, $media_id)
    {
        $result = $this->get_hashtags($hashtags);
        $data = [];
        foreach ($result as $res)
        {
            $data[] = [
                'media_id' => $media_id,
                'hashtag_id' => $res['id'],
            ];
        }
        $this->db->where(['media_id' => $media_id])
            ->delete('_media_hashtag');
        $this->db->insert_batch('_media_hashtag', $data);
    }

    public function convert()
    {
        $this->load->helper('common');
        $result = $this->db->get($this->table)->result_array();
        foreach ($result as $res) {
            $this->db->where(['id' => $res['id']])->update($this->table, ['uri' => generateSeoURL($res['name'])]);
        }
    }

}
