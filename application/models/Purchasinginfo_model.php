<?php
// use app\libraries\CommonHelper;
// use app\libraries\DateHelper;
// use app\libraries\Tenants;
defined('BASEPATH') or exit('No direct script access allowed');

class Purchasinginfo_model extends MY_Model
{
	public $table;

	public function __construct()
	{
		parent::__construct();
		$this->table = '_purchasing_info';
	}

	public function get($member_id)
	{
		return $this->db->from($this->table)
			->where(['member_id' => $member_id, 'payment_status' => 'COMPLETE'])
			->order_by('order_no', 'ASC')
			->get()->result_array();
	}

	public function create($params)
	{
		$this->load->helper('common');
		$token = strtolower(generateRandomString(25));
		$data = array_merge($params, [
			'member_id' => $params['member_id'],
			'checkout_session' => $params['checkout_session'],
			'effective_date' => $params['effective_date'],
			'expiry_date' => $params['expiry_date'],
			'payment_status' => 'NOT_START',
		]);
		$this->db->set('created_at', 'NOW()', false);
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function checkExpiryDate($member_id)
    {
        $purchasing = $this->get($member_id);
        if (! $purchasing) {
            return null;
        }
        $last = $this->db->from($this->table)
            ->order_by('expiry_date', 'DESC')
            ->limit(1)
            ->get()->row_array();
		return $last['expiry_date'];
    }

    public function getExpiryDate()
    {
        $purchasing = $this->get($this->session->userdata('uonlive_user')['id']);
        if (! $purchasing) {
            return strtotime('now');
        }
        $last = $this->db->from($this->table)
            ->order_by('expiry_date', 'DESC')
            ->limit(1)
            ->get()->row_array();
        if ($last['expiry_date'] >= strtotime('now')) {
            return strtotime("+1 day", $last['expiry_date']);
        }
        else {
            return strtotime('now');
        }
    }

	public function update($params, $id)
	{
		$data = array_merge($params, [
		]);
		$this->db->set('updated_at', 'NOW()', false);
		$this->db->where(['id' => $id])
			->update($this->table, $data);
		// $this->load->model('Landing_model', 'landing');
		// $landing = $this->landing->getLandingPageByToken($this->session->userdata('landing_token'));
		// $this->load->model('Course_Model', 'course');
		// $course = $this->course->getCourseByFlowId($landing['flow_id']);
		// $this->load->model('Sendgrid_model', 'sendgrid');
		// $this->sendgrid->sendmail(
		// 	[
		// 		'subject' => $course['name'],
		// 		'email' => $this->session->userdata('member_info')['email'],
		// 		// 'name' => $params['registrant_name'],
		// 		// 'token' => $token,
		// 		'email_template' => 'day2',
		// 		'variables' => [
		// 			'registrant_name' => $this->session->userdata('member_info')['display_name'],
		// 			'order_no' => $params['order_no'],
		// 			// 'registrant_token' => $token,
		// 			'course_name' => $course['name'],
		// 			'username' => $this->session->userdata('member_info')['email'],
		// 			'password' => ($this->session->userdata('member_info')['password'] == '') ? '&lt;使用你原有的密碼&gt;' : $this->session->userdata('member_info')['password'],
		// 		],
		// 	]
		// );
	}

	public function update_payment_status($status, $id)
	{
		$data = [
			'payment_status' => $status,
		];
		$this->db->where('id', $id)
			->update($this->tbl, $data);
	}

	public function getNextOrderNo($prefix)
	{
		$enroll = $this->db->like('order_no', $prefix, 'after')
			->from($this->table)
			->order_by('order_no', 'DESC')
			->get()->row_array();
		if (! $enroll) {
			$new_no = 1;
		}
		else {
			$new_no = intval(substr($enroll['order_no'], strlen($prefix))) + 1;
		}
		return sprintf('%s%04d', $prefix, $new_no);
	}

	public function getByMember()
	{
		return $this->db->from($this->table)
			->where(['member_id' => $this->session->userdata('uonlive_user')['id']])
			->order_by('created_at', 'DESC')
			->get()->result_array();
	}

	public function getAll()
	{
		return $this->db->from($this->table)
			->select($this->table . '.*, _members.first_name, _members.last_name')
			->join('_members', '_members.id = ' . $this->table . '.member_id')
			// ->where(['member_id' => $this->session->userdata('uonlive_user')['id']])
			->order_by('created_at', 'DESC')
			->get()->result_array();
	}

}
