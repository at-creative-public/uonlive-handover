<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Provider_brands_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_providers_brand';
    }

    public function getAll($provider_id)
    {
        $result = $this->db->from($this->table)    
            ->where(['providers_id' => $provider_id])
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function getLatest($num = 10)
    {
        return $this->db->from($this->table)
            ->select($this->table . '.*, _providers.shop_name, _providers.shop_uri, _district.name AS district_name, _providers.head_office_building')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->join('_district', '_district.id = _providers.head_office_area')
            // ->where([$this->table . '.providers' => 1])
            ->where(['_providers.status' => 'publish'])
            ->order_by('created_at', 'DESC')
            ->limit($num)
            ->get()->result_array();
    }

    public function getLatestByCategory($cat, $params, $num = 10)
    {
        $this->db->from($this->table)
            ->select($this->table . '.*, _providers.shop_name, _providers.shop_uri, _district.name AS district_name, _providers.head_office_building, _providers.tag_list_id, _providers.tag_2nd_list_id, _providers.head_office_area')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->join('_district', '_district.id = _providers.head_office_area')
            ->where(['tag_list_id' => $cat, '_providers.status' => 'publish'])
            ->order_by('created_at', 'DESC')
            ->limit($num);
        if (count($params['regions']) > 0) {
            $this->db->where_in('head_office_area', $params['regions']);
        }
        if (count($params['tags']) > 0) {
            $this->db->where_in('tag_2nd_list_id', $params['tags']);
        }
        return $this->db->get()->result_array();
    }

    public function create($params)
    {
        $data = [
            'providers_id' => $params['providers_id'],
            'brand' => trim($params['brand']),
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            // ->set('expired_at', 'UNIX_TIMESTAMP(DATE_SUB(DATE_ADD(CURDATE(),INTERVAL 1 MONTH), INTERVAL 1 DAY))', false)
            ->from($this->table)    
            ->insert($this->table, $data);
        // $insert_id = $this->db->insert_id();
        
        // return $insert_id;
    }

    public function update($params)
    {
        $data = [
            'brand' => trim($params['brand']),
        ];
        $this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
            ->where(['id' => $params['id']])
            ->update($this->table, $data);
        return true;
    }

    public function delete($id)
    {
        $this->db->where(['id' => $id])
            ->delete($this->table);
    }

    public function deleteAll($provider_id)
    {
        $this->db->where(['providers_id' => $provider_id])
            ->delete($this->table);
    }

}
