<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Conversion_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_conversion';
    }

    public function ask($params)
    {
        $this->load->helper('common');
        $data = [
            'message_type' => 'ask',
            'parent_id' => $params['parent_id'],
            'member_id' => $params['member_id'],
            'provider_id' => $params['provider_id'],
            'title' => $params['title'],
            'message_uri' => generateRandomString(10) . '__' . generateSeoURL($params['title']),
            'message' => $params['message'],
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            ->insert($this->table, $data);
    }

    public function reply($params)
    {
        $this->load->helper('common');
        $data = [
            'message_type' => 'reply',
            'parent_id' => $params['parent_id'],
            'member_id' => $params['member_id'],
            'provider_id' => $params['provider_id'],
            'title' => $params['title'],
            'message_uri' => generateRandomString(10) . '__' . generateSeoURL($params['title']),
            'message' => $params['message'],
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            ->insert($this->table, $data);
    }

    public function markRead($id) 
    {
        $this->db->where(['id' => $id])
            ->update($this->table, ['is_read' => 1]);
        return;
    }

    public function getByUri($uri) 
    {
        $result = $this->db->from($this->table)
            ->select($this->table . '.*, _members.first_name, _members.last_name, _members.email, _members.phone, _members.avatar_photo')
            ->join('_members', '_members.id = ' . $this->table . '.member_id')
            ->where(['message_uri' => $uri])
            ->get()->row_array();
        if ($result) {
            $result['replies'] = $this->getByParentId($result['id']);
        }
        return $result;
    }

    public function getByParentId($parent_id) 
    {
        return $this->db->from($this->table)
            ->select($this->table . '.*, _members.first_name, _members.last_name, _members.email AS member_email, _members.phone AS member_phone, _members.avatar_photo, _providers.brand_provider, _providers.shop_name, _providers.shop_uri, _providers.head_office_building, _providers.phone AS provider_phone, _providers.email AS provider_email, _providers.business_hours, _providers.acting_brand, _providers.shop_photo')
            ->where(['parent_id' => $parent_id])
            ->join('_members', '_members.id = ' . $this->table . '.member_id')
            ->join('_providers', '_providers.id = ' . $this->table . '.provider_id')
            ->order_by('created_at', 'DESC')
            ->get()->result_array();
    }

    public function getByProvider($provider_id)
    {
        $result = $this->db->from($this->table)
            ->select($this->table . '.*, _members.first_name, _members.last_name, _members.email, _members.phone, _members.avatar_photo')
            ->join('_members', '_members.id = ' . $this->table . '.member_id')
            ->where([$this->table . '.provider_id' => $provider_id, 'parent_id' => 0])
            ->order_by('created_at', 'DESC')
            ->get()->result_array();
        foreach ($result as $idx => $res) {
            $result[$idx]['replies'] = $this->getByParentId($res['id']);
        }
        return $result;
    }

    public function getByMember($member_id)
    {
        return $this->db->from($this->table)
            ->select($this->table . '.*, _providers.brand_provider, _providers.shop_name, _providers.shop_uri, _providers.head_office_building, _providers.phone, _providers.email, _providers.business_hours, _providers.acting_brand, _providers.shop_photo')
            ->join('_providers', '_providers.id = ' . $this->table . '.provider_id')
            ->where([$this->table . '.member_id' => $member_id, 'parent_id' => 0])
            ->order_by('created_at', 'DESC')
            ->get()->result_array();
        foreach ($result as $idx => $res) {
            $result[$idx]['replies'] = $this->getByParentId($res['id']);
        }
        return $result;
    }
}
