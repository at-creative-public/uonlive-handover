<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Website_setting_model extends MY_Model{

    public function __construct()
    {
        parent::__construct();
        $this->table = "_website_setting";
    }

    public function update_seo($params){
        
        if(!empty($params)){
            $this->db->update('tbl_website_setting',$params);
        }
    }

    public function get(){
        return $this->db->from($this->table)
            ->get()
            ->row_array();
     
    }
}