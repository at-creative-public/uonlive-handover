<?php
defined("BASEPATH") or exit("No direct script access allowed");

class District_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_district';
    }

    public function get($id)
    {
        return $this->db->from($this->table)
            ->where(['id' => $id])
            ->get()->row_array();
    }

    public function getDistrictByArea($area_id)
    {
        return $this->db->from($this->table)
            ->where(['area_id' => $area_id])
            ->get()->result_array();
    }

    public function getAll()
    {
        $this->load->model('Area_model', 'area');
        $all_areas = $this->area->getAll();
        $district = [];
        foreach ($all_areas as $area)
        {
            $result = $this->getDistrictByArea($area['id']);
            $district[] = [
                'area_id' => $area['id'],
                'name' => $area['name'],
                'district' => $result,
            ];
        }
        return $district;
    }
}
