<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Member_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_members';
    }

    private function _get_extra_info($id)
    {
        $this->load->model('Provider_model', 'provider');
        return $this->provider->get($id);
    }

    public function check_login($email, $password)
    {
        $result = $this->db->from($this->table)    
            ->where(['email' => $email, 'password' => sha1($password)])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $result['provider'] = $this->_get_extra_info($result['id']);
        return $result;
    }

    public function reset_password($email)
    {
        $member = $this->getMemberByEmail($email);
        if (count($member) == 0) {
            return [];
        }
        $this->load->helper('common');
        $password = generateRandomString(8);
        $data = [
            'password' => sha1($password),
        ];
        $result = $this->db->where(['email' => $email])
            ->update($this->table, $data);
        $this->load->model('Sendgrid_model', 'sendgrid');
        $html = file_get_contents(FCPATH . 'application/libraries/templates/forget_password.html');
        $html = str_replace('{{base_url}}', base_url(), $html);
        $html = str_replace('{{display_name}}', $member['first_name'] . ', ' . $member['last_name'], $html);
        $html = str_replace('{{password}}', $password, $html);
        $html = str_replace('{{facebook}}', $this->config->item('uonlive')['social_network']['facebook'], $html);
        $html = str_replace('{{mail}}', $this->config->item('site_settings')['company_email'], $html);
        $this->sendgrid->sendmail([
            'subject' => '忘記密碼',
            'email' => $email,
            'html_content' => $html,
            'text_content' => $html,
        ]);
        return [
            'status' => 'success',
        ];
    }

    public function getActiveCount()
    {
        return $this->db->from($this->table)
            ->where(['deleted_at' => 0])
            ->count_all_results();
    }

    public function getAll()
    {
        return $this->db->from($this->table)
            ->order_by('created_at', 'DESC')
            ->get()->result_array();
    }

    public function reload_info()
    {
        $result = $this->db->from($this->table)    
            ->where(['id' => $this->session->userdata('uonlive_user')['id']])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $result['provider'] = $this->_get_extra_info($result['id']);
        return $result;
    }

    public function getMemberByEmail($email)
    {
        $result = $this->db->from($this->table)    
            ->where(['email' => $email])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $result['provider'] = $this->_get_extra_info($result['id']);
        return $result;
    }

    public function getMemberByEmailToken($email, $token)
    {
        $result = $this->db->from($this->table)    
            ->where(['email' => $email, 'token' => $token])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $result['provider'] = $this->_get_extra_info($result['id']);
        return $result;
    }

    public function getMemberByToken($token)
    {
        $result = $this->db->from($this->table)  
            ->select($this->table . '.*, a.name AS resident_area_string, b.name AS business_area_string')
            ->join('_district a', 'a.id = ' . $this->table . '.resident_area', 'left')  
            ->join('_district b', 'b.id = ' . $this->table . '.business_area', 'left')  
            ->where(['token' => $token])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $result['provider'] = $this->_get_extra_info($result['id']);
        return $result;
    }

    public function create($params)
    {
        $this->load->helper('common');
        $data = [
            'token' => generateRandomString(32),
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'phone' => $params['phone'],
            'occupation' => $params['occupation'],
            'resident_area' => $params['resident_area'],
            'resident_building' => $params['resident_building'],
            'resident_formatted_address' => $params['resident_formatted_address'], 
            'resident_latitude' => $params['resident_latitude'] * 10000000,
            'resident_latitude_rad' => deg2rad($params['resident_latitude']),
            'resident_longitude' => $params['resident_longitude'] * 10000000,
            'resident_longitude_rad' => deg2rad($params['resident_longitude']),
            'business_area' => $params['business_area'],
            'business_building' => $params['business_building'],
            'business_formatted_address' => $params['business_formatted_address'], 
            'business_latitude' => $params['business_latitude'] * 10000000,
            'business_latitude_rad' => deg2rad($params['business_latitude']),
            'business_longitude' => $params['business_longitude'] * 10000000,
            'business_longitude_rad' => deg2rad($params['business_longitude']),
            'password' => sha1($params['password']),
            'avatar_photo' => '',
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            ->from($this->table)    
            ->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($params)
    {
        $data = [
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'phone' => $params['phone'],
            'occupation' => $params['occupation'],
            'resident_area' => $params['resident_area'],
            'resident_building' => $params['resident_building'],
            'resident_formatted_address' => $params['resident_formatted_address'], 
            'resident_latitude' => $params['resident_latitude'] * 10000000,
            'resident_latitude_rad' => deg2rad($params['resident_latitude']),
            'resident_longitude' => $params['resident_longitude'] * 10000000,
            'resident_longitude_rad' => deg2rad($params['resident_longitude']),
            'business_area' => $params['business_area'],
            'business_building' => $params['business_building'],
            'business_formatted_address' => $params['business_formatted_address'], 
            'business_latitude' => $params['business_latitude'] * 10000000,
            'business_latitude_rad' => deg2rad($params['business_latitude']),
            'business_longitude' => $params['business_longitude'] * 10000000,
            'business_longitude_rad' => deg2rad($params['business_longitude']),
            // 'password' => sha1($params['password']),
        ];
        if (array_key_exists('password', $params)) {
            if ($params['password'] != "") {
                $data['password'] = sha1($params['password']);
            }
        }
        if (array_key_exists('avatar_file', $params)) {
            if ($params['avatar_file'] != "") {
                $data['avatar_photo'] = $params['avatar_file'];
            }
        }
        if (array_key_exists('remove_avatar', $params)) {
            $member = $this->getMemberByEmail($params['login_name']);
            if ($member['avatar_photo'] != '') {
                $path = 'assets/img/uploads/members/' . $member['avatar_photo'];
                if (file_exists(FCPATH . $path)) {
                    unlink (FCPATH . $path);
                }
            }
            $data['avatar_photo'] = '';
        }
        $this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
            ->where(['email' => $params['login_name']])
            ->update($this->table, $data);
        return true;
    }

	public function image_handling($request, $files)
	{
		$data = [];
		if (! is_array($request['image_id'])) {
			return [
				'success' => false,
				'error_message' => '傳送參數錯誤',
				'data' => [],
			];
		}
		$fullPath = FCPATH . 'assets/img/uploads/' . $request['image_type'] . '/';
		if (! file_exists($fullPath)) {
			mkdir ($fullPath);
		}
		foreach ($files['image']['name'] as $idx => $name) {
			$fileExt = pathinfo($name, PATHINFO_EXTENSION);
			$newFilename = md5(rand(1, 10) . microtime()) . '.' . $fileExt;
			if (!move_uploaded_file($files['image']['tmp_name'][$idx], $fullPath . $newFilename)) {
				return [
					'success' => false,
					'error_message' => '圖片讀寫錯誤',
					'data' => [],
				];
			}
			$data[] = [
				'id' => $request['image_id'][$idx],
				'filename' => $newFilename,
			];
		}
		return [
			'success' => true,
			'error_message' => '',
			'data' => $data,
		];
	}

}
