<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Media_news_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_media_news';
    }

    public function get($media_id)
    {
        return $this->db->where(['media_id' => $media_id])
            ->from($this->table)
            ->get()->row_array();
    }

    public function keywordSearch($search, $num = 10)
    {
        $result = $this->db->like('title', $search)
            ->limit($num)
            ->from($this->table)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function create($params, $media_id)
    {
        $this->db->insert($this->table, [
            'media_id' => $media_id,
            'title' => $params['news_title'],
            'content' => $params['news_summary'],
            'url' => $params['news_link'],
        ]);
    }

    public function update($params, $media_id)
    {
        $this->db->where(['media_id' => $media_id])
            ->update($this->table, [
                'title' => $params['news_title'],
                'content' => $params['news_summary'],
                'url' => $params['news_link'],
        ]);
    }

}
