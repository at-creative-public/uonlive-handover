<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Provider_socials_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_providers_socials';
    }

    public function getAll($provider_id)
    {
        $result = $this->db->from($this->table)    
            ->where(['providers_id' => $provider_id])
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function social_exists($social_type, $provider_id)
    {
        if ($this->db->from($this->table)
            ->where(['social_type' => $social_type, 'providers_id' => $provider_id])
            ->count_all_results() > 0)
        {
            return true;
        }
        return false;
    }

    public function create($params)
    {
        $data = [
            'providers_id' => $params['providers_id'],
            'social_type' => $params['type'],
            'social_url' => $params['url'],
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            // ->set('expired_at', 'UNIX_TIMESTAMP(DATE_SUB(DATE_ADD(CURDATE(),INTERVAL 1 MONTH), INTERVAL 1 DAY))', false)
            ->from($this->table)    
            ->insert($this->table, $data);
        // $insert_id = $this->db->insert_id();
        
        // return $insert_id;
    }

    public function update($params)
    {
        $data = [
            'social_url' => $params['url'],
        ];
        $this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
            ->where(['providers_id' => $params['providers_id'], 'social_type' => $params['type']])
            ->update($this->table, $data);
        return true;
    }

}
