<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Area_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_area';
    }

    public function get($id)
    {
        return $this->db->from($this->table)
            ->where(['id' => $id])
            ->get()->row_array();
    }

    public function getAll()
    {
        return $this->db->from($this->table)
            ->get()->result_array();
    }
}
