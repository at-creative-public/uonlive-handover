<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Contact_us_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_contact';
    }

    public function save($params)
    {
        $data = [
            'email' => $params['email'],
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            ->insert($this->table, $data);
    }

}
