<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Provider_model extends MY_Model
{
    public $member_id;

    public function __construct()
    {
        parent::__construct();
        $this->table = '_providers';
    }

    public function getExtra($provider_id, $brand_provider) 
    {
        $result = [];
        $this->load->model('Hashtag_model', 'hashtag');
        $result['hashtags'] = $this->hashtag->get_provider_hashtags($provider_id);
        $result['socials'] = [];
        foreach ($this->config->item('uonlive')['socials'] as $social) {
            $result['socials'][$social] = [];
        }
        $this->load->model('Provider_socials_model', 'socials');
        $socials = $this->socials->getAll($provider_id);
        foreach ($socials as $social) {
            $result['socials'][$social['social_type']] = $social;
        }
        $result['brands'] = '';
        if ($brand_provider == 'B') {
            $this->load->model('Provider_brands_model', 'brands');
            $brands = $this->brands->getAll($provider_id);
            $tmp = [];
            foreach($brands as $brand) {
                $tmp[] = $brand['brand'];
            }
            $result['brands'] = implode(',', $tmp);
        }
        $this->load->model('Media_model', 'media');
        $result['medias'] = $this->media->getAllByProvider($provider_id);
        $this->load->model('Purchasinginfo_model', 'purchasinginfo');
        $result['expired_at'] = $this->purchasinginfo->checkExpiryDate($this->member_id);
        return $result;
    }

    public function get($member_id)
    {
        $this->member_id = $member_id;
        $result = $this->db->from($this->table)    
            ->where(['member_id' => $member_id])
            ->get()->row_array();
        if (! $result) {
            return [
                'brand_provider' => 'P',
                'brands' => '',
            ];
        }
        return array_merge($result, $this->getExtra($result['id'], $result['brand_provider']));
    }

    public function getById($id)
    {
        $result = $this->db->from($this->table)    
            ->where(['id' => $id])
            ->get()->row_array();
        if (! $result) {
            return [
                'brand_provider' => 'P',
                'brands' => '',
            ];
        }
        return array_merge($result, $this->getExtra($result['id'], $result['brand_provider']));
    }

    public function getActiveProviderCount()
    {
        return $this->db->from($this->table)
            ->where(['deleted_at' => 0, 'brand_provider' => 'P'])
            ->count_all_results();
    }

    public function getActiveBrandCount()
    {
        return $this->db->from($this->table)
            ->where(['deleted_at' => 0, 'brand_provider' => 'B'])
            ->count_all_results();
    }

    public function getLatest($brand_provider = 'B', $num = 10)
    {
        $result = $this->db->from($this->table)    
            ->where(['status' => 'publish', 'brand_provider' => $brand_provider])
            ->order_by('created_at', 'DESC')
            ->limit($num)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function getLatestByCategory($cat, $brand_provider = 'B', $params, $num = 10)
    {
        $this->db->from($this->table)    
            ->where(['status' => 'publish', 'brand_provider' => $brand_provider, 'tag_list_id' => $cat])
            ->order_by('created_at', 'DESC')
            ->limit($num);
        if (count($params['regions']) > 0) {
            $this->db->where_in('head_office_area', $params['regions']);
        }
        if (count($params['tags']) > 0) {
            $this->db->where_in('tag_2nd_list_id', $params['tags']);
        }
        $result = $this->db->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function keywordSearch($search, $num = 10)
    {
        $result = $this->db->like('shop_name', $search)
            ->or_like('owner_name', $search)
            ->or_like('head_office_building', $search)
            ->or_like('acting_brand', $search)
            ->where(['status' => 'publish'])
            ->from($this->table)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        return $result;
    }

    public function getByUri($uri)
    {
        $result = $this->db->from($this->table . ' p')
            ->select('p.*, t.name as tag_name, d.name as district_name')
            ->join('_tag_list t', 't.id = p.tag_list_id')
            ->join('_district d', 'd.id = p.head_office_area')
            ->where(['p.status' => 'publish', 'p.shop_uri' => $uri])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        return array_merge($result, $this->getExtra($result['id'], $result['brand_provider']));
    }
    
    public function getNearbyShops($params)
    {
        $radiusOfEarth = 6371;
        $lat = deg2rad($params['lat']);
        $lng = deg2rad($params['lng']);
        $this->db->select('*, ' . $radiusOfEarth . ' * (2 * asin(sqrt(' .
            'power(sin((latitude_rad - ' . $lat . ') / 2), 2) + ' .
            'cos(' . $lat . ') * ' .
            'cos(latitude_rad) * ' .
            'power(sin((longitude_rad - ' . $lng . ') / 2), 2)' .
            '))) AS `distance`')
            ->from($this->table)
            ->having(['`distance` <=' => $params['range'], 'status' => 'publish'])
            ->order_by('`distance`', 'ASC');
        if ($params['category'] != 0) {
            $this->db->having(['tag_list_id' => $params['category']]);
        }
        return $this->db->get()->result_array();
    }

    public function create()
    {
        $data = [
            'brand_provider' => 'P',
            'tag_list_id' => 1,
            'member_id' => $this->session->userdata('uonlive_user')['id'],
            'shop_name' => '',
            'shop_uri' => '',
            'owner_name' => '',
            'head_office_area' => 1,
            'head_office_building' => '',
            'head_office_formatted_address' => '',
            'latitude' => 0,
            'longitude' => 0,
            'phone' => '',
            'email' => '',
            'business_hours' => '',
            'acting_brand' => '',
            'brand_required' => '{}',
            'avatar_photo' => '',
            'shop_photo' => '',
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            // ->set('expired_at', 'UNIX_TIMESTAMP(DATE_ADD(' . $expiry_date . ', INTERVAL 30 DAY))', false)
            ->from($this->table)    
            ->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($params)
    {
        $this->load->helper('common');
        switch ($params['brand_provider']) {
            case "B":
                $params['business_hours'] = '';
                break;
            case "P":
                $params['acting_brand'] = [];
                $params['brand_required'] = [];
                break;
        }
        $data = [
            'brand_provider' => $params['brand_provider'],
            'tag_list_id' => $params['tag_list_id'],
            'shop_name' => $params['shop_name'],
            'shop_uri' => generateSeoURL($params['shop_name']),
            'owner_name' => $params['owner_name'],
            'head_office_area' => $params['head_office_area'],
            'head_office_building' => $params['head_office_building'],
            'head_office_formatted_address' => $params['head_office_formatted_address'],
            'latitude' => $params['latitude'] * 10000000,
            'latitude_rad' => deg2rad($params['latitude']),
            'longitude' => $params['longitude'] * 10000000,
            'longitude_rad' => deg2rad($params['longitude']),
            'phone' => $params['phone'],
            'email' => $params['email'],
            'business_hours' => $params['business_hours'],
            'acting_brand' => '',
            'brand_required' => json_encode($params['brand_required'], JSON_UNESCAPED_UNICODE),
            'status' => $params['status'],
        ];
        if (array_key_exists('avatar_file', $params)) {
            if ($params['avatar_file'] != "") {
                $data['avatar_photo'] = $params['avatar_file'];
            }
        }
        if (array_key_exists('shop_file', $params)) {
            if ($params['shop_file'] != "") {
                $data['shop_photo'] = $params['shop_file'];
            }
        }
        $this->load->model('Member_model', 'member');
        $member = $this->member->getMemberByEmail($params['login_name']);
        $provider = $this->get($member['id']);

        if (array_key_exists('remove_avatar', $params)) {
            if ($provider['avatar_photo'] != '') {
                $path = 'assets/img/uploads/providers/' . $provider['avatar_photo'];
                if (file_exists(FCPATH . $path)) {
                    unlink (FCPATH . $path);
                }
            }
            $data['avatar_photo'] = '';
        }
        if (array_key_exists('remove_banner', $params)) {
            if ($provider['shop_photo'] != '') {
                $path = 'assets/img/uploads/providers/' . $provider['shop_photo'];
                if (file_exists(FCPATH . $path)) {
                    unlink (FCPATH . $path);
                }
            }
            $data['shop_photo'] = '';
        }
        $this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
            ->where(['member_id' => $member['id']])
            ->update($this->table, $data);

        if ($params['tags'] != "") {
            $temp = explode(',', $params['tags']);
            $hashtags = array_map(function ($tag) { return trim($tag); }, $temp);
            $this->load->model('Hashtag_model', 'hashtag');
            $this->hashtag->create($hashtags);
            $this->hashtag->build_provider_hashtags($hashtags, $provider['id']);
        }

        $this->load->model('Provider_socials_model', 'socials');
        $data = [];
        $socials = $this->config->item('uonlive')['socials'];
        foreach ($socials as $social) {
            if (array_key_exists($social, $params)) {
                if ($params[$social] != '') {
                    if ($this->socials->social_exists($social, $provider['id'])) {
                        $this->socials->update([
                            'providers_id' => $provider['id'],
                            'type' => $social,
                            'url' => $params[$social],
                        ]);
                    }
                    else {
                        $this->socials->create([
                            'providers_id' => $provider['id'],
                            'type' => $social,
                            'url' => $params[$social],
                        ]);
                    }
                }
            }
        }

        $this->load->model('Provider_brands_model', 'brands');
        $brands = $this->brands->getAll($provider['id']);
        if ($params['brand_provider'] == 'B') {
            $new_brands = explode(",", $params['acting_brand']);
            $new = [];
            foreach ($new_brands as $idx => $brand) {
                if ($idx + 1 > count($brands)) {
                    $new[] = [
                        'id' => 0,
                        'providers_id' => $provider['id'],
                        'brand' => $brand,
                        'action' => 'create',
                    ];
                }
                else {
                    $new[] = [
                        'id' => $brands[$idx]['id'],
                        'providers_id' => $provider['id'],
                        'brand' => $brand,
                        'action' => 'update',
                    ];
                }
            }
            if (count($brands) > count($new_brands)) {
                for($i = count($new_brands); $i < count($brands); $i++) {
                    $new[] = [
                        'id' => $brands[$i]['id'],
                        'providers_id' => $provider['id'],
                        'brand' => '',
                        'action' => 'delete',
                    ];
                }
            }
            foreach ($new as $brand) {
                switch ($brand['action']) {
                    case 'create':
                        $this->brands->create([
                            'providers_id' => $brand['providers_id'],
                            'brand' => $brand['brand'],
                        ]);
                        break;
                    case 'update':
                        $this->brands->update([
                            'id' => $brand['id'],
                            'brand' => $brand['brand'],
                        ]);
                        break;
                    case 'delete':
                        $this->brands->delete($brand['id']);
                        break;
                }
            }
        }
        else {
            $this->brands->deleteAll($provider['id']);
        }
        return true;
    }

	public function image_handling($request, $files)
	{
		$data = [];
		if (! is_array($request['image_id'])) {
			return [
				'success' => false,
				'error_message' => '傳送參數錯誤',
				'data' => [],
			];
		}
		$fullPath = FCPATH . 'assets/img/uploads/' . $request['image_type'] . '/';
		if (! file_exists($fullPath)) {
			mkdir ($fullPath);
		}
		foreach ($files['image']['name'] as $idx => $name) {
			$fileExt = pathinfo($name, PATHINFO_EXTENSION);
			$newFilename = md5(rand(1, 10) . microtime()) . '.' . $fileExt;
			if (!move_uploaded_file($files['image']['tmp_name'][$idx], $fullPath . $newFilename)) {
				return [
					'success' => false,
					'error_message' => '圖片讀寫錯誤',
					'data' => [],
				];
			}
			$data[] = [
				'id' => $request['image_id'][$idx],
				'filename' => $newFilename,
			];
		}
		return [
			'success' => true,
			'error_message' => '',
			'data' => $data,
		];
	}

    public function service_update($params)
    {
        
    }

}
