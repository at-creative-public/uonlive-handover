<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Browsing_history_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_browsing_history';
    }

    public function add($params)
    {
        $data = [
            'member_id' => $params['member_id'],
            'page_type' => $params['page_type'],
            'page_id' => $params['page_id'],
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            ->insert($this->table, $data);
    }

    public function getByMember($member_id, $type)
    {
        $where = [];
        $where['member_id'] = $member_id;
        if ($type != '') {
            if ($type == 'category') {
                $where['page_type'] = 'c';
            }
            else {
                $where['page_type'] = 's';
            }
        }
        $result = $this->db->from($this->table)
            ->where($where)
            ->order_by('created_at', 'DESC')
            ->get()->result_array();
        $this->load->model('Tag_list_model', 'tag');
        $this->load->model('provider_model', 'provider');
        foreach ($result as $idx => $res) {
            switch($res['page_type']) {
                case 'c':
                    if (($type == '') || ($type == 'category')) {
                        $result[$idx]['page_info'] = $this->tag->getById($res['page_id']);
                    }
                    break;
                case 's':
                    $page = $this->provider->getById($res['page_id']);
                    if (($type == '') || 
                        (($type == 'brand') && ($page['brand_provider'] == 'B')) ||
                        (($type == 'provider') && ($page['brand_provider'] == 'P'))) {
                            $result[$idx]['page_info'] = $page;
                    }
                    else {
                        unset($result[$idx]);
                    }
                    break;
            }
        }
        return $result;
    }

    public function getByProvider($provider_id)
    {
        $where = [];
        $where['page_id'] = $provider_id;
        $where['page_type'] = 's';
        $result = $this->db->from($this->table)
            ->where($where)
            ->join('_members', '_members.id = ' . $this->table . '.member_id')
            ->order_by($this->table . '.created_at', 'DESC')
            ->get()->result_array();
        return $result;
    }
}
