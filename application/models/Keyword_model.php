<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keyword_model extends MY_Model{

    public function __construct()
    {
        parent::__construct();
        $this->table = '_suggested_keyword';
    }

    public function add($arr)
    {
        $max = $this->db->select_max('display_order')
            ->from($this->table)
            ->get()->row_array();
        if (! $max) {
            $new = 1;
        }
        else {
            $new = $max['display_order'] + 1;
        }
        $this->load->helper('common');
        $data = [
            'tag_code' => $arr['tag_code'],
            'tag_2nd_code' => $arr['tag_2nd_code'],
            'code' => generateRandomString(16),
            'name' => $arr['name'],
            'display_order' => $new,
            'status' => 1,
            'deleted' => 0,
        ];
        $this->db->set('create_time', 'NOW()', false)
            ->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function modify($arr)
    {
        $data = [
            'tag_code' => $arr['tag_code'],
            'tag_2nd_code' => $arr['tag_2nd_code'],
            'name' => $arr['name'],
        ];
        $this->db->where(['code' => $arr['code']])
            ->update($this->table, $data);
    }

    public function getByName($name)
    {
//		$this->db->select('blog_tag_list.id,blog_tag_list.name,blog_tag_list.color,blog_tag_list.status, blog_tag_list.deleted, blog_tag_list.display_order, blog_tag_list.remark, blog_tag_list.update_time, blog_tag_list.create_time');
        return $this->db->from($this->table)
            ->where(['name' => $name, 'deleted' => 0, 'status' => 1])
            // ->order_by("display_order", "asc")
            ->get()
            ->row_array();
    }

    public function getFirstTag()
    {
        return $this->db->from($this->table)
            ->where(['deleted' => 0, 'status' => 1])
            ->order_by("display_order", "asc")
            ->limit(1)
            ->get()
            ->row_array();
    }

    public function get()
    {
//		$this->db->select('blog_tag_list.id,blog_tag_list.name,blog_tag_list.color,blog_tag_list.status, blog_tag_list.deleted, blog_tag_list.display_order, blog_tag_list.remark, blog_tag_list.update_time, blog_tag_list.create_time');
        return $this->db->from($this->table)
            ->select($this->table . '.*, _tag_list.name AS tag_name, _tag_2nd_list.name AS tag_2nd_name')
            ->join('_tag_list', '_tag_list.code = ' . $this->table . '.tag_code')
            ->join('_tag_2nd_list', '_tag_2nd_list.code = ' . $this->table . '.tag_2nd_code')
            ->where([$this->table . '.status' => 1])
            ->order_by("display_order", "asc")
            ->get()
            ->result_array();
    }

    public function getById($id)
    {
        return $this->db->where('id', $id)
            ->get($this->table)
            ->row_array();
    }

    public function getByTagCode($tag_code, $tag_2nd_code)
    {
        return $this->db->where([$this->table . '.tag_code' => $tag_code, $this->table . '.tag_2nd_code' => $tag_2nd_code])
            ->select($this->table . '.*, _tag_list.name AS tag_name, _tag_2nd_list.name AS tag_2nd_name')
            ->join('_tag_list', '_tag_list.code = ' . $this->table . '.tag_code')
            ->join('_tag_2nd_list', '_tag_2nd_list.code = ' . $this->table . '.tag_2nd_code')
            ->order_by($this->table . '.display_order', 'ASC')
            ->get($this->table)
            ->result_array();
    }

    public function getByTagId($tag_id, $tag_2nd_id)
    {
        $this->load->model('Tag_list_model', 'tag_list');
        $this->load->model('Tag_2nd_list_model', 'tag_2nd_list');
        $tag_code = $this->tag_list->getById($tag_id)['code'];
        $tag_2nd_code = $this->tag_2nd_list->getById($tag_2nd_id)['code'];
        return $this->getByTagCode($tag_code, $tag_2nd_code);
    }

    public function getAllByTag()
    {

    }
    
    public function getByCode($code)
    {
        return $this->db->where(['code' => $code])
            ->get($this->table)
            ->row_array();
    }

    public function delete($id)
    {
        $this->db->where(['id' => $id])
            ->update($this->table, ['deleted' => 1]);
    }

}
