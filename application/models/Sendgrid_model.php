<?php

defined('BASEPATH') or exit('No direct script access allowed');

use SendGrid\Mail\Mail;

class Sendgrid_model extends MY_Model
{

// 	public function sendmail($params)
// 	{
// 		$this->load->library('twilio_sendgrid');
// 		/**@var Mail */
// 		$mail = $this->twilio_sendgrid->create();
// 		$mail->setFrom("developer@at-appmaker.com", $this->config->item('site_settings')['company_name']);
// 		$mail->setSubject($params['subject']);
// 		$mail->addTo($params['email']);
// //		$mail->addBcc('arvid@at-creative.com');
// //		$mail->addBcc('daniel@at-creative.com');
// //		$mail->addBcc('admin@wealthskey.com');
// 		$html = file_get_contents(APPPATH . "libraries/templates/nlp-" . $params['email_template'] . ".html");
// 		$html = str_replace('{{base_url}}', base_url(), $html);
// 		foreach ($params['variables'] as $key => $value) {
// 			$html = str_replace('{{' . $key . '}}', $value, $html);
// 		}
// //		$mail->addContent("text/plain", $params['text_content']);
// 		$mail->addContent(
// 			"text/html", $html
// 		);

// 		try {
// 			$sendResponse = $this->twilio_sendgrid->send($mail, true);
// 			return true;

// 			// Response
// 		} catch (Exception $e) {
// 			return false;
// 		}
// 	}

	public function sendmail($params)
	{
		$this->load->library('twilio_sendgrid');
		/**@var Mail */
		$mail = $this->twilio_sendgrid->create();
		$mail->setFrom($this->config->item('uonlive')['sendgrid']['sender'], $this->config->item('site_settings')['company_name']);
		$mail->setSubject($params['subject']);
		$mail->addTo($params['email']);
		$mail->addContent("text/plain", $params['text_content']);
		$mail->addContent(
			"text/html", $params['html_content']
		);

		try {
			$sendResponse = $this->twilio_sendgrid->send($mail, true);
			return true;

			// Response
		} catch (Exception $e) {
			return false;
		}
	}

}
