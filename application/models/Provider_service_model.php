<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Provider_service_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_providers_service';
    }

    public function get($provider_id)
    {
        $result = $this->db->from($this->table)    
            ->where(['providers_id' => $provider_id])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $result['photos'] = $this->db->from($this->table . '_photo')
            ->where(['providers_service_id' => $result['id']])
            ->order_by('displayorder', 'ASC')
            ->get()->result_array();
        return $result;
    }

    public function getByToken($token)
    {
        $result = $this->db->from($this->table)    
            ->where(['token' => $token])
            ->get()->row_array();
        if (! $result) {
            return [];
        }
        $result['photos'] = $this->db->from($this->table . '_photo')
            ->where(['providers_service_id' => $result['id']])
            ->order_by('displayorder', 'ASC')
            ->get()->result_array();
        return $result;
    }

    public function getAll($provider_id)
    {
        $result = $this->db->from($this->table)    
            ->where(['providers_id' => $provider_id])
            ->order_by('displayorder', 'ASC')
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        foreach ($result as $idx => $res) {
            $result[$idx]['photos'] = $this->db->from($this->table . '_photo')
                ->where(['providers_service_id' => $res['id']])
                ->order_by('displayorder', 'ASC')
                ->get()->result_array();
        }
        return $result;
    }

    public function getLatest($num = 10)
    {
        $result = $this->db->from($this->table)
            ->select($this->table . '.*, _providers.brand_provider AS provider_type, _providers.shop_name AS provider_name, _providers.shop_uri AS provider_uri, _district.name AS district_name, _providers.head_office_building, _providers.avatar_photo')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->join('_district', '_district.id = _providers.head_office_area')
            ->where(['visible' => 1, '_providers.status' => 'publish'])
            ->order_by('created_at', 'DESC')
            ->limit($num)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        foreach ($result as $idx => $res) {
            $result[$idx]['photos'] = $this->db->from($this->table . '_photo')
                ->where(['providers_service_id' => $res['id']])
                ->order_by('displayorder', 'ASC')
                ->get()->result_array();
        }
        return $result;
    }

    public function getLatestByCategory($cat, $params, $num = 10)
    {
        $this->db->from($this->table)
            ->select($this->table . '.*, _providers.brand_provider AS provider_type, _providers.shop_name AS provider_name, _providers.shop_uri AS provider_uri, _district.name AS district_name, _providers.head_office_building, _providers.avatar_photo, _providers.tag_list_id, _providers.tag_2nd_list_id, _providers.head_office_area')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->join('_district', '_district.id = _providers.head_office_area')
            ->where(['visible' => 1, 'tag_list_id' => $cat, '_providers.status' => 'publish'])
            ->order_by('created_at', 'DESC')
            ->limit($num);
        if (count($params['regions']) > 0) {
            $this->db->where_in('head_office_area', $params['regions']);
        }
        if (count($params['tags']) > 0) {
            $this->db->where_in('tag_2nd_list_id', $params['tags']);
        }
        $result = $this->db->get()->result_array();
        if (! $result) {
            return [];
        }
        foreach ($result as $idx => $res) {
            $result[$idx]['photos'] = $this->db->from($this->table . '_photo')
                ->where(['providers_service_id' => $res['id']])
                ->order_by('displayorder', 'ASC')
                ->get()->result_array();
        }
        return $result;
    }

    public function getHottest($num = 10)
    {
        $result = $this->db->from($this->table)
            ->select($this->table . '.*, _providers.brand_provider AS provider_type, _providers.shop_name AS provider_name, _providers.shop_uri AS provider_uri, _district.name AS district_name, _providers.head_office_building, _providers.avatar_photo')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->join('_district', '_district.id = _providers.head_office_area')
            ->where(['visible' => 1, '_providers.status' => 'publish'])
            ->order_by('created_at', 'DESC')
            ->limit($num)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        foreach ($result as $idx => $res) {
            $result[$idx]['photos'] = $this->db->from($this->table . '_photo')
                ->where(['providers_service_id' => $res['id']])
                ->order_by('displayorder', 'ASC')
                ->get()->result_array();
        }
        return $result;
    }

    public function getHottestByCategory($cat, $params, $num = 10)
    {
        $this->db->from($this->table)
            ->select($this->table . '.*, _providers.brand_provider AS provider_type, _providers.shop_name AS provider_name, _providers.shop_uri AS provider_uri, _district.name AS district_name, _providers.head_office_building, _providers.avatar_photo, _providers.tag_list_id, _providers.tag_2nd_list_id, _providers.head_office_area')
            ->join('_providers', '_providers.id = ' . $this->table . '.providers_id')
            ->join('_district', '_district.id = _providers.head_office_area')
            ->where(['visible' => 1, 'tag_list_id' => $cat, '_providers.status' => 'publish'])
            ->order_by('created_at', 'DESC')
            ->limit($num);
        if (count($params['regions']) > 0) {
            $this->db->where_in('head_office_area', $params['regions']);
        }
        if (count($params['tags']) > 0) {
            $this->db->where_in('tag_2nd_list_id', $params['tags']);
        }
        $result = $this->db->get()->result_array();
        if (! $result) {
            return [];
        }
        foreach ($result as $idx => $res) {
            $result[$idx]['photos'] = $this->db->from($this->table . '_photo')
                ->where(['providers_service_id' => $res['id']])
                ->order_by('displayorder', 'ASC')
                ->get()->result_array();
        }
        return $result;
    }

    public function keywordSearch($search, $num = 10)
    {
        $result = $this->db->like('name', $search)
            ->or_like('summary', $search)
            ->or_like('content', $search)
            ->where([$this->table . '.visible' => 1])
            ->join('_providers', $this->table . '.providers_id = _providers.id')
            ->limit($num)
            ->select($this->table . '.*, _providers.shop_name, _providers.shop_uri, _providers.head_office_area, _providers.head_office_building, _providers.phone, _providers.business_hours')
            ->from($this->table)
            ->get()->result_array();
        if (! $result) {
            return [];
        }
        foreach ($result as $idx => $res) {
            $result[$idx]['photos'] = $this->db->from($this->table . '_photo')
            ->where(['providers_service_id' => $res['id']])
            ->order_by('displayorder', 'ASC')
            ->get()->result_array();
        }
        return $result;
    }

    private function getNextDisplayOrder($provider_id)
    {
        $result = $this->db->from($this->table)
            ->select_max('displayorder')
            ->where(['providers_id' => $provider_id])
            ->get()->row_array();
        if (! $result['displayorder']) {
            return 1;
        }
        return $result['displayorder'] + 1;
    }

    public function create($params)
    {
        $this->load->model('Provider_model', 'provider');
        $this->load->helper('common');
        $provider = $this->provider->get($this->session->userdata('uonlive_user')['id']);
        $data = [
            'providers_id' => $provider['id'],
            'token' => generateRandomString(32),
            'name' => $params['name'],
            'summary' => $params['summary'],
            'content' => $params['content'],
            'displayorder' => $this->getNextDisplayOrder($provider['id']),
            'visible' => $params['show_in_shop'],
        ];
        $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
            // ->set('expired_at', 'UNIX_TIMESTAMP(DATE_SUB(DATE_ADD(CURDATE(),INTERVAL 1 MONTH), INTERVAL 1 DAY))', false)
            // ->from($this->table)    
            ->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        foreach ($params['image_file'] as $idx => $image) {
            $data = [
                'providers_service_id' => $insert_id,
                'displayorder' => $idx + 1,
                'photo' => $image,
            ];
            $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
                ->insert($this->table . '_photo', $data);
        }
    }

    public function update($params)
    {
        $service = $this->getByToken($params['token']);
        $displayorder = $service['photos'][count($service['photos']) - 1]['displayorder'];
        $data = [
            'name' => $params['name'],
            'summary' => $params['summary'],
            'content' => $params['content'],
            'visible' => $params['show_in_shop'],
        ];
        $this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
            ->where(['token' => $params['token']])
            ->update($this->table, $data);
        foreach ($params['image_file'] as $idx => $image) {
            $data = [
                'providers_service_id' => $service['id'],
                'displayorder' => $displayorder + $idx + 1,
                'photo' => $image,
            ];
            $this->db->set('created_at', 'UNIX_TIMESTAMP()', false)
                ->insert($this->table . '_photo', $data);
        }
        $deleted_photos = explode(',', $params['deleted_photo']);
        if (count($deleted_photos) > 0) {
            $photos = [];
            foreach ($deleted_photos as $del) {
                foreach ($service['photos'] as $photo) {
                    if ($photo['id'] == $del) {
                        $photos[] = $photo['photo'];
                        break;
                    }
                }
            }
            $this->db->where_in('id', $deleted_photos)
                ->delete($this->table . '_photo');
            foreach ($photos as $photo) {
                unlink (FCPATH . 'assets/img/uploads/services/' . $photo);
            }
        }
        return true;
    }

	public function image_handling($request, $files)
	{
		$data = [];
		$fullPath = FCPATH . 'assets/img/uploads/' . $request['image_type'] . '/';
		if (! file_exists($fullPath)) {
			mkdir ($fullPath);
		}
		foreach ($files['image']['name'] as $idx => $name) {
			$fileExt = pathinfo($name, PATHINFO_EXTENSION);
			$newFilename = md5(rand(1, 10) . microtime()) . '.' . $fileExt;
			if (!move_uploaded_file($files['image']['tmp_name'][$idx], $fullPath . $newFilename)) {
				return [
					'success' => false,
					'error_message' => '圖片讀寫錯誤',
					'data' => [],
				];
			}
			$data[] = $newFilename;
		}
		return [
			'success' => true,
			'error_message' => '',
			'data' => $data,
		];
	}

    public function service_update($params)
    {
        
    }

    public function update_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );
        $this->db->where(['id' => $id])
            ->update($this->table, $data);
    }

}
