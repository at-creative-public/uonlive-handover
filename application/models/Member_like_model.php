<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Member_like_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = '_members_like';
    }

    public function check_liked($member_id, $provider_id)
    {
        if ($member_id == 0) {
            return false;
        }
        $result = $this->db->from($this->table)    
            ->where(['member_id' => $member_id, 'provider_id' => $provider_id])
            ->get()->row_array();
        if ($result) {
            if ($result['liked'] == 1) {
                return true;
            }
        }
        return false;
    }

    public function toggle($member_id, $provider_id)
    {
        if ($member_id == 0) {
            return [];
        }
        if ($this->if_exists($member_id, $provider_id)) {
            $this->db->set('liked', '1 - liked', false)
                ->set('updated_at', 'UNIX_TIMESTAMP()', false)
                ->where(['member_id' => $member_id, 'provider_id' => $provider_id])
                ->update($this->table, []);
            return $this->get($member_id, $provider_id);
        }
        $this->db->set('updated_at', 'UNIX_TIMESTAMP()', false)
            ->insert($this->table, [
                'member_id' => $member_id,
                'provider_id' => $provider_id,
                'liked' => 1,
        ]);
        $insert_id = $this->db->insert_id();
        return $this->db->where(['id' => $insert_id])
            ->get($this->table)->row_array();
    }

    public function if_exists($member_id, $provider_id)
    {
        return ($this->db->from($this->table)    
            ->where(['member_id' => $member_id, 'provider_id' => $provider_id])
            ->count_all_results() > 0) ? true : false;
    }

    public function get($member_id, $provider_id)
    {
        return $this->db->from($this->table)    
            ->where(['member_id' => $member_id, 'provider_id' => $provider_id])
            ->get()->row_array();
    }

    public function getByMember($member_id)
    {
        return $this->db->from($this->table)
            ->select($this->table . '.*, _providers.id AS providers_id, _providers.shop_name, _providers.shop_uri, _providers.head_office_area, _providers.head_office_building, _providers.phone, _providers.business_hours, _providers.shop_photo, _providers.shop_uri')
            ->where([$this->table . '.member_id' => $member_id, 'liked' => 1])
            ->join('_providers', '_providers.id = ' . $this->table . '.provider_id')
            ->order_by('updated_at', 'DESC')
            ->get()->result_array();
    }

    public function getAll($member_id)
    {
        return $this->db->from($this->table)    
            ->where(['member_id' => $member_id])
            ->order_by('updated_at', 'DESC')
            ->get()->result_array();
    }

}
