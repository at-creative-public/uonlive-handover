var _have_avatar = false;
var _have_card = false;
var _avatarBlob = null;
var _cardBlob = null;

$(document).ready(function () {
	$('.date-picker').datepicker();
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		if (! _have_avatar && ! _have_card) {
			formSubmit();
		}
		else {
			uploadImage();
		}
	});
	$('#btnReset').on('click', function () {
		$('#phpform')[0].reset();
	});
	$('#referrer_label').autocomplete({
		source: function (request, response) {
			$.getJSON(base_url + "api/member/list/" + $('#referrer_label').val(), function (data) {
				response(data);
			});
		},
		select: function( event, ui ) {
			event.preventDefault();
			$('#referrer_label').val(ui.item.label);
			$('INPUT[name="referrer"]').val(ui.item.value);
		},
		minLength: 2,
		delay: 100
	});
});

function formSubmit() {
	$('#phpform').submit();
}

function uploadImage() {

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	if (_have_avatar) {
		formData.append('image[]', _avatarBlob);
		formData.append('image_id[]', 'avatar');
	}
	if (_have_card) {
		formData.append('image[]', _cardBlob);
		formData.append('image_id[]', 'card');
	}
	formData.append('image_type', 'member');

	$.ajax({
		type: "POST",
		url: base_url + 'at-admin/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		console.log(response['data']);
		for (var i = 0; i < response['data'].length; i++) {
			switch (response['data'][i]['id']) {
				case 'avatar':
					$('INPUT[name="avatar_file"]').val(response['data'][i]['filename']);
					break;
				case 'card':
					$('INPUT[name="card_file"]').val(response['data'][i]['filename']);
					break;
			}
		}
		formSubmit();
	}).error(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	var element_id;
	var dz_id = evt.target.id
	var type = dz_id.replace('-drop_zone', '');
	var type = type.replace('-browse', '');
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	switch (type) {
		case "avatar":
			_avatarBlob = files[0];
			_have_avatar = true;
			break;
		case "card":
			_cardBlob = files[0];
			_have_card = true;
			break;
	}
	element_id = type + '-media';

	const media = document.getElementById(element_id);

	const reader = new FileReader();

	reader.onload = function (e) {
		media.style.display = 'block';
		media.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var avatarDropZone;
	var cardDropZone;
	var avatarBrowse;
	var cardBrowse;

	avatarDropZone = document.getElementById('avatar-drop_zone')
	avatarBrowse = document.getElementById('avatar-browse')
	avatarDropZone.addEventListener('dragover', handleDragOver, false)
	avatarDropZone.addEventListener('drop', handleFileSelect, false)
	avatarBrowse.addEventListener('change', handleFileSelect, false)
	cardDropZone = document.getElementById('card-drop_zone')
	cardBrowse = document.getElementById('card-browse')
	cardDropZone.addEventListener('dragover', handleDragOver, false)
	cardDropZone.addEventListener('drop', handleFileSelect, false)
	cardBrowse.addEventListener('change', handleFileSelect, false)
});
