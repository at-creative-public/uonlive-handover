$(document).ready(function () {
	$('.date-picker').datepicker();
	$('.timepicker').timepicker({
		timeFormat: 'HH:mm',
		interval: 15,
		// minTime: '10',
		// maxTime: '6:00pm',
		// defaultTime: '11',
		// startTime: '10:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true,		
	});
	$('#branch').on('change', function () {
		var token = $('#branch option:selected').val();
		getRemoteData(
			base_url + 'api/meeting/next_meeting_date/' + token,
			{},
			getMeetingDateSuccess,
			getRemoteDataFail
		);
	});
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});
	$('#btnReset').on('click', function () {
		$('#phpform')[0].reset();
	});
});

function formSubmit() {
	$('#phpform').submit();
}

function getMeetingDateSuccess(data) {
	console.log(data.data.meeting);
	var meeting = data.data.meeting;
	$('#meeting-date').val(meeting.start);
	$('#time_start').val(meeting.time_start);
	$('#time_end').val(meeting.time_end);
}
