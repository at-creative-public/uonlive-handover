var _blob = null;

$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		uploadImage();
	});

	$('#btnReset').on('click', function () {
		$('#plan-form')[0].reset();
	});

	$('#image').show();
});

function formSubmit() {
	$('#plan-form').submit();
}

function uploadImage() {

	if (_blob === null) {
		formSubmit();
		return;
	}

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	formData.append('image[]', _blob);
	formData.append('image_type', 'payment');
	formData.append('token', $('#item_id option:selected').data('token'));

	$.ajax({
		type: "POST",
		url: base_url + 'at-admin/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function() {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function(event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function(res) {
		console.log(res);
		var response = JSON.parse(res);
		if (! response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		$('INPUT[name="button_photo"]').val(response['data'][0]);
		formSubmit();
	}).error(function(e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	_blob = files[0];

	const image = document.getElementById('image');
	const reader = new FileReader();

	reader.onload = function (e) {
		image.style.display = 'block';
		image.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var dropZone = document.getElementById('drop_zone')
	var browse = document.getElementById('browse')
	dropZone.addEventListener('dragover', handleDragOver, false)
	dropZone.addEventListener('drop', handleFileSelect, false)
	browse.addEventListener('change', handleFileSelect, false)
});
