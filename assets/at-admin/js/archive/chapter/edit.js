// function overlay(){
// 	var overlay = $('<div id="overlay"> </div>');
// 	overlay.appendTo(document.body);
// }
//

var _blob;

function getEditorValue(elementID) {
	return document.getElementById(elementID).innerHTML.replace(/(\n|\t)/g, '');
}

$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});
	$('#btnReset').on('click', function () {
		$('#chapter-form')[0].reset();
	});
	$('.btn-video-preview').on('click', function() {
		var vimeo_id = $('#video option:checked').data('id');
		// console.log(vimeo_id);
		var lightbox = lity('//https://vimeo.com/' + vimeo_id);
	});
});

function formSubmit() {
	$('INPUT[name="introduction"]').val(getEditorValue('chapterIntroduction'));
	$('INPUT[name="description"]').val(getEditorValue('chapterDescription'));
	$('#chapter-form').submit();
}

$(function () {
	function initToolbarBootstrapBindings() {
		var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
				'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
				'Times New Roman', 'Verdana'],
			fontTarget = $('[title=Font]').siblings('.dropdown-menu');
		$.each(fonts, function (idx, fontName) {
			fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
		});
		$('a[title]').tooltip({ container: 'body' });
		$('.dropdown-menu input').click(function () { return false; })
			.change(function () { $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); })
			.keydown('esc', function () { this.value = ''; $(this).change(); });

		$('[data-role=magic-overlay]').each(function () {
			var overlay = $(this), target = $(overlay.data('target'));
			overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
		});
		var editor = ['Introduction', 'Description'];
		editor.forEach(function(element) {
			if ("onwebkitspeechchange" in document.createElement("input")) {
				var editorOffset = $('#chapter' + element).offset();
				$('#voice' + element + 'Btn').css('position', 'absolute').offset({ top: editorOffset.top, left: editorOffset.left + $('#editor').innerWidth() - 35 });
			} else {
				$('#voice' + element + 'Btn').hide();
			}
		});
	}

	function showErrorAlert(reason, detail) {
		var msg = '';
		if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
			'<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
	}

	initToolbarBootstrapBindings();
	$('.wysiwyg-editor').wysiwyg({ fileUploadError: showErrorAlert });
	window.prettyPrint && prettyPrint();
});
