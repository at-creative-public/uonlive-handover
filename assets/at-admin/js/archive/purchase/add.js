$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});

	$('#btnReset').on('click', function () {
		$('#purchase-form')[0].reset();
	});
	$('#item_id').on('change', function () {
		var url = $('#item_id option:selected').data('url');
		getRemoteData(
			base_url + 'api/get_plan',
			{
				url: url,
			},
			getPlanSuccess,
			getRemoteDataFail
		);
	});
});

function getPlanSuccess(data) {
	console.log(data);
	var element;
	$('#plan').empty();
	for (var i = 0; i <= data.data.plan.length; i++) {
		element = data.data.plan[i];
		$('#plan').append(
			$('<option>').attr({
				value: element.id
			}).text(element.period + ' ' + element.unit + '($' + element.price + ')')
		);
	}
	console.log(data);
}


function formSubmit() {
	$('#purchase-form').submit();
}
