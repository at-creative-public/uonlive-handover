var _detailsEditor;
var _responseEditor;

$(document).ready(function () {
	$('.date-picker').datepicker({ dateFormat: 'yyyy-mm-dd' });
	CKSource.Editor
		.create( document.querySelector( '#details' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_detailsEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );
	CKSource.Editor
		.create( document.querySelector( '#response_message' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_responseEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});
	$('#btnReset').on('click', function () {
		$('#phpform')[0].reset();
	});
	$('#provider_label').autocomplete({
		source: function (request, response) {
			$.getJSON(base_url + "api/member/list/" + $('#provider_label').val(), function (data) {
				response(data);
			});
		},
		select: function( event, ui ) {
			event.preventDefault();
			$('#provider_label').val(ui.item.label);
			$('INPUT[name="provider"]').val(ui.item.value);
		},
		minLength: 2,
		delay: 100
	});
	$('#receiver_label').autocomplete({
		source: function (request, response) {
			$.getJSON(base_url + "api/member/list/" + $('#receiver_label').val(), function (data) {
				response(data);
			});
		},
		select: function( event, ui ) {
			event.preventDefault();
			$('#receiver_label').val(ui.item.label);
			$('INPUT[name="receiver"]').val(ui.item.value);
		},
		minLength: 2,
		delay: 100
	});
	// console.log(Array.from( _thisEditor.ui.componentFactory.names() ));
});

function formSubmit() {
	$('input[name="details"]').val(_detailsEditor.getData());
	$('input[name="response_message"]').val(_responseEditor.getData());
	$('#phpform').submit();
}
