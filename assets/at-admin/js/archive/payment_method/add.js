var _blob = [null, null];
var uploads = ['brand', 'qrcode'];

$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		uploadImage();
	});

	$('#btnReset').on('click', function () {
		$('#payment-form')[0].reset();
	});

	$('#item_id').on('change', function () {
		var url = $('#item_id option:selected').data('url');
		getRemoteData(
			base_url + 'api/get_plan',
			{
				url: url,
			},
			getPlanSuccess,
			getRemoteDataFail
		);
	});
});

function getPlanSuccess(data) {
	console.log(data);
	var element;
	$('#plan_id').empty();
	for (var i = 0; i <= data.data.plan.length; i++) {
		element = data.data.plan[i];
		$('#plan_id').append(
			$('<option>').attr({
				value: element.id
			}).text(element.period + ' ' + element.unit + '($' + element.price + ')')
		);
	}
	console.log(data);
}

function formSubmit() {
	$('#payment-form').submit();
}

function uploadImage() {

	var token = $('#item_id option:selected').data('token');
	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	for (var i = 0; i < uploads.length; i++) {
		formData.append('image[]', _blob[i]);
		formData.append('image_id[]', uploads[i]);
	}
	formData.append('image_type', 'payment_method');
	formData.append('token', token);

	$.ajax({
		type: "POST",
		url: base_url + 'at-admin/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		Object.keys(response['data']).forEach(function (element, idx) {
			$('INPUT[name="' + element + '_photo"]').val(response['data'][element]);
		});
		formSubmit();
	}).error(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()

	var dz_id = evt.target.id
	var type = dz_id.replace('-drop_zone', '');
	var type = type.replace('-browse', '');
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	_blob[uploads.indexOf(type)] = files[0];

	const image = document.getElementById(type + '-image');
	const reader = new FileReader();

	reader.onload = function (e) {
		image.style.display = 'block';
		image.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {

	for (var i = 0; i < uploads.length; i++) {
		var dropZone = document.getElementById(uploads[i] + '-drop_zone')
		var browse = document.getElementById(uploads[i] + '-browse')
		dropZone.addEventListener('dragover', handleDragOver, false)
		dropZone.addEventListener('drop', handleFileSelect, false)
		browse.addEventListener('change', handleFileSelect, false)
	}
});
