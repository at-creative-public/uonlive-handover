var _blob = [null, null, null, null, null];
var _blob_media = [null, null];
var uploads = ['featured', 'cover', 'preview', 'joomag-desktop', 'joomag-mobile'];
var _have_change = false;

function getEditorValue(elementID) {
	return document.getElementById(elementID).innerHTML.replace(/(\n|\t)/g, '');
}

$(document).ready(function () {
	var pickr_param = {
		preview: true,
		opacity: true,
		hue: true,
		// Input / output Options
		interaction: {
			hex: true,
			rgba: true,
			hsla: true,
			hsva: true,
			cmyk: true,
			input: true,
			clear: true,
			save: true
		}
	};
	const textcolor = Pickr.create({
		el: "#textcolor_picker",
		theme: "classic",
		components: pickr_param,
		default: v_textcolor,
	});
	const textcolor_light = Pickr.create({
		el: "#textcolor_light_picker",
		theme: "classic",
		components: pickr_param,
		default: v_textcolor_light,
	});
	const bgcolor = Pickr.create({
		el: "#bgcolor_picker",
		theme: "classic",
		components: pickr_param,
		default: v_bgcolor,
	});
	const footer_color = Pickr.create({
		el: "#footer_color_picker",
		theme: "classic",
		components: pickr_param,
		default: v_footer_color,
	});
	const footer_bgcolor = Pickr.create({
		el: "#footer_bgcolor_picker",
		theme: "classic",
		components: pickr_param,
		default: v_footer_bgcolor,
	});
	const copyright_bgcolor = Pickr.create({
		el: "#copyright_bgcolor_picker",
		theme: "classic",
		components: pickr_param,
		default: v_copyright_bgcolor,
	});
	textcolor.on("change", function (e) {
		$("#textcolor").val(e.toHEXA().toString().replace(',', ''));
	});
	textcolor_light.on("change", function (e) {
		$("#textcolor_light").val(e.toHEXA().toString().replace(',', ''));
	});
	bgcolor.on("change", function (e) {
		$("#bgcolor").val(e.toHEXA().toString().replace(',', ''));
	});
	footer_color.on("change", function (e) {
		$("#footer_color").val(e.toHEXA().toString().replace(',', ''));
	});
	footer_bgcolor.on("change", function (e) {
		$("#footer_bgcolor").val(e.toHEXA().toString().replace(',', ''));
	});
	copyright_bgcolor.on("change", function (e) {
		$("#copyright_bgcolor").val(e.toHEXA().toString().replace(',', ''));
	});
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		uploadImage();
	});
	$('#btnReset').on('click', function () {
		$('#landing-form')[0].reset();
	});
	$('#btnLayout').on('click', function (e) {
		e.preventDefault();
		var newForm = $('<form>', {
			'action': base_url + 'at-admin/landing/layout/' + $(this).data('id'),
			'method': 'post',
			'target': '_blank',
		});
		$(document.body).append(newForm);
		newForm.submit();
	});
	$('#featured-image, #cover-image, #preview-image, #joomag-desktop-image, #joomag-mobile-image').show();
});

function formSubmit() {
	$('INPUT[name="description"]').val(getEditorValue('description'));
	$('INPUT[name="description_left"]').val(getEditorValue('descriptionLeft'));
	$('INPUT[name="description_right"]').val(getEditorValue('descriptionRight'));
	['textcolor', 'textcolor_light', 'bgcolor', 'footer_color', 'footer_bgcolor', 'copyright_bgcolor'].forEach(function (element) {
		$('INPUT[name="' + element + '"]').val($('INPUT[name="' + element + '"]').val().substr(1));
	});
	$('#landing-form').submit();
}

function uploadImage() {

	if (! _have_change) {
		formSubmit();
		return;
	}

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	for (var i = 0; i < uploads.length; i++) {
		if (_blob[i] !== null) {
			formData.append('image[]', _blob[i]);
			formData.append('image_id[]', uploads[i]);
		}
	}
	formData.append('image_type', 'landing');
	formData.append('token', token);

	$.ajax({
		type: "POST",
		url: base_url + 'at-admin/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		console.log(response['data']);
		Object.keys(response['data']).forEach(function (element, idx) {
			switch (element) {
				case 'featured':
					$('INPUT[name="purchase_featured_photo"]').val(response['data'][element]);
					break;
				case 'cover':
					$('INPUT[name="cover_photo"]').val(response['data'][element]);
					break;
				case 'preview':
					$('INPUT[name="preview_button"]').val(response['data'][element]);
					break;
				case 'joomag-desktop':
					$('INPUT[name="joomag_button"]').val(response['data'][element]);
					break;
				case 'joomag-mobile':
					$('INPUT[name="joomag_button_sm"]').val(response['data'][element]);
					break;
			}
		});
		formSubmit();
	}).error(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

$(function () {
	function initToolbarBootstrapBindings() {
		var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
				'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
				'Times New Roman', 'Verdana'],
			fontTarget = $('[title=Font]').siblings('.dropdown-menu');
		$.each(fonts, function (idx, fontName) {
			fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
		});
		$('a[title]').tooltip({ container: 'body' });
		$('.dropdown-menu input').click(function () { return false; })
			.change(function () { $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); })
			.keydown('esc', function () { this.value = ''; $(this).change(); });

		$('[data-role=magic-overlay]').each(function () {
			var overlay = $(this), target = $(overlay.data('target'));
			overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
		});
		var editor = ['description', 'descriptionLeft', , 'descriptionRight'];
		editor.forEach(function(element) {
			if ("onwebkitspeechchange" in document.createElement("input")) {
				var editorOffset = $('#' + element).offset();
				$('#voice' + element + 'Btn').css('position', 'absolute').offset({ top: editorOffset.top, left: editorOffset.left + $('#editor').innerWidth() - 35 });
			} else {
				$('#voice' + element + 'Btn').hide();
			}
		});
	}

	function showErrorAlert(reason, detail) {
		var msg = '';
		if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
			'<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
	};
	initToolbarBootstrapBindings();
	$('.wysiwyg-editor').wysiwyg({ fileUploadError: showErrorAlert });
	window.prettyPrint && prettyPrint();
});

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	_have_change = true;
	evt.stopPropagation()
	evt.preventDefault()
	var element_id;
	var dz_id = evt.target.id
	var type = dz_id.replace('-drop_zone', '');
	var type = dz_id.replace('-browse', '');
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	_blob[uploads.indexOf(type)] = files[0];
	element_id = type + '-image';
	const media = document.getElementById(element_id);

	const reader = new FileReader();

	reader.onload = function (e) {
		media.style.display = 'block';
		media.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var dropZone;
	var browse;

	for (var i = 0; i < uploads.length; i++) {
		dropZone = document.getElementById(uploads[i] + '-drop_zone')
		browse = document.getElementById(uploads[i] + '-browse')
		dropZone.addEventListener('dragover', handleDragOver, false)
		dropZone.addEventListener('drop', handleFileSelect, false)
		browse.addEventListener('change', handleFileSelect, false)
	}
});
