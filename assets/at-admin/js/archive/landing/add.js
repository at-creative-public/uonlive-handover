$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});
	$('#btnReset').on('click', function () {
		$('#landing-form')[0].reset();
	});
});

function formSubmit() {
	$('#landing-form').submit();
}
