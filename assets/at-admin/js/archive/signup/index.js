$(document).ready(function () {

	//Delete an Existing Row
	$('#editabledatatable').on("click", 'a.delete', function(e) {
		e.preventDefault();
		if (confirm("你真的要刪除這隻記錄？")) {
			window.location.href = delete_meeting_url + '/' + $(this).data('id');
		}
	});

});

document.addEventListener('DOMContentLoaded', function() {
	var calendarEl = document.getElementById('calendar');
	var calendar = new FullCalendar.Calendar(calendarEl, {
	  timeZone: 'Asia/Hong_Kong',
	  initialView: 'dayGridMonth',
	  headerToolbar: {
		start: 'prev,next,today',
		center: 'title',
		end: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek',
	  },
	  eventSources: [
		{
		  url: base_url + 'api/attendance',
		//   method: 'POST',
		//   extraParams: {
		// 	custom_param1: 'something',
		// 	custom_param2: 'somethingelse'
		//   },
		  failure: function() {
			alert('there was an error while fetching events!');
		  },
		  color: 'yellow',   // a non-ajax option
		  textColor: 'white' // a non-ajax option
		}
	  ],
	  eventClick: function(event, jsEvent, view) {
		//Here you can call your method to open your modal.
		//As I don't know your method, I will assume is called 'openModal', that receives an id as a parameter.
		window.location.href = signup_url + '/' + event.event.id;
		return;
	  },
	});
	calendar.setOption('locale', 'zh-tw');
	calendar.render();
});
