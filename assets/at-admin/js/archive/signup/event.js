function getAttendSuccess(data) {
    window.location.reload();
}

$(document).ready(function () {
	$('.timepicker').timepicker({
		timeFormat: 'HH:mm',
		interval: 1,
		// minTime: '10',
		// maxTime: '6:00pm',
		// defaultTime: '11',
		// startTime: '10:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true,		
	});

    $('#select_all').on('change', function() {
        if ($(this).prop('checked')) {
            $('input[id^="cb_"]').prop('checked', true);
        }
        else {
            $('input[id^="cb_"]').prop('checked', false);
        }
    });

    $('.btn-attend').on('click', function() {
        var data = $(this).data('token').split('_');
        var time = $('#attend_time_' + data[1]).val();
		getRemoteData(
			base_url + 'api/attendance/attend',
			{
                status: data[0],
                token: data[1],
                time: time,
            },
			getAttendSuccess,
			getRemoteDataFail
		);
    });

    $('.btn-leave').on('click', function() {
        var data = $(this).data('token').split('_');
        var time = $('#leave_time_' + data[1]).val();
		getRemoteData(
			base_url + 'api/attendance/leave',
			{
                status: data[0],
                token: data[1],
                time: time,
            },
			getAttendSuccess,
			getRemoteDataFail
		);
    });

    $('.btn-group-attend').on('click', function() {
        var status = $(this).data('status');
        var time = $('#attend_time_group').val();
        var selected = $('input[name="cb_attendance"]:checked');
        var tokens = [];
        selected.each(function() {
            tokens.push($(this).val());
        });
		getRemoteData(
			base_url + 'api/attendance/attend',
			{
                status: status,
                token: tokens.join(','),
                time: time,
            },
			getAttendSuccess,
			getRemoteDataFail
		);
    });

    $('.btn-group-leave').on('click', function() {
        var status = $(this).data('status');
        var time = $('#leave_time_group').val();
        var selected = $('input[name="cb_attendance"]:checked');
        var tokens = [];
        selected.each(function() {
            tokens.push($(this).val());
        });
		getRemoteData(
			base_url + 'api/attendance/leave',
			{
                status: status,
                token: tokens.join(','),
                time: time,
            },
			getAttendSuccess,
			getRemoteDataFail
		);
    });
});
