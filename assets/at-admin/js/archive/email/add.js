$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});
	$('#btnReset').on('click', function () {
		$('#email-form')[0].reset();
	});
});

function formSubmit() {
	$('#email-form').submit();
}
