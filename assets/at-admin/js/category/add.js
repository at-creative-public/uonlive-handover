var _have_icon = false;
var _iconBlob = null;

function formSubmit() {
	$('#phpform').submit();
}

$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		uploadImage();
	});
	$('#btnReset').on('click', function () {
		$('#phpform')[0].reset();
	});
});

function uploadImage() {

	if (! _have_icon) {
		alert('請選擇需要上載的類別圖示');
		return;
	}

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	formData.append('image[]', _iconBlob);
	formData.append('image_id[]', 'icon');
	formData.append('image_type', 'categories');
	formData.append('filename', icon_file);

	$.ajax({
		type: "POST",
		url: base_url + 'at-admin/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		console.log(response['data']);
		// for (var i = 0; i < response['data'].length; i++) {
		// 	switch (response['data'][i]['id']) {
		// 		case 'icon':
		// 			$('INPUT[name="icon_file"]').val(response['data'][i]['filename']);
		// 			break;
		// 	}
		// }
		formSubmit();
	}).error(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	var element_id;
	var dz_id = evt.target.id
	var type = dz_id.replace('-drop_zone', '');
	var type = type.replace('-browse', '');
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	switch (type) {
		case "icon":
			_iconBlob = files[0];
			_have_icon = true;
			break;
	}
	element_id = type + '-media';

	const media = document.getElementById(element_id);

	const reader = new FileReader();

	reader.onload = function (e) {
		media.style.display = 'block';
		media.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var iconDropZone;
	var iconBrowse;

	iconDropZone = document.getElementById('icon-drop_zone')
	iconBrowse = document.getElementById('icon-browse')
	iconDropZone.addEventListener('dragover', handleDragOver, false)
	iconDropZone.addEventListener('drop', handleFileSelect, false)
	iconBrowse.addEventListener('change', handleFileSelect, false)
});
