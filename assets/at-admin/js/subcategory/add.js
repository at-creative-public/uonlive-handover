function formSubmit() {
	$('#phpform').submit();
}

$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});
	$('#btnReset').on('click', function () {
		$('#phpform')[0].reset();
	});
});
