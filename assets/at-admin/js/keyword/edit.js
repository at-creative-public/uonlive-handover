function formSubmit() {
	$('#phpform').submit();
}

$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		formSubmit();
	});
	$('#btnReset').on('click', function () {
		$('#phpform')[0].reset();
	});
	$('#tag_code').on('change', function() {
		var tag_code = $('#tag_code option:selected').val();
		console.log(tag_2nd[tag_code]);
		$('#tag_2nd_code').empty();
		tag_2nd[tag_code]['tag_2nd'].forEach(value => {
			$('#tag_2nd_code').append(
				'<option value="' + value['code'] + '">' + value['name'] + '</option>'
			);
		});
	});
});
