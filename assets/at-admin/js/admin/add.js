$(document).ready(function () {
	$('#btnSubmit').on('click', function (e) {
		e.preventDefault();
		if (! email_validation_pattern.test($('#email').val())) {
			alert('電郵格式不正確');
			return false;
		}
		if ($('#password').val() !== $('#confirm').val()) {
			alert('密碼及確認密碼不符');
			return false;
		}
		$('#admin-form').submit();
	});

	$('#btnReset').on('click', function () {
		$('#admin-form')[0].reset();
	});

});
