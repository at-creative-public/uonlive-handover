$(document).ready(function() {
    $('#page-type').on('change', function() {
        switch($("#page-type option:selected").val()) {
            case "provider":
                window.location.href = base_url + 'member/browsing_history?type=provider';
                break;
            case "brand":
                window.location.href = base_url + 'member/browsing_history?type=brand';
                break;
            case "category":
                window.location.href = base_url + 'member/browsing_history?type=category';
                break;
            default:
                window.location.href = base_url + 'member/browsing_history';
                break;
        }
    });
});