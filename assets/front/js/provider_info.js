var _have_avatar = false;
var _have_shop = false;
var _avatarBlob = null;
var _shopBlob = null;
var _thisEditor;
var _provider;
var _alreadySentRequest = false;
var _providerNeedConfirm = false;

function toggleBrandProvider() {
	if ($('#brand_provider option:selected').val() == 'P') {
		$('.provider-block').show();
		$('.brand-block').hide();
	}
	else {
		$('.provider-block').hide();
		$('.brand-block').show();
	}
}

function refreshKeywordList() {
	var tag_code = $('#tag_list_id option:selected').data('code');
	var tag_2nd_code = $('#tag_2nd_list_id option:selected').data('code');
	$('#keywordDialogModal .modal-body .row').empty();
	if (tag_2nd[tag_code]['tag_2nd'] === undefined) {
		return;
	}
	if (tag_2nd[tag_code]['tag_2nd'][tag_2nd_code]['keywords'] === undefined) {
		return;
	}
	var keywords = tag_2nd[tag_code]['tag_2nd'][tag_2nd_code]['keywords'];
	var html;
	for (var i = 0; i < keywords.length; i++) {
		html = '	<div class="col-12 col-md-6 col-lg-4">' +
				'		<div class="form-check">' +
				'			<input type="checkbox" class="form-check-input" name="keyword" value="' + keywords[i]['name'] + '" id="keyword-' + keywords[i]['id'] + '" />' +
				'			<label class="form-check-label" for="keyword-' + keywords[i]['id'] + '">' + keywords[i]['name'] + '</label>' +
				'		</div>' +
				'	</div>';
		$('#keywordDialogModal .modal-body .row').append(html);
	}
}

$(document).ready(function () {
	toggleBrandProvider();
	$('#add-keyword').on('click', function () {
		$('#keywordDialogModal').modal('show');
	});
	$('#keyword-dialog-insert').on('click', function() {
		var tags;
		if ($('#tags').val().trim() == '') {
			tags = [];
		}
		else {
			tags = $('#tags').val().trim().split(',');
		}
		console.log(tags);
		if ($('input[name="keyword"]:checked').length > 0) {
			$('input[name="keyword"]:checked').each(function() {
				tags.push(this.value);
			});
			$('input[name="keyword"]').prop('checked', false);
			$('#tags').val(tags.join(','));
		}
		$('#keywordDialogModal').modal('hide');
	});
	$('#keyword-dialog-close').on('click', function() {
		$('#keywordDialogModal').modal('hide');
	});
	$('#tag_list_id').on('change', function() {
		var tag_code = $('#tag_list_id option:selected').data('code');
		console.log(tag_2nd[tag_code]['tag_2nd']);
		$('#tag_2nd_list_id').empty();
		var keys = Object.keys(tag_2nd[tag_code]['tag_2nd']);
		var tag;
		if (keys.length > 0) {
			for (var i = 0; i < keys.length; i++) {
				tag = tag_2nd[tag_code]['tag_2nd'][keys[i]];
				$('#tag_2nd_list_id').append($("<option></option>")
					.attr('data-code', tag['code'])
					.attr('value', tag['code'])
					.text(tag['name'])
				);
			}
			$('#tag_2nd_list_id option[value=' + tag_2nd[tag_code]['tag_2nd'][keys[0]]['code'] + ']').attr('selected','selected');
			refreshKeywordList();
		}
	});
	$('#tag_2nd_list_id').on('change', function() {
		refreshKeywordList();
	});
	$('#head_office_building').on('change', function () {
		_alreadySentRequest = false;
	});
	$('.btn-save').on('click', function (e) {
		e.preventDefault();
		if (! _alreadySentRequest) {
			getProvider();
			return;
		}
		else {
			if (_providerNeedConfirm) {
				if ($('input:radio[name="provider_id"]').is(":checked")) {
					var id = parseInt($('INPUT[name="provider_id"]:checked').val());
					$('INPUT[name="head_office_formatted_address"]').val(_provider.data.results[0]['formatted_address']);
					$('#head_office_formatted_address').text(_provider.data.results[0]['formatted_address']);
					$('INPUT[name="latitude"]').val(_provider.data.results[id].geometry.location.lat);
					$('INPUT[name="longitude"]').val(_provider.data.results[id].geometry.location.lng);
					_providerNeedConfirm = false;
				}
			}
			uploadImage();
		}
	});
	$('#btnReset').on('click', function () {
		$('#frmUpdate')[0].reset();
	});
	$('#brand_provider').on('change', function() {
		toggleBrandProvider();
	});
	CKSource.Editor
		.create( document.querySelector( '#business_hours' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_thisEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );
	// $('#avatar-media, #shop-media').hide();
	// console.log(Array.from( _thisEditor.ui.componentFactory.names() ));
});

function formSubmit() {
	$('INPUT[name="business_hours"]').val(_thisEditor.getData());
	$('#frmUpdate').submit();
}

function getProvider() {
	_alreadySentRequest = true;
	if ($('#head_office_building').val() != "") {
		axios.get('https://maps.googleapis.com/maps/api/geocode/json',{
			params: {
			  address: $('#head_office_building').val() + ',香港',
			  key: map_api_key,
			}
		})
		.then(function(response){
			// Log full response
			_provider = response;
			console.log(_provider);
			$('#provider_list tbody').empty();
			if (_provider.data.results.length == 0) {
				alert('找不到總店大廈名稱');
				return;
			}
			else {
				if (_provider.data.results.length == 1) {
					$('INPUT[name="head_office_formatted_address"]').val(_provider.data.results[0]['formatted_address']);
					$('#head_office_formatted_address').text(_provider.data.results[0]['formatted_address']);
					$('INPUT[name="latitude"]').val(_provider.data.results[0].geometry.location.lat);
					$('INPUT[name="longitude"]').val(_provider.data.results[0].geometry.location.lng);
					uploadImage();
				}
				else {
					for(var i = 0; i < _provider.data.results.length; i++) {
						$('#provider_list tbody').append(
							'<tr>' +
								'<td>' + 
									'<input type="radio" id="provider_id-' + i + '" name="provider_id" value="' + i + '" />' +
								'</td>' + 
								'<td>' +
								_provider.data.results[i]['formatted_address'] +
								'</td>' + 
							'</tr>'
						);
					}
					_providerNeedConfirm = true;
				}
			}
		})
		.catch(function(error){
			console.log(error);
		});
	}
}

function uploadImage() {

	if (! _have_avatar && ! _have_shop) {
		formSubmit();
		return;
	}
// if (! _have_avatar) {
	// 	alert('請選擇需要上載的分會圖示');
	// 	return;
	// }

	// if (! _have_shop) {
	// 	alert('請選擇需要上載的分會圖片');
	// 	return;
	// }

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	if (_have_avatar) {
		formData.append('image[]', _avatarBlob);
		formData.append('image_id[]', 'avatar');
	}
	if (_have_shop) {
		formData.append('image[]', _shopBlob);
		formData.append('image_id[]', 'shop');
	}
	formData.append('image_type', 'providers');

	$.ajax({
		type: "POST",
		url: base_url + 'api/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		console.log(response['data']);
		for (var i = 0; i < response['data'].length; i++) {
			switch (response['data'][i]['id']) {
				case 'avatar':
					$('INPUT[name="avatar_file"]').val(response['data'][i]['filename']);
					break;
				case 'shop':
					$('INPUT[name="shop_file"]').val(response['data'][i]['filename']);
					break;
			}
		}
		formSubmit();
	}).fail(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	var element_id;
	var dz_id = evt.target.id
	var type = dz_id.replace('-drop_zone', '');
	var type = type.replace('-browse', '');
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	switch (type) {
		case "avatar":
			_avatarBlob = files[0];
			_have_avatar = true;
			break;
		case "shop":
			_shopBlob = files[0];
			_have_shop = true;
			break;
	}
	element_id = type + '-media';

	const media = document.getElementById(element_id);

	const reader = new FileReader();

	reader.onload = function (e) {
		media.style.display = 'block';
		media.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var avatarDropZone;
	var shopDropZone;
	var avatarBrowse;
	var shopBrowse;

	avatarDropZone = document.getElementById('avatar-drop_zone')
	avatarBrowse = document.getElementById('avatar-browse')
	avatarDropZone.addEventListener('dragover', handleDragOver, false)
	avatarDropZone.addEventListener('drop', handleFileSelect, false)
	avatarBrowse.addEventListener('change', handleFileSelect, false)
	shopDropZone = document.getElementById('shop-drop_zone')
	shopBrowse = document.getElementById('shop-browse')
	shopDropZone.addEventListener('dragover', handleDragOver, false)
	shopDropZone.addEventListener('drop', handleFileSelect, false)
	shopBrowse.addEventListener('change', handleFileSelect, false)
});
