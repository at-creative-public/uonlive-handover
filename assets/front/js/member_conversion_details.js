function leaveMessageSuccess(data) {
    alert('留言已發送');
    window.location.reload();
}

$(document).ready(function() {
	CKSource.Editor
		.create( document.querySelector( '#message-content' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_detailsEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );
    $('.btn-send-message').on('click', function() {
		if ($('input[name="member"]').val() == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/leave_message',
			{
				member_id: $('input[name="member"]').val(),
				provider_id: [ $('input[name="provider"]').val() ],
				parent_id: $('input[name="parent_id"]').val(),
                title: '',
                message: _detailsEditor.getData(),
			},
			leaveMessageSuccess,
			getRemoteDataFail
		);
    });
});