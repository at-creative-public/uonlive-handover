var _have_image = false;
var _blob;
var _thisEditor;

$(document).ready(function () {
	$('#media_type').on('change', function () {
		$('.photo-block, .news-block, .video-block').hide();
		$('.' + $('#media_type option:selected').val() + '-block').show();
	});

	$('.btn-add-new').on('click', function (e) {
		e.preventDefault();
		if ($('#media_type option:selected').val() != 'news') {
			uploadPhoto();
		}
		else {
			formSubmit();
		}
	});
	$('.btn-cancel').on('click', function () {
		window.location.href = base_url + 'provider/media';
	});

	CKSource.Editor
		.create( document.querySelector( '#news-summary' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_thisEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );

});

function formSubmit() {
	// var formData = new FormData();
	// formData.append('image[]', _blob);
	// formData.append('image_type', 'medias');
	$('INPUT[name="news_summary"]').val(_thisEditor.getData());
	$('#media-form').submit();
}

function uploadPhoto() {

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	formData.append('image[]', _blob);
	formData.append('image_type', 'medias');

	$.ajax({
		type: "POST",
		url: base_url + 'api/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		console.log(response['data']);
		$('INPUT[name="media_file"]').val(response['data'][0]);
		formSubmit();
	}).error(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()

	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	_have_image = true;
	_blob = files[0];

	const photo = document.getElementById($('#media_type').val() + '-photo');
	const reader = new FileReader();

	reader.onload = function (e) {
		photo.style.display = 'block';
		photo.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var photoDropZone = document.getElementById('photo_drop_zone')
	var photoBrowse = document.getElementById('photo_browse')
	var videoDropZone = document.getElementById('video_drop_zone')
	var videoBrowse = document.getElementById('video_browse')
	photoDropZone.addEventListener('dragover', handleDragOver, false)
	photoDropZone.addEventListener('drop', handleFileSelect, false)
	photoBrowse.addEventListener('change', handleFileSelect, false)
	videoDropZone.addEventListener('dragover', handleDragOver, false)
	videoDropZone.addEventListener('drop', handleFileSelect, false)
	videoBrowse.addEventListener('change', handleFileSelect, false)
})
