var _have_image = false;
var _imageBlob = [];
var _thisEditor;
var _deletedImage = [];

$(document).ready(function () {
	$('a[id^="btnDelete-"]').on('click', function () {
		var id = $(this).attr('id').replace('btnDelete-', '');
		_deletedImage.push(id);
		$('#photo-' + id).remove();
	});
	$('.btn-save').on('click', function (e) {
		e.preventDefault();
		if (_have_image) {
			uploadImage();
		}
		else {
			formSubmit();
		}
	});
	$('#btnReset').on('click', function () {
		$('#media-form')[0].reset();
	});
	CKSource.Editor
		.create( document.querySelector( '#content' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_thisEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );
	// $('#image-media').hide();
	// console.log(Array.from( _thisEditor.ui.componentFactory.names() ));
});

function formSubmit() {
	if (_deletedImage.length > 0) {
		$('input[name="deleted_photo"]').val(_deletedImage.join());
	}
	$('INPUT[name="content"]').val(_thisEditor.getData());
	$('#media-form').submit();
}

function uploadImage() {

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	for(var i = 0; i < _imageBlob.length; i++) {
		formData.append('image[]', _imageBlob[i]);
	}
	// formData.append('image_id[]', 'image');
	formData.append('image_type', 'services');

	$.ajax({
		type: "POST",
		url: base_url + 'api/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		console.log(response['data']);
		for (var i = 0; i < response['data'].length; i++) {
			$('form#media-form').append('<input type="hidden" name="image_file[]" value="' + response['data'][i] + '" />');
			// switch (response['data'][i]['id']) {
			// 	case 'image':
			// $('INPUT[name="image_file"]').val(response['data'][i]['filename']);
			// 		break;
			// }
		}
		formSubmit();
	}).fail(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	var element_id;
	var dz_id = evt.target.id
	var type = dz_id.replace('-drop_zone', '');
	var type = type.replace('-browse', '');
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	switch (type) {
		case "image":
			_imageBlob.push(files[0]);
			_have_image = true;
			break;
	}
	element_id = type + '-media';

	const media = document.getElementById(element_id);

	const reader = new FileReader();

	reader.onload = function (e) {
		// media.style.display = 'block';
		// media.setAttribute('src', e.target.result);
		media.insertAdjacentHTML( 'beforeend', '<img src="' + e.target.result + '" style="object-fit: cover;width: 80px;height: 80px; padding: 0 1%;" class="col-4 d-inline-block" />');
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var imageDropZone;
	var imageBrowse;

	imageDropZone = document.getElementById('image-drop_zone')
	imageBrowse = document.getElementById('image-browse')
	imageDropZone.addEventListener('dragover', handleDragOver, false)
	imageDropZone.addEventListener('drop', handleFileSelect, false)
	imageBrowse.addEventListener('change', handleFileSelect, false)
});
