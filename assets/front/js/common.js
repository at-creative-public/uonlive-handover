$(document).ready(function () {

	$('.back_to_top').on("click", function() {

		document.documentElement.scrollTop = 0;

	})

    $(".open_wechat_qrcode").click(function(){
        $.fancybox.open({
            src  : "#open_popup",
            type : 'inline',
            opts : {
                margin:0,
                touch : false
            }
        });
    });

    var x = $('.footer_table');

    var heightest = 0;

    for (var i = 0; i < x.length; i++) {

        if (x[i].offsetHeight > heightest) {

            heightest = x[i].offsetHeight;

        }

    }

	$('.footer_table').css('height', heightest);

	$(".btn_common_share").on('click', function() {

		$("#share_url").show();

		var copyText = document.getElementById("share_url");

		copyText.select();

		copyText.setSelectionRange(0, 99999);

		document.execCommand("copy");

		$("#share_url").hide();

		alert("已複製本頁面超連結");

	});

});

function getRemoteData(remote_url, data, callback, callbackFail) {
	$.ajax({
//async: false,
//timeout: 30000, //最長等候回應時間
		type: "GET", //提交類型
		url: remote_url, //提交地址
		data: data, //提交內容
		dataType: 'json', //返回數據類型
		success: function (data) { //請求完成並成功
			if (data.success) {
				callback(data);
			} else {
				callbackFail();
			}
		},
		error: function (data) { //請求返回錯誤信息
			callbackFail();
		},
		beforeSend: function (XHR) { //请求開始前执行
		},
		complete: function (XHR, status) { //请求完成后最终执行
		}
	});
}

function getRemoteDataFail() {
	alert("發生錯誤");
}
