var current_selected;
var current_selected1;

function likeSuccess(data)
{
	if (data.data.liked == 1) {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('far').addClass('fa');
	}
	else {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

function bookmarkSuccess(data)
{
	if (data.data.bookmarked == 1) {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('far').addClass('fa');
	}
	else {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

function closeAll()
{
	var menu = ['region', 'subcategory', 'age', 'gender'];
	for (var i = 0; i < menu.length; i++) {
		$('.service_' + menu[i] + '_popup').hide();
	}
}

function doProcess(item)
{
	$('#filter_service_' + current_selected).removeClass('active1');
	$('#filter_target_' + current_selected1).removeClass('active1');
	$('.service_target_bar').hide();
	$('.service_target i').removeClass('fa-angle-up').addClass('fa-angle-down');
	if ($('.service_' + item + '_popup').is(":visible")) {
		$('.service_' + item + '_popup').slideUp();
		$('#filter_service_' + item).removeClass('active1');
	} else {
		closeAll();
		$('.service_' + item + '_popup').slideDown();
		$('#filter_service_' + item).addClass('active1');
	}
}

function doProcess1(item)
{
	$('#filter_target_' + current_selected1).removeClass('active1');
	if ($('.service_' + item + '_popup').is(":visible")) {
		$('.service_' + item + '_popup').slideUp();
		$('#filter_target_' + item).removeClass('active1');
	} else {
		closeAll();
		$('.service_' + item + '_popup').slideDown();
		$('#filter_target_' + item).addClass('active1');
	}
}

function generateParams() {
	var arr = [];
	var para = [];
	$('input:checkbox[name="_region"]:checked').each(function(){
		arr.push($(this).val());
	});
	if (arr.length > 0) {
		para.push('r=' + arr.join(','))
	}
	arr.length = 0;
	$('input:checkbox[name="_tag"]:checked').each(function(){
		arr.push($(this).val());
	});
	if (arr.length > 0) {
		para.push('t=' + arr.join(','))
	}
	return para.join('&');
}

$(document).ready(function () {
	closeAll();
	$('.service_target').on('click', function () {
		$('#filter_service_' + current_selected).removeClass('active1');
		closeAll();
		if ($('.service_target_bar').is(":visible")) {
			$('.service_target_bar').slideUp();
			$('.service_target i').removeClass('fa-angle-up').addClass('fa-angle-down');
			$('#filter_target_' + current_selected1).removeClass('active1');
		}
		else {
			$('.service_target_bar').slideDown();
			$('.service_target i').removeClass('fa-angle-down').addClass('fa-angle-up');
		}
	});
	$('#filter_service_region').on('click', function () {
		doProcess('region')
		current_selected = 'region';
	});
	$('#filter_service_subcategory').on('click', function () {
		doProcess('subcategory')
		current_selected = 'subcategory';
	});
	$('#filter_service_age').on('click', function () {
		doProcess('age')
		current_selected = 'age';
	});
	$('#filter_service_gender').on('click', function () {
		doProcess('gender')
		current_selected = 'gender';
	});
	$('#filter_target_region').on('click', function () {
		doProcess1('region')
		current_selected1 = 'region';
	});
	$('#filter_target_time').on('click', function () {
		doProcess1('time')
		current_selected1 = 'time';
	});
	$('#filter_target_age').on('click', function () {
		doProcess1('age')
		current_selected1 = 'age';
	});
	$('#filter_target_gender').on('click', function () {
		doProcess1('gender')
		current_selected1 = 'gender';
	});
	$('a[id^="service-"]').on('click', function() {
		var type = $(this).attr('id').replace('service-', '');
		switch(type) {
			case "provider":
				$('.service-provider').show();
				$('.service-brand').hide();
				break;
			case "brand":
				$('.service-provider').hide();
				$('.service-brand').show();
				break;
			default:
				$('.service-provider, .service-brand').show();
				break;
		}
	});
	$('a[id^="hottest-"]').on('click', function() {
		var type = $(this).attr('id').replace('hottest-', '');
		switch(type) {
			case "provider":
				$('.hottest-provider').show();
				$('.hottest-brand').hide();
				break;
			case "brand":
				$('.hottest-provider').hide();
				$('.hottest-brand').show();
				break;
			default:
				$('.hottest-provider, .hottest-brand').show();
				break;
		}
	});
	$('input[name="_region"], input[name="_tag"]').on('change', function() {
		window.location.href = current_url + '?' + generateParams();
	});
	$('.like-page').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/like',
			{
				member_id: member,
				provider_id: provider,
			},
			likeSuccess,
			getRemoteDataFail
		);
	});
	$('.bookmark-page').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/bookmark',
			{
				member_id: member,
				provider_id: provider,
			},
			bookmarkSuccess,
			getRemoteDataFail
		);
	});
	var keys = Object.keys(services_id);
	for (var i = 0; i < keys.length; i++) {
		new needShareDropdown(document.getElementById('share-button_' + keys[i]), {
			// iconStyle: 'box',
			// boxForm: 'vertical',
			title: services_id[keys[i]]['title'],
			url: services_id[keys[i]]['url'],
			networks: 'Mailto,Twitter,Facebook,linkedin'
		});
	}
});
