$(document).ready(function() {
    $('#page-type').on('change', function() {
        switch($("#page-type option:selected").val()) {
            case "unread":
                window.location.href = base_url + 'member/conversion_history?type=unread';
                break;
            case "read":
                window.location.href = base_url + 'member/conversion_history?type=read';
                break;
            default:
                window.location.href = base_url + 'member/conversion_history';
                break;
        }
    });
});