var _position;
function showPosition(position) {
	_position = position;
	console.log("Latitude: " + position.coords.latitude +
		"<br>Longitude: " + position.coords.longitude);
	// $('.filter_content_section #map').html('<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14763.358586819528!2d' + position.coords.longitude + '!3d' + position.coords.latitude + '!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2shk!4v1660128312522!5m2!1sen!2shk" width="600" height="450" style="border:0;" allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>');
	getNearbyShops();
}

function positionNotAllow(error) {
	if (error.code == error.PERMISSION_DENIED) {
		alert ('無法讀取你的位置坐標');
		_position = {
			coords: {
				latitude: 22.3193,
				longitude: 114.1694,
			}
		};
		showPosition(_position);
	}
}
function show_shops(providers) {
	console.log (providers);
	$('.shop-list').empty();
	$('.total-shops').text(providers.length);
	for (var i = 0; i < providers.length; i++) {
		var distance = parseFloat(providers[i]['distance']);
		if (distance < 1) {
			distance_str = Math.ceil(distance * 1000) + 'm';
		}
		else {
			distance_str = Math.ceil(distance) + 'km';
		}
		html = '	<div class="row pt-3">';
		html += '		<div class="col-md-2"><img src="' + base_url + 'assets/img/uploads/providers/' + providers[i]['shop_photo'] + '" class="img-group-same-height-1 img-fluid d-block mx-auto" alt=""></div>';
		html += '		<div class="col-md-10">';
		html += '			<table class="table">';
		html += '				<tbody>';
		html += '				<tr>';
		html += '					<td style="width: 20px;"><input type="checkbox" id="chk_message_' + providers[i]['id'] + '" value="' + providers[i]['id'] + '" /></td>';
		html += '					<td style="width: 40px;"><span>' + (i + 1) + '.</span></td>';
		html += '					<td><a href="' + base_url + 'service/details/' + providers[i]['shop_uri'] + '"><span>' + providers[i]['shop_name'] + '</span></a></td>';
		html += '				</tr>';
		html += '				<tr>';
		html += '					<td colspan="2"></td>';
		html += '					<td><p>' + providers[i]['head_office_building'] + ' | ' + providers[i]['phone'] + '</p></td>';
		html += '				</tr>';
		html += '				<tr>';
		html += '					<td colspan="2"></td>';
		html += '					<td><p>' + providers[i]['business_hours'] + '</p></td>';
		html += '				</tr>';
		html += '				<tr>';
		html += '					<td colspan="2"></td>';
		html += '					<td>距離:' + distance_str + '</td>';
		html += '				</tr>';
		html += '				</tbody>';
		html += '			</table>';
		html += '		</div>';
		html += '	</div>';
		$('.shop-list').append(html);
	}
}
function getNearbyShops() {
	var formData = new FormData();
	switch ($('#position option:selected').val()) {
		case "1":
			formData.append('lat', _position.coords.latitude);
			formData.append('lng', _position.coords.longitude);
			break;
		case "2":
			formData.append('lat', resident_lat);
			formData.append('lng', resident_lng);
			break;
		case "3":
			formData.append('lat', business_lat);
			formData.append('lng', business_lng);
			break;
	}
	formData.append('range', $('#range option:selected').val());
	formData.append('category', $('#category option:selected').val());

	$.ajax({
		type: "POST",
		url: base_url + 'api/location/nearby_shops',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		show_shops(response['data']);
	}).fail(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

}
$(document).ready(function () {

	$('.btn-toggle').on('click', function() {
		$(this).find('.btn').toggleClass('active');
		$(this).find('.btn').toggleClass('btn-on');
		$(this).find('.btn').toggleClass('btn-off');
	});
	$('.btn-map').on('click', function () {
		window.location.href = base_url + 'service/nearby_map';
	});
	$('#range, #category, #position').on('change', function () {
		getNearbyShops();
	});
	$('#message-all').on('change', function() {
		$('input[id^="chk_message_"]').prop('checked', $('#message-all').prop('checked'));
	})
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, positionNotAllow);
	} else {
		alert ('無法讀取你的位置坐標');
		showPosition({
			coords: {
				latitude: 22.3193,
				longitude: 114.1694,
			}
		});
	}

});
