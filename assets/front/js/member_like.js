function likeSuccess(data)
{
	if (data.data.liked == 1) {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('far').addClass('fa');
	}
	else {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

function bookmarkSuccess(data)
{
	if (data.data.bookmarked == 1) {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('far').addClass('fa');
	}
	else {
		$('.provider_id_' + data.data.provider_id + ' i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

$(document).ready(function() {
	$('.like-page').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/like',
			{
				member_id: member,
				provider_id: provider,
			},
			likeSuccess,
			getRemoteDataFail
		);
	});
	$('.bookmark-page').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/bookmark',
			{
				member_id: member,
				provider_id: provider,
			},
			bookmarkSuccess,
			getRemoteDataFail
		);
	});
	var keys = Object.keys(services_id);
    var id;
	for (var i = 0; i < keys.length; i++) {
        id = keys[i].replace('provider_', '');
		new needShareDropdown(document.getElementById('share-button_' + id), {
			// iconStyle: 'box',
			// boxForm: 'vertical',
			title: services_id[keys[i]]['title'],
			url: services_id[keys[i]]['url'],
			networks: 'Mailto,Twitter,Facebook,linkedin'
		});
	}
});