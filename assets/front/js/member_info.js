var _have_avatar = false;
var _avatarBlob = null;
var _resident;
var _business;
var _alreadySentRequest = false;
var _residentNeedConfirm = false;
var _businessNeedConfirm = false;
var _showpass = false;

$(document).ready(function () {
	$('#resident_building, #business_building').on('change', function () {
		_alreadySentRequest = false;
	});
	$('.btn-password-toggle').on('click', function() {
		if (_showpass) {
			$('.btn-password-toggle i').removeClass('fa-eye').addClass('fa-eye-slash');
			$('#password').attr('type', 'password');
			$('#confirm_password').attr('type', 'password');
		}
		else {
			$('.btn-password-toggle i').removeClass('fa-eye-slash').addClass('fa-eye');
			$('#password').attr('type', 'text');
			$('#confirm_password').attr('type', 'text');
		}
		_showpass = ! _showpass;
	});
	$('.btn-save').on('click', function (e) {
		e.preventDefault();
		if ($('input[name="login_name"]').val() != $('#email').val()) {
			getRemoteData(
				base_url + 'api/member/registered_email',
				{
					email: $('#email').val(),
				},
				checkEmailSuccess,
				getRemoteDataFail
			);
		}
		else {
			checkEmailSuccess({
				data: {
					token: ''
				}
			});
		}

	});
	$('#btnReset').on('click', function () {
		$('#frmUpdate')[0].reset();
	});
	// $('#avatar-media').hide();
	// console.log(Array.from( _thisEditor.ui.componentFactory.names() ));
});

function checkEmailSuccess(data) {
	if (data['data']['token'] != '') {
		alert('電郵已經被註冊');
		return;
	}
	if (! formCheck()) {
		return;
	}
	if (! _alreadySentRequest) {
		getResident();
		return;
	}
	else {
		if (_residentNeedConfirm) {
			if ($('input:radio[name="resident_id"]').is(":checked")) {
				var id = parseInt($('INPUT[name="resident_id"]:checked').val());
				$('INPUT[name="resident_formatted_address"]').val(_resident.data.results[id]['formatted_address']);
				$('#resident_formatted_address').text(_resident.data.results[id]['formatted_address']);
				$('INPUT[name="resident_latitude"]').val(_resident.data.results[id].geometry.location.lat);
				$('INPUT[name="resident_longitude"]').val(_resident.data.results[id].geometry.location.lng);
				_residentNeedConfirm = false;
			}
		}
		if (_businessNeedConfirm) {
			if ($('input:radio[name="business_id"]').is(":checked")) {
				var id = parseInt($('INPUT[name="business_id"]:checked').val());
				$('INPUT[name="business_formatted_address"]').val(_business.data.results[id]['formatted_address']);
				$('#business_formatted_address').text(_business.data.results[id]['formatted_address']);
				$('INPUT[name="business_latitude"]').val(_business.data.results[id].geometry.location.lat);
				$('INPUT[name="business_longitude"]').val(_business.data.results[id].geometry.location.lng);
				_businessNeedConfirm = false;
			}
		}
		uploadImage();
	}
	// if (_have_avatar) {
	// 	uploadImage();
	// }
	// else {
	// 	formSubmit();
	// }
}

function checkEmpty(field) {
	if ($('#' + field).val() == '') {
		return true;
	}
	return false;
}

function formCheck() {
	if (! checkEmpty('password')) {
		if (! $('#password-strength-status').hasClass('strong-password')) {
			alert('密碼強度不足');
			return false;
		}
		if ($('#password').val() != $('#confirm_password').val()) {
			alert('密碼與確認密碼不符');
			return false;
		}
	}
	return true;
}

function formSubmit() {
	$('#frmUpdate').submit();
}

function getBusiness() {
	if ($('#business_building').val() != "") {
		axios.get('https://maps.googleapis.com/maps/api/geocode/json',{
			params: {
			  address: $('#business_building').val() + ',香港',
			  key: map_api_key,
			}
		})
		.then(function(response){
			// Log full response
			_business = response;
			console.log(_business.data.results);
			$('#business_list tbody').empty();
			if (_business.data.results.length == 0) {
				alert('找不到工作大廈名稱');
				return;
			}
			else {
				if (_business.data.results.length == 1) {
					$('INPUT[name="business_formatted_address"]').val(_business.data.results[0]['formatted_address']);
					$('#business_formatted_address').text(_business.data.results[0]['formatted_address']);
					$('INPUT[name="business_latitude"]').val(_business.data.results[0].geometry.location.lat);
					$('INPUT[name="business_longitude"]').val(_business.data.results[0].geometry.location.lng);
					if (! _residentNeedConfirm) {
						uploadImage();
					}
				}
				else {
					for(var i = 0; i < _business.data.results.length; i++) {
						$('#business_list tbody').append(
							'<tr>' +
								'<td>' + 
									'<input type="radio" id="business_id-' + i + '" name="business_id" value="' + i + '" />' +
								'</td>' + 
								'<td>' +
								_business.data.results[i]['formatted_address'] +
								'</td>' + 
							'</tr>'
						);
					}
					_businessNeedConfirm = true;
				}
			}
		})
		.catch(function(error){
			console.log(error);
		});
	}
}

function getResident() {
	_alreadySentRequest = true;
	if ($('#resident_building').val() != "") {
		axios.get('https://maps.googleapis.com/maps/api/geocode/json',{
			params: {
			  address: $('#resident_building').val() + ',香港',
			  key: map_api_key,
			}
		})
		.then(function(response){
			// Log full response
			_resident = response;
			console.log(_resident.data.results);
			$('#resident_list tbody').empty();
			if (_resident.data.results.length == 0) {
				alert('找不到居住大廈名稱');
				return;
			}
			else {
				if (_resident.data.results.length == 1) {
					$('INPUT[name="resident_formatted_address"]').val(_resident.data.results[0]['formatted_address']);
					$('#resident_formatted_address').text(_resident.data.results[0]['formatted_address']);
					$('INPUT[name="resident_latitude"]').val(_resident.data.results[0].geometry.location.lat);
					$('INPUT[name="resident_longitude"]').val(_resident.data.results[0].geometry.location.lng);
				}
				else {
					for(var i = 0; i < _resident.data.results.length; i++) {
						$('#resident_list tbody').append(
							'<tr>' +
								'<td>' + 
									'<input type="radio" id="resident_id-' + i + '" name="resident_id" value="' + i + '" />' +
								'</td>' + 
								'<td>' +
								_resident.data.results[i]['formatted_address'] +
								'</td>' + 
							'</tr>'
						);
					}
					_residentNeedConfirm = true;
				}
				getBusiness();
			}
		})
		.catch(function(error){
			console.log(error);
		});
	}
}

function uploadImage() {

	if (_residentNeedConfirm) {
		alert('請先選取居住地址');
		return;
	}

	if (_businessNeedConfirm) {
		alert('請先選取工作地址');
		return;
	}

	if (! _have_avatar) {
		formSubmit();
		return;
	}

	var results = document.getElementById('results')

	/* Clear the results div */
	while (results.hasChildNodes()) results.removeChild(results.firstChild)

	/* Rest the progress bar and show it */
	updateProgress(0)
	document.getElementById('progress-container').style.display = 'block'

	/* Image uploader */
	var formData = new FormData();
	formData.append('image[]', _avatarBlob);
	formData.append('image_id[]', 'avatar');
	formData.append('image_type', 'members');

	$.ajax({
		type: "POST",
		url: base_url + 'api/image/upload',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		xhr: function () {
			//upload Progress
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener('progress', function (event) {
					updateProgress(event.loaded / event.total);
				}, true);
			}
			return xhr;
		},
		mimeType: "multipart/form-data",
	}).done(function (res) {
		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		console.log(response['data']);
		for (var i = 0; i < response['data'].length; i++) {
			switch (response['data'][i]['id']) {
				case 'avatar':
					$('INPUT[name="avatar_file"]').val(response['data'][i]['filename']);
					break;
			}
		}
		formSubmit();
	}).fail(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

	/* local function: show a user message */
	function showMessage(html, type) {
		/* hide progress bar */
		document.getElementById('progress-container').style.display = 'none'

		/* display alert message */
		var element = document.createElement('div')
		element.setAttribute('class', 'alert alert-' + (type || 'success'))
		element.innerHTML = html
		results.appendChild(element)
	}

}

/**
 * Called when files are dropped on to the drop target or selected by the browse button.
 * For each file, uploads the content to Drive & displays the results when complete.
 */
function handleFileSelect(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	var element_id;
	var dz_id = evt.target.id
	var type = dz_id.replace('-drop_zone', '');
	var type = type.replace('-browse', '');
	var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
	switch (type) {
		case "avatar":
			_avatarBlob = files[0];
			_have_avatar = true;
			break;
	}
	element_id = type + '-media';

	const media = document.getElementById(element_id);

	const reader = new FileReader();

	reader.onload = function (e) {
		media.style.display = 'block';
		media.setAttribute('src', e.target.result);
	};

	reader.onprogress = function (e) {
		console.log('progress: ', Math.round((e.loaded * 100) / e.total));
	};

	reader.readAsDataURL(files[0]);

}

/**
 * Dragover handler to set the drop effect.
 */
function handleDragOver(evt) {
	evt.stopPropagation()
	evt.preventDefault()
	evt.dataTransfer.dropEffect = 'copy'
}

/**
 * Updat progress bar.
 */
function updateProgress(progress) {
	progress = Math.floor(progress * 100)
	var element = document.getElementById('progress')
	element.setAttribute('style', 'width:' + progress + '%')
	element.innerHTML = '&nbsp;' + progress + '%'
}

/**
 * Wire up drag & drop listeners once page loads
 */
document.addEventListener('DOMContentLoaded', function() {
	var avatarDropZone;
	var avatarBrowse;

	avatarDropZone = document.getElementById('avatar-drop_zone')
	avatarBrowse = document.getElementById('avatar-browse')
	avatarDropZone.addEventListener('dragover', handleDragOver, false)
	avatarDropZone.addEventListener('drop', handleFileSelect, false)
	avatarBrowse.addEventListener('change', handleFileSelect, false)
});

function checkPasswordStrength() {
	var number = /([0-9])/;
	var alphabets = /([a-zA-Z])/;
	var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
	var password = $('#password').val().trim();
	if (password.length < 6) {
		$('#password-strength-status').removeClass();
		$('#password-strength-status').addClass('weak-password');
		$('#password-strength-status').html("弱（密碼長度不能少於6個字元）");
	} else {
		if (password.match(number) && password.match(alphabets) && password.match(special_characters)) {
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('strong-password');
			$('#password-strength-status').html("強");
		}
		else {
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('medium-password');
			$('#password-strength-status').html("中（密碼必須包含英文、數字及符號）");
		}
	}
}
