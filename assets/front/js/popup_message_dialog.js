var selected_member;
var selected_provider;
var _detailsEditor;

function leaveMessageSuccess(data) {
    $('#messageDialogModal').modal('hide');
    alert('留言已發送');
}

$(document).ready(function() {
	CKSource.Editor
		.create( document.querySelector( '#message-content' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_detailsEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );
	$('.btn-leave-message').on('click', function() {
        selected_member = $(this).data('member');
        selected_provider = $(this).data('provider');
		$('#messageDialogModal').modal('show');
	});
    $('.btn-close-dialog').on('click', function() {
        $('#messageDialogModal').modal('hide');
    });
    $('.btn-send-message').on('click', function() {
		if (selected_member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/leave_message',
			{
				member_id: selected_member,
				provider_id: [ selected_provider ],
                title: $('#message-title').val(),
				parent_id: 0,
                message: _detailsEditor.getData(),
			},
			leaveMessageSuccess,
			getRemoteDataFail
		);
    });
});