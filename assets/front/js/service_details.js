function likeSuccess(data)
{
	if (data.data.liked == 1) {
		$('.like-page i').removeClass('far').addClass('fa');
	}
	else {
		$('.like-page i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

function bookmarkSuccess(data)
{
	if (data.data.bookmarked == 1) {
		$('.bookmark-page i').removeClass('far').addClass('fa');
	}
	else {
		$('.bookmark-page i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

$(document).ready(function() {
	$('span[id^="item-handler-"]').on('click', function () {
		var id = $(this).attr('id').replace('item-handler-', '');
		if ($('#item-handler-' + id + ' i').hasClass('fa-angle-right')) {
			$('#service-item-' + id).slideDown('slow');
			$('#item-handler-' + id + ' i').removeClass('fa-angle-right').addClass('fa-angle-down');
		}
		else {
			$('#service-item-' + id).slideUp('slow');
			$('#item-handler-' + id + ' i').removeClass('fa-angle-down').addClass('fa-angle-right');
		}
	});
	$('.like-page').off('click').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/like',
			{
				member_id: member,
				provider_id: provider,
			},
			likeSuccess,
			getRemoteDataFail
		);
	});
	$('.bookmark-page').off('click').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/bookmark',
			{
				member_id: member,
				provider_id: provider,
			},
			bookmarkSuccess,
			getRemoteDataFail
		);
	});
	new needShareDropdown(document.getElementById('share-button'), {
		// iconStyle: 'box',
		// boxForm: 'vertical',
		networks: 'Mailto,Twitter,Facebook,linkedin'
	  });
});
