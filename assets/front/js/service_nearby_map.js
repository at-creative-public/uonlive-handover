var open = true;
var drawerWidth = '380px';
var switchLayout = 576;
var _position = default_location;
var _map;
var selected_member;
var selected_provider;
var _detailsEditor;

function checkCurrentPosition() {
	console.log('checkCurrentPosition');
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(setDefaultPosition, positionNotAllow);
	} else {
		alert ('無法讀取你的位置坐標');
		setDefaultPosition(default_location);
	}
}

function setDefaultPosition(position) {
	console.log('setDefaultPosition');
	_position = position;
	showMap();
}

function addMarkers(providers) {
	var infowindow =  new google.maps.InfoWindow({});
	// addMarker({ lat: providers[i]['latitude'] / 10000000, lng: providers[i]['longitude'] / 10000000 }, providers[i]['shop_name']);
//	console.log(label + ': ' + position.lat + ', ' + position.lng);
	var marker, count;
	for (count = 0; count < providers.length; count++) {
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(providers[count]['latitude'] / 10000000, providers[count]['longitude'] / 10000000),
			title: providers[count]['shop_name'],
			map: _map,
		});
		google.maps.event.addListener(marker, 'click', (function (marker, count) {
			return function () {
				infowindow.setContent('<h4>' + providers[count]['shop_name'] + '</h4>' + providers[count]['head_office_building'] + '<br />' + providers[count]['head_office_formatted_address']);
				infowindow.open(_map, marker);
			}
		})(marker, count));
	}
}

function showMap() {
	console.log("Latitude: " + _position.coords.latitude +
		"<br>Longitude: " + _position.coords.longitude);
	// $('.filter_content_section #map').html('<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14763.358586819528!2d' + position.coords.longitude + '!3d' + position.coords.latitude + '!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2shk!4v1660128312522!5m2!1sen!2shk" width="600" height="450" style="border:0;" allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>');
	var center = { lat: _position.coords.latitude, lng: _position.coords.longitude };
	var label = "你的位置";
	_map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 15,
	  center: center
	});
	var marker = new google.maps.Marker({
		position: center,
		icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png',
		title: label,
		map: _map,
	});
	var geocoder = new google.maps.Geocoder();
	service = new google.maps.places.PlacesService(_map);
	google.maps.event.addListener(_map, 'click', function(event) {
		geocoder.geocode({
		  'latLng': event.latLng
		}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {
//			console.log(results);
			if (results[0]) {
				var request = {
					placeId: results[0].place_id,
					fields: ['name', 'type']
				};
				service.getDetails(request, function(place, status) {
					if (status == google.maps.places.PlacesServiceStatus.OK) {
						console.log(place);
					}
				});
			}
		  }
		});
	  });

}

function initMap() {
	console.log('initMap');
	if (! _position) {
		checkCurrentPosition();
		return;
	}
	showMap();
}

function positionNotAllow(error) {
	if (error.code == error.PERMISSION_DENIED) {
		alert ('無法讀取你的位置坐標');
		_position = {
			coords: {
				latitude: 22.3193,
				longitude: 114.1694,
			}
		};
		setDefaultPosition(_position);
	}
}
function show_shops(providers) {
	$('.shop-list').empty();
	$('.total-shops').text(providers.length);
	for (var i = 0; i < providers.length; i++) {
		var distance = parseFloat(providers[i]['distance']);
		if (distance < 1) {
			distance_str = Math.ceil(distance * 1000) + 'm';
		}
		else {
			distance_str = Math.ceil(distance) + 'km';
		}
		html = '	<div class="row">';
		html += '		<div class="col-2 text-nowrap"><input type="checkbox" id="chk_message_' + providers[i]['id'] + '" value="' + providers[i]['id'] + '" /> <span>' + (i + 1) + '.</span></div>';
		html += '		<div class="col-10">';
		html += '			<table class="table shop-info">';
		html += '				<tbody>';
		html += '				<tr>';
		html += '					<td colspan="2"><a href="' + base_url + 'service/details/' + providers[i]['shop_uri'] + '"><span>' + providers[i]['shop_name'] + '</span></a></td>';
		html += '				</tr>';
		html += '				<tr>';
		html += '					<td colspan="2"><p>' + providers[i]['head_office_building'] + ' | ' + providers[i]['phone'] + '</p></td>';
		html += '				</tr>';
		html += '				<tr>';
		html += '					<td colspan="2"><p>' + providers[i]['business_hours'] + '</p></td>';
		html += '				</tr>';
		html += '				<tr>';
		html += '					<td width="75%">距離:' + distance_str + '</td>';
		html += '					<td class="text-right"><button id="sendProvider_' + providers[i]['id'] + '" class="btn btn-message">發訊息</button></td>';
		html += '				</tr>';
		html += '				</tbody>';
		html += '			</table>';
		html += '		</div>';
		html += '	</div>';
		$('.shop-list').append(html);
	}
	addMarkers(providers);
	buildMessageButton();
}
function openDrawer() {
	$('#footerSlideContent').animate({ width: drawerWidth });
	$('#footerSlideButton').animate({ right: drawerWidth });
	$('.filter_content_section > .row').animate({ 'margin-right': drawerWidth });
	open = true;
}

function closeDrawer() {
	$('#footerSlideContent').animate({ width: '0px' });
	$('#footerSlideButton').animate({ right: '0px' });
	$('.filter_content_section > .row').animate({ 'margin-right': '0px' });
	open = false;
}

function closeDrawerNoAnimation() {
	$('#footerSlideContent').width(0);
	$('#footerSlideButton').css({ right: '0px' });
	$('.filter_content_section > .row').css({ 'margin-right': '0' });
	open = false;
}

function getNearbyShops() {
	console.log('getNearbyShops');
	var formData = new FormData();
//	console.log($('#position option:selected').val());
	switch ($('#position option:selected').val()) {
		case "1":
			checkCurrentPosition();
			formData.append('lat', _position.coords.latitude);
			formData.append('lng', _position.coords.longitude);
			// new_pos = _position;
			break;
		case "2":
			formData.append('lat', resident_lat);
			formData.append('lng', resident_lng);
			setDefaultPosition({
				coords: {
					latitude: resident_lat,
					longitude: resident_lng,
				}
			});
			break;
		case "3":
			formData.append('lat', business_lat);
			formData.append('lng', business_lng);
			setDefaultPosition({
				coords: {
					latitude: business_lat,
					longitude: business_lng,
				}
			});
			break;
	}
//	showPosition(new_pos);
	formData.append('range', $('#range option:selected').val());
	formData.append('category', $('#category option:selected').val());

	$.ajax({
		type: "POST",
		url: base_url + 'api/location/nearby_shops',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
	}).done(function (res) {
//		console.log(res);
		var response = JSON.parse(res);
		if (!response['success']) {
			showMessage('<strong>發生錯誤</strong>: ' + response['message'], 'danger')
			return;
		}
		show_shops(response['data']);
	}).fail(function (e) {
		console.log(e);
		showMessage('<strong>Error</strong>: ' + JSON.parse(e), 'danger')
	});

}

$(window).bind('resize', function(e){
	window.resizeEvt;
	$(window).resize(function(){
		clearTimeout(window.resizeEvt);
		window.resizeEvt = setTimeout(function(){
			if ($(window).width() > switchLayout) {
				// $('#footerSlideButton').show();
				openDrawer();
			}
			else {
				closeDrawerNoAnimation();
			}
		}, 250);
	});
});

function leaveMessageSuccess(data) {
    $('#messageDialogModal').modal('hide');
	$('input[id^="chk_message_"]:checked').prop('checked', false);
    alert('留言已發送');
}

function buildMessageButton() {
	$('button[id^="sendProvider_"]').off('click').on('click', function(e) {
		var provider_id = $(this).attr('id').replace('sendProvider_', '');
		selected_member = id;
		selected_provider = [ provider_id ];
		$('#messageDialogModal').modal('show');
	});
}

$(document).ready(function () {
	console.log('document ready');
	CKSource.Editor
		.create( document.querySelector( '#message-content' ), {
			removePlugins: [ 'Title', 'Markdown' ],
			licenseKey: '',
		} )
		.then( editor => {
			_detailsEditor = editor;
		} )
		.catch( error => {
			console.error( error );
		} );
	$('.btn-toggle').on('click', function() {
		$(this).find('.btn').toggleClass('active');
		$(this).find('.btn').toggleClass('btn-on');
		$(this).find('.btn').toggleClass('btn-off');
	});
	buildMessageButton();
    $('.btn-close-dialog').on('click', function() {
        $('#messageDialogModal').modal('hide');
    });
	$('#message-all').on('change', function() {
		$('input[id^="chk_message_"]').prop('checked', $('#message-all').prop('checked'));
	})
    $('.btn-send-message').on('click', function() {
		if (selected_member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/leave_message',
			{
				member_id: selected_member,
				provider_id: selected_provider,
                title: $('#message-title').val(),
				parent_id: 0,
                message: _detailsEditor.getData(),
			},
			leaveMessageSuccess,
			getRemoteDataFail
		);
    });
	$('.btn-broadcast').on('click', function() {
		if ($('input[id^="chk_message_"]:checked').length == 0) {
			alert('請選擇店舖');
			return;
		}
		selected_member = id;
		selected_provider = [];
		$('input[id^="chk_message_"]:checked').each(function() {
			selected_provider.push($(this).val());
		});
		console.log(selected_provider);
		$('#messageDialogModal').modal('show');
	});
	$('.btn-list').on('click', function () {
		window.location.href = base_url + 'service/nearby_list';
	});
	$('#range, #category, #position').on('change', function () {
		getNearbyShops();
	});
	$('#footerSlideButton').click(function() {
		// $(this).hide()
		if(open === false) {
			openDrawer();
		} else {
			closeDrawer();
		}
		// $('#footerSlideButton').show();
	});

	if ($(window).width() > switchLayout) {
		openDrawer();
	}
	getNearbyShops();

});
