function likeSuccess(data)
{
	if (data.data.liked == 1) {
		$('.like-page.provider_id_' + data.data.provider_id + ' i').removeClass('far').addClass('fa');
	}
	else {
		$('.like-page.provider_id_' + data.data.provider_id + ' i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

function bookmarkSuccess(data)
{
	if (data.data.bookmarked == 1) {
		$('.bookmark-page.provider_id_' + data.data.provider_id + ' i').removeClass('far').addClass('fa');
	}
	else {
		$('.bookmark-page.provider_id_' + data.data.provider_id + ' i').removeClass('fa').addClass('far');
	}
	console.log(JSON.stringify(data));
}

$(document).ready(function() {
	$('a[id^="service-"]').on('click', function() {
		var type = $(this).attr('id').replace('service-', '');
		switch(type) {
			case "provider":
				$('.service-provider').show();
				$('.service-brand').hide();
				break;
			case "brand":
				$('.service-provider').hide();
				$('.service-brand').show();
				break;
			default:
				$('.service-provider, .service-brand').show();
				break;
		}
	});
	$('a[id^="hottest-"]').on('click', function() {
		var type = $(this).attr('id').replace('hottest-', '');
		switch(type) {
			case "provider":
				$('.hottest-provider').show();
				$('.hottest-brand').hide();
				break;
			case "brand":
				$('.hottest-provider').hide();
				$('.hottest-brand').show();
				break;
			default:
				$('.hottest-provider, .hottest-brand').show();
				break;
		}
	});
	$('.like-page').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/like',
			{
				member_id: member,
				provider_id: provider,
			},
			likeSuccess,
			getRemoteDataFail
		);
	});
	$('.bookmark-page').on('click', function() {
		var member = $(this).data('member');
		var provider = $(this).data('provider');
		if (member == "0") {
			alert ("請先登入");
			return;
		}
		getRemoteData(
			base_url + 'api/member/bookmark',
			{
				member_id: member,
				provider_id: provider,
			},
			bookmarkSuccess,
			getRemoteDataFail
		);
	});
	var keys = Object.keys(services_id);
	for (var i = 0; i < keys.length; i++) {
		new needShareDropdown(document.getElementById('share-button_' + keys[i]), {
			// iconStyle: 'box',
			// boxForm: 'vertical',
			title: services_id[keys[i]]['title'],
			url: services_id[keys[i]]['url'],
			networks: 'Mailto,Twitter,Facebook,linkedin'
		});
	}
});