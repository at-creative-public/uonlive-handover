fetch(base_url + 'api/stripe/config')
	.then(function (result) {
		return result.json();
	})
	.then(function (json) {
		window.config = json;
		window.stripe = Stripe(config.publicKey);
	});

$(document).ready(function() {

    $('.btn-upgrade').on('click', function() {
        $.ajax ({
            //async: false,
            //timeout: 30000, //最長等候回應時間
            type: "GET", //提交類型
            url: base_url + 'api/stripe/create_checkout_session', //提交地址
            data: {
                'effective_date': $('#effective_date').val(),
                'expiry_date': $('#expiry_date').val(),
                'item': $('#item').val(),
                'amount': $('#amount').val(),
                'success_url': base_url + 'member/upgrade_success',
                'cancel_url': base_url + 'member/upgrade_fail',
            }, //提交內容
            dataType: 'json', //返回數據類型
            success: function(data) { //請求完成並成功
                console.log('Create checkout session: success');
            },
            error: function(data) { //請求返回錯誤信息
                console.log('Create checkout session: error');
            },
            beforeSend: function(XHR) { //请求開始前执行
                console.log('Create checkout session: beforeSend');
            },
            complete: function(XHR, status) { //请求完成后最终执行
                console.log('Create checkout session: complete');
            }
        }).done(function (result) {
            console.log("1st Done");
            console.log(result);
            return result;
        }).done(function (data) {
            console.log("2nd Done");
            console.log(data);
            console.log(stripe);
            stripe.redirectToCheckout({
                sessionId: data.sessionId,
            })
        }).then(function (result) {
            console.log("Then");
            console.log(result);
            // If redirection fails, display an error to the customer.
            if (result.error) {
                var displayError = document.getElementById('error-message');
                displayError.textContent = result.error.message;
            }
        });
    });

});